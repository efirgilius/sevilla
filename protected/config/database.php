<?php

// This is the database connection configuration.
return array(
	// uncomment the following lines to use a MySQL database
	/*
	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	*/
	'connectionString' => 'mysql:host=localhost;dbname=sevilla_db',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
);
