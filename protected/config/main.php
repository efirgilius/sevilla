<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Mine',

	//'theme'=>'',
	'defaultController'=>'home',
	/*
    'language'=>'id',
    */

	// preloading 'log' component
	'preload'=>array('log','counter'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'ext.slug.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		*/
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'hasan97@',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(

		'counter'=>[
			'class'=>'ext.UserCounter',
			'tableUsers'=>'pcounter_users',
			'tableSave'=>'pcounter_save',
			'autoInstallTables'=>true,
			'onlineTime'=>5,
		],

		'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),

		'settings'=>array(
	        'class'                 => 'HsnSettings',
	        'cacheComponentId'  => 'cache',
	        'cacheId'           => 'global_website_settings',
	        'cacheTime'         => 84000,
	        'tableName'     => 'tbl_settings',
	        'dbComponentId'     => 'db',
	        'createTable'       => true,
	        'dbEngine'      => 'InnoDB',
        ),

        'hsnImage'=>array(
	        'class'=>'HsnImage',
        ),

		'user' => array(
            'class'=>'application.components.WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl'=>array('apps/login'),
        ),

		'member' => array(
            'class'=>'application.components.WebMember',
            // enable cookie-based authentication
            'allowAutoLogin' => true,       
            'loginUrl'=>array('site/login'),
        ),
        
        'request'=>array(
            'enableCsrfValidation'=>true,
        ),

		// uncomment the following to enable URLs in path-format
		/*
		*/
		'urlManager'=>array(
			'urlFormat'=>'path',
			'urlSuffix'=>'.html',
            'showScriptName'=>false,
			'rules'=>array(
				'contact'=>'site/contact',
				// '<slug>'=>'blog/view',
				// 'p/<slug>'=>'page/index',
				// 'category/<slug>'=>'category/index',
				// 't/<q>'=>'blog/tag',
				// 'page/<page>'=>'home/index',
				// 'search'=>'search/index',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'image'=>array(
          	'class'=>'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
            'params'=>array('directory'=>'/files/local/bin'),
        ),
		'iwi' => array(
		     'class' => 'ext.iwi.IwiComponent',
		     // GD or ImageMagick
		     'driver' => 'GD',
		     // ImageMagick setup path
		     //'params'=>array('directory'=>'C:/ImageMagick'),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'hasandotprayoga@gmail.com',
	),
);
