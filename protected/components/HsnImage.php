<?php if ( ! defined('YII_PATH')) exit('No direct script access allowed');

class HsnImage extends CApplicationComponent
{

	public function generateThumb($width,$height,$folder,$subfolder,$image,$sizeType=false)
	{
		$loadpath = Yii::app()->basePath.'/../archives/'.$folder.'/'.$subfolder.'/'.$image;
        
        if ($sizeType) {
            $path = Yii::app()->basePath.'/../archives/'.$folder.'/'.$subfolder.'/'.$sizeType.'_'.$image;
        }else{
            $path = Yii::app()->basePath.'/../archives/'.$folder.'/'.$subfolder.'/'.$image;
        }

		$thumb=Yii::app()->iwi->load($loadpath)
                        ->adaptive($width,$height)
                        ->save($path);
	}

	public static function generateThumbUrl($filename,$folder,$subfolder,$sizeType)
	{
		$url = 'archives/'.$folder.'/'.$subfolder.'/'.$filename;
		return str_replace($filename, '', $url).$sizeType.'_'.$filename;
	}

    public function generateName($filename)
	{
		$ts=time();
		return $ts.'_'.preg_replace("/[^a-zA-Z0-9\.]/", "_", strtolower($filename));
	}

    public function SureRemoveDir($folder,$subfolder) {
        $dir = $this->getDirUrl($folder,$subfolder);

        if(!$dh = @opendir($dir)) return;
        while (false !== ($obj = readdir($dh))) {
            if($obj=='.' || $obj=='..') continue;
            if (!@unlink($dir.'/'.$obj)) SureRemoveDir($dir.'/'.$obj, true);
        }
        //closedir($dh);            
        @rmdir($dir);           
    }

    public function getDirUrl($folder,$subfolder)
    {
        $link_dir = str_replace('protected','',Yii::app()->basePath);
          return $link_dir.'archives/'.$folder.'/'.$subfolder;
    }

    public function _createDir($folder,$subfolder)
    {
        $baseDir=Yii::app()->basePath.'/../archives/'.$folder.'/';
        $path_upload = $baseDir.'/'.$subfolder;

        if(!is_dir($path_upload))
        {
                mkdir($path_upload, 0777,true);
        }
        
        $path_upload = str_replace(Yii::app()->basePath.'/../', "", $path_upload);
        return $path_upload;
    }
    
    public function removeImg($img,$folder=false,$subfolder=false,$type=false)
    {
        if ($type) {
            if(is_file($this->getDirUrl($folder,$subfolder).'/'.$type.'_'.$img)){
                chmod($this->getDirUrl($folder,$subfolder).'/'.$type.'_'.$img, 0755);
                unlink($this->getDirUrl($folder,$subfolder).'/'.$type.'_'.$img);
            } 
        }else{
            if(is_file($this->getDirUrl($folder,$subfolder).'/'.$img)){
                chmod($this->getDirUrl($folder,$subfolder).'/'.$img, 0755);
                unlink($this->getDirUrl($folder,$subfolder).'/'.$img);
            }
        }
        return true;
    }

    public function getUrl($folder,$subfolder)
    {
        return Yii::app()->baseUrl.'/archives/'.$folder.'/'.$subfolder.'/';
    }

    public function getPathUrl($folder,$subfolder,$image=false)
    {
        if ($image) {
            return Yii::app()->basePath.'/../archives/'.$folder.'/'.$subfolder.'/'.$image;
        }else{
            return Yii::app()->basePath.'/../archives/'.$folder.'/'.$subfolder.'/';
        }
    }

}