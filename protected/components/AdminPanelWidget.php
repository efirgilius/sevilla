<?php

class AdminPanelWidget extends CWidget {
	
	public function	run() {
		$unread = Contact::model()->unread()->count('reply_id is null');
		$this->render('_adminPanelWidget',[
			'unread'=>$unread
			]);
		return parent::run();
	}

}