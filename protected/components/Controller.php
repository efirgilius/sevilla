<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $title;
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='front';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $activeMenu;
        
   	public $activeSubMenu;

    public $useSidebar;

   	public $metaDescription;
   	public $metaKeywords;
   	public $metaAuthor;

   	public function metaTag($content,$attribut){
   		return Yii::app()->clientScript->registerMetaTag($content,$attribut);
   	}

   	public function getPageTitle() {
        parent::getPageTitle();
        if ($this->title) {
          return $this->title.' :: '.$this->g('general','site_name');
        }else{
          return $this->g('meta','meta_title');
        }
    }

    //-----setting

    public function img($key)
    {
        return Yii::app()->hsnImage->getUrl('settings','logo').self::g('general',$key);
    }

    public function g($category='system', $key='', $default=null){
        return Yii::app()->settings->get($category,$key,$default);
    }

    public function s($category='system', $key='', $value='', $toDatabase=true){
        return Yii::app()->settings->set($category,$key,$value,$toDatabase);
    }

    public function getTime($time)
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
            return $formatter->format($format, $time);
    }
}