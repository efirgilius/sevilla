<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class HybridIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user= Member::model()->find('LOWER(mid)=?',array(strtolower($this->username)));
                
                $this->_id=$user->id;
                $this->errorCode=self::ERROR_NONE;
                $this->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']); 
                $this->setState('name',$user->name);
                $this->setState('mid',$user->mid);
                    if($user->last_login !== null)
                        $this->setState('lastLogin',$user->last_login);
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}