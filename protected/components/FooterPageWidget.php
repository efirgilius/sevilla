<?php

class FooterPageWidget extends CWidget {
	
	public function	run() {
		
		$page = Content::model()->publish()->type('page')->findAll();

		$this->render('_footerPageWidget',[
			'page'=>$page
			]);
		return parent::run();
	}

}