<?php

class MenuWidget extends CWidget {
	
	public function	run() {

		$page = Content::model()->publish()->isMenu()->type('page')->findAll();

		$this->render('_menuWidget',[
			'page'=>$page
			]);
		return parent::run();
	}

}