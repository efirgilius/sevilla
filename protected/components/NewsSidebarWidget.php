<?php

class NewsSidebarWidget extends CWidget {
	
	public function	run() {

		$popular = Content::model()->publish()->order('viewed DESC')->type('news')->limit(3)->findAll();
		$tags = Tag::model()->order('frequency DESC')->limit(20)->type('news')->findAll();

		$this->render('_newsSidebarWidget',[
			'popular'=>$popular,
			'tags'=>$tags
			]);
		return parent::run();
	}

}