<aside class="sidebar">
	<div class="widget widget-recent-posts">
		<h3 class="widget-title">Berita Terbaru</h3>
		<?php if ($popular):?>
			<?php foreach ($popular as $key => $v):?>
			<article class="clearfix">
				<div class="wrapper">
					<div class="media">
						<?=CHtml::link(CHtml::image($v->small_url),['/news/view','slug'=>$v->slug]);?>
					</div>
					<div class="content">
						<?=CHtml::link($v->title,['/news/view','slug'=>$v->slug]);?>
					</div>
				</div>
			</article>
			<?php endforeach;?>
		<?php else:?>
			<p>Belum ada berita.</p>
		<?php endif;?>
	</div>
	<div class="widget widget-tag-cloud">
		<h3 class="widget-title">Label</h3>
		<?php if ($tags):?>
		<div class="tagcloud">
			<?php foreach ($tags as $k => $v):?>
				<?=CHtml::link($v->name, ['/news/tag','q'=>$v->name]);?>
			<?php endforeach;?>
		<?php else:?>
			<p>Belum ada tag.</p>
		<?php endif;?>
		</div>
	</div>
</aside>