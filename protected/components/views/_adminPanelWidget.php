<aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <?php echo CHtml::link(CHtml::image(Yii::app()->controller->img('logo'),'logo',['height'=>68,'width'=>121]),['/apps/dashboard'],['class'=>'sSidebar_hide']);?>
                <?php echo CHtml::link(CHtml::image(Yii::app()->controller->img('logo'),'logo',['height'=>32,'width'=>32]),['/apps/dashboard'],['class'=>'sSidebar_show']);?>
            </div>
        </div>
        
        <div class="menu_section">
            <ul>
                <li<?=(Yii::app()->controller->activeMenu=='dashboard')?' class="current_section"':'';?> title="Dashboard">
                    <?=CHtml::link('<span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                        <span class="menu_title">Dashboard</span>',['/apps/dashboard']);?>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='cms')?' class="current_section"':'';?> title="CMS">
                    <a>
                        <span class="menu_icon"><i class="material-icons">&#xE1BD;</i></span>
                        <span class="menu_title">CMS</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='banner')?' class="act_item"':'';?> title="Banner">
                            <?=CHtml::link('Banner', ['/apps/banner']);?>
                        </li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='news')?' class="act_item"':'';?> title="News">
                            <?=CHtml::link('News', ['/apps/news']);?>
                        </li>
                        <hr>
                        <li class="menu_subtitle">Page</li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='page')?' class="act_item"':'';?> title="Page List">
                            <?=CHtml::link('Page List', ['/apps/page']);?>
                        </li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pagegroup')?' class="act_item"':'';?> title="Page Group">
                            <?=CHtml::link('Page Group', ['/apps/page/group']);?>
                        </li>
                        <hr>
                        <li class="menu_subtitle">Blog</li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='blogcat')?' class="act_item"':'';?> title="Category">
                            <?=CHtml::link('Category', ['/apps/category','type'=>'blog']);?>
                        </li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='blogpost')?' class="act_item"':'';?> title="Blog Posts">
                            <?=CHtml::link('Blog Posts', ['/apps/blog']);?>
                        </li>
                        <!-- <hr>
                        <li class="menu_subtitle">Menu</li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='menu')?' class="act_item"':'';?> title="Menu">
                            <?=CHtml::link('Menu', ['/apps/menu']);?>
                        </li> -->
                    </ul>
                </li><hr>
				<li<?=(Yii::app()->controller->activeMenu=='product')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">shopping_cart</i></span>
                        <span class="menu_title">Product</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='list-product')?' class="act_item"':'';?>><?=CHtml::link('Product List',['/apps/product']);?></li>
                        <!--<li<?=(Yii::app()->controller->activeSubMenu=='cat-product')?' class="act_item"':'';?>><?=CHtml::link('Category',['/apps/category','type'=>'product']);?></li>-->
                    </ul>
                </li><hr>
                <li<?=(Yii::app()->controller->activeMenu=='member')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">perm_identity</i></span>
                        <span class="menu_title">Member</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='active')?' class="act_item"':'';?>><?=CHtml::link('Active Member',['/apps/member','state'=>'active']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='inactive')?' class="act_item"':'';?>><?=CHtml::link('Inactive Member',['/apps/member','state'=>'inactive']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='blocked')?' class="act_item"':'';?>><?=CHtml::link('Blocked Member',['/apps/member','state'=>'blocked']);?></li>
                    </ul>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='updatebank')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">perm_identity</i></span>
                        <span class="menu_title">Request Update Bank</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='rpending')?' class="act_item"':'';?>><?=CHtml::link('Pending',['/apps/updatebank','state'=>'pending']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='rprocessed')?' class="act_item"':'';?>><?=CHtml::link('Processed',['/apps/updatebank','state'=>'processed']);?></li>
                    </ul>
                </li>
                <hr>
                <li<?=(Yii::app()->controller->activeMenu=='pin')?' class="current_section"':'';?> title="PIN">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">sim_card</i></span>
                        <span class="menu_title">PIN &amp; Serial Number</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='p1')?' class="act_item"':'';?>><?=CHtml::link('Standard Type',['/apps/card','type'=>'standard']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='p2')?' class="act_item"':'';?>><?=CHtml::link('Premium Type',['/apps/card','type'=>'premium']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='p3')?' class="act_item"':'';?>><?=CHtml::link('Deluxe Type',['/apps/card','type'=>'deluxe']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='p4')?' class="act_item"':'';?>><?=CHtml::link('Executive Type',['/apps/card','type'=>'executive']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='p5')?' class="act_item"':'';?>><?=CHtml::link('Executive Diamond Type',['/apps/card','type'=>'diamond']);?></li>
                    </ul>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='mconfirm')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">attach_money</i></span>
                        <span class="menu_title">PIN Transaction</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pro-mconfirm')?' class="act_item"':'';?>><?=CHtml::link('Processed Confirmation',['/apps/membershipConfirmation','type'=>'proceed']);?></li>
                    </ul>
                    <hr>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='ewallet')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">attach_money</i></span>
                        <span class="menu_title">Ewallet Transaction</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pen-ewallet')?' class="act_item"':'';?>><?=CHtml::link('Pending Transaction',['/apps/ewallet','status'=>'pending']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pro-ewallet')?' class="act_item"':'';?>><?=CHtml::link('Processed Transaction',['/apps/ewallet','status'=>'approved']);?></li>
                    </ul>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='wdewallet')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">attach_money</i></span>
                        <span class="menu_title">Withdraw Ewallet</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pen-wdewallet')?' class="act_item"':'';?>><?=CHtml::link('Pending Transaction',['/apps/withdraw','type'=>'new']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pro-wdewallet')?' class="act_item"':'';?>><?=CHtml::link('Processed Transaction',['/apps/withdraw','type'=>'approved']);?></li>
                    </ul>
                </li><hr>
                <li<?=(Yii::app()->controller->activeMenu=='wdewallet')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">attach_money</i></span>
                        <span class="menu_title">Bonus Report</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pen-wdewallet')?' class="act_item"':'';?>><?=CHtml::link('Pending Transaction',['/apps/withdraw','type'=>'new']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='pro-wdewallet')?' class="act_item"':'';?>><?=CHtml::link('Processed Transaction',['/apps/withdraw','type'=>'approved']);?></li>
                    </ul>
                </li>
                <hr>
                <li<?=(Yii::app()->controller->activeMenu=='mailbox')?' class="current_section"':'';?> title="Mailbox">
                    <?=CHtml::link('<span class="menu_icon"><i class="material-icons">&#xE158;</i></span><span class="menu_title">Mailbox</span> <span class="uk-badge" id="unread">'.$unread.'</span>',['/apps/contact']);?>
                    <hr>
                </li>
               <li<?=(Yii::app()->controller->activeMenu=='user')?' class="current_section"':'';?> title="Users">
                    <?=CHtml::link('<span class="menu_icon"><i class="material-icons md-36">&#xE7FB;</i></span>
                        <span class="menu_title">Users</span>',['/apps/user']);?>
                    <hr>
                </li>
                <li<?=(Yii::app()->controller->activeMenu=='setting')?' class="submenu_trigger act_section current_section"':'';?>>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE8B8;</i></span>
                        <span class="menu_title">Settings</span>
                    </a>
                    <ul>
                        <li<?=(Yii::app()->controller->activeSubMenu=='general')?' class="act_item"':'';?>><?=CHtml::link('General',['/apps/setting/general']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='application')?' class="act_item"':'';?>><?=CHtml::link('Application',['/apps/setting/application']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='bonus')?' class="act_item"':'';?>><?=CHtml::link('Bonus',['/apps/setting/bonus']);?></li>
                        <li<?=(Yii::app()->controller->activeSubMenu=='rewardsetting')?' class="act_item"':'';?>><?=CHtml::link('Reward',['/apps/rewardsetting']);?></li>
                    </ul>
                </li>
                <hr>
            </ul>
        </div>
    </aside>