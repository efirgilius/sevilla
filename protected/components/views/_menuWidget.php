<nav id="mainnav" class="mainnav">
	<ul class="menu">
		<li class="<?=(Yii::app()->controller->activeMenu=='home')?'active':'';?>"><?=CHtml::link('Beranda',Yii::app()->homeurl);?></li>
		<!-- <li><a href="contact.html">Contact us</a>
			<ul class="sub-menu">
				<li><a href="contact.html">Contact 1</a></li>
				<li><a href="contact02.html">Contact 2</a></li>
				<li><a href="contact03.html">Contact 3</a></li>
				<li><a href="contact04.html">Contact 4</a></li>
				<li><a href="contact05.html">Contact 5</a></li>
			</ul>
		</li> -->
		<?php if ($page):?>
			<?php foreach ($page as $key => $v):?>
				<li class="<?=(Yii::app()->controller->activeMenu==$v->slug)?'active':'';?>"><?=CHtml::link($v->title,['/page/view','slug'=>$v->slug]);?></li>
			<?php endforeach;?>
		<?php endif;?>
		<li class="<?=(Yii::app()->controller->activeMenu=='product')?'active':'';?>"><?=CHtml::link('Produk',['/product']);?></li>
		<li class="<?=(Yii::app()->controller->activeMenu=='news')?'active':'';?>"><?=CHtml::link('Berita',['/news']);?></li>
		<li class="<?=(Yii::app()->controller->activeMenu=='contact')?'active':'';?>"><?=CHtml::link('Kontak',['/site/contact']);?></li>
	 </ul><!-- /.menu -->
</nav>