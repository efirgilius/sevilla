<h5>Panel Member</h5>
<hr>
<div class="sidebar-nav">
        <ul>
                <li><?=CHtml::link('Profil',['/member'],['class'=>(Yii::app()->controller->activeSubMenu=='profile')?'active':'']);?></li>
                <li><?=CHtml::link('Ubah Profil',['/member/updateprofile'],['class'=>(Yii::app()->controller->activeSubMenu=='uprofile')?'active':'']);?></li>
                <li><?=CHtml::link('Ganti Password',['/member/changepassword'],['class'=>(Yii::app()->controller->activeSubMenu=='cpass')?'active':'']);?></li>
                <hr>
        	<li><?=CHtml::link('Stock PIN',['/member/pin'],['class'=>(Yii::app()->controller->activeSubMenu=='pin')?'active':'']);?></li> 
                <li><?=CHtml::link('Genealogy Jaringan Sponsor',['/member/network'],['class'=>(Yii::app()->controller->activeSubMenu=='network')?'active':'']);?></li>
                <li><?=CHtml::link('Genealogy Jaringan',['/member/genealogy'],['class'=>(Yii::app()->controller->activeSubMenu=='genealogy')?'active':'']);?></li>
                <li><?=CHtml::link('Bonus Sponsor',['/member/sponsorbonus'],['class'=>(Yii::app()->controller->activeSubMenu=='sbonus')?'active':'']);?></li>
                <li><?=CHtml::link('Bonus Pasangan',['/member/pairingbonus'],['class'=>(Yii::app()->controller->activeSubMenu=='pbonus')?'active':'']);?></li>
                <li><?=CHtml::link('Bonus Loyalty',['/member/loyaltybonus'],['class'=>(Yii::app()->controller->activeSubMenu=='lbonus')?'active':'']);?></li>
                <li><?=CHtml::link('Reward',['/member/reward'],['class'=>(Yii::app()->controller->activeSubMenu=='reward')?'active':'']);?></li>
                
                <li><?=CHtml::link('Statement Bonus',['/member/statement'],['class'=>(Yii::app()->controller->activeSubMenu=='statement')?'active':'']);?></li>
                <hr>
                <li><?=CHtml::link('Ewallet',['/member/ewallet'],['class'=>(Yii::app()->controller->activeSubMenu=='ewallet')?'active':'']);?></li>
                
                <hr>
                <li><?=CHtml::link('Keluar',['/site/logout'],['class'=>(Yii::app()->controller->activeSubMenu=='logout')?'active':'']);?></li>
        </ul>
</div>