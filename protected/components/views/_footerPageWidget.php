<nav class="footernav02">
	<h3>Informasi</h3>
	<?php if ($page):?>
	<ul class="ourservices">
		<?php foreach ($page as $k => $v):?>
			<li><?=CHtml::link($v->title,['/page/view','slug'=>$v->slug]);?></li>
		<?php endforeach;?>
	 </ul>
	<?php else:?>
		<p>Page is not available.</p>
	<?php endif;?>
</nav>