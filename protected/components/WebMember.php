<?php
/**
 * Class component for web user which can access from Yii::app()->member
 *
 * @author eris
 */
class WebMember extends CWebUser
{
    private $_model;     
    
    public function getModel()
    {
        if($this->_model===NULL)
        {
            $this->_model= Member::model()->findByPk((int)Yii::app()->member->id);
        }
        return $this->_model;
    } 
    
    public function getLastlogin()
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
        return $formatter->formatDateTime(strtotime(Yii::app()->member->getState('lastLogin')));
    }
    
    
}
?>
