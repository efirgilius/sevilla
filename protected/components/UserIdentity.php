<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{		
                $user = User::model()->find('LOWER(email)=?',array(strtolower($this->username)));
		if($user===null)
                {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
                        $this->errorMessage="We can not find your email";
                }
		else if(!$user->validatePassword($this->password))
                {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
                        $this->errorMessage="Your password is not valid.";
                }
		else
		{
			$this->_id=$user->id;
                        $this->setState('adminId',$user->id);
			$this->errorCode=self::ERROR_NONE;
                        if($user->last_login !== null)
                                $this->setState('lastLogin',$user->last_login);
                        $user->saveAttributes(array('last_login'=>date('y-m-d H:i:s')));
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	public function getId()
        {
            return $this->_id;
        }
}