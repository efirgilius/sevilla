<?php
/**
 * Class component for web user which can access from Yii::app()->user
 *
 * @author eris
 */
class WebUser extends CWebUser
{
    private $_adminModel;   
    
    public function getEmail()
    {
        if($this->isGuest)
            return FALSE;
        else
            return $this->model->email;
    }
        
    public function getModel()
    {
        if($this->_adminModel===NULL)
        {
            $this->_adminModel= User::model()->findByPk((int)Yii::app()->user->id);
        }
        return $this->_adminModel;
    } 
    
    public function getLastlogin()
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
        return $formatter->formatDateTime(strtotime(Yii::app()->user->getState('userLastLogin')));
    }
    
    public function getIsManager()
    {
            if($this->isGuest)
                    return FALSE;
            elseif(Yii::app()->user->id && $this->model->level==1)
                    return TRUE;
            else
                    return FALSE;
    }
    
    public function getIsAdmin()
    {
        if($this->isGuest)
                    return FALSE;
        elseif(Yii::app()->user->id && $this->model->level==2)
                return TRUE;
        else
            return FALSE;
    }
    
    public function getIsOperator()
    {
        if($this->isGuest)
                    return FALSE;
        elseif(Yii::app()->user->id && $this->model->level==3)
                return TRUE;
        else
            return FALSE;
    }
    
}
?>
