<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class MemberIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user= Member::model()->find('LOWER(mid)=? AND (status=1 OR status=2)',array(strtolower($this->username)));
                
		if($user===null)
                {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
                        $this->errorMessage="Email is invalid.";
                }
		else if(!$user->validatePassword($this->password))
                {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
                        $this->errorMessage="Password is invalid.";
                }
		else
		{
			$this->_id=$user->id;
			$this->errorCode=self::ERROR_NONE;
	            $this->setState('name',$user->name);
	            if($user->last_login !== null)
	                $this->setState('lastLogin',$user->last_login);
	            $user->saveAttributes(array('last_login'=>date('y-m-d H:i:s')));
		}
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}