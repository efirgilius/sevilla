<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/favicon.ico"/>
    
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheets.css" rel="stylesheet" type="text/css" />  
    <!--[if lt IE 8]>
        <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->            
    
    
</head>
<body>
    <div class="errorPage">        
        <?php echo $content;?>
        <p><button class="btn btn-warning" onClick="history.back();">Previous page</button></p>        
    </div>
</body>
</html>