<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<!-- Mirrored from themesflat.com/html/consultant/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Aug 2016 12:52:50 GMT -->
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title><?=CHtml::encode($this->pageTitle);?></title>
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseurl;?>/stylesheets/revolution-slider.css">
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseurl;?>/stylesheets/style.css">
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseurl;?>/stylesheets/responsive.css">
<link href='../../../fonts.googleapis.com/css9b11.css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
<link href="<?=Yii::app()->baseurl;?>/images/common/favicon.ico" rel="shortcut icon">
<!--[if lt IE 9]>
	<script src="<?=Yii::app()->baseurl;?>/javascript/html5shiv.js"></script>
	<script src="<?=Yii::app()->baseurl;?>/javascript/respond.min.js"></script>
<![endif]-->
</head>

<body class="<?=($this->useSidebar)?'right-sidebar':'';?>">
	<!-- Preloader -->
	<div class="preloader">
		<div class="clear-loading loading-effect-2">
			<span></span>
		</div>
	</div>
	<div class="topbar visible-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="left">
						<ul>
						<?php if(Yii::app()->member->id):?>
							<li><?=CHtml::link('Hai, '.Yii::app()->member->name,['/member']);?></li>
							<li><?=CHtml::link('Keluar',['/site/logout']);?></li>
						<?php else:?>
							<li><?=CHtml::link('Masuk',['/site/login']);?></li>
							<li><?=CHtml::link('Daftar',['/site/register']);?></li>
						<?php endif;?>
						</ul>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="right">
						<ul class="social">
							<li><a href="http://twitter.com/<?=$this->g('socmed','twitter');?>"><i class="ion-social-twitter"></i></a></li>
							<li><a href="http://facebook.com/<?=$this->g('socmed','facebook');?>"><i class="ion-social-facebook"></i></a></li>
							<li><a href="http://plus.google.com/<?=$this->g('socmed','google_plus');?>"><i class="ion-social-googleplus"></i></a></li>
							<li><a href="#"><i class="ion-social-pinterest"></i></a></li>
							<li><a href="#"><i class="ion-social-linkedin"></i></a></li>
						</ul>
						<!-- <img class="flag" src="<?=Yii::app()->baseurl;?>/images/common/en-flag.gif" alt="english">
						<select name="language" id="language" class="language">
							<option value="en">English</option>
							<option value="fr">France</option>
						</select> -->
					</div>
				</div>
			</div>  
		</div>
	</div>
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p id="logo" class="logo">
						<?=CHtml::link(CHtml::image($this->img('logo'),$this->g('general','site_name')),Yii::app()->homeurl,['rel'=>'home','']);?>
					</p><!-- /.logo -->
					<div class="info">
						<div class="address">
							<p class="icon"><i class="ion-android-call"></i></p>
							<p class="text">Hubungi <?=CHtml::link($this->g('contact','phone'),'tel:'.$this->g('contact','phone'));?><br><span><?=$this->g('contact','address');?></span></p>
						</div>
						<!-- <div class="open-hours">
							<p class="icon"><i class="ion-android-watch"></i></p>
							<p class="text">Open Hours<br><span>Mon - Sat: 8 am - 5 pm, Sunday: CLOSED </span></p>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Header -->
	<header id="header" class="header header-sticky">
		<div class="header-wrap">
			<div class="header-nav">
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="btn-menu"></div>
							<?php $this->widget('MenuWidget');?>
						</div>
						<div class="col-sm-3">
							<p class="get-appointment">
								<?php if(Yii::app()->member->id):?>
									<?=CHtml::link('Hai, '.Yii::app()->member->name,['/member']);?>
								<?php else:?>
									<?=CHtml::link('Daftar Sekarang Juga',['/site/register']);?>
								<?php endif;?>
							</p>
						</div>
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.header-wrap -->
	</header><!-- /.header -->
		
	<?=$content;?>

	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footer-info">
						<p class="footer-logo"><img src="<?=$this->img('logo');?>" alt="<?=$this->g('general','site_name');?>"></p>
						<p class="description"><?=$this->g('general','site_description');?></p>
						<p class="phone"><?=$this->g('contact','phone');?> <span><?=$this->g('contact','email');?></span></p>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="footernav">
						<nav class="footernav01">
							<h3>Ikuti Kami</h3>
							<ul class="followus">
								<li><a href="http://facebook.com/<?=$this->g('socmed','facebook');?>"><i class="ion-social-facebook"></i>Facebook</a></li>
								<li><a href="http://twitter.com/<?=$this->g('socmed','twitter');?>"><i class="ion-social-twitter"></i>Twitter</a></li>
								<li><a href="http://plus.google.com/<?=$this->g('socmed','google_plus');?>"><i class="ion-social-googleplus"></i>Google Plus</a></li>
								<li><a href="#"><i class="ion-social-linkedin"></i>Linkedin</a></li>
								<li><a href="#"><i class="ion-social-youtube"></i>Youtube</a></li>
							 </ul>
						</nav>
						<?php $this->widget('FooterPageWidget');?>
						<nav class="footernav03">
							<h3>Business hours</h3>
							<span>Our suppoer available to help you 24 hours a day, seven days week</span>
							<p>Monday-Saturday: ............... 8.00-18.00</p>
							<p>Sunday: ............................................ Closed</p>
							<p>Calendar Events: .......................... Closed</p>
						</nav>
					</div>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
		<div class="bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-10">
						<p class="copyright">© <?=date('Y');?> <?=$this->g('general','site_name');?>. All rights reserved</p>
					</div><!-- /.col-md-10 -->
					<div class="col-sm-2">
						<p class="totop"><a href="#">Back Top<img src="<?=Yii::app()->baseurl;?>/images/common/icon-totop.png" alt="back top"></a></p>
					</div>
				</div><!-- /.row -->
			</div>
		</div>
	</footer>
	<!-- Javascript -->
	<script type="text/javascript" src="<?=Yii::app()->baseurl;?>/javascript/plugins.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseurl;?>/javascript/main.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseurl;?>/javascript/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseurl;?>/javascript/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseurl;?>/javascript/slider.js"></script>
</body>

<!-- Mirrored from themesflat.com/html/consultant/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Aug 2016 12:54:06 GMT -->
</html>