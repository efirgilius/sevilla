<div id="page_content">
    <div id="page_content_inner">  

        <?php Notify::renderFlash();?>
        <h4 class="heading_a uk-margin-bottom">Basic settings</h4>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
        )); ?>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-1-3 uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Logo</label><br><br>
                                <?php if ($this->img('logo')):?>
                                    <?= CHtml::image($this->img('logo'),'Logo');?>
                                <?php endif;?>
                                <input id="Setting_file" name="general[logo]" type="file" class="md-input"/>
                                <p>Resolution 30px X 30px.</p>
                            </div>
                            <div class="uk-form-row">
                                <label>Site Name</label>
                                <input class="md-input" type="text" name="general[site_name]" value="<?=$this->g('general','site_name');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Site Description</label>
                                <input class="md-input" type="text" name="general[site_description]" value="<?=$this->g('general','site_description');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Title</label>
                                <input class="md-input" type="text" name="meta[meta_title]" value="<?=$this->g('meta','meta_title');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Meta Keywords</label>
                                <input class="md-input" type="text" name="meta[meta_keywords]" value="<?=$this->g('meta','meta_keywords');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Meta Description</label>
                                <input class="md-input" type="text" name="meta[meta_description]" value="<?=$this->g('meta','meta_description');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Meta Author</label>
                                <input class="md-input" type="text" name="meta[meta_author]" value="<?=$this->g('meta','meta_author');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Address</label>
                                <input class="md-input" type="text" name="contact[address]" value="<?=$this->g('contact','address');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Email</label>
                                <input class="md-input" type="text" name="contact[email]" value="<?=$this->g('contact','email');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Phone</label>
                                <input class="md-input" type="text" name="contact[phone]" value="<?=$this->g('contact','phone');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Mailbox Reply</label>
                                <input class="md-input" type="text" name="contact[mailbox_reply]" value="<?=$this->g('contact','mailbox_reply');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="heading_a">Social Media</h3>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Facebook</label>
                                <input class="md-input" type="text" name="socmed[facebook]" value="<?=$this->g('socmed','facebook');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Twitter</label>
                                <input class="md-input" type="text" name="socmed[twitter]" value="<?=$this->g('socmed','twitter');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Instagram</label>
                                <input class="md-input" type="text" name="socmed[instagram]" value="<?=$this->g('socmed','instagram');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Google+</label>
                                <input class="md-input" type="text" name="socmed[google_plus]" value="<?=$this->g('socmed','google_plus');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <ul class="uk-tab" data-uk-tab="{connect:'#settings_users', animation: 'slide-horizontal' }">
                                <li class="uk-active"><a href="#">Appearance</a></li>
                            </ul>
                            <ul id="settings_users" class="uk-switcher uk-margin">
                                <li>
                                    <ul class="md-list">
                                        <li>
                                            <div class="md-list-content">
                                                <div class="uk-float-right">
                                                    <?= CHtml::dropDownList('appearance[show_banner]', $this->g('appearance','show_banner'),['Yes'=>'Yes','No'=>'No']);?>
                                                </div>
                                                <span class="md-list-heading">Show banner in front end</span>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>
			
			<h3 class="heading_a">Front Text</h3>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
								<textarea class="md-input" name="front[text]"><?=$this->g('front','text');?></textarea>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="md-fab-wrapper">
                <button type="submit" class="md-fab md-fab-primary" name="saveSetting" id="page_settings_submit">
                    <i class="material-icons">&#xE161;</i>
                </button>
            </div>

        <?php $this->endWidget();?>

    </div>
</div>