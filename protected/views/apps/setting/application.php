<div id="page_content">
    <div id="page_content_inner">  

        <?php Notify::renderFlash();?>
        <h4 class="heading_a uk-margin-bottom">Application settings</h4>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
        )); ?>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
							<div class="uk-form-row">
                                <label>Biaya Administrasi (%)</label>
                                <input class="md-input" type="text" name="application[biaya_administrasi]" value="<?=$this->g('application','biaya_administrasi');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Biaya Paket Standard (IDR)</label>
                                <input class="md-input" type="text" name="application[biaya_paket_standard]" value="<?=$this->g('application','biaya_paket_standard');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Biaya Paket Deluxe (IDR)</label>
                                <input class="md-input" type="text" name="application[biaya_paket_deluxe]" value="<?=$this->g('application','biaya_paket_deluxe');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Biaya Paket Premium (IDR)</label>
                                <input class="md-input" type="text" name="application[biaya_paket_premium]" value="<?=$this->g('application','biaya_paket_premium');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Biaya Paket Executive (IDR)</label>
                                <input class="md-input" type="text" name="application[biaya_paket_executive]" value="<?=$this->g('application','biaya_paket_executive');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Biaya Paket Executive Diamond (IDR)</label>
                                <input class="md-input" type="text" name="application[biaya_paket_executive_diamond]" value="<?=$this->g('application','biaya_paket_executive_diamond');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
							<div class="uk-form-row">
                                <label>Jenjang Karir Manager</label>
                                <input class="md-input" type="text" name="application[manager]" value="<?=$this->g('application','manager');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Jenjang Karir Sapphire</label>
                                <input class="md-input" type="text" name="application[sapphire]" value="<?=$this->g('application','sapphire');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Jenjang Karir Ruby</label>
                                <input class="md-input" type="text" name="application[ruby]" value="<?=$this->g('application','ruby');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Jenjang Karir Emerald</label>
                                <input class="md-input" type="text" name="application[emerald]" value="<?=$this->g('application','emerald');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Jenjang Karir Diamond</label>
                                <input class="md-input" type="text" name="application[diamond]" value="<?=$this->g('application','diamond');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Jenjang Karir Crown</label>
                                <input class="md-input" type="text" name="application[crown]" value="<?=$this->g('application','crown');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="md-fab-wrapper">
                <button type="submit" class="md-fab md-fab-primary" name="saveSetting" id="page_settings_submit">
                    <i class="material-icons">&#xE161;</i>
                </button>
            </div>

        <?php $this->endWidget();?>

    </div>
</div>