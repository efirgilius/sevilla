<div id="page_content">
    <div id="page_content_inner">  

        <?php Notify::renderFlash();?>
        <h4 class="heading_a uk-margin-bottom">Bonus &amp; Jenjang Karir Setting</h4>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
        )); ?>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Bonus Sponsor Per Titik (Rp)</label>
                                <input class="md-input" type="text" name="bonus[bonus_sponsor]" value="<?=$this->g('bonus','bonus_sponsor');?>"/>
                            </div>
                            
                            <div class="uk-form-row">
                                <label>Bonus Pasangan (Rp)</label>
                                <input class="md-input" type="text" name="bonus[bonus_pasangan]" value="<?=$this->g('bonus','bonus_pasangan');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Flush Out Pasangan</label>
                                <input class="md-input" type="text" name="bonus[flush_out]" value="<?=$this->g('bonus','flush_out');?>"/>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div class="uk-width-large-1-3 uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-form-row">
                                <label>Reward Manager</label>
                                <input class="md-input" type="text" name="bonus[reward_manager]" value="<?=$this->g('bonus','reward_manager');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Reward Sapphire</label>
                                <input class="md-input" type="text" name="bonus[reward_sapphire]" value="<?=$this->g('bonus','reward_sapphire');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Reward Ruby</label>
                                <input class="md-input" type="text" name="bonus[reward_ruby]" value="<?=$this->g('bonus','reward_ruby');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Reward Emerald</label>
                                <input class="md-input" type="text" name="bonus[reward_emerald]" value="<?=$this->g('bonus','reward_emerald');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Reward Diamond</label>
                                <input class="md-input" type="text" name="bonus[reward_diamond]" value="<?=$this->g('bonus','reward_diamond');?>"/>
                            </div>
                            <div class="uk-form-row">
                                <label>Reward Crown</label>
                                <input class="md-input" type="text" name="bonus[reward_crown]" value="<?=$this->g('bonus','reward_crown');?>"/>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>

            <div class="md-fab-wrapper">
                <button type="submit" class="md-fab md-fab-primary" name="saveSetting" id="page_settings_submit">
                    <i class="material-icons">&#xE161;</i>
                </button>
            </div>

        <?php $this->endWidget();?>

    </div>
</div>