<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'validation',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
)); ?>
                            
    <div id="page_content">
            <div id="page_heading">
                <h1 id="product_edit_name">Update Reward</h1>
            </div>
            
            <div id="page_content_inner">

                <?php Notify::renderFlash();?>
                <?php echo $this->renderPartial('_form', array('model'=>$model,'form'=>$form)); ?>
            </div>
    </div>

    <div class="md-fab-wrapper">
        <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE161;</i></button>
    </div>
<?php $this->endWidget(); ?>
