<?=$form->errorSummary($model);?>
<div class="uk-grid" data-uk-grid-margin>
	<div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-3 uk-width-large-1-3">
                                    <?php echo $form->checkbox($model,'status',['data-switchery'=>'true']); ?>
                                <label class="inline-label">Is Active</label>
                            </div>
                        </div>
                    </div>
                </div>
		<div class="md-card">
			<div class="md-card-content large-padding">
				
				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'downline'); ?>
					<?php echo $form->numberField($model,'downline',array('size'=>50,'maxlength'=>50,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'downline'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'reward'); ?>
					<?php echo $form->textField($model,'reward',array('size'=>60,'maxlength'=>255,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'reward'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'reward_value'); ?>
					<?php echo $form->numberField($model,'reward_value',array('size'=>60,'maxlength'=>255,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'reward_value'); ?>
				</div>

			</div>
		</div>
	</div>
</div>