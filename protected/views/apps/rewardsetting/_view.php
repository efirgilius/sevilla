<?php
/* @var $this RewardsettingController */
/* @var $data RewardSetting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('downline')); ?>:</b>
	<?php echo CHtml::encode($data->downline); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reward')); ?>:</b>
	<?php echo CHtml::encode($data->reward); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reward_value')); ?>:</b>
	<?php echo CHtml::encode($data->reward_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>