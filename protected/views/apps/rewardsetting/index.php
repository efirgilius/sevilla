<div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Reward Setting</h3>
            <?php Notify::renderFlash();?>
            <div class="md-card">                
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>Kiri - Kanan</th>
                                            <th>Reward</th>
                                            <th>Nilai (Rp)</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($model):?>
			                                <?php $i=1; foreach($model as $data):?>
		                                        <tr>
                                                            <td class="uk-text-large uk-text-nowrap"><?=  number_format($data->downline);?></td>
		                                            <td class="uk-text-large uk-text-nowrap"><?= $data->reward;?></td>
                                                            <td class="uk-text-large uk-text-nowrap"><?= number_format($data->reward_value);?></td>
		                                            <td>
	                                                    <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$data->id));?>
	                                                    <a href="#" class="del" data-id="<?php echo $data->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
		                                            </td>
		                                        </tr>
		                                        
		                                    <?php $i++; endforeach;?>
			                            <?php else:?>
	                                        <tr>
	                                            <td colspan="4"><p>Reward setting is unavailable.</p></td>
	                                        </tr>
			                            <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

	<div class="md-fab-wrapper">
        <?=CHtml::link('<i class="material-icons">&#xE150;</i>', ['create'],['class'=>'md-fab md-fab-accent']);?>
    </div>    

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete the selected item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/rewardsetting/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/rewardsetting')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>