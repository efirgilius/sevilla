<div id="page_content">
        <div id="page_heading">
            <h1>Withdraw <small>Ewallet</small></h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th> 
                                            <th>Member</th>
                                            <th>Nilai Transaksi</th>
                                            <th>Tanggal</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 <?php if($model):?>
                                 <?php $i=1; foreach($model as $row):?>
                                    <tr>                                    
                                        <td><?php echo $i;?></td>    
                                        <td><?php echo $row->member->mid.' - '.$row->member->name;?></td>
                                        <td>Rp. <?= number_format($row->value);?></td>
                                        <td><?php echo $row->created;?></td>
                                        <td><?php if($row->status==0):?><?php echo CHtml::link('<i class="md-icon material-icons">spellcheck</i>','javascript:void(0);',['class'=>'app','data-id'=>$row->id]);?> <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a><?php endif;?></td>                                    
                                    </tr>
                                 <?php $i++; endforeach;?>
                                 <?php else:?>
                                    <tr>
                                        <td colspan="8">Transaksi ewallet masih kosong.</td>
                                    </tr>
                                 <?php endif;?>

                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('action', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this withdraw ewallet?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/withdraw/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/withdraw',['type'=>$_GET['type']])."'; 
            }
        });
    return false;
    });
    
    $('.app').on('click',function(){
        if(!confirm('Are you sure want to APPROVE this withdraw ewallet?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/withdraw/approve')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/withdraw',['type'=>$_GET['type']])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>
