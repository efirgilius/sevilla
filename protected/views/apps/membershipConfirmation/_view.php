                                <?php if($model):?>
                                 <?php foreach($model as $row):?>
                                    <tr>                                    
                                        <td><?php echo $i;?></td>
                                        <td><?php echo CHtml::link($row->member->mid.' - '.$row->member->name,['/apps/member/view','id'=>$row->member_id]);?></td>
                                        <td><?php echo number_format($row->value);?></td>
                                        <td><?php echo $row->bank_receiver;?></td>
                                        <td><?php echo $row->bank_sender;?></td>
                                        <td><?php echo $row->account_name;?></td>
                                        <td><?php echo $row->date;?></td>
                                        <td><?php echo $row->statusname;?></td>
                                        <?php if($row->status=='0'):?>
                                        <td>
                                                <a href="#" class="app" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE5CA;</i></a>
                                        </td>   
                                        <td>
                                                <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                                        </td>   
                                        <?php endif;?>
                                    </tr>
                                 <?php $i++; endforeach;?>
                                 <?php else:?>
                                    <tr>
                                        <td colspan="9">Membership confirmation is unavailable.</td>
                                    </tr>
                                 <?php endif;