<div id="page_content">
        <div id="page_heading">
            <h1><?=$title;?> Membership Confirmation</h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="md-card">
                <div class="md-card-content">
                    <?php echo CHtml::form(array('/apps/membershipConfirmation/search'), 'GET',array('class'=>'search'));?>
                    <div class="uk-grid" data-uk-grid-margin="">
                        
                        <div class="uk-width-medium-8-10">
                            <label for="order_search">Key</label>
                            <?php echo CHtml::textField('q', '', array('class'=>'md-input','id'=>'order_search'));?>
                        </div>
                        
                        <div class="uk-width-medium-2-10 uk-text-center">
                            <button type="submit" class="md-btn md-btn-primary uk-margin-small-top">Search</button>
                        </div>
                        
                    </div>
                    <?php echo CHtml::endForm();?>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th>Member</th>
                                            <th>Amount (IDR)</th>
                                            <th>Bank Receiver</th>
                                            <th>Bank Sender</th>
                                            <th>Account Name</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <?php if($title=='New'):?>
                                            <th>Approve</th>
                                            <th>Delete</th>
                                            <?php endif;?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model,'i'=>$i));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php if($title=='Processed'):?>
                    <?= CHtml::endForm();?>
                    <?php endif;?>
                </div>
            </div>

        </div>
    </div>

<?php 
if(isset($_GET['type']))
    $tpage=$_GET['type'];
else
    $tpage='new';

Yii::app()->clientScript->registerScript('delete-pay', "
    $('.app').on('click',function(){
        if(!confirm('Are you sure want to APPROVE this membership payment confirmation?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/membershipConfirmation/approve')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/membershipConfirmation',['type'=>'new'])."'; 
            }
        });
    return false;
    });
    
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to DELETE this membership payment confirmation?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/membershipConfirmation/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/membershipConfirmation',['type'=>'new'])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>