<div id="page_content">
        <div id="page_content_inner">

            <div class="uk-width-medium-10-10 uk-container-center reset-print">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-margin>
                    <div class="uk-width-large-7-10">
                        <div class="md-card md-card-single main-print" id="invoice" style="opacity: 1; transform: scale(1);">
                            <div id="invoice_preview">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions hidden-print">
                <i class="md-icon material-icons" id="invoice_print"></i>
                <div class="md-card-dropdown" data-uk-dropdown="">
                    <i class="md-icon material-icons"></i>
                    <div class="uk-dropdown uk-dropdown-flip uk-dropdown-small">
                        <ul class="uk-nav">
                            <li><a href="#">Archive</a></li>
                            <li><a href="#" class="uk-text-danger">Remove</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <h3 class="md-card-toolbar-heading-text large" id="invoice_name">
                Package Order Detail, No. <?= $model->code;?>
            </h3>
        </div>
        <div class="md-card-content" >
            <div class="uk-margin-medium-bottom">
                <span class="uk-text-muted uk-text-small uk-text-italic">Date:</span> <?= $model->posted;?>
                <br>
                <span class="uk-text-muted uk-text-small uk-text-italic">Status:</span> <?php echo ($model->status=='0')?'<span class="uk-badge uk-badge-warning">'.$model->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$model->statusname.'</span>';?>
                <?php if($model->status=='2'):?>
                <br>
                <span class="uk-text-muted uk-text-small uk-text-italic">Resi Number:</span> <span class="uk-badge uk-badge-warning"><?= $model->resi;?></span>
                <br>
                <?php endif;?>
            </div>
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-small-3-5">
                    <div class="uk-margin-bottom">
                        <span class="uk-text-muted uk-text-small uk-text-italic">To:</span>
                        <address>
                            <p><strong><?= CHtml::link($model->member->fullname.' - '.$model->member->mid,['/apps/member/view','id'=>$model->member_id]);?></strong></p>
                            <p><?= $model->addresss->address;?><br>Kec. <?= $model->addresss->district;?>, <?= $model->addresss->city_type;?> <?= $model->addresss->city;?><br><?= $model->addresss->province;?> - <?= $model->addresss->post_code;?></p>
                            <p>Phone: <?= $model->addresss->mobile_phone;?></p>
                        </address>
                    </div>
                </div>
                <div class="uk-width-small-2-5">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Total:</span>
                    <p class="heading_b uk-text-success">Rp <?= number_format($model->total);?></p>
                    <p class="uk-text-small uk-text-muted uk-margin-top-remove">Unique code Rp <?= $model->unique_code;?></p>
                </div>
            </div>
            <div class="uk-grid uk-margin-large-bottom">
                <div class="uk-width-1-1">
                    <table class="uk-table">
                        <thead>
                        <tr class="uk-text-upper">
                            <th>Product</th>
                            <th class="uk-text-center">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="uk-table-middle">
                                <td>
                                    <span class="uk-text-large"><?= $model->product->code;?></span><br>
                                    <span class="uk-text-muted uk-text-small"><?= $model->product->name;?></span>
                                </td>
                                <td class="uk-text-center">
                                    1
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="uk-grid">
                
                <div class="uk-width-1-2">
                    <span class="uk-text-muted uk-text-big uk-text-italic">Payment Confirmation:</span>
                    <?php if($model->status=='1'):?>
                    <p class="uk-margin-top-remove">
                        Transfer to: <strong><?= $model->bank_receiver;?></strong><br>
                        From to: <strong><?= $model->bank_sender;?></strong><br>
                        Date: <strong><?= $model->date;?></strong><br>
                        Amount: <strong>IDR <?= number_format($model->total + $model->unique_code);?></strong>
                    </p>
                   
                    <?php else:?>
                    <p class="uk-margin-top-remove">-</p>
                    <?php endif;?>
                    <?php if($model->status!=='2'):?>
                    <?php echo CHtml::beginForm(['approve'],'POST');?>
                        <?= CHtml::hiddenField('pid',$model->id);?>
                                                        <button type="submit" class="md-btn md-btn-success" id="process">Process?</button>
                    <?php echo CHtml::endForm(); ?>
                                                        <?php endif;?>
                </div>
            </div>
        </div>
    </div>
                        </div>
                    </div>
                    <div class="uk-width-large-3-10 hidden-print uk-visible-large">
                        <div class="md-list-outside-wrapper">
                            <ul class="md-list md-list-outside invoices_list" id="invoices_list">
                                <li class="heading_list">10 Latest Order</li>
                                <?php if($models):?>
                                <?php foreach($models as $m):?>
                                <li>
                                    <a href="<?= Yii::app()->createUrl('/apps/order/view',['id'=>$m->id]);?>" class="md-list-content" data-invoice-id="1">
                                        <span class="md-list-heading uk-text-truncate">Invoice <?= $m->code;?> <span class="uk-text-small uk-text-muted"><?= $m->posted;?></span></span>
                                        <span class="uk-text-small uk-text-muted">Rp <?= number_format($m->totalPrice);?></span> <?php echo ($m->status=='0')?'<span class="uk-badge uk-badge-warning">'.$m->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$m->statusname.'</span>';?>
                                    </a>
                                </li>
                                <?php endforeach;?>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php Yii::app()->clientScript->registerScript('app-member', "
    $('#process').on('click',function(){
        if(!confirm('Are you sure want to process this package order (member will be activated automatically)?')) return false;
    });
",CClientScript::POS_READY);?>