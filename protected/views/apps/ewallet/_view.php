<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>  
 <?php if($model):?>
 <?php $i=1; foreach($model as $row):?>
    <tr>                                    
        <td><?php echo $i;?></td>    
        <td><?php echo $row->bank->name;?> - <?php echo $row->bank->acc_number;?></td>
        <td><?php echo $row->name;?></td>
        <td><?= number_format($row->ewallet->value);?></td>
        <td><?= number_format($row->value);?></td>
        <td><?= $row->note;?></td>
        <td><?=$row->statusname;?></td>
        <td>
            <?php if($row->status==0):?>
            <?php echo CHtml::link('<i class="md-icon material-icons">spellcheck</i>','javascript:void(0);',['class'=>'app','data-id'=>$row->id]);?> <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
            <?php endif;?>
        </td>                                    
    </tr>
 <?php $i++; endforeach;?>
 <?php else:?>
    <tr>
        <td colspan="8">Transaksi ewallet masih kosong.</td>
    </tr>
 <?php endif;?>
