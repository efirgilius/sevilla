<?php
/* @var $this EwalletController */
/* @var $model Ewallet */

$this->breadcrumbs=array(
	'Ewallet Transaction'=>array('index'),
	'Edit',
);
?>

            <div class="workplace">
                <div class="page-header">
                    <h1><small>Edit</small> Ewallet Transaction</h1>
                </div>
                <div class="row-fluid">
                    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
                </div>
                <div class="dr"><span></span></div>  
            </div>