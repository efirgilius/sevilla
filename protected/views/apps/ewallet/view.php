<?php
/* @var $this EwalletController */
/* @var $model Ewallet */

$this->breadcrumbs=array(
	'Ewallets'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ewallet', 'url'=>array('index')),
	array('label'=>'Create Ewallet', 'url'=>array('create')),
	array('label'=>'Update Ewallet', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ewallet', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ewallet', 'url'=>array('admin')),
);
?>

<h1>View Ewallet #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'type',
		'note',
		'value',
		'status',
		'created_on',
	),
)); ?>
