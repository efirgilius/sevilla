<?php
/* @var $this EwalletController */
/* @var $model Ewallet */
/* @var $form CActiveForm */
?>

                    <div class="span12">
                        <div class="head clearfix">
                            <div class="isw-ok"></div>
                            <h1>Ewallet Form</h1>
                        </div>
                        <div class="block-fluid">      
                            <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'validation',
                                    'enableAjaxValidation'=>false,
                            )); ?>
                            
                            <div class="row-form clearfix">
                                <div class="span3"><?php echo $form->labelEx($model,'value'); ?></div>
                                <div class="span9">
                                    <?php echo $form->numberField($model,'value',array('size'=>50,'maxlength'=>50,'class'=>'validate[required]')); ?><span><?php echo $form->error($model,'value'); ?></span>
                                </div>
                            </div>                  
                                
                            <div class="footer tar">
                                <?php echo CHtml::link('Cancel','javascript: history.go(-1)',array('class'=>'btn btn-danger'));?>&nbsp;&nbsp;&nbsp;<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
                            </div>                 
                                
                            <?php $this->endWidget(); ?>
                        </div>

                    </div>