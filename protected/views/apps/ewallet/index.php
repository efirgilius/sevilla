<div id="page_content">
        <div id="page_heading">
            <h1>Ewallet <small>Transaction</small></h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th> 
                                            <th>Ke Bank</th>
                                            <th>Nama Pengirim</th>
                                            <th>Jumlah Deposit</th>
                                            <th>Jumlah Transfer</th>
                                            <th>Keterangan</th>
                                            <th>Status</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('category', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this ewallet transaction?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/ewallet/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/ewallet',['status'=>$_GET['status']])."'; 
            }
        });
    return false;
    });
    
    $('.app').on('click',function(){
        if(!confirm('Are you sure want to APPROVE this ewallet transaction?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/ewallet/approve')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/ewallet',['status'=>$_GET['status']])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>
