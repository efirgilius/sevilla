<div id="page_content">
        <div id="page_content_inner">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'validation',
                'enableAjaxValidation'=>false,
                'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
            )); ?>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-12-12">
                        <div class="md-card">
                            <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                                <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <?=CHtml::image($model->large_url,'');?>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="User[avatar]" id="User[avatar]">
                                        </span>
                                        <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput">
                                            <i class="material-icons">&#xE5CD;</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="user_heading_content">
                                    <h2 class="heading_b">
                                        <span class="uk-text-truncate" id="user_edit_uname">
                                            <?=$model->name;?>
                                        </span>
                                        <span class="sub-heading" id="user_edit_position"><?=$model->levelname;?></span>
                                    </h2>
                                </div>
                                <div class="md-fab-wrapper">
                                    <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                        <i class="material-icons">&#xE8BE;</i>
                                        <div class="md-fab-toolbar-actions">
                                            <button type="submit" id="user_edit_save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Save"><i class="material-icons md-color-white">&#xE161;</i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="user_content">
                                <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                    <li class="uk-active"><a href="#">General info</a></li>
                                </ul>
                                <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                    <li>
                                        <div class="uk-margin-top">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1">
                                                    <label for="user_edit_uname_control">Full Name</label>
                                                    <?php echo $form->textField($model,'name',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                                                    <?php echo $form->error($model,'name'); ?>
                                                </div>
                                            </div>
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                    <label for="user_edit_uname_control">Email</label>
                                                    <?php echo $form->emailField($model,'email',array('maxlength'=>50,'class'=>'md-input','required'=>'required')); ?>
                                                    <?php echo $form->error($model,'email'); ?>
                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <label for="user_edit_uname_control">New Password</label>
                                                    <?php echo $form->passwordField($model,'newPassword',array('maxlength'=>30,'class'=>'md-input')); ?><span><em>Leave it empty if you won't change your password.</em></span>
                                                    <?php echo $form->error($model,'newPassword'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>