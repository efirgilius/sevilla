<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

                            <?php if($model):?>
                                <?php $i=1; foreach($model as $row):?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$row->name;?></td>
                                            <td><?= $row->email;?></td>
                                            <td><?= $row->levelname;?></td>
                                            <td><span class="uk-badge uk-badge-<?=($row->status==0)?'warning':'success';?>"><?= $row->statusname;?></span></td>
                                            <td><?= $row->last_login;?></td>
                                            <td class="uk-text-nowrap">
                                                <?php if($row->level!=='1'):?>
                                                    <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$row->id));?>
                                                    <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                <?php $i++; endforeach;?>
                            <?php else:?>
                                        <tr>
                                            <td colspan="7"><p>User is unavailable.</p></td>
                                        </tr>
                            <?php endif; ?>

