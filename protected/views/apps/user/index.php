<div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">User Management</h3>
            <?php Notify::renderFlash();?>
            <div class="md-card">
                
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Level</th>
                                            <th>Status</th>
                                            <th>Last Login</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="pagination center">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="#new_data" data-uk-modal="{center:true}">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>

    <div class="uk-modal" id="new_data">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'form_validation',
                'action'=>Yii::app()->controller->createUrl('/apps/user/create'),
                'enableAjaxValidation'=>false,
                //'enableClientValidation'=>true
            )); ?>
                <?=CHtml::hiddenField('Category[type]',1);?>
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Add New User</h3>
                </div>
                
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'name'); ?>
                    <?php echo $form->textField($newModel,'name',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'name'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'email'); ?>
                    <?php echo $form->emailField($newModel,'email',array('maxlength'=>50,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'email'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'password'); ?>
                    <?php echo $form->passwordField($newModel,'password',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'password'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'level'); ?><br>
                    <?php echo CHtml::dropDownList('User[level]',isset($newModel->level)?$newModel->level:'', User::levelList(),['class'=>'md-input','required'=>'required']); ?>
                    <?php echo $form->error($newModel,'level'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'status'); ?><br>
                    <?php echo CHtml::dropDownList('User[status]',isset($newModel->status)?$newModel->status:'', User::statusList(),['class'=>'md-input','required'=>'required']); ?>
                    <?php echo $form->error($newModel,'status'); ?>
                </div>
                <div class="uk-modal-footer">
                    <button type="submit" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete-user', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this user?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/user/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/user')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>