            <?=$form->errorSummary($model);?>
            <div class="md-card">
                <div class="md-card-content large-padding">
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'name'); ?>
                    <?php echo $form->textField($model,'name',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->emailField($model,'email',array('maxlength'=>50,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'newPassword'); ?>
                    <?php echo $form->passwordField($model,'newPassword',array('maxlength'=>30,'class'=>'md-input')); ?><span><em>Leave it empty if you won't change members's password.</em></span>
                    <?php echo $form->error($model,'newPassword'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'level'); ?><br>
                    <?php echo CHtml::dropDownList('User[level]',isset($model->level)?$model->level:'', User::levelList(),['class'=>'md-input','required'=>'required']); ?>
                    <?php echo $form->error($model,'level'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'status'); ?><br>
                    <?php echo CHtml::dropDownList('User[status]',isset($model->status)?$model->status:'', User::statusList(),['class'=>'md-input','required'=>'required']); ?>
                    <?php echo $form->error($model,'status'); ?>
                </div>
                </div>
            </div>
