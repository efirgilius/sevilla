    <div id="page_content">
        <div id="page_content_inner">

            <!-- statistics (small charts) -->
            <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Visitors (Online)</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?=Yii::app()->counter->getOnline();?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Visitors (Today)</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?=Yii::app()->counter->getToday();?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Visitors (Yesterday)</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?=Yii::app()->counter->getYesterday();?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Visitors (Total)</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?=Yii::app()->counter->getTotal();?></span></h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>