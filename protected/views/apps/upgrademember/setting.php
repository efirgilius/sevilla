<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

    <div id="page_content">
        <div id="page_heading">
            <h1 id="product_edit_name">Upgrade Member Information</h1>
        </div>
        <div id="page_content_inner">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'validation',
                    'enableAjaxValidation'=>false,
                    'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
            )); ?>

            <?=$form->errorSummary($page);?>
            <?php Notify::renderFlash();?>
            <div class="md-card">
                <div class="md-card-content large-padding">

                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="parsley-row">
                                    <?php echo $form->labelEx($page,'title'); ?>
                                    <?php echo $form->textField($page,'title',array('maxlength'=>100,'class'=>'md-input','required'=>'required')); ?>
                                    <?php echo $form->error($page,'title'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid">
                            
                            <div class="uk-width-1-1">
                                <div class="parsley-row">
                                    <?php echo $form->labelEx($page,'content'); ?><br>
                                    <?php echo $form->textArea($page,'content',array('cols'=>10,'rows'=>8,'maxlength'=>330,'data-parsley-trigger'=>'keyup','class'=>'md-input','required'=>'required','id'=>'editor1'));?>
                                    <?php echo $form->error($page,'content'); ?>
                                </div>
                            </div>
                        </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="md-fab-wrapper">
        <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE161;</i></button>
        <?=CHtml::link('<i class="material-icons">undo</i>','javascript: history.go(-1)',['class'=>'md-fab md-fab-warning']);?>
    </div>
<?php $this->endWidget(); ?>
<script>
    CKEDITOR.replace( 'editor1', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>