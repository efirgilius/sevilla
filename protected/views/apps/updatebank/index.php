<div id="page_content">
        <div id="page_heading">
            <h1><?=$title;?> Update Bank Account Request</h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
           
            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th>Member</th>
                                            <th>Bank Name</th>
                                            <th>Account Number</th>
                                            <th>Account Holder Name</th>
                                            <?php if($state=='pending'):?>
                                            <th>Approve</th>
                                            <th>Delete</th>
                                            <?php endif;?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model,'i'=>$i,'state'=>$state));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php if($title=='Processed'):?>
                    <?= CHtml::endForm();?>
                    <?php endif;?>
                </div>
            </div>

        </div>
    </div>

<?php 
if(isset($_GET['state']))
    $tpage=$_GET['state'];
else
    $tpage='pending';

Yii::app()->clientScript->registerScript('delete-pay', "
    $('.app').on('click',function(){
        if(!confirm('Are you sure want to APPROVE this request?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/updatebank/approve')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/updatebank',['state'=>$tpage])."'; 
            }
        });
    return false;
    });
    
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to DELETE this request?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/updatebank/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/updatebank',['state'=>$tpage])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>