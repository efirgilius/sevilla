                                <?php if($model):?>
                                 <?php foreach($model as $row):?>
                                    <tr>                                    
                                        <td><?php echo $i;?></td>
                                        <td><?php echo CHtml::link($row->member->mid.' - '.$row->member->name,['/apps/member/view','id'=>$row->member_id]);?></td>
                                        <td><?= $row->bank;?></td>
                                        <td><?= $row->account_number;?></td>
                                        <td><?= $row->account_name;?></td>
                                        <?php if($state=='pending'):?>
                                        <td>
                                            <a href="#" class="app" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE5CA;</i></a>
                                        </td>   
                                        <td>
                                            <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                                        </td>   
                                        <?php endif;?>
                                    </tr>
                                 <?php $i++; endforeach;?>
                                 <?php else:?>
                                    <tr>
                                        <td colspan="9">Request update bank account is unavailable.</td>
                                    </tr>
                                 <?php endif;