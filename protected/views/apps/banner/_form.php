            <?=$form->errorSummary($model);?>
            <div class="md-card">
                <div class="md-card-content large-padding">
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <?php echo $form->labelEx($model,'image'); ?><br><br>
                                    <?php if(!$model->isNewRecord):?>
                                    <?php echo CHtml::image($model->small_url);?><br/>
                                    <?php endif;?>
                                    <input id="upload" name="Banner[image]" type="file" <?php echo ($model->isNewRecord OR $model->image==null)?'required="required"':'';?> style="margin-top: 15px;" class="md-input"/>
                                    <p class="uk-text-small uk-margin-remove padding20">Select an image to upload. Up to 1 MB. Recommended image size <strong><?=Banner::LARGE_THUMB_WIDTH;?>px X <?=Banner::LARGE_THUMB_HEIGHT;?>px</strong>.</p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <div class="uk-form-row">
                                    <?php echo $form->labelEx($model,'order'); ?><br>
                                    <?php echo $form->textField($model,'order',array('maxlength'=>2,'class'=>'md-input')); ?>
                                </div>
                                <div class="uk-form-row">
                                    <?php echo $form->checkbox($model,'is_active',['data-switchery'=>'true']); ?>
                                    <label class="inline-label">Is Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <?php echo $form->labelEx($model,'title'); ?>
                                    <?php echo $form->textField($model,'title',array('maxlength'=>100,'class'=>'md-input')); ?>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <?php echo $form->labelEx($model,'url'); ?>
                                    <?php echo $form->textField($model,'url',array('maxlength'=>100,'class'=>'md-input')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="parsley-row">
                                    <?php echo $form->labelEx($model,'description'); ?>
                                    <?php echo $form->textArea($model,'description',array('cols'=>10,'rows'=>8,'maxlength'=>330,'data-parsley-trigger'=>'keyup','class'=>'md-input'));?>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
