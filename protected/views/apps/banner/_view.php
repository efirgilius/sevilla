<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if($model):?>
    <?php $i=1; foreach($model as $row):?>
            <tr>
                <td><?=$i;?></td>
                <td><?=CHtml::image($row->small_url);?></td>
                <td class="uk-text-large uk-text-nowrap"><?=$row->title;?></td>
                <td><?=$row->description;?></td>
                <td><?php echo ($row->url!==null)?$row->url:'-';?></td>
                <td><?=$row->order;?></td>
                <td><?php echo ($row->is_active=='0')?'<span class="uk-badge uk-badge-danger">'.$row->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$row->statusname.'</span>';?></td>
                <td class="uk-text-nowrap">
                    <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$row->id));?>
                    <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                </td>
            </tr>
    <?php $i++; endforeach;?>
<?php else:?>
            <tr>
                <td colspan="7"><p>Banner is unavailable.</p></td>
            </tr>
<?php endif; ?>

