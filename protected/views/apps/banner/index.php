<div id="page_content">
    <div id="page_content_inner">
        <h3 class="heading_b uk-margin-bottom">Banner</h3>
        <?php Notify::renderFlash();?>
        <div class="md-card">
            
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>URL</th>
                                        <th>Order</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $this->renderPartial('_view',array('model'=>$model));?>
                                </tbody>
                            </table>
                        </div>
                        <?php if($count>10):?>
                        <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                        <?php 
                        $this->widget('CLinkPager', array(
                            'pages' => $pages,
                            'header'=>'',
                            'footer'=>'',
                            'nextPageLabel'=>'»',
                            'prevPageLabel'=>'«',
                            'id'=>'link_pager',
                        ))?>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="md-fab-wrapper">
    <?=CHtml::link('<i class="material-icons">edit</i>',['/apps/banner/create'],['class'=>'md-fab md-fab-primary','title'=>'Create New']);?>
</div>

<?php Yii::app()->clientScript->registerScript('delete-banner', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this banner?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/banner/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/banner')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>