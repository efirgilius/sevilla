<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'form_validation',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
)); ?>
<div id="page_content">
    <div id="page_heading">
        <h1 id="product_edit_name"><?=$model->name;?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"><?=$model->code;?></span>
    </div>
    <div id="page_content_inner">
        <?php echo $this->renderPartial('_form', array('model'=>$model,'form'=>$form)); ?>
    </div>
</div>

<div class="md-fab-wrapper">
    <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE161;</i></button>
</div>
<?php $this->endWidget(); ?>