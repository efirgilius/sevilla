<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?php Notify::renderFlash();?>
<?=$form->errorSummary($model);?>
<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
    <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right">
                    <?php echo $form->checkbox($model,'status',array('data-switchery'=>true)); ?>
                </div>
                <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Publish</label>
            </div>
        </div>
        <div class="md-card">
            <!--
            <div class="md-card-toolbar">
                <div class="md-card-toolbar-actions">
                    <i class="md-icon material-icons">&#xE146;</i>
                </div>
                <h3 class="md-card-toolbar-heading-text">
                    Photos
                </h3>
            </div>
            -->
            <div class="md-card-content">
                <div class="uk-form-row">
                    <label for="main_image">Main Image</label>
                    <div class="uk-margin-bottom uk-text-center uk-position-relative">
                        <?php if(!$model->isNewRecord):?>
                            <?php $mainImg=ProductImage::model()->main()->findByAttributes(['product_id'=>$model->id]);?>
                            <?php if($mainImg):?>
                                <div class="uk-position-relative MultiFile-label" id="img_<?php echo $mainImg->id;?>">
                                    <?php echo CHtml::link('<span class="product-id hidden" style="display:none;">'.$model->id.'</span><span class="img-id hidden" style="display:none;">'.$mainImg->id.'</span><span class="layer-id hidden" style="display:none;">img_'.$mainImg->id.'</span>
                                        <span class="uk-modal-close uk-close uk-close-alt uk-position-absolute"/></span>', '#img_'.$mainImg->id, array(
                                       'class'=>'delete-image del-main',
                                       'data-main-id'=>1
                                    ));?>
                                    <?php echo CHtml::image($model->small_url,$model->name,array('style'=>'max-height:60px;'));?>
                                </div>
                            <?php else:?>
                                <?php echo CHtml::image($model->small_url,$model->name,array('style'=>'max-height:60px;'));?><br/>
                            <?php endif;?>
                        <?php endif;?>
                        <div id="mainImg" <?php if((!$model->isNewRecord) && (!empty($mainImg))):?>style="display: none;"<?php endif;?>>
                            <input id="main_image" name="mainimage" type="file" <?php if($model->isNewRecord):?>required="required"<?php endif;?>/>
                            <br>
                            <em style="font-size:11px;">Select an image to upload. Up to 1 MB. Recommended image size <strong><?=ProductImage::LARGE_THUMB_WIDTH;?>px X <?=ProductImage::LARGE_THUMB_HEIGHT;?>px</strong>.</em>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    <?php echo $form->labelEx($model,'category_id',['class'=>'uk-form-label']); ?>
                </h3>
            </div>
            <div class="md-card-content">
                <div class="uk-form-row">
                    <select data-md-selectize name="Product[category_id]" id="Product_category_id">
                        <?php $parents = Category::model()->type('product')->active()->null()->findAll();?>
                        <?php foreach ($parents as $k => $v):?>
                            <?php if ($v->categories):?>
                                <optgroup label=<?=$v->name;?>>
                                <?php foreach ($v->categories as $k => $v):?>
                                    <option value="<?=$v->id;?>" <?=($model->category_id==$v->id)?'selected':'';?>><?=$v->name;?></option>
                                <?php endforeach;?>
                                </optgroup>
                            <?php else:?>
                                <option value="<?=$v->id;?>" <?=($model->category_id==$v->id)?'selected':'';?>><?=$v->name;?></option>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                    <?php echo $form->error($model,'category_id'); ?>
                </div>
            </div>
        </div>
        <div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    <?php echo $form->labelEx($model,'brand_id',['class'=>'uk-form-label']); ?>
                </h3>
            </div>
            <div class="md-card-content">
                <div class="uk-form-row">
                    <?php echo $form->dropDownList($model,'brand_id',CHtml::listData(Brand::model()->findAll(),'id','name'),['prompt'=>'','data-md-selectize'=>true]); ?>
                    <?php echo $form->error($model,'brand_id'); ?>
                </div>
            </div>
        </div>-->
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right">
                    <?php echo $form->checkbox($model,'is_stock',array('data-switchery'=>true)); ?>
                </div>
                <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Stock</label>
            </div>
            <div class="md-card-content">
                <div class="uk-float-right">
                    <?php echo $form->checkbox($model,'is_new',array('data-switchery'=>true)); ?>
                </div>
                <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">New</label>
            </div>
            <div class="md-card-content">
                <div class="uk-float-right">
                    <?php echo $form->checkbox($model,'is_recommend',array('data-switchery'=>true)); ?>
                </div>
                <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Recommend</label>
            </div>
        </div>
        <!--<div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    <label class="uk-form-label">SEO Meta Tag</label>
                </h3>
            </div>
            <div class="md-card-content">
                <div class="uk-form-row">
                    <?php echo $form->labelEx($model,'meta_title'); ?>
                    <?php echo $form->textField($model,'meta_title',array('size'=>70,'maxlength'=>70,'class'=>'md-input')); ?>
                    <?php echo $form->error($model,'meta_title'); ?>
                </div>
                <div class="uk-form-row">
                    <?php echo $form->labelEx($model,'meta_description'); ?>
                    <?php echo $form->textField($model,'meta_description',array('size'=>160,'maxlength'=>160,'class'=>'md-input')); ?>
                    <?php echo $form->error($model,'meta_description'); ?>
                </div>
                <div class="uk-form-row">
                    <?php echo $form->labelEx($model,'meta_keywords'); ?>
                    <?php echo $form->textField($model,'meta_keywords',array('size'=>255,'maxlength'=>255,'class'=>'md-input')); ?>
                    <?php echo $form->error($model,'meta_keywords'); ?>
                </div>
            </div>
        </div>-->
    </div>
    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
        <div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    Details
                </h3>
            </div>
            <div class="md-card-content large-padding">
                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-large-1-2">
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'name'); ?>
                            <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'md-input','required'=>'required')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'code'); ?>
                            <?php echo $form->textField($model,'code',array('size'=>10,'maxlength'=>10,'class'=>'md-input','required'=>'required')); ?>
                            <?php echo $form->error($model,'code'); ?>
                        </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'price'); ?>
                            <?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10,'class'=>'md-input','required'=>'required')); ?>
                            <?php echo $form->error($model,'price'); ?>
                        </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'discount'); ?>
                            <?php echo $form->textField($model,'discount',array('size'=>3,'maxlength'=>3,'class'=>'md-input')); ?>
                            <?php echo $form->error($model,'discount'); ?>
                        </div>
                        <!--<div class="uk-form-row">
                            <?php echo $form->labelEx($model,'weight'); ?>
                            <?php echo $form->textField($model,'weight',array('size'=>11,'maxlength'=>11,'class'=>'md-input')); ?>
                            <?php echo $form->error($model,'weight'); ?>
                        </div>-->
                    </div>
                    <div class="uk-width-large-1-2">
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'overview'); ?>
                            <?php echo $form->textArea($model,'overview',array('cols'=>30,'rows'=>4,'class'=>'md-input','maxlength'=>255)); ?>
                            <?php echo $form->error($model,'overview'); ?>
                        </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'tags'); ?>
                            <?php echo $form->textArea($model,'tags',array('cols'=>30,'rows'=>4,'class'=>'md-input','maxlength'=>255)); ?>
                            <?php echo $form->error($model,'tags'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    <?php echo $form->labelEx($model,'description'); ?>
                </h3>
            </div>
            <div class="md-card-content">
                <?php echo $form->textArea($model,'description',['class'=>'md-input','id'=>'editor','required'=>'required']); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
        </div>
        <div class="md-card">
            <div class="md-card-toolbar">
                <h3 class="md-card-toolbar-heading-text">
                    <?php echo $form->labelEx($model,'specification'); ?>
                </h3>
            </div>
            <div class="md-card-content">
                <?php echo $form->textArea($model,'specification',['class'=>'md-input','id'=>'editor1','required'=>'required']); ?>
                <?php echo $form->error($model,'specification'); ?>
            </div>
        </div>-->
    </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'editor', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>
<script type="text/javascript">
    CKEDITOR.replace( 'editor1', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>

<?php Yii::app()->clientScript->registerScript('delete','

    $(".delete-image").on("click",function(){
            var tagId=$(this).find(".product-id").text();
            var imgId=$(this).find(".img-id").text();
            var layerId=$(this).find(".layer-id").text();
            var mainId=$(this).attr("data-main-id");
            $(".product-dialog-id").text(tagId);
            $(".img-dialog-id").text(imgId);
            $(".layer-dialog-id").text(layerId);
            $(".main-id").text(mainId);
            $("#imagedialog_").dialog("open"); 
            return false;
        });
        
',  CClientScript::POS_READY);?>       

<?php 
if(!$model->isNewRecord){
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'imagedialog_',
    'options'=>array(
        'title'=>'Delete Image',
        'autoOpen'=>false,
        'modal'=>true,
        'buttons' => array(
                    'Ok' => 'js: function() {
                        var tagId =  $(".product-dialog-id").text();
                        var imgId =  $(".img-dialog-id").text();
                        var layerId = $(".layer-dialog-id").text();
                        var mainId = $(".main-id").text();
                        var Img = parseInt($("#countImg").text());
                        var countImg = Img + 1;
                        $.ajax({
                                url:"' . CController::createUrl('apps/product/deleteimage') . '",
                                data: "productid="  + tagId + "&imgid=" + imgId,
                                dataType: "json",
                                type:"get",
                                beforeSend: function(j) {
                                        $("#"+ layerId).animate({"backgroundColor":"#fb6c6c"},300);
                                },
                                success: function() {
                                        $("#"+ layerId).slideUp(300,function() {
                                                $("#"+ layerId).remove();
                                        });
                                        $("#countImg").text(countImg);
                                        if(Img < 10)
                                        {
                                            $("#browseFile").show();
                                        }
                                        if(mainId == "1")
                                        {
                                            $("#mainImg").show();
                                        }
                                }     
                            });
                        $(this).dialog(\'close\');
                         }',
                            'Cancel' => 'js: function() {$(this).dialog(\'close\');}',
                    ),
    ),
));

echo 'Are you sure want to delete this image? <span class="hidden product-dialog-id" style="display:none;"></span><span class="hidden img-dialog-id" style="display:none;"></span><span class="hidden layer-dialog-id" style="display:none;"></span><span class="hidden main-id" style="display:none;"></span>';

$this->endWidget('zii.widgets.jui.CJuiDialog');}?>

