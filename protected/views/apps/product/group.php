<div id="page_content">
    <?php Notify::renderFlash();?>
    <div id="page_content_inner">
        <?php if ($category):?>
        <?php foreach ($category as $k => $v):?>
        <h4 class="heading_a uk-margin-bottom"><?=$v->name;?> 

        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-hover">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $model=$v->productGroups;?>
                            <?php if($model):?>
                            <?php $i=1; foreach($model as $row):?>
                                <tr>
                                    <td class="uk-text-large uk-text-nowrap"><?=$row->product->name;?></td>
                                    <td class="uk-text-large uk-text-nowrap"><?=$row->product->slug;?></td>
                                    <td>
                                    <?php if ($row->product->status==1):?>
                                            <i class="material-icons uk-text-success md-24">&#xE834;</i>
                                        <?php else:?>
                                            <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <a class="updateproductgroup" href="#update_productgroup" data-group-id="<?=$row->category->id;?>" data-product-id="<?=$row->product->id;?>" data-uk-modal="{ center:true }">
                                            <i class="md-icon material-icons">&#xE254;</i>
                                        </a>
                                        <a href="#" class="delfromgroup" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            <?php else:?>
                                <tr>
                                    <td colspan="7"><p>Products is unavailable.</p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <?php endif;?>
    </div>
</div>


    <div class="md-fab-wrapper">
        <a title="Add Product To Group" class="md-fab md-fab-accent" href="#add_product" data-uk-modal="{ center:true }">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>

    <div class="uk-modal" id="add_product">
        <div class="uk-modal-dialog">
            <?=CHtml::beginForm('addproductgroup','post',['class'=>'uk-form-stacked']);?>
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Product</label>
                    <?=CHtml::dropDownList('ProductGroup[product_id]','ProductGroup[product_id]',CHtml::listData($product,'id','name'),['class'=>'uk-form','data-md-selectize-inline'=>true]);?>
                </div>
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Group</label>
                    <?=CHtml::dropDownList('ProductGroup[category_id]','ProductGroup[category_id]',CHtml::listData($groups,'id','name'),['class'=>'uk-form-width','data-md-selectize-inline'=>true]);?>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="btn-update">Add Product</button>
                </div>
            <?=CHtml::endForm();?>
        </div>
    </div>

    <div class="uk-modal" id="update_productgroup">
        <div class="uk-modal-dialog">
            <?=CHtml::beginForm('updateproductgroup','post',['class'=>'uk-form-stacked']);?>
                <?=CHtml::hiddenField('ProductGroup[productid]','',['class'=>'md-input']);?>
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Group</label>
                    <?=CHtml::dropDownList('ProductGroup[groupid]','ProductGroup[groupid]',CHtml::listData($groups,'id','name'),['class'=>'md-input']);?>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="btn-update">Update Group</button>
                </div>
            <?=CHtml::endForm();?>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete', "

    $('.delfromgroup').on('click',function(){
        if(!confirm('Are you sure want to delete this from group?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/product/deletefromgroup')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/product/group')."'; 
            }
        });
    return false;
    });

",CClientScript::POS_READY);?>

<?php Yii::app()->clientScript->registerScript('update', "
    $('.updateproductgroup').on('click',function(){
        var productid=$(this).attr('data-product-id');
        var groupid=$(this).attr('data-group-id'); 
        $('#ProductGroup_productid').val(productid);
        $('#ProductGroup_groupid').val(groupid);
    });
",CClientScript::POS_READY);?>