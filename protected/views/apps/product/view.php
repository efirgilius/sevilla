<div id="page_content">
    <div id="page_heading">
        <h1><?=$model->name;?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small"><?=$model->code;?></span>
    </div>
    <div id="page_content_inner">
    <?php Notify::renderFlash();?>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <?php if ($model->status==1):?>
                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                            <?php else:?>
                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                            <?php endif;?>
                        </div>
                        <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Publish</label>
                    </div>
                </div>
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text">
                            Photos
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <div class="uk-margin-bottom uk-text-center">
                            <?=CHtml::image($model->medium_url,'',['class'=>'img_medium']);?>
                        </div>
                        
                    </div>
                </div>
                
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <?php if ($model->is_stock==1):?>
                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                            <?php else:?>
                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                            <?php endif;?>
                        </div>
                        <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Stock</label>
                    </div>
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <?php if ($model->is_new==1):?>
                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                            <?php else:?>
                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                            <?php endif;?>
                        </div>
                        <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">New</label>
                    </div>
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <?php if ($model->is_recommend==1):?>
                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                            <?php else:?>
                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                            <?php endif;?>
                        </div>
                        <label class="uk-display-block uk-margin-small-top" for="product_edit_active_control">Recommend</label>
                    </div>
                </div>
                <!--<div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text">
                            SEO Meta Tag
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <ul class="md-list">
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Meta Title</span>
                                <p><?=$model->meta_title;?></p>
                            </div>
                            <hr class="uk-grid-divider">
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Meta Description</span>
                                <p><?=$model->meta_description;?></p>
                            </div>
                            <hr class="uk-grid-divider">
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Meta Keywords</span>
                                <p><?=$model->meta_keywords;?></p>
                            </div>
                            <hr class="uk-grid-divider">
                        </ul>
                    </div>
                </div>-->
            </div>
            <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text">
                            Details
                        </h3>
                    </div>
                    <div class="md-card-content large-padding">
                        <div class="uk-grid uk-grid-divider uk-grid-medium">
                            <div class="uk-width-large-1-2">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Product Name</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle"><?=$model->name;?></span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">SKU Number</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <?=$model->code;?>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Price</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        IDR <?=number_format($model->price);?>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Discount</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <?=$model->discount;?>%
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                
                                <hr class="uk-grid-divider uk-hidden-large">
                            </div>
                            <div class="uk-width-large-1-2">
                                <div class="md-list-content">
                                    <span class="uk-text-small uk-text-muted uk-display-block">Overview</span>
                                    <p><?=$model->overview;?></p>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="md-list-content">
                                    <span class="uk-text-small uk-text-muted uk-display-block">Tags</span>
                                    <p>
                                        <?php if ($model->tags):?>
                                        <?php foreach ($model->tagThis as $k => $v):?>
                                            <span class="uk-badge uk-badge-success"><?=$v;?></span>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </p>
                                </div>
                                <hr class="uk-grid-divider">
                                <hr class="uk-grid-divider uk-hidden-large">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
</div>

<div class="md-fab-wrapper">
    <?=CHtml::link('<i class="material-icons">&#xE150;</i>', ['update','id'=>$model->id],['class'=>'md-fab md-fab-primary']);?>
</div>