<div id="page_content">
    <div id="page_content_inner">
        <?php Notify::renderFlash();?>
        <!-- <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-3-10">
                        <label for="product_search_name">Product Name</label>
                        <input type="text" class="md-input" id="product_search_name">
                    </div>
                    <div class="uk-width-medium-1-10">
                        <label for="product_search_price">Price</label>
                        <input type="text" class="md-input" id="product_search_price">
                    </div>
                    <div class="uk-width-medium-3-10">
                        <div class="uk-margin-small-top">
                            <select id="product_search_status" data-md-selectize multiple data-md-selectize-bottom>
                                <option value="">Status</option>
                                <option value="1">In stock</option>
                                <option value="2">Out of stock</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-10">
                        <div class="uk-margin-top uk-text-nowrap">
                            <input type="checkbox" name="product_search_active" id="product_search_active" data-md-icheck/>
                            <label for="product_search_active" class="inline-label">Active</label>
                        </div>
                    </div>
                    <div class="uk-width-medium-2-10 uk-text-center">
                        <a href="#" class="md-btn md-btn-primary uk-margin-small-top">Filter</a>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Stock</th>
                                        <th>Is Publish</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($model):?>
                                    <?php foreach ($model as $k => $v):?>
                                    <tr>
                                        
                                        <td class="uk-text-large uk-text-nowrap"><?=CHtml::link($v->name, ['view','id'=>$v->id]);?></td>
                                        <td class="uk-text-nowrap">IDR <?=number_format($v->price);?></td>
                                        <td><?=$v->discount;?>%</td>
                                        <td>
                                            <?php if ($v->is_stock==1):?>
                                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                                            <?php else:?>
                                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                                            <?php endif;?>
                                        </td>
                                        <td>
                                            <?php if ($v->status==1):?>
                                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                                            <?php else:?>
                                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                                            <?php endif;?>
                                        </td>
                                        <td class="uk-text-nowrap">
                                            <?php echo CHtml::link('<i class="md-icon material-icons">&#xE8F4;</i>',array('view','id'=>$v->id));?>
                                            <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$v->id));?>
                                            <a href="#" class="del" data-id="<?php echo $v->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    <?php else:?>
                                        <p>Product not available.</p>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="md-fab-wrapper">
    <?=CHtml::link('<i class="material-icons">&#xE150;</i>', ['create'],['class'=>'md-fab md-fab-accent']);?>
</div>

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/product/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/product')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>
