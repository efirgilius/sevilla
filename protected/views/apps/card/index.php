<div id="page_content">
        <div id="page_heading">
            <h1><?= $this->title;?> <small>Stock</small></h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="uk-grid uk-grid-width-large-1-5 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Total PIN</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?php echo $pin;?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Active</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?php echo $pinActive;?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Activated</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?php echo $pinInactive;?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Available in stock</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?php echo $pinStock;?></span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <span class="uk-text-muted uk-text-small">Sent to member</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe"><?php echo ($pin - $pinStock);?></span></h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="md-card">
                <div class="md-card-content">
                    <?php echo CHtml::link('Generate PIN',array('create'),array('class'=>'md-btn md-btn-primary uk-margin-small-top'));?>
                    <?php echo CHtml::link('Send PIN',array('send'),array('class'=>'md-btn md-btn-primary uk-margin-small-top'));?>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-content">
                    <?= CHtml::beginForm(['/apps/card'],'get');?>
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-8-10">
                            <label for="product_search_name">Find PIN</label>
                            <input type="text" class="md-input" name="q"/>
                        </div>
                        <div class="uk-width-medium-2-10 uk-text-center">
                            <input type="submit" class="md-btn md-btn-primary uk-margin-small-top" value="Search" />
                        </div>
                    </div>
                    <?=CHtml::endForm();?>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th width="15%">Serial Number</th>
                                            <th width="10%">PIN</th>
                                            <th width="10%">Sudah Digunakan?</th>
                                            <th width="15%">Anggota Pengguna</th>
                                            <th width="10%">Sudah Aktif?</th>
                                            <th width="15%">Pemilik</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model,'i'=>$i));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete-contact', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to DELETE this PIN?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/card/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/card',array('type'=>$_GET['type']))."'; 
            }
        }); 
    return false;
    });
    
    $('.act').on('click',function(){
        if(!confirm('Are you sure want to ACTIVATE this PIN?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/card/activate')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/card',array('type'=>$_GET['type']))."'; 
            }
        }); 
    return false;
    });
",CClientScript::POS_READY);?>