<?php
/* @var $this CardsController */
/* @var $data Card */
?>

                                <?php if($model):?>
                                 <?php foreach($model as $row):?>
                                    <tr>                                    
                                        <td><?php echo $i;?></td>
                                        <?php if($title=='Company'):?>
                                        <td><?= $row->member->mid.' - '.$row->member->name;?></td>
                                        <?php else:?>
                                        <td><?php echo ($row->partner_id!==NULL)?$row->partner->mid.' - '.$row->partner->name:'';?></td>
                                        <td><?php echo ($row->member_id!==NULL)?$row->member->mid.' - '.$row->member->name:'';?></td>
                                        <?php endif;?>
                                        <td><?php echo $row->typename;?></td>
                                        <?php if($title=='Company'):?>
                                        <td><?php echo $row->credit;?></td>
                                        <?php else:?>
                                        <td><?php echo $row->debit;?></td>
                                        <?php endif;?>
                                        <td><?php echo $row->posted;?></td>
                                        <td><?php echo $row->trans;?></td>
                                        <td><?php echo $row->statusname;?></td>
                                                                          
                                    </tr>
                                 <?php $i++; endforeach;?>
                                 <?php else:?>
                                    <tr>
                                        <td colspan="7">PIN transaction history is unavailable.</td>
                                    </tr>
                                 <?php endif;?>