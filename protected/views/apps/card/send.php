<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'form_validation',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
)); ?>
<div id="page_content">
    <div id="page_heading">
        <h1 id="product_edit_name"><?php if($valid==true):?>Send PIN Form<?php else:?>Password Form<?php endif;?></h1>
    </div>
    <div id="page_content_inner">
        <?php Notify::renderFlash();?>
        <?=$form->errorSummary($model);?>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content large-padding">
                        <?php if($valid==true):?>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'member_id'); ?><br>
                            <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'model'=>$model,
                                'attribute'=>'id_member',
                                'source'=>$this->createUrl('loadmember'),
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'select'=>"js:function(event, ui) {
                                      $(this).val(ui.item.value);
                                      $('#CardbalanceForm_member_id').val(ui.item.id);
                                    }"
                                ),
                                'cssFile'=>false,
                                'htmlOptions'=>array('class'=>'form-control  form-control--contact md-input','placeholder'=>'Ketik User ID','required'=>'required')
                            ));?>
                            <input type="hidden" value="" name="CardbalanceForm[member_id]" id="CardbalanceForm_member_id" placeholder="" required="required">
                            <?php echo $form->error($model,'member_id'); ?>
                        </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'type'); ?><br>
                            <?php echo $form->dropDownList($model,'type',  Card::typeList(),['prompt'=>'- Select PIN Type -','class'=>'md-input','required'=>'required']); ?>
                            <?php echo $form->error($model,'type'); ?>
                         </div>
                        <div class="uk-form-row">
                            <?php echo $form->labelEx($model,'value'); ?>
                            <?php echo $form->numberField($model,'value',array('maxlength'=>3,'class'=>'md-input','min'=>1,'max'=>300)); ?>
                            <?php echo $form->error($model,'credit'); ?>
                        </div>
                        <?php else:?>

                        <div class="uk-form-row">
                            <?php echo $form->labelEx($card,'password'); ?>
                            <?php echo $form->passwordField($card,'password',array('maxlength'=>30,'class'=>'md-input')); ?>
                            <?php echo $form->error($card,'password'); ?>
                        </div>

                        <div class="uk-form-row">
                            <?php echo $form->labelEx($card,'verifyCode'); ?>
                            <br><br>
                            <?php if(extension_loaded('gd')): ?>
                                <?php $this->widget('CCaptcha'); ?>
                            <?php endif;?>
                            <?php echo $form->textField($card,'verifyCode',array('maxlength'=>10,'class'=>'md-input')); ?>
                            <?php echo $form->error($card,'verifyCode'); ?>
                        </div>
                        <?php endif;?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="md-fab-wrapper">
    <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE90A;</i></button>
    <?=CHtml::link('<i class="material-icons">undo</i>','javascript: history.go(-1)',['class'=>'md-fab md-fab-warning']);?>
</div>
<?php $this->endWidget(); ?>