<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?=$form->errorSummary($model);?>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-2">
        <div class="md-card">
            <div class="md-card-content large-padding">
                <?php if($valid==true):?>
                <div class="uk-form-row">
                    <?php echo $form->labelEx($model,'value'); ?>
                    <?php if($model->isNewRecord):?>
                    <?php echo $form->numberField($model,'value',array('maxlength'=>3,'class'=>'md-input','min'=>1,'max'=>300,'required'=>'required')); ?>
                    <?php echo $form->error($model,'value'); ?>
                    <?php else:?>
                    <?php echo $form->textField($model,'serial',array('maxlength'=>20,'disabled'=>'disabled')); ?>
                    <?php endif;?>
                </div>
                <div class="uk-form-row">
                   <?php echo $form->labelEx($model,'type'); ?><br>
                   <?php echo $form->dropDownList($model,'type',  Card::typeList(),['prompt'=>'- Select PIN Type -','class'=>'md-input','required'=>'required']); ?>
                   <?php echo $form->error($model,'type'); ?>
                </div>
                <?php else:?>

                <div class="uk-form-row">
                   <?php echo $form->labelEx($card,'password'); ?>
                   <?php echo $form->passwordField($card,'password',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                   <?php echo $form->error($card,'password'); ?>
                </div>

                <div class="uk-form-row">
                    <?php echo $form->labelEx($card,'verifyCode'); ?>
                    <br><br>
                    <?php if(extension_loaded('gd')): ?>
                    <?php $this->widget('CCaptcha'); ?>
                    <?php endif;?>
                    <?php echo $form->textField($card,'verifyCode',array('maxlength'=>10,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($card,'verifyCode'); ?>
                        
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>