<?php if($model):?>
<?php foreach($model as $row):?>
<tr>                                    
    <td><?php echo $i;?></td>
    <td><?php echo $row->serial;?></td>
    <td><?php echo $row->pin;?></td>
    <td>
        <?php if($row->status=='0'):?>
        <span class="label label-success"><?php echo $row->statusname;?></span>
        <?php else:?>
        <span class="label label-important"><?php echo $row->statusname;?></span>
        <?php endif;?>
    </td>
    <td><?php echo $row->owner;?></td>
    <td>
        <?php if($row->is_active=='0'):?>
        <span class="label label-success"><?php echo $row->isactive;?></span>
        <?php else:?>
        <span class="label label-important"><?php echo $row->isactive;?></span>
        <?php endif;?>
    </td>
    <td><?php echo ($row->member_id!==null)?CHtml::link($row->member->mid.' - '.$row->member->name,['/apps/member/view','id'=>$row->member_id]):'';?></td>
    <td>
        <?php if(!$row->members):?><?php if($row->member_id==null):?><?php echo CHtml::link('<i class="md-icon material-icons">&#xE872;</i>',array('delete','id'=>$row->id),array('data-id'=>$row->id,'class'=>'del'));?><?php endif;?><?php endif;?>
        <?php if($row->is_active=='0'):?><?php echo CHtml::link('<i class="md-icon material-icons">spellcheck</i>',array('activate','id'=>$row->id),array('data-id'=>$row->id,'class'=>'act'));?><?php endif;?>
    </td>                                   
</tr>
<?php $i++; endforeach;?>
<?php else:?>
<tr>
    <td colspan="10">E-voucher is unavailable.</td>
</tr>
<?php endif;?>