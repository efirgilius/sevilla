<?php
/* @var $this CardsController */
/* @var $model Card */

$this->breadcrumbs=array(
	'PIN'=>array('index'),
	$model->serial,
	'Edit',
);
?>

            <div class="workplace">
                <div class="page-header">
                    <h1><small>Edit</small> PIN</h1>
                </div>
                <div class="row-fluid">
                    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
                </div>
                <div class="dr"><span></span></div>  
            </div>