<?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'form_validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
    )); ?>
    <div id="page_content">
        <div id="page_heading">
            <h1 id="product_edit_name"><?=$this->title;?></h1>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <?php echo $this->renderPartial('_form', array('valid'=>$valid,'card'=>$card,'model'=>$model,'form'=>$form)); ?>
        </div>
    </div>

    <div class="md-fab-wrapper">
        <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE90A;</i></button>
        <?=CHtml::link('<i class="material-icons">undo</i>','javascript: history.go(-1)',['class'=>'md-fab md-fab-warning']);?>
    </div>
<?php $this->endWidget(); ?>