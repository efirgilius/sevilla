<?php
/* @var $this CardsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	$title.' PIN Transaction History',
);
?>

<div class="workplace">
                <div class="page-header">
                    <h1><?php echo $title;?> PIN Transaction <small>History</small></h1>
                </div>
                <?php Notify::renderFlash();?>
                
                <div class="row-fluid">

                    <div class="span12">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>PIN</h1>             
                        </div>
                        <div class="block-fluid">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table">
                                <thead>
                                    <tr>                                    
                                        <th width="2%">#</th>
                                        <?php if($title=='Company'):?>
                                        <th>Member</th>
                                        <?php else:?>
                                        <th>Buyer</th>
                                        <th>Seller</th>
                                        <?php endif;?>
                                        <th>Type</th>
                                        
                                        <th width="15%">Total</th>
                                        <th>Transaction Type</th>
                                        <th width="20%">Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $this->renderPartial('_viewHistory',array('model'=>$model,'i'=>$i,'title'=>$title));?>                              
                                </tbody>
                            </table>   
                            <?php if($count>10):?>
                            <div class="pagination right">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                        
                    </div>           

                </div>            

                <div class="dr"><span></span></div>  
            </div>