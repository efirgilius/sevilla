<?=$form->errorSummary($model);?>
            <div class="md-card">
                <div class="md-card-content large-padding">
                    <div class="uk-margin-large-bottom">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </div>
                    <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'acc_number'); ?>
                    <?php echo $form->textField($model,'acc_number',array('maxlength'=>20,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($model,'acc_number'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($model,'acc_name'); ?>
                    <?php echo $form->textField($model,'acc_name',array('maxlength'=>20,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($model,'acc_name'); ?>
                </div>
                    <div class="uk-margin-large-bottom">
                        <?php echo $form->labelEx($model,'image'); ?>
                        <?php if(!$model->isNewRecord):?>
                        <?php echo ($model->image!==NULL)?CHtml::image(Yii::app()->baseUrl.'/'.$model->small_url).'<br/>':'';?>
                        <?php endif;?>
                        <input id="upload" name="Bank[image]" type="file" <?php echo ($model->isNewRecord OR $model->image==null)?'required="required"':'';?>/><span>Select an image to upload. Up to 1 MB. Recommended image size <strong>120px X 36px</strong>.</span>
                    </div>
                </div>
            </div>
