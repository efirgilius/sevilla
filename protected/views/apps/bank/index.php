<div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Bank</h3>
            <?php Notify::renderFlash();?>
            <div class="md-card">
                
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>&nbsp;</th>
                                            <th>Name</th>
                                            <th>Account Number</th>
                                            <th>Account Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="pagination center">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="#new_data" data-uk-modal="{center:true}">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>

    <div class="uk-modal" id="new_data">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'form_validation',
                                    'action'=>Yii::app()->controller->createUrl('/apps/bank/create'),
                                    'enableAjaxValidation'=>false,
                                    'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
                            )); ?>
                <?=CHtml::hiddenField('Category[type]',1);?>
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Create New Bank</h3>
                </div>
                
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'name'); ?>
                    <?php echo $form->textField($newModel,'name',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'name'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'acc_number'); ?>
                    <?php echo $form->textField($newModel,'acc_number',array('maxlength'=>20,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'acc_number'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                    <?php echo $form->labelEx($newModel,'acc_name'); ?>
                    <?php echo $form->textField($newModel,'acc_name',array('maxlength'=>20,'class'=>'md-input','required'=>'required')); ?>
                    <?php echo $form->error($newModel,'acc_name'); ?>
                </div>
                <div class="uk-margin-large-bottom">
                        <?php echo $form->labelEx($newModel,'image'); ?>
                        <?php if(!$newModel->isNewRecord):?>
                        <?php echo ($newModel->image!==NULL)?CHtml::image(Yii::app()->baseUrl.'/'.$newModel->small_url).'<br/>':'';?>
                        <?php endif;?>
                        <input id="upload" name="Bank[image]" type="file" <?php echo ($newModel->isNewRecord OR $newModel->image==null)?'required="required"':'';?>/><span>Select an image to upload. Up to 1 MB. Recommended image size <strong>120px X 36px</strong>.</span>
                    </div>
                
                <div class="uk-modal-footer">
                    <button type="submit" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete-cat', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this bank?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/bank/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/bank')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>