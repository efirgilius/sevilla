    <div id="page_content">
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="md-card-list-wrapper" id="mailbox">
                <div class="uk-width-large-8-10 uk-container-center">
                    <div class="md-card-list">
                        <div class="md-card-list-header md-card-list-header-combined heading_list">All Messages</div>
                        <?php if($model):?>
                        <ul class="hierarchical_slide">
                            <?php foreach($model as $data):?>
                            <li class="click_<?=$data->id;?>" data-id="<?=$data->id;?>">
                                <div class="md-card-list-item-menu" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                    <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <?php if($data->status==0):?><li><a href="#" class="read this_<?=$data->id;?>" data-id="<?=$data->id;?>"><i class="material-icons">&#xE876;</i> Mark as read</a></li><?php endif;?>
                                            <li><a href="#" class="del" data-id="<?=$data->id;?>"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="md-card-list-item-date"><?=Yii::app()->getDateFormatter()->formatDateTime(strtotime($data->created_on));?></span>
                                <div class="md-card-list-item-avatar-wrapper">
                                    <span class="md-card-list-item-avatar md-bg-grey"><?=substr($data->name,0,1);?></span>
                                </div>
                                <div class="md-card-list-item-sender">
                                    <span><?=$data->name;?> <?=($data->status==0)?'<span class=new_'.$data->id.'><b><i>New</i></b></span>':'';?></span>
                                </div>
                                <div class="md-card-list-item-subject">
                                    <div class="md-card-list-item-sender-small">
                                        <span><?=$data->name;?></span>
                                    </div>
                                    <span><?=$data->subject;?></span>
                                </div>
                                <div class="md-card-list-item-content-wrapper">
                                    <div class="md-card-list-item-content">
                                        <?=CHtml::decode($data->message);?>
                                    </div>
                                    <?php if($data->reply_id!=='null'):?>
                                    <div class="form_reply form_reply_<?=$data->id;?>">
                                        <label for="mailbox_reply_<?=$data->id;?>">Reply to <span><?=$data->name;?> [<?=$data->email;?>]</span></label>
                                        <textarea class="md-input md-input-full msg_<?=$data->id;?>" name="mailbox_reply_<?=$data->id;?>" id="mailbox_reply_<?=$data->id;?>" cols="30" rows="4"></textarea>
                                        <a class="md-btn md-btn-flat md-btn-flat-primary send_reply btn_<?=$data->id;?>" data-id="<?=$data->id;?>">Send</a>
                                    </div>
                                    <?php else:?>
                                        <?php $reply= Contact::model()->find('t.reply_id=:id',[':id'=>$data->id]);?>
                                        <label for="mailbox_reply_<?=$data->id;?>">Replied to <span><?=$data->name;?> [<?=$data->email;?>] at <?=$data->created_on;?></span></label>
                                        <?php if($reply):?>
                                        <p><?=$reply->message;?></p>
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                        <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        <?php else:?>
                        <p>Mailbox is empty.</p>
                        <?php endif;?>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('js--contact', "
    
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this message?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/contact/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/contact')."'; 
            }
        });
    return false;
    });

    $('.read').on('click',function(){
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/contact/read')."',
            type: 'GET',
            data: 'id='+id,
            beforeSend:function(){
                $('.this_'+id).html('Loading...');
            },
            success:function(){
                $('.new_'+id).hide();
                $('.this_'+id).hide();
                $('#unread').html($('#unread').html() - 1);
            }
        });
    return false;
    });

    $('.send_reply').on('click',function(){
        var id=$(this).attr('data-id');
        var msg=$('.msg_'+id).val();
        var csrf='".Yii::app()->request->csrfToken."';
        $.ajax({
            url:'".$this->createUrl('apps/contact/reply')."',
            type: 'POST',
            data: 'id='+id+'&msg='+msg+'&YII_CSRF_TOKEN='+csrf,
            dataType:'JSON',
            beforeSend:function(){
                $('.btn_'+id).html('Sending...');
            },
            success:function(data){
                $('.form_reply_'+id).after('<label>Replied to <span>'+data.name+' ['+data.email+'] at '+data.time+'</span></label><p>'+data.message+'</p>');
                $('.form_reply_'+id).hide();
                $('.new_'+id).hide();
                $('#unread').html($('#unread').html() - 1);
            }
        });
    });

",CClientScript::POS_READY);?>