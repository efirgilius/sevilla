<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if($model):?>
    <?php $i=1; foreach($model as $row):?>
            <tr>
                <td><?php echo CHtml::link(CHtml::image($row->member->small_url, 'img'.$row->member->id,array('class'=>'img-polaroid','style'=>'width:42px;max-height:42px;')),array('view','id'=>$row->member->id));?></td>
                <td class="uk-text-large uk-text-nowrap"><?=CHtml::link($row->member->mid,['view','id'=>$row->member->id]);?></td>
                <td><?= $row->member->fullname;?></td>
                <td><?= $row->paytypename;?></td>
                <td><?= $row->created_on;?></td>
                <td><?php echo ($row->status=='0')?'<span class="uk-badge uk-badge-danger">'.$row->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$row->statusname.'</span>';?></td>
                <td class="uk-text-nowrap">
                    <?php if($row->status==0):?><a href="#" class="app" data-id="<?php echo $row->id;?>" title="Approve"><i class="md-icon material-icons">spellcheck</i></a><?php endif;?>
                </td>
            </tr>
    <?php $i++; endforeach;?>
<?php else:?>
            <tr>
                <td colspan="8"><p><?= $this->title;?> is unavailable.</p></td>
            </tr>
<?php endif; ?>
