<div id="page_content">
        <div id="page_heading">
            <h1><?= $title;?> Member</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small"><?= $this->title;?></span>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Member ID</th>
                                            <th>Name</th>
                                            <th>Payment Method</th>
                                            <th>Upgrade Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model,'title'=>$title));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('up-members', "
    
    $('.app').on('click',function(){
        if(!confirm('Are you sure want to APPROVE this upgrade')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/upgrade/approve')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/upgrade',['state'=>$_GET['state']])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);
