<div id="page_content">
        <div id="page_content_inner">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'validation',
                'enableAjaxValidation'=>false,
                'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
            )); ?>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-12-12">
                        <div class="md-card">
                            <div class="user_content">
                                <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                    <li class="uk-active"><a href="#">Change Member Password</a></li>
                                </ul>
                                <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                    <li>
                                        <div class="uk-margin-top">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-1">
                                                    <label for="user_edit_uname_control">New Password</label>
                                                    <?php echo $form->passwordField($model,'newPassword',array('maxlength'=>30,'class'=>'md-input')); ?>
                                                    <?php echo $form->error($model,'newPassword'); ?>
                                                </div>
                                                <div class="uk-width-medium-1-1">
                                                    <button type="submit">Change</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>