<div id="page_content">
        <div id="page_heading">
            <h1><?= $title;?> Member</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small"><?= $title;?></span>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="md-card">
                <div class="md-card-content">
                    <?php echo CHtml::form(array('/apps/member/search'), 'GET',array('class'=>'search'));?>
                    <div class="uk-grid" data-uk-grid-margin="">
                        
                        <div class="uk-width-medium-8-10">
                            <label for="member_search_name">Member Key</label>
                            <?php echo CHtml::textField('q', '', array('class'=>'md-input','id'=>'member_search_name'));?>
                        </div>
                        
                        <div class="uk-width-medium-2-10 uk-text-center">
                            <button type="submit" class="md-btn md-btn-primary uk-margin-small-top">Search</button>
                        </div>
                        
                    </div>
                    <?php echo CHtml::endForm();?>
                </div>
            </div>

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>Member ID</th>
                                            <th>Name</th>
                                            <th>Join Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $this->renderPartial('_view',array('model'=>$model,'title'=>$title));?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if($count>10):?>
                            <div class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete-members', "
    
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to DEACTIVATE this member?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/member/activate')."',
            type: 'GET',
            data: 'id='+id+'&type=deactivate',
            success:function(){
                window.location.href = '".$this->createUrl('apps/member',['state'=>$_GET['state']])."'; 
            }
        });
    return false;
    });
    
    $('.activate').on('click',function(){
        if(!confirm('Are you sure want to ACTIVATE this member')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/member/activate')."',
            type: 'GET',
            data: 'id='+id+'&type=activate',
            success:function(){
                window.location.href = '".$this->createUrl('apps/member',['state'=>$_GET['state']])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);
