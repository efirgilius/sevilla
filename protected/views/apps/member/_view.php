<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if($model):?>
    <?php $i=1; foreach($model as $row):?>
            <tr>
                <td class="uk-text-large uk-text-nowrap"><?=CHtml::link($row->mid,['view','id'=>$row->id]);?></td>
                <td><?= $row->name;?></td>
                <td><?= $row->created;?></td>
                <td><?php echo ($row->status=='0')?'<span class="uk-badge uk-badge-danger">'.$row->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$row->statusname.'</span>';?></td>
                <td class="uk-text-nowrap">
                    <?php if($row->status!=='2'):?><?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$row->id));?><?php endif;?>
                    <?php if($row->status=='0' || $row->status=='2' || $row->status=='3'):?><a href="#" class="activate" data-id="<?php echo $row->id;?>" title="Activate"><i class="md-icon material-icons">spellcheck</i></a><?php endif;?>
                    <?php if($row->status==1):?><a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a><?php endif;?>
                </td>
            </tr>
    <?php $i++; endforeach;?>
<?php else:?>
            <tr>
                <td colspan="8"><p><?= $title;?> Member is unavailable.</p></td>
            </tr>
<?php endif; ?>
