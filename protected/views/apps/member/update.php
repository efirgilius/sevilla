<?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'validation',
                                    'enableAjaxValidation'=>false,
                                    'htmlOptions'=>['class'=>'uk-form-stacked']
                            )); ?>
<div id="page_content">
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-7-10">
                        <div class="md-card">
                            <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                                <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <?= CHtml::image(Yii::app()->baseUrl.'/'.$model->main_url, 'img'.$model->id,array('class'=>'img-polaroid'));?>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="user_edit_avatar_control" id="user_edit_avatar_control">
                                        </span>
                                        <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                    </div>
                                </div>
                                <div class="user_heading_content">
                                    <h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname"><?= $model->name;?></span><span> 
                                </div>
                                
                            </div>
                            <div class="user_content">
                                
                                        <div class="uk-margin-top">
                                            <h3 class="full_width_in_card heading_c">
                                                General info
                                            </h3>
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-1">
                                                    <?php echo $form->labelEx($model,'name'); ?>
                                                    <?php echo $form->textField($model,'name',array('maxlength'=>30,'class'=>'md-input')); ?>
                                                    <?php echo $form->error($model,'name'); ?>
                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <?php echo $form->labelEx($model,'gender'); ?><br>
                                                    <?php echo CHtml::dropDownList('Member[gender]',isset($model->gender)?$model->gender:'', Member::genderList(),['class'=>'md-input','data-md-selectize'=>'']); ?>
                                                    <?php echo $form->error($model,'gender'); ?>
                                                </div>
                                                
                                                <div class="uk-width-medium-1-2">
                                                    <?php echo $form->labelEx($model,'birth_date'); ?>
                                                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                                            'model'=>$model,
                                                                            'attribute'=>'birth_date',
                                                                            // additional javascript options for the date picker plugin
                                                                            'options'=>array(
                                                                                'showAnim'=>'fold',
                                                                                'dateFormat' => 'yy-mm-dd',
                                                                                'yearRange'=>'-70:-17',
                                                                                'changeYear'=>'true',
                                                                                'changeMonth'=>'true',
                                                                            ),
                                                                            'htmlOptions'=>array(
                                                                                'class'=>'md-input','required'=>'required'
                                                                            ),
                                                                        ));?>
                                                    <?php echo $form->error($model,'birth_date'); ?>
                                                </div>
                                                
                                            </div>
                                            
                                            
                                            <h3 class="full_width_in_card heading_c">
                                                Contact info
                                            </h3>
                                            <div class="uk-grid">
                                                <div class="uk-width-1-1">
                                                    
                                                    <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">home</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'address_1'); ?>
                                                                <?php echo $form->textArea($model,'address_1',array('cols'=>30,'rows'=>3,'maxlength'=>330,'data-parsley-trigger'=>'keyup','class'=>'md-input','required'=>'required','id'=>'editor1'));?>
                                                                <?php echo $form->error($model,'address_1'); ?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'email'); ?>
                                                                <?php echo $form->emailField($model,'email',array('maxlength'=>50,'class'=>'md-input','required'=>'required')); ?>
                                                                <?php echo $form->error($model,'email'); ?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'mobile_phone'); ?>
                                                                <?php echo $form->textField($model,'mobile_phone',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                                                                <?php echo $form->error($model,'mobile_phone'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="full_width_in_card heading_c">
                                                Bank info
                                            </h3>
                                                <div class="uk-width-1-1">
                                                    <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">account_balance</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'bank_name'); ?><br>
                                                                <?php echo $form->dropDownList($model,'bank_name',  Member::bankList(),array(
                                                                                                    'class'=>'md-input','data-md-selectize'=>1,
                                                                                                    'required'=>'required',
                                                                                                    )); ?>
                                                                <?php echo $form->error($model,'bank_name'); ?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">account_balance_wallet</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'bank_acc_number'); ?>
                                                                <?php echo $form->textField($model,'bank_acc_number',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                                                                <?php echo $form->error($model,'bank_acc_number'); ?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">account_balance_wallet</i>
                                                                </span>
                                                                <?php echo $form->labelEx($model,'bank_acc_name'); ?>
                                                                <?php echo $form->textField($model,'bank_acc_name',array('maxlength'=>100,'class'=>'md-input','required'=>'required')); ?>
                                                                <?php echo $form->error($model,'bank_acc_name'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-3-10">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_c uk-margin-medium-bottom">Other settings</h3>
                                <div class="uk-form-row">
                                    <?=$form->checkBox($model,'status',['data-switchery'=>'']);?>
                                    <label for="Member_status" class="inline-label">User Active</label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

            

        </div>
    </div>
    <div class="md-fab-wrapper">
        <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE161;</i></button>
        <?=CHtml::link('<i class="material-icons">undo</i>','javascript: history.go(-1)',['class'=>'md-fab md-fab-warning']);?>
    </div>
    <?php $this->endWidget(); ?>