<?php
/* @var $this MembersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Change Bank Account Request',
);
?>

<div class="workplace">
                <div class="page-header">
                    <h1>Change Bank Account Request <small>List</small></h1>
                </div>
                <?php Notify::renderFlash();?>
                
                <div class="row-fluid">

                    <div class="span12">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>Change Bank Account Request</h1>             
                        </div>
                        <div class="block-fluid">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table listUsers">
                                <tbody>
                                    <?php $this->renderPartial('_viewBank',array('model'=>$model,'i'=>$i));?>                              
                                </tbody>
                            </table>   
                            <?php if($count>10):?>
                            <div class="pagination right">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                            <?php endif;?>
                        </div>
                        
                    </div>           

                </div>            

                <div class="dr"><span></span></div>  
            </div>

<?php Yii::app()->clientScript->registerScript('delete-members', "
    $('.app').live('click',function(){
        if(!confirm('Are you sure want to APPROVE this request?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/member/apprequest')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/member/bankaccount')."'; 
            }
        });
    return false;
    });
    
    $('.del').live('click',function(){
        if(!confirm('Are you sure want to REJECT this request??')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/member/rejectrequest')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/member/bankaccount')."'; 
            }
        });
    return false;
    });
    
",CClientScript::POS_READY);?>
