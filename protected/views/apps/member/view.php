<div id="page_content">
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-10-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_menu" data-uk-dropdown>
                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-flip uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><?=CHtml::link('Change Password',['updatepassword','id'=>$model->id]);?></li>
                                        <li><a href="#">Deactivated</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="user_heading_avatar">
                                <?= CHtml::image(Yii::app()->baseUrl.'/'.$model->main_url, 'img'.$model->id,array('class'=>'img-polaroid'));?>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?= $model->name;?> [<?= $model->mid;?>]</span></h2>
                                <ul class="user_stats">
                                   
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">About</a></li>
                                <li><a href="#">Genealogy Sponsor</a></li>
                                <li><a href="#">Genealogy Jaringan</a></li>
                                <li><a href="#">Bonus Sponsor</a></li>
                                <li><a href="#">Bonus Pasangan</a></li>
                                <li><a href="#">Reward</a></li>
                            </ul>
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Member Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">payment</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->identity_number;?></span>
                                                        <span class="uk-text-small uk-text-muted">Identity Number</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE63D;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->gendername;?></span>
                                                        <span class="uk-text-small uk-text-muted">Gender</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">local_hospital</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->birth_place;?>, <?= $model->birth_date;?></span>
                                                        <span class="uk-text-small uk-text-muted">Birth place and date</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">home</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->address_1;?></span>
                                                        <span class="uk-text-small uk-text-muted">Address</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">home</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->address_2;?></span>
                                                        <span class="uk-text-small uk-text-muted">Mailing Address</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->email;?></span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">smartphone</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->mobile_phone;?></span>
                                                        <span class="uk-text-small uk-text-muted">Mobile Phone</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->phone;?></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <hr>
                                            <h4 class="heading_c uk-margin-small-bottom">Work Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">work</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->work;?></span>
                                                        <span class="uk-text-small uk-text-muted">Work</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE916;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->work_time;?></span>
                                                        <span class="uk-text-small uk-text-muted">Work Time</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">home</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->work_address;?></span>
                                                        <span class="uk-text-small uk-text-muted">Work Address</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">phone</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->work_phone;?></span>
                                                        <span class="uk-text-small uk-text-muted">Work Phone</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <h4 class="heading_c uk-margin-small-bottom">Bank Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">account_balance</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->bank_name;?></span>
                                                        <span class="uk-text-small uk-text-muted">Bank name</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">account_balance_wallet</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->bank_acc_number;?></span>
                                                        <span class="uk-text-small uk-text-muted">Account number</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">account_box</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->bank_acc_name;?></span>
                                                        <span class="uk-text-small uk-text-muted">Account name</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            
                                            <h4 class="heading_c uk-margin-small-bottom">Family Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">account_box</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->family_name;?></span>
                                                        <span class="uk-text-small uk-text-muted">Name</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">home</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->family_address;?></span>
                                                        <span class="uk-text-small uk-text-muted">Address</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">phone</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->family_phone;?></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <hr>
                                            <h4 class="heading_c uk-margin-small-bottom">Heir Info</h4>
                                            <h5>Heir 1</h5>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">account_box</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->heir_name_1;?></span>
                                                        <span class="uk-text-small uk-text-muted">Name</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE040;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->heir_relationship_as_1;?></span>
                                                        <span class="uk-text-small uk-text-muted">Relationship</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">local_hospital</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->heir_birth_place_1;?>, <?= $model->heir_birth_date_1;?></span>
                                                        <span class="uk-text-small uk-text-muted">Birth place and date</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            
                                        </div>
                                    </div>
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Membership Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE88E;</i>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE88E;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= $model->statusname;?></span>
                                                        <span class="uk-text-small uk-text-muted">Status</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE906;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?= ($model->sponsor_id!==NULL)?CHtml::link($model->sponsor->name.' ['.$model->sponsor->mid.']',['view','id'=>$model->sponsor_id]):'-';?></span>
                                                        <span class="uk-text-small uk-text-muted">Sponsor</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE335;</i>
                                                    </div>
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-bottom">Timeline</h4>
                                            <div class="timeline">
                                                <div class="timeline_item">
                                                    <div class="timeline_icon timeline_icon_success"><i class="material-icons">access_time</i></div>
                                                    <div class="timeline_date">
                                                        <span><?= $model->last_login;?></span>
                                                    </div>
                                                    <div class="timeline_content">Last login</div>
                                                </div>
                                                <div class="timeline_item">
                                                    <div class="timeline_icon timeline_icon_primary"><i class="material-icons">alarm_add</i></div>
                                                    <div class="timeline_date">
                                                        <span><?= $model->created;?></span>
                                                    </div>
                                                    <div class="timeline_content">Register on</div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
                                </li>
                                <li>
                                    
                                </li>
                                <li>
                                    <div id="tree-diagram" class="clearfix">
								<div class="treewrap clearfix">
									<div class="lev-1">
										<div class="box-user">
											<div class="fig-user">
                                                                                            <?php if ($model->id===Yii::app()->member->id):?>
                                                                                                              <?php echo CHtml::image(Yii::app()->baseUrl.'/'.$model->avatar_url, $model->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto'));?>
                                                                                                    <?php else:?>
                                                                                                              <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model->avatar_url, $model->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model->id),array('title'=>'Upline '.$model->name));?>
                                                                                                    <?php endif?>
												<div class="desc-user">
                                                                                                    
													<span class="name-s">
                                                                                                            <?php if ($model->id===Yii::app()->member->id):?>
                                                                                                                <?php echo $model->mid;?>
                                                                                                            <?php else:?>
                                                                                                                <?php echo CHtml::link($model->mid, array('/apps/member/view','id'=>$model->id),array('title'=>'Upline '.$model->name));?>
                                                                                                            <?php endif;?>
                                                                                                        </span>
													<span class="desc-s"><?php echo substr($model->name,0,10);?></span>
                                                                                                        <span class="desc-s">(<?= $model->typename;?>)</span>
													
												</div>
											</div>
										</div>
                                                                            <div class="vLine">
                                                                                <div class="box-center">
                                                                                    <div class="pull-left">
                                                                                        <?php echo $model->total_left;?>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <?php echo $model->total_right;?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
										<div class="hLine"></div> <!-- end Level 1 -->
										
										<div class="lev-2 fl">
											<div class="box-user">
												<div class="fig-user">
                                                                                                    <?php if ($model->foot_left!==NULL):?>
                                                                                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model2_l->avatar_url, $model2_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model2_l->id),array('title'=>'Upline '.$model2_l->name));?>
                                                                                                    <?php endif;?>
													<div class="desc-user">
                                                                                                            <?php if ($model->foot_left!==NULL):?>
														<span class="name-s"><?php echo CHtml::link($model2_l->mid, array('/apps/member/view','id'=>$model2_l->id),array('title'=>'Upline '.$model2_l->name));?></span>
														<span class="desc-s"><?php echo substr($model2_l->name,0,10);?></span>
                                                                                                                <span class="desc-s">(<?= $model2_l->typename;?>)</span>
                                                                                                            <?php else:?>
                                                                                                                <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model->id));?>
                                                                                                            <?php endif;?>
													</div>
												</div>
											</div>
                                                                                    <div class="vLine">
                                                                                        <?php if ($model->foot_left!==NULL):?>
                                                                                        <div class="box-center">
                                                                                            <div class="pull-left">
                                                                                                <?php echo $model2_l->total_left;?>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <?php echo $model2_l->total_right;?>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php endif;?>
                                                                                    </div>
											<div class="hLine"></div>
											<div class="treewrap clearfix">
												<div class="lev-3 fl">
													<div class="box-user">
														<div class="fig-user">
                                                                                                                    <?php if(!empty($model2_l)):?>
                                                                                                                        <?php if($model3_2l_l):?>
                                                                                                                        <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2l_l->avatar_url, $model3_2l_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model3_2l_l->id),array('title'=>'Upline '.$model3_2l_l->name));?>
                                                                                                                        <?php endif;?>
                                                                                                                    <?php endif;?>
															<div class="desc-user">
                                                                                                                        <?php if(!empty($model2_l)):?>
                                                                                                                            <?php if($model3_2l_l):?>
                                                                                                                                <span class="name-s"><?php echo CHtml::link($model3_2l_l->mid, array('/apps/member/view','id'=>$model3_2l_l->id),array('title'=>'Upline '.$model3_2l_l->name));?></span>
                                                                                                                                <span class="desc-s"><?php echo substr($model3_2l_l->name,0,10);?></span>
                                                                                                                                <span class="desc-s">(<?= $model3_2l_l->typename;?>)</span>
                                                                                                                            <?php else:?>
                                                                                                                                <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model2_l->id));?>
                                                                                                                            <?php endif;?>
                                                                                                                        <?php endif;?>
															</div>
														</div>
													</div>
                                                                                                    <div class="vLine">
                                                                                                        <?php if(!empty($model2_l)):?>
                                                                                                        <?php if($model3_2l_l):?>
                                                                                                        <div class="box-center">
                                                                                                            <div class="pull-left">
                                                                                                                <?php echo $model3_2l_l->total_left;?>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <?php echo $model3_2l_l->total_right;?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php endif;?>
                                                                                                        <?php endif;?>
                                                                                                    </div>
													<div class="hLine"></div>
													<div class="treewrap clearfix">
														<div class="lev-4 fl">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2l_l)):?>
                                                                                                                                        <?php if($model4_32ll_l):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32ll_l->avatar_url, $model4_32ll_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32ll_l->id),array('title'=>'Upline '.$model4_32ll_l->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2l_l)):?>
                                                                                                                                        <?php if($model4_32ll_l):?>      
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32ll_l->mid, array('/apps/member/view','id'=>$model4_32ll_l->id),array('title'=>'Upline '.$model4_32ll_l->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32ll_l->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32ll_l->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model3_2l_l->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2l_l)):?>
                                                                                                                        <?php if($model4_32ll_l):?>  
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32ll_l->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32ll_l->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
														<div class="lev-4 fr">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2l_l)):?>
                                                                                                                                        <?php if($model4_32ll_r):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32ll_r->avatar_url, $model4_32ll_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32ll_r->id),array('title'=>'Upline '.$model4_32ll_r->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2l_l)):?>
                                                                                                                                        <?php if($model4_32ll_r):?>         
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32ll_r->mid, array('/apps/member/view','id'=>$model4_32ll_r->id),array('title'=>'Upline '.$model4_32ll_r->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32ll_r->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32ll_r->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model3_2l_l->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2l_l)):?>
                                                                                                                        <?php if($model4_32ll_r):?>       
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32ll_r->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32ll_r->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
													</div>
												</div><!-- end Level 3 -->
												
												<div class="lev-3 fr">
													<div class="box-user">
														<div class="fig-user">
                                                                                                                    <?php if(!empty($model2_l)):?>
                                                                                                                            <?php if($model3_2l_r):?>
                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2l_r->avatar_url, $model3_2l_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model3_2l_r->id),array('title'=>'Upline '.$model3_2l_r->name));?>
                                                                                                                    <?php endif;?>
                                                                                                                        <?php endif;?>
															<div class="desc-user">
                                                                                                                        <?php if(!empty($model2_l)):?>
                                                                                                                            <?php if($model3_2l_r):?>
                                                                                                                                <span class="name-s"><?php echo CHtml::link($model3_2l_r->mid, array('/apps/member/view','id'=>$model3_2l_r->id),array('title'=>'Upline '.$model3_2l_r->name));?></span>
                                                                                                                                <span class="desc-s"><?php echo substr($model3_2l_r->name,0,10);?></span>
                                                                                                                                <span class="desc-s">(<?= $model3_2l_r->typename;?>)</span>
                                                                                                                            <?php else:?>
                                                                                                                                <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model2_l->id));?>
                                                                                                                            <?php endif;?>
                                                                                                                        <?php endif;?>
															</div>
														</div>
													</div>
                                                                                                    <div class="vLine">
                                                                                                        <?php if(!empty($model2_l)):?>
                                                                                                        <?php if($model3_2l_r):?>
                                                                                                        <div class="box-center">
                                                                                                            <div class="pull-left">
                                                                                                                <?php echo $model3_2l_r->total_left;?>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <?php echo $model3_2l_r->total_right;?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php endif;?>
                                                                                                        <?php endif;?>
                                                                                                    </div>
													<div class="hLine"></div>
													<div class="treewrap clearfix">
														<div class="lev-4 fl">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2l_r)):?>
                                                                                                                                        <?php if($model4_32lr_l):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32lr_l->avatar_url, $model4_32lr_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32lr_l->id),array('title'=>'Upline '.$model4_32lr_l->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2l_r)):?>
                                                                                                                                        <?php if($model4_32lr_l):?>      
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32lr_l->mid, array('/apps/member/view','id'=>$model4_32lr_l->id),array('title'=>'Upline '.$model4_32lr_l->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32lr_l->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32lr_l->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model3_2l_r->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2l_r)):?>
                                                                                                                        <?php if($model4_32lr_l):?>
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32lr_l->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32lr_l->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
														<div class="lev-4 fr">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2l_r)):?>
                                                                                                                                        <?php if($model4_32lr_r):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32lr_r->avatar_url, $model4_32lr_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32lr_r->id),array('title'=>'Upline '.$model4_32lr_r->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2l_r)):?>
                                                                                                                                        <?php if($model4_32lr_r):?>     
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32lr_r->mid, array('/apps/member/view','id'=>$model4_32lr_r->id),array('title'=>'Upline '.$model4_32lr_r->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32lr_r->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32lr_r->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model3_2l_r->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2l_r)):?>
                                                                                                                        <?php if($model4_32lr_r):?>  
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32lr_r->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32lr_r->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
													</div>
												</div><!-- end Level 3 -->
											</div>
										</div><!-- end Level 2 -->
										
										<div class="lev-2 fr">
											<div class="box-user">
												<div class="fig-user">
                                                                                                    <?php if ($model->foot_right !== NULL):?>
                                                                                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model2_r->avatar_url, $model2_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model2_r->id),array('title'=>'Upline '.$model2_r->name));?>
                                                                                                    <?php endif;?>
													<div class="desc-user">
                                                                                                        <?php if ($model->foot_right !== NULL):?>
                                                                                                            <span class="name-s"><?php echo CHtml::link($model2_r->mid, array('/apps/member/view','id'=>$model2_r->id),array('title'=>'Upline '.$model2_r->name));?></span>
                                                                                                            <span class="desc-s"><?php echo substr($model2_r->name,0,10);?></span>
                                                                                                            <span class="desc-s">(<?= $model2_r->typename;?>)</span>
                                                                                                        <?php else:?>
                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model->id));?>
                                                                                                        <?php endif;?>
													</div>
												</div>
											</div>
                                                                                    <div class="vLine">
                                                                                        <?php if ($model->foot_right !== NULL):?>
                                                                                        <div class="box-center">
                                                                                            <div class="pull-left">
                                                                                                <?php echo $model2_r->total_left;?>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <?php echo $model2_r->total_right;?>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php endif;?>
                                                                                    </div>
											<div class="hLine"></div>
											
											<div class="treewrap clearfix">
												<div class="lev-3 fl">
													<div class="box-user">
														<div class="fig-user">
                                                                                                                    <?php if(!empty($model2_r)):?>
                                                                                                                            <?php if($model3_2r_l):?>
                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2r_l->avatar_url, $model3_2r_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model3_2r_l->id),array('title'=>'Upline '.$model3_2r_l->name));?>
                                                                                                                    <?php endif;?>
                                                                                                                        <?php endif;?>
															<div class="desc-user">
                                                                                                                        <?php if(!empty($model2_r)):?>
                                                                                                                            <?php if($model3_2r_l):?>
                                                                                                                                <span class="name-s"><?php echo CHtml::link($model3_2r_l->mid, array('/apps/member/view','id'=>$model3_2r_l->id),array('title'=>'Upline '.$model3_2r_l->name));?></span>
                                                                                                                                <span class="desc-s"><?php echo substr($model3_2r_l->name,0,10);?></span>
                                                                                                                                <span class="desc-s">(<?= $model3_2r_l->typename;?>)</span>
                                                                                                                            <?php else:?>
                                                                                                                                <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model2_r->id));?>
                                                                                                                            <?php endif;?>
                                                                                                                        <?php endif;?>
															</div>
														</div>
													</div>
                                                                                                    <div class="vLine">
                                                                                                        <?php if(!empty($model2_r)):?>
                                                                                                        <?php if($model3_2r_l):?>
                                                                                                        <div class="box-center">
                                                                                                            <div class="pull-left">
                                                                                                                <?php echo $model3_2r_l->total_left;?>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <?php echo $model3_2r_l->total_right;?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php endif;?>
                                                                                                        <?php endif;?>
                                                                                                    </div>
													<div class="hLine"></div>
													<div class="treewrap clearfix">
														<div class="lev-4 fl">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2r_l)):?>
                                                                                                                                        <?php if($model4_32rl_l):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rl_l->avatar_url, $model4_32rl_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32rl_l->id),array('title'=>'Upline '.$model4_32rl_l->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2r_l)):?>
                                                                                                                                        <?php if($model4_32rl_l):?>       
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32rl_l->mid, array('/apps/member/view','id'=>$model4_32rl_l->id),array('title'=>'Upline '.$model4_32rl_l->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32rl_l->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32rl_l->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model3_2r_l->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2r_l)):?>
                                                                                                                        <?php if($model4_32rl_l):?>   
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32rl_l->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32rl_l->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
														<div class="lev-4 fr">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2r_l)):?>
                                                                                                                                        <?php if($model4_32rl_r):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rl_r->avatar_url, $model4_32rl_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32rl_r->id),array('title'=>'Upline '.$model4_32rl_r->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2r_l)):?>
                                                                                                                                        <?php if($model4_32rl_r):?>    
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32rl_r->mid, array('/apps/member/view','id'=>$model4_32rl_r->id),array('title'=>'Upline '.$model4_32rl_r->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32rl_r->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32rl_r->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model3_2r_l->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2r_l)):?>
                                                                                                                        <?php if($model4_32rl_r):?>     
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32rl_r->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32rl_r->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
													</div>
												</div>
												
												<div class="lev-3 fr">
													<div class="box-user">
														<div class="fig-user">
                                                                                                                    <?php if(!empty($model2_r)):?>
                                                                                                                            <?php if($model3_2r_r):?>
                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2r_r->avatar_url, $model3_2r_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model3_2r_r->id),array('title'=>'Upline '.$model3_2r_r->name));?>
                                                                                                                    <?php endif;?>
                                                                                                                        <?php endif;?>
															<div class="desc-user">
                                                                                                                        <?php if(!empty($model2_r)):?>
                                                                                                                            <?php if($model3_2r_r):?>
                                                                                                                                <span class="name-s"><?php echo CHtml::link($model3_2r_r->mid, array('/apps/member/view','id'=>$model3_2r_r->id),array('title'=>'Upline '.$model3_2r_r->name));?></span>
                                                                                                                                <span class="desc-s"><?php echo substr($model3_2r_r->name,0,10);?></span>
                                                                                                                                <span class="desc-s">(<?= $model3_2r_r->typename;?>)</span>
                                                                                                                            <?php else:?>
                                                                                                                                <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model2_r->id));?>
                                                                                                                            <?php endif;?>
                                                                                                                        <?php endif;?>
															</div>
														</div>
													</div>
                                                                                                    <div class="vLine">
                                                                                                        <?php if(!empty($model2_r)):?>
                                                                                                        <?php if($model3_2r_r):?>
                                                                                                        <div class="box-center">
                                                                                                            <div class="pull-left">
                                                                                                                <?php echo $model3_2r_r->total_left;?>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <?php echo $model3_2r_r->total_right;?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php endif;?>
                                                                                                        <?php endif;?>
                                                                                                    </div>
													<div class="hLine"></div>
													<div class="treewrap clearfix">
														<div class="lev-4 fl">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2r_r)):?>
                                                                                                                                        <?php if($model4_32rr_l):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rr_l->avatar_url, $model4_32rr_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32rr_l->id),array('title'=>'Upline '.$model4_32rr_l->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2r_r)):?>
                                                                                                                                        <?php if($model4_32rr_l):?>        
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32rr_l->mid, array('/apps/member/view','id'=>$model4_32rr_l->id),array('title'=>'Upline '.$model4_32rr_l->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32rr_l->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32rr_l->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'l','upline'=>$model3_2r_r->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                    <?php if(!empty($model3_2r_r)):?>
                                                                                                                    <?php if($model4_32rr_l):?>      
                                                                                                                    <div class="box-center">
                                                                                                                        <div class="pull-left">
                                                                                                                            <?php echo $model4_32rr_l->total_left;?>
                                                                                                                        </div>
                                                                                                                        <div class="pull-right">
                                                                                                                            <?php echo $model4_32rr_l->total_right;?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <?php endif;?>
                                                                                                                    <?php endif;?>
														</div><!-- end Level 4 -->
														<div class="lev-4 fr">
															<div class="box-user">
																<div class="fig-user">
                                                                                                                                    <?php if(!empty($model3_2r_r)):?>
                                                                                                                                        <?php if($model4_32rr_r):?>                                                                         
                                                                                                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rr_r->avatar_url, $model4_32rr_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('/apps/member/view','id'=>$model4_32rr_r->id),array('title'=>'Upline '.$model4_32rr_r->name));?>
                                                                                                                                    <?php endif;?>
                                                                                                                                        <?php endif;?>
																	<div class="desc-user">
                                                                                                                                        <?php if(!empty($model3_2r_r)):?>
                                                                                                                                        <?php if($model4_32rr_r):?>  
                                                                                                                                            <span class="name-s"><?php echo CHtml::link($model4_32rr_r->mid, array('/apps/member/view','id'=>$model4_32rr_r->id),array('title'=>'Upline '.$model4_32rr_r->name));?></span>
                                                                                                                                            <span class="desc-s"><?php echo substr($model4_32rr_r->name,0,10);?></span>
                                                                                                                                            <span class="desc-s">(<?= $model4_32rr_r->typename;?>)</span>
                                                                                                                                        <?php else:?>
                                                                                                                                            <?php echo CHtml::link('', array('member/register','position'=>'r','upline'=>$model3_2r_r->id));?>
                                                                                                                                        <?php endif;?>
                                                                                                                                        <?php endif;?>
																	</div>
																</div>
															</div>
                                                                                                                        <?php if(!empty($model3_2r_r)):?>
                                                                                                                        <?php if($model4_32rr_r):?> 
                                                                                                                        <div class="box-center">
                                                                                                                            <div class="pull-left">
                                                                                                                                <?php echo $model4_32rr_r->total_left;?>
                                                                                                                            </div>
                                                                                                                            <div class="pull-right">
                                                                                                                                <?php echo $model4_32rr_r->total_right;?>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <?php endif;?>
                                                                                                                        <?php endif;?>
														</div><!-- end Level 4 -->
													</div>
												</div>
											</div>
										</div><!-- end Level 2 -->
										
									</div>
								</div>
							</div>
                                </li>
                                <li>
                                    <?php if($model->bonusSponsors):?>
                                    <ul class="md-list">
                                        <?php foreach($model->bonusSponsors as $inv):?>
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><?= $inv->from->mid;?></span>
                                                <div class="uk-margin-small-top">
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small"><?= $inv->date;?></span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE88E;</i> <span class="uk-text-muted uk-text-small"><?= $inv->bonus;?></span>
                                                </span>
                                                </div>
                                            </div>
                                        </li>
                                        <?php endforeach;?>
                                    </ul>
                                    <?php else:?>
                                    <p>Bonus sponsor is empty.</p>
                                    <?php endif;?>
                                </li>
                                <li>
                                    <?php if($model->bonusPairings):?>
                                    <ul class="md-list">
                                        <?php foreach($model->bonusPairings as $inv):?>
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><?= $inv->couple;?> pairing(s)</span>
                                                <div class="uk-margin-small-top">
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small"><?= $inv->date;?></span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE88E;</i> <span class="uk-text-muted uk-text-small"><?= $inv->bonus;?></span>
                                                </span>
                                                </div>
                                            </div>
                                        </li>
                                        <?php endforeach;?>
                                    </ul>
                                    <?php else:?>
                                    <p>Bonus pasangan tidak tersedia.</p>
                                    
                                    <?php endif;?>
                                </li>
                                <li>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<div class="md-fab-wrapper">
        <?=CHtml::link('<i class="material-icons">list</i>',['/apps/member','state'=>($model->status==0)?'inactive':'active'],['class'=>'md-fab md-fab-primary']);?>
        <?=CHtml::link('<i class="material-icons">&#xE0DA;</i>',['login','id'=>$model->id],['class'=>'md-fab md-fab-warning','target'=>'_blank']);?>
        <?=CHtml::link('<i class="material-icons">&#xE150;</i>',['update','id'=>$model->id],['class'=>'md-fab md-fab-success']);?>
        <?php if($model->status==0 || $model->status==1):?>
        <?=CHtml::link(($model->status==0)?'<i class="material-icons">&#xE86C;</i>':'<i class="material-icons">&#xE15D;</i>','javascript:void(0);',['class'=>'md-fab md-fab-danger del','data-id'=>$model->id]);?>
        <?php endif;?>
    </div>
<?php if($model->status<='1') {
    $v=($model->status>0)?'DEACTIVATE':'ACTIVATE';
    Yii::app()->clientScript->registerScript('delete-member', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to ".$v." this member?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/member/activate',['id'=>$model->id,'type'=>($model->status==0)?'activate':'deactivate'])."',
            type: 'GET',
            success:function(){
                window.location.href = '".$this->createUrl('apps/member',['state'=>($model->status==0)?'inactive':'active'])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);}?>