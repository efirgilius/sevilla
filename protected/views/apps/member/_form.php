<?php
/* @var $this MembersController */
/* @var $model Member */
/* @var $form CActiveForm */
?>
<style>
    label {display: inline !important;}
</style>
<?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'validation',
                                'enableAjaxValidation'=>false,
                        )); ?>
                        <?php echo $form->errorSummary($model);?>
                   <div class="span11">
                       <?php if(!$model->isNewRecord):?>
                        <div class="ushort clearfix">
                            <?php echo CHtml::link($model->name,array('view','id'=>$model->id));?>
                                                        <a href="#"><?php echo CHtml::image(Yii::app()->baseUrl.'/'.$model->main_url, 'img'.$model->id,array('class'=>'img-polaroid'));?></a>
                        </div> 
                       <?php endif;?>

                        <div class="block-fluid without-head">
                            <div class="toolbar nopadding-toolbar clearfix">
                                <h4>Member Form</h4>
                            </div>    
                            
                            <div class="row-form clearfix">
                                <div class="span3">Membership</div>
                                <div class="span9"><?php $tp = array('0'=>'&nbsp;Silver (35PV)','1'=>'&nbsp;Gold (100PV)', '2'=>'&nbsp;Platinum (300PV)', '3'=>'&nbsp;Diamond (700PV)');
                        echo $form->radioButtonList($model,'type',$tp,array('uncheckValue' => '','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','class'=>'type','required'=>'required'));?></div>
                            </div>
                            <?php if($model->isNewRecord):?>
                            <div class="row-form clearfix">
                                <div class="span3">Sponsor*</div>
                                <div class="span9">
                                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                        'model'=>$model,
                                        'attribute'=>'id_member',
                                        'source'=>$this->createUrl('loadupmember',['up'=>$_GET['upline']]),
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                            'select'=>"js:function(event, ui) {
                                                                              $(this).val(ui.item.value);
                                                                              $('#Member_sponsor_mid').val(ui.item.mid);
                                                                            }"
                                            
                                        ),
                                        'cssFile'=>false,
                                        'htmlOptions'=>array('class'=>'form-control  form-control--contact validate[required]','placeholder'=>'Type Sponsor ID','required'=>'required')
                                    ));?>
                                    <input type="hidden" value="" name="Member[sponsor_mid]" id="Member_sponsor_mid" placeholder="" required="required">
                        <?php echo $form->error($model,'sponsor_mid'); ?>
                                </div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Upline</div>
                                <div class="span9"><?= $upId;?></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Position</div>
                                <div class="span9"><?= $pos;?></div>
                            </div>
                            <?php endif;?>
                            <div class="row-form clearfix">
                                <div class="span3">Email*</div>
                                <div class="span9"><?php echo $form->emailField($model,'email',array('maxlength'=>50,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'email'); ?></span></div>
                            </div>
                            <?php if($model->isNewRecord):?>
                            <div class="row-form clearfix">
                                <div class="span3">Password*</div>
                                <div class="span9"><?php echo $form->passwordField($model,'password',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'password'); ?></span></div>
                            </div>
                            
                            <div class="row-form clearfix">
                                <div class="span3">Re-Password*</div>
                                <div class="span9"><?php echo $form->passwordField($model,'repassword',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'repassword'); ?></span></div>
                            </div>
                            <?php endif;?>
                            
                            <div class="row-form clearfix">
                                <div class="span3">Name*</div>
                                <div class="span9"><?php echo $form->textField($model,'name',array('maxlength'=>50,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'name'); ?></span></div>
                            </div>
                            
                            <div class="row-form clearfix">
                                <div class="span3">Gender*</div>
                                <div class="span9"><?php $sex = array('1'=>'Male', '0'=>'Female');
                                echo $form->radioButtonList($model,'gender',$sex,array('uncheckValue' => '','separator'=>'&nbsp;&nbsp;','class'=>'gender','required'=>'required'));?></div>
                            </div> 
                            
                            
                                            
                            <div class="row-form clearfix">
                                <div class="span3">Address*</div>
                                <div class="span9"><?php echo $form->textArea($model,'address',array('rows'=>3, 'cols'=>30,'class'=>'validate[required]')); ?><span><?php echo $form->error($model,'address'); ?></span></div>
                            </div>                       
                            
                            <div class="row-form clearfix">
                                <div class="span3">Region*</div>
                                <div class="span9"><?php echo $form->textField($model,'region',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'region'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">City</div>
                                <div class="span9"><?php echo $form->textField($model,'city',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'city'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Post Code*</div>
                                <div class="span9"><?php echo $form->textField($model,'post_code',array('size'=>5,'maxlength'=>5,'class'=>'validate[required]','required'=>'required')); ?></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Phone*</div>
                                <div class="span9"><?php echo $form->telField($model,'mobile_phone',array('maxlength'=>15,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'mobile_phone'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Bank Name*</div>
                                <div class="span9"><?php echo $form->textField($model,'bank',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'bank'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Account Number*</div>
                                <div class="span9"><?php echo $form->numberField($model,'account_number',array('maxlength'=>20,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'account_number'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Account Name*</div>
                                <div class="span9"><?php echo $form->textField($model,'account_name',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'account_name'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Heir Name*</div>
                                <div class="span9"><?php echo $form->textField($model,'heir_name',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'heir_name'); ?></span></div>
                            </div>
                            <div class="row-form clearfix">
                                <div class="span3">Heir Relation*</div>
                                <div class="span9"><?php echo $form->textField($model,'heir_relation',array('maxlength'=>30,'class'=>'validate[required]','required'=>'required')); ?><span><?php echo $form->error($model,'heir_relation'); ?></span></div>
                            </div>
                            
                            <?php if(!$model->isNewRecord):?>
                            <div class="row-form clearfix">
                                <div class="span3">Sponsor*</div>
                                <div class="span9">
                                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                        'model'=>$model,
                                        'attribute'=>'id_sponsor',
                                        'source'=>$this->createUrl('loadallmember',['up'=>$model->upline_id]),
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                            'select'=>"js:function(event, ui) {
                                                                              $(this).val(ui.item.value);
                                                                              $('#Member_sponsor_id').val(ui.item.id);
                                                                            }"
                                            
                                        ),
                                        'cssFile'=>false,
                                        'htmlOptions'=>array('class'=>'form-control  form-control--contact validate[required]','placeholder'=>'Type Sponsor ID','required'=>'required')
                                    ));?>
                                    <input type="hidden" value="" name="Member[sponsor_id]" id="Member_sponsor_id" placeholder="" required="required">
                        <?php echo $form->error($model,'sponsor_mid'); ?>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="footer tar">
                                <?php echo CHtml::link('Cancel','javascript: history.go(-1)',array('class'=>'btn btn-danger'));?>&nbsp;&nbsp;&nbsp;<?php echo CHtml::submitButton('Save',array('class'=>'btn')); ?>
                            </div>
                        </div>                    
                    </div>
<?php $this->endWidget(); ?>