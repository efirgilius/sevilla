<?php
/* @var $this MembersController */
/* @var $data Member */
?>

<?php if($model):?>
                                 <?php foreach($model as $row):?>
                                    <tr>                                    
                                        <td width="60">
                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$row->member->main_url, 'img'.$row->id,array('class'=>'img-polaroid','style'=>'width:42px;max-height:42px;')),array('view','id'=>$row->id));?>
                                            
                                        </td>
                                        <td>
                                            <?php echo CHtml::link($row->member->mid.' - '.$row->member->name,array('view','id'=>$row->member->id),array('class'=>'user'));?>
                                            <p class="about">
                                                <span class="icon-home" title="Name"></span> <?php echo $row->bank;?> <br/>
                                                <span class="icon-th-list" title="Email"></span> <?php echo $row->account_number;?> <br/>
                                                <span class="icon-user" title="Phone"></span> <?php echo $row->account_name;?>
                                            </p>
                                        </td>
                                        <td width="320">
                                            <p class="info">
                                                <a href="javascript:void(0);" class="app" data-id="<?php echo $row->id;?>"><span class="icon-ok"></span> <span class="green">Approve?</span></a><br/>
                                                <a href="javascript:void(0);" class="del" data-id="<?php echo $row->id;?>"><span class="icon-remove"></span> <span class="red">Reject?</span></a><br/><br>
                                                <span class="icon-calendar"></span> <span class="blue"><?php echo $row->posted;?></span>
                                            </p>
                                        </td>
                                    </tr>
                                 <?php $i++; endforeach;?>
                                 <?php else:?>
                                    <tr>
                                        <td colspan="6">Change account request is unavailable.</td>
                                    </tr>
                                 <?php endif;?>