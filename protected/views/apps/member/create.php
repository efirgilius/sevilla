<?php
/* @var $this NewsController */
/* @var $model Content */

$this->breadcrumbs=array(
	'Member'=>array('index'),
	'Create',
);
?>

<div class="workplace">
                <div class="page-header">
                    <h1><small>Add</small> Member</h1>
                </div>
                <div class="row-fluid">
                    <?php echo $this->renderPartial('_form', array('model'=>$model,'upId'=>$upId,'pos'=>$pos)); ?>
                </div>
                <div class="dr"><span></span></div>  
            </div>