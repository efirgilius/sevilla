<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?=$form->errorSummary($model);?>
<div class="uk-grid" data-uk-grid-margin>
	<div class="uk-width-medium-3-4">
		<div class="md-card">
			<div class="md-card-content large-padding">
				
				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'title'); ?>
					<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100,'class'=>'md-input','required'=>'required')); ?>
					<?php echo $form->error($model,'title'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'image'); ?><br>
                    <?php if($model->image):?>
                        <?php echo CHtml::image($model->small_url);?><br/>
                    <?php endif;?>
                    <input id="upload" name="Content[image]" type="file" class="md-input"/>
                    <p class="uk-text-small uk-margin-remove padding20">
                    Select an image to upload. Up to 1 MB. Recommended image size <strong><?=Content::NEWS_LARGE_THUMB_WIDTH;?>px X <?=Content::NEWS_LARGE_THUMB_HEIGHT;?>px</strong>.</p>
				</div>

				<div class="uk-form-row">
					<?php echo $form->textArea($model,'content',array('row'=>6, 'cols'=>50,'class'=>'md-input','id'=>'editor','required'=>'required')); ?>
					<?php echo $form->error($model,'content'); ?>
				</div>

				<div class="uk-form-row">
	                <?php echo $form->labelEx($model,'tags'); ?>
			        <?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>255,'class'=>'md-input')); ?>
			        <?php echo $form->error($model,'tags'); ?>
	            </div>

			</div>
		</div>
	</div>
	<div class="uk-width-medium-1-4">
		<div class="md-card">
            <div class="md-card-content">
                <div class="uk-form-row">
                    <?=$form->checkbox($model,'status',['data-switchery'=>'true']); ?>
                    <label class="inline-label">Publish</label>
                </div>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'editor', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>