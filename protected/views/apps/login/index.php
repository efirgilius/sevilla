    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'validation',
                            'enableAjaxValidation'=>false,
                            'htmlOptions'=>array('class'=>'form-horizontal')
                        )); ?>
                <?php echo $form->errorSummary($model);?>
                    <div class="uk-form-row">
                        <label for="LoginForm_username">Username</label>
                        <?php echo $form->textField($model,'username',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                    <div class="uk-form-row">
                        <label for="LoginForm_password">Password</label>
                        <?php echo $form->passwordField($model,'password',array('maxlength'=>30,'class'=>'md-input','required'=>'required')); ?>
                        <?php echo $form->error($model,'password'); ?>
                    </div>
                    <div class="uk-margin-medium-top">
                        <?php echo CHtml::submitButton('Sign in',array('id'=>'buttonLogin','class'=>'md-btn md-btn-primary md-btn-block md-btn-large')); ?>
                    </div>
                    <div class="uk-margin-top">
                        <a href="#" id="login_help_show" class="uk-float-right">Need help?</a>
                        <span class="icheck-inline">
                            <?php echo $form->checkBox($model,'rememberMe',['id'=>'login_page_stay_signed','data-md-icheck'=>'']); ?>
                            <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                        </span>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_b uk-text-success">Can't log in?</h2>
                <p>Here’s the info to get you back in to your account as quickly as possible.</p>
                <p>First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
                <p>If your password still isn’t working, it’s time to contact Super Admin.</p>
            </div>
            
        </div>
        
    </div>