<?=$form->errorSummary($model);?>
<div class="uk-grid" data-uk-grid-margin>
	<div class="uk-width-medium-1-2">
		<div class="md-card">
			<div class="md-card-content large-padding">
				<?php if ($type!=='page'):?>
				<div class="uk-form-row">
					<div class="uk-width-medium-1-1">
	                    <?= $form->dropDownList($model,'parent_id',CHtml::ListData($parents,'id','name'),['prompt'=>'Select Parent','data-md-selectize'=>'true']);?>
						<?php echo $form->error($model,'parent_id'); ?>
					</div>
				</div>
				<?php endif;?>
				
				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'name'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'image'); ?><br>
					<?php if ($model->image):?>
						<?=CHtml::image($model->small_url);?>
					<?php endif;?>
					<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>255,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'image'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'description'); ?>
					<?php echo $form->textArea($model,'description',array('row'=>6, 'cols'=>50,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'description'); ?>
				</div>

			</div>
		</div>
	</div>
	<div class="uk-width-medium-1-2">
		<div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3 uk-width-large-1-3">
	                    <?php echo $form->checkbox($model,'status',['data-switchery'=>'true']); ?>
                        <label class="inline-label">Is Active</label>
                    </div>
                </div>
            </div>
        </div>
		<div class="md-card">
			<div class="md-card-content large-padding">
				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'meta_title'); ?>
					<?php echo $form->textField($model,'meta_title',array('size'=>60,'maxlength'=>70,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'meta_title'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'meta_keywords'); ?>
					<?php echo $form->textField($model,'meta_keywords',array('size'=>60,'maxlength'=>255,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'meta_keywords'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'meta_description'); ?>
					<?php echo $form->textField($model,'meta_description',array('size'=>60,'maxlength'=>160,'class'=>'md-input')); ?>
					<?php echo $form->error($model,'meta_description'); ?>
				</div>
			</div>
		</div>
	</div>
</div>