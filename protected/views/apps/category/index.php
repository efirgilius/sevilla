<div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=ucfirst($type);?> Categories</h3>
            <?php Notify::renderFlash();?>
            <div class="md-card">                
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Slug</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($parents):?>
			                                <?php $i=1; foreach($parents as $data):?>
		                                        <tr>
		                                            <td class="uk-text-large uk-text-nowrap"><?=$data->name;?></td>
		                                            <td class="uk-text-large uk-text-nowrap"><?= $data->slug;?></td>
                                                    <td>
                                                        <?php if($data->status):?>
                                                            <i class="material-icons uk-text-success md-24">&#xE834;</i>
                                                        <?php else:?>
                                                            <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                                                        <?php endif;?>
                                                    </td>
		                                            <td>
	                                                    <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$data->id));?>
	                                                    <?php if (!$data->categories):?>
	                                                    <a href="#" class="del" data-id="<?php echo $data->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
	                                                	<?php endif;?>
		                                            </td>
		                                        </tr>
		                                        <?php if ($data->categories):?>
		                                        	<?php $ii=1; foreach($data->categories as $data):?>
			                                        <tr>
                                                        <td class="uk-text-large uk-text-nowrap">____ <?=$data->name;?></td>
			                                            <td class="uk-text-large uk-text-nowrap"><?= $data->slug;?></td>
			                                            <td>
                                                            <?php if($data->status):?>
                                                                <i class="material-icons uk-text-success md-24">&#xE834;</i>
                                                            <?php else:?>
                                                                <i class="material-icons uk-text-muted md-24">&#xE835;</i>
                                                            <?php endif;?>
                                                        </td>
			                                            <td>
		                                                    <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$data->id));?>
		                                                    <a href="#" class="del" data-id="<?php echo $data->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
			                                            </td>
			                                        </tr>
			                                        <?php $ii++; endforeach;?>
		                                    	<?php endif;?>
		                                    <?php $i++; endforeach;?>
			                            <?php else:?>
	                                        <tr>
	                                            <td colspan="6"><p><?=ucfirst($type);?> Category is unavailable.</p></td>
	                                        </tr>
			                            <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pagination center">
                            <?php 
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'header'=>'',
                                'footer'=>'',
                                'nextPageLabel'=>'»',
                                'prevPageLabel'=>'«',
                                'id'=>'link_pager',
                            ))?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

	<div class="md-fab-wrapper">
        <?=CHtml::link('<i class="material-icons">&#xE150;</i>', ['create','type'=>$type],['class'=>'md-fab md-fab-accent']);?>
    </div>    

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete the selected item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/category/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/category',['type'=>$type])."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>