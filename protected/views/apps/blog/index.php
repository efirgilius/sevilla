<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div id="page_content">
    <?php Notify::renderFlash();?>
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom"><?=$this->title;?></h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	<?php if($model):?>
							<?php $i=1; foreach($model as $row):?>
							    <tr>
                                    <td><?=$i;?></td>
                                    <td><?=CHtml::image($row->small_url,$row->title.'_image');?></td>
							        <td class="uk-text-large uk-text-nowrap"><?=$row->title;?></td>
                                    <td><?=$row->category->name;?></td>
							        <td><?php echo ($row->status=='0')?'<span class="uk-badge uk-badge-danger">'.$row->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$row->statusname.'</span>';?></td>
                                    <td><?=$row->createTime;?></td>
							        <td class="uk-text-nowrap">
							            <?php echo CHtml::link('<i class="md-icon material-icons">&#xE8F4;</i>',array('view','id'=>$row->id));?>
							            <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$row->id));?>
							            <a href="#" class="del" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
							        </td>
							    </tr>
							<?php $i++; endforeach;?>
							<?php else:?>
							    <tr>
							        <td colspan="7"><p><?=$this->title;?> is unavailable.</p></td>
							    </tr>
							<?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="pagination center">
                <?php 
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header'=>'',
                    'footer'=>'',
                    'nextPageLabel'=>'»',
                    'prevPageLabel'=>'«',
                    'id'=>'link_pager',
                ))?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="md-fab-wrapper">
    <?=CHtml::link('<i class="material-icons">&#xE150;</i>', ['create'],['class'=>'md-fab md-fab-accent']);?>
</div>

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/blog/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/blog')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>

<script type="text/javascript">
    CKEDITOR.replace( 'editor', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>