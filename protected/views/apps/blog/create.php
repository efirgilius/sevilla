<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'form_validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data','class'=>'uk-form-stacked'),
    )); ?>
    <div id="page_content">
        <div id="page_heading">
            <h1 id="product_edit_name"><?=$this->title;?></h1>
            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"><?=$model->title;?></span>
        </div>
        <div id="page_content_inner">
            <?php Notify::renderFlash();?>
            <?php echo $this->renderPartial('_form', array('model'=>$model,'form'=>$form)); ?>
        </div>
    </div>

    <div class="md-fab-wrapper">
        <button type="submit" class="md-fab md-fab-success"><i class="material-icons">&#xE161;</i></button>
        <?=CHtml::link('<i class="material-icons">undo</i>','javascript: history.go(-1)',['class'=>'md-fab md-fab-warning']);?>
    </div>
<?php $this->endWidget(); ?>
<script>
    CKEDITOR.replace( 'editor1', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>