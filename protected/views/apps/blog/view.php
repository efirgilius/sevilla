<div id="page_content">
    <div id="page_content_inner">
        <?php Notify::renderFlash();?>
        <div class="uk-grid blog_article" data-uk-grid-margin>
            <div class="uk-width-medium-3-4">
                <div class="md-card">
                    <div class="md-card-content large-padding">
                        <div class="uk-article">
                            <div class="md-card-toolbar-actions">
                                <?php echo CHtml::link('<i class="md-icon material-icons">list</i>',array('/apps/blog'));?>
                                <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$model->id));?>
                                <a href="#" class="del" data-id="<?php echo $model->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                            </div>
                            <h1 class="uk-article-title"><?=$model->title;?></h1>
                            <p class="uk-article-meta">
                                Written by <?=$model->user->name;?> on <?=$model->createTime;?>
                                <br>Posted in <a href="#"><?=$model->category->name;?></a>
                                <br>Status <em><?=$model->statusname;?></em>
                            </p>
                            
                            <hr class="uk-article-divider">
                            <p>
                                <?php if ($model->image):?>
                                <figure class="uk-thumbnail uk-thumbnail-large thumbnail_left">
                                <?=CHtml::image($model->medium_url);?>
                                </figure>
                                <?php endif;?>
                                <?=$model->content;?>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-4">
                <h5 class="uk-margin-small-bottom uk-text-upper  uk-margin-large">Tags</h5>
                <div class="blog_tags">
                    <?php if ($tags):?>
                        <?php foreach ($tags as $k => $v):?>
                            <a href="#" class="uk-badge"><?=$v;?></a>
                        <?php endforeach;?>
                    <?php else:?>
                        <p>Tags not available.</p>
                    <?php endif;?>
                </div>
            </div>
        </div>

    </div>
</div>

    <div class="md-fab-wrapper">
        <?=CHtml::link('<i class="material-icons">list</i>',['/apps/blog'],['class'=>'md-fab md-fab-primary']);?>
        <?=CHtml::link('<i class="material-icons">&#xE150;</i>',['update','id'=>$model->id],['class'=>'md-fab md-fab-success']);?>
        <?=CHtml::link('<i class="material-icons">&#xE872;</i>','javascript:void(0);',['class'=>'md-fab md-fab-danger del','data-id'=>$model->id]);?>
    </div>

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/blog/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/blog')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>