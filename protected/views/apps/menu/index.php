<div id="page_content">
    <?php Notify::renderFlash();?>
    <div id="page_content_inner">
        <?php if ($menus):?>
        <?php foreach ($menus as $k => $v):?>
        <h4 class="heading_a uk-margin-bottom"><?=$v->name;?> 
        <!-- <a class="groupupdate" href="#update_group" data-id="<?=$v->id;?>" data-name="<?=$v->name;?>" data-uk-modal="{ center:true }">
            <i class="md-icon material-icons">&#xE254;</i>
        </a>
        <?php if (!$v->menuItems):?>
            <a href="#" class="del" data-id="<?php echo $v->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
        <?php endif;?> -->
        </h4>

        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-hover">
                        <thead>
                        <tr>
                        
                            <th>Link</th>
                            <th>Label</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Action</th>
                       
                        </tr>
                        </thead>
                        <tbody>
                        	<?php if ($v->menuItems):?>
                            <?php foreach ($v->menuItems as $k => $v):?>
                            <tr>
                                <td><?=$v->valueName();?></td>
                                <td><?=$v->label;?></td>
                                <td><?=$v->typeName();?></td>
                                <td><?=$v->order;?></td>
                            </tr>
                            <?php endforeach;?>
                            <?php else:?>
                                <p></p>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <?php endif;?>
    </div>
</div>


<!-- <div class="md-fab-wrapper">
    <a title="Add Menu" class="md-fab md-fab-accent" href="#add_menu" data-uk-modal="{ center:true }">
        <i class="material-icons">&#xE145;</i>
    </a>
</div>

<div class="uk-modal" id="add_menu">
    <div class="uk-modal-dialog">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'validation',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>[
                'class'=>'uk-form-stacked'
            ],
        )); ?>
            <?=$form->errorSummary($menu);?>
            <div class="uk-margin-medium-bottom">
                <?php echo $form->labelEx($menu,'name'); ?>
                <?php echo $form->textField($menu,'name',array('size'=>20,'maxlength'=>20,'class'=>'md-input')); ?>
                <?php echo $form->error($menu,'name'); ?>
            </div>
            <div class="uk-margin-medium-bottom">
                <?php echo $form->labelEx($menu,'type'); ?>
                <?php echo $form->dropDownList($menu,'type',$menu->positionList(),array('class'=>'md-input')); ?>
                <?php echo $form->error($menu,'type'); ?>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save">Add Menu</button>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div> -->

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this menu?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/menu/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/menu')."'; 
            }
        });
    return false;
    });

",CClientScript::POS_READY);?>