<div id="page_content">
    <div id="page_content_inner">
        <?php Notify::renderFlash();?>
        <div class="uk-grid blog_article" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                <div class="md-card">
                    <div class="md-card-content large-padding">
                        <div class="uk-article">
                            <div class="md-card-toolbar-actions">
                                <?php echo CHtml::link('<i class="md-icon material-icons">list</i>',array('/apps/page'));?>
                                <?php echo CHtml::link('<i class="md-icon material-icons">&#xE254;</i>',array('update','id'=>$model->id));?>
                                <a href="#" class="del" data-id="<?php echo $model->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
                            </div>
                            <h1 class="uk-article-title"><?=$model->title;?></h1>
                            <p class="uk-article-meta">
                                Created on <?=$this->getTime($model->created);?>, Updated on <?=$this->getTime($model->modified);?><br>
                                <em><?=$model->statusname;?></em>
                            </p>
                            
                            <hr class="uk-article-divider">
                            <p>
                                <?=$model->content;?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

    <div class="md-fab-wrapper">
        <?=CHtml::link('<i class="material-icons">list</i>',['/apps/page'],['class'=>'md-fab md-fab-primary']);?>
        <?=CHtml::link('<i class="material-icons">&#xE150;</i>',['update','id'=>$model->id],['class'=>'md-fab md-fab-success']);?>
        <?=CHtml::link('<i class="material-icons">&#xE872;</i>','javascript:void(0);',['class'=>'md-fab md-fab-danger del','data-id'=>$model->id]);?>
    </div>

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this item?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/page/delete')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/page')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>