<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?=$form->errorSummary($model);?>
<div class="uk-grid" data-uk-grid-margin>
	<div class="uk-width-medium-3-4">
		<div class="md-card">
			<div class="md-card-content large-padding">
				
				<div class="uk-form-row">
					<?php echo $form->labelEx($model,'title'); ?>
					<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100,'class'=>'md-input','required'=>'required')); ?>
					<?php echo $form->error($model,'title'); ?>
				</div>

				<div class="uk-form-row">
					<?php echo $form->textArea($model,'content',array('row'=>6, 'cols'=>50,'class'=>'md-input','id'=>'editor','required'=>'required')); ?>
					<?php echo $form->error($model,'content'); ?>
				</div>

			</div>
		</div>
	</div>
	<div class="uk-width-medium-1-4">
		<div class="md-card">
            <div class="md-card-content">
                <div class="uk-form-row">
                    <?php echo $form->checkbox($model,'is_menu',['data-switchery'=>'true']); ?>
                    <label class="inline-label">Show on menu</label>
                </div>
                <div class="uk-form-row">
                    <?php echo $form->checkbox($model,'status',['data-switchery'=>'true']); ?>
                    <label class="inline-label">Publish</label>
                </div>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'editor', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>