<div id="page_content">
    <?php Notify::renderFlash();?>
    <div id="page_content_inner">
        <?php if ($category):?>
        <?php foreach ($category as $k => $v):?>
        <h4 class="heading_a uk-margin-bottom"><?=$v->name;?> 
        <a class="groupupdate" href="#update_group" data-id="<?=$v->id;?>" data-name="<?=$v->name;?>" data-uk-modal="{ center:true }">
            <i class="md-icon material-icons">&#xE254;</i>
        </a>
        <?php if (!$v->contents):?>
            <a href="#" class="del" data-id="<?php echo $v->id;?>"><i class="md-icon material-icons">&#xE872;</i></a></h4>
        <?php endif;?>

        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-hover">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	<?php $model=Content::model()->findAllByAttributes(['category_id'=>$v->id]);?>
                            <?php if($model):?>
							<?php $i=1; foreach($model as $row):?>
							    <tr>
							        <td class="uk-text-large uk-text-nowrap"><?=$row->title;?></td>
                                    <td class="uk-text-large uk-text-nowrap"><?=$row->slug;?></td>
							        <td><?php echo ($row->status=='0')?'<span class="uk-badge uk-badge-danger">'.$row->statusname.'</span>':'<span class="uk-badge uk-badge-success">'.$row->statusname.'</span>';?></td>
                                    <td>
                                        <a class="updatepagegroup" href="#update_pagegroup" data-group-id="<?=$row->category->id;?>" data-page-id="<?=$row->id;?>" data-uk-modal="{ center:true }">
                                            <i class="md-icon material-icons">&#xE254;</i>
                                        </a>
                                        <a href="#" class="delfromgroup" data-id="<?php echo $row->id;?>"><i class="md-icon material-icons">&#xE872;</i></a>
							        </td>
							    </tr>
							<?php $i++; endforeach;?>
							<?php else:?>
							    <tr>
							        <td colspan="7"><p><?=$v->name;?> page is unavailable.</p></td>
							    </tr>
							<?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <?php endif;?>
    </div>
</div>


    <div class="md-fab-wrapper">
        <a title="Add Page To Group" class="md-fab md-fab-accent" href="#add_page" data-uk-modal="{ center:true }">
            <i class="material-icons">&#xE145;</i>
        </a>
        <a title="Create New Group Page" class="md-fab md-fab-accent" href="#new_group" data-uk-modal="{ center:true }">
            <i class="material-icons">create</i>
        </a>
    </div>

    <div class="uk-modal" id="new_group">
        <div class="uk-modal-dialog">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'validation',
                'enableAjaxValidation'=>false,
            )); ?>
            <form class="uk-form-stacked">
                <div class="uk-margin-medium-bottom">
                    <?php echo $form->labelEx($group,'name'); ?>
                    <?php echo $form->textField($group,'name',array('size'=>50,'maxlength'=>50,'class'=>'md-input')); ?>
                    <?php echo $form->error($group,'name'); ?>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save">Add Group</button>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <div class="uk-modal" id="update_group">
        <div class="uk-modal-dialog">
            <?=CHtml::beginForm('updategroup','post',['class'=>'uk-form-stacked']);?>
                <div class="uk-margin-medium-bottom">
                    <label>Name</label>
                    <input type="hidden" name="group[id]" id="groupid">
                    <input type="text" name="group[name]" size=50 maxlength="50" id="groupname" class="md-input label-fixed">
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="btn-update">Update Group</button>
                </div>
            <?=CHtml::endForm();?>
        </div>
    </div>

    <div class="uk-modal" id="add_page">
        <div class="uk-modal-dialog">
            <?=CHtml::beginForm('addpagegroup','post',['class'=>'uk-form-stacked']);?>
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Page</label>
                    <?=CHtml::dropDownList('GroupPage[content_id]','GroupPage[content_id]',CHtml::listData($pages,'id','title'),['class'=>'uk-form','data-md-selectize-inline'=>true]);?>
                </div>
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Group</label>
                    <?=CHtml::dropDownList('GroupPage[category_id]','GroupPage[category_id]',CHtml::listData($groups,'id','name'),['class'=>'uk-form-width','data-md-selectize-inline'=>true]);?>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="btn-update">Add Page</button>
                </div>
            <?=CHtml::endForm();?>
        </div>
    </div>

    <div class="uk-modal" id="update_pagegroup">
        <div class="uk-modal-dialog">
            <?=CHtml::beginForm('updatepagegroup','post',['class'=>'uk-form-stacked']);?>
                <input type="hidden" name="GroupPage[page_id]" id="GroupPage_page_id">
                <div class="uk-margin-bottom">
                    <label class="uk-form-label">Group</label>
                    <?=CHtml::dropDownList('GroupPage[group_id]','GroupPage[group_id]',CHtml::listData($groups,'id','name'),['class'=>'md-input']);?>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="btn-update">Update Group</button>
                </div>
            <?=CHtml::endForm();?>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Are you sure want to delete this group?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/page/deletegroup')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/page/group')."'; 
            }
        });
    return false;
    });

    $('.delfromgroup').on('click',function(){
        if(!confirm('Are you sure want to delete this from group?')) return false;
        var id=$(this).attr('data-id');
        $.ajax({
            url:'".$this->createUrl('apps/page/deletefromgroup')."',
            type: 'GET',
            data: 'id='+id,
            success:function(){
                window.location.href = '".$this->createUrl('apps/page/group')."'; 
            }
        });
    return false;
    });

",CClientScript::POS_READY);?>

<?php Yii::app()->clientScript->registerScript('update', "
    $('.groupupdate').on('click',function(){
        var id=$(this).attr('data-id');
        var name=$(this).attr('data-name'); 
        $('#groupid').val(id);
        $('#groupname').val(name);
    });

    $('.updatepagegroup').on('click',function(){
        var pageid=$(this).attr('data-page-id');
        var groupid=$(this).attr('data-group-id'); 
        $('#GroupPage_page_id').val(pageid);
        $('#GroupPage_group_id').val(groupid);
    });
",CClientScript::POS_READY);?>