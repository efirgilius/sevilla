<!-- Page Title -->
<section class="breadcrumb-wrap">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?=$this->title;?></h1>
        <ul class="breadcrumb">
          <li><?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Home',Yii::app()->homeurl);?></li>
          <li><?=CHtml::link('Login',['/site/login']);?></li>
          <li class="last"><?=$this->title;?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- Contact Page 02 -->
<section class="page-contact02 mgb20">
  <div class="container">
    <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-6 col-sm-offset-3">           
              <div class="brochure text-center">   
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                  <?php if(isset($model)):?>
                    <h3>RESET PASSWORD</h3><hr>
                    <p>Silahkan masukkan password baru Anda</p>
                    <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm',))); ?>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'password'); ?>
                      </div>
                      <div class="input-email">
                        <?php echo $form->labelEx($model,'verifypassword'); ?>
                        <?php echo $form->passwordField($model,'verifypassword',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'verifypassword'); ?>
                      </div>
                      <button type="submit" class="submit-btn">Reset<i class="icon-paper-plane"></i></button>
                    <?php $this->endWidget(); ?>
                  <php else:?>
                    <div class="alert alert-success loop">
                       <?= $message;?>
                   </div>
                  <?php endif;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
</section>