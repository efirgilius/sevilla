<!-- Page Title -->
<section class="breadcrumb-wrap">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?=$this->title;?></h1>
        <ul class="breadcrumb">
          <li><?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Home',Yii::app()->homeurl);?></li>
          <li class="last"><?=$this->title;?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- Contact Page 02 -->
<section class="page-contact02 mgb20">
  <div class="container">
    <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-6">           
              <div class="brochure text-center">   
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                  <h3>LOGIN</h3>
                  <hr>
                  <p>Silahkan masukkan ID Member dan Password Anda</p>
                  <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm',))); ?>
                    <div class="input-email">
                      <?php echo $form->textField($model,'username',['class'=>'form-control','required'=>'required','placeholder'=>'ID Member']); ?>
                      <?php echo $form->error($model,'username'); ?>
                    </div>
                    <div class="input-text">
                      <?php echo $form->passwordField($model,'password',['class'=>'form-control','required'=>'required','placeholder'=>'Password']); ?>
                      <?php echo $form->error($model,'password'); ?>
                    </div>
                    <button type="submit" class="submit-btn">Login<i class="icon-paper-plane"></i></button>
                  <?php $this->endWidget(); ?>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="brochure text-center">
                <h3>BELUM DAFTAR?</h3>
                <hr>
                <p class="btn-cons"><?= CHtml::link('DAFTAR SEKARANG',['/site/register']);?></p>
                <p><?=CHtml::link('Lupa Password?',['/site/forgotpassword']);?></p>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
</section>