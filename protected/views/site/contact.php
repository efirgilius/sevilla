<!-- Page Title -->
<section class="breadcrumb-wrap">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?=$this->title;?></h1>
        <ul class="breadcrumb">
          <li><?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Home',Yii::app()->homeurl);?></li>
          <li class="last"><?=$this->title;?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- Contact Page 02 -->
<section class="page-contact02">
  <div class="container">
    <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-8">
              <div class="item">
                <span class="icon-wrap"><i class="ion-ios-home-outline"></i></span>
                <h4>Address</h4>
                <p><?=$this->g('contact','address');?></p>
              </div>
              <div class="item">
                <span class="icon-wrap"><i class="ion-ios-telephone-outline"></i></span>
                <h4>Phone</h4>
                <p>Support: <?=$this->g('contact','phone');?></p>
              </div>
              <div class="item">
                <span class="icon-wrap"><i class="ion-ios-email-outline"></i></span>
                <h4>Email</h4>
                <p>Support: <a href="mailto:<?=$this->g('contact','email');?>"><?=$this->g('contact','email');?></a></p>
              </div>
              <div><span class="separator mgt30 mgb30"></span></div>
              <div class="intro">
                <p><?=$this->g('general','contact_message');?></p>
              </div>
            </div>
            <div class="col-sm-4">
              <?php Notify::renderMflash();?>
              <div class="contact-form">
                <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm',))); ?>
                  <div class="input-text">
                      <?php echo $form->textField($model,'name',array('class'=>'input-name form-control','maxlength'=>30,'required'=>'required','placeholder'=>'Name')); ?>
                      <?php echo $form->error($model,'name'); ?>
                    </div>
                    <div class="input-email">
                        <?php echo $form->textField($model,'email',array('class'=>'input-name form-control','maxlength'=>50,'required'=>'required','placeholder'=>'Email')); ?>
                          <?php echo $form->error($model,'email'); ?>
                    </div>
                    <div class="input-text">
                        <?php echo $form->textField($model,'subject',array('class'=>'input-subject form-control','maxlength'=>100,'required'=>'required','placeholder'=>'Subject')); ?>
                          <?php echo $form->error($model,'subject'); ?>
                    </div>
                    <div class="textarea-message">
                        <?php echo $form->textArea($model,'message',array('class'=>'','required'=>'required','maxlength'=>1000,'placeholder'=>'Message','maxlength'=>'5000','rows'=>'4')); ?>
                        <?php echo $form->error($model,'message'); ?>
                    </div>
                    <button type="submit" class="submit-btn">Send Now<i class="icon-paper-plane"></i></button>
                <?php $this->endWidget(); ?>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
</section>
<div id="gmap" class="mgt50"></div>
<section class="more-info">
  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <div class="text">
          <h3>Need more information?</h3>
          <p>We can call you and help with your problem, just leave us your phone number.</p>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="subscribe">
          <div class="subscribe">
          <form method="post" id="subscribeForm" class="subscribe-form" action="http://themesflat.com/html/consultant/contact/subscribe-process.php">
            <label>
              <input type="email" id="email02" name="email02" placeholder="Enter you email..." value="" required="required" title="Enter your email">
            </label>
            <input type="submit" class="submit" value="Subscribe">
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>