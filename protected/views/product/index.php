<!-- Page Title -->
<section class="breadcrumb-wrap">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1><?=$this->title;?></h1>
				<ul class="breadcrumb">
					<li>
					<?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
					</li>
					<li class="last"><?=$this->title;?></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<!-- Blog post -->
<section class="page-wrap page-portfolio">
	<div class="container">
		<div class="row">
			<main class="main-content">
				<div class="content">
					<div class="portfolio popup-gallery">
						<?php if($model):?>
						<?php foreach ($model as $k => $v):?>
						<div class="col-sm-4 col-xs-12 item business">
							<article>
								<p class="media"><?=CHtml::link(CHtml::image($v->medium_url),['/product/view','slug'=>$v->slug]);?></p>
								<h3><?=CHtml::link($v->name,['/product/view','slug'=>$v->slug]);?></h3>
								<p>IDR <?=number_format($v->saleprice);?> <?php if ($v->discount>0):?> <del>IDR <?=number_format($v->oldprice);?></del><?php endif;?></p>
							</article>
						</div>
						<?php endforeach;?>
						<?php else:?>
							<p>Belum ada produk.</p>
						<?php endif;?>
					</div>
					<div class="text-center">
						<?php $this->widget('CLinkPager', array(
							'pages' => $pages,
							'header'=>'',
							'footer'=>'',
							'nextPageLabel'=>'»',
							'prevPageLabel'=>'«',
							'id'=>'link_pager',
						))?>
						</div>
				</div>
			</main>
		</div>
	</div>
</section>
