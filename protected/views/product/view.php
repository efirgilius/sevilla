<!-- Page Title -->
	<section class="breadcrumb-wrap">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?=$model->name;?></h1>
					<ul class="breadcrumb">
						<li>
						<?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
						</li>
						<li><?=CHtml::link('Produk',['/product']);?></li>
						<li class="last"><?=$model->name;?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<!-- Blog post -->
	<section class="page-wrap page-portfolio-single">
		<div class="container">
			<div class="row">
				<main class="main-content">
					<div class="content">
						<div class="portfolio-single">
							<article>
								<div class="col-sm-6">
									<h2><?=$model->name;?></h2>
									<p><?=$model->overview;?></p>
									<div class="info">
										<p><span>Harga</span> IDR <?=number_format($model->salePrice);?> 
											<?php if ($model->discount>0):?>
												<del>IDR <?=number_format($model->oldprice);?></del>
											<?php endif;?></p>
										<div class="share"><span>Share:</span>
											<ul>
												<li><a target="_blank" href="https://web.facebook.com/sharer.php?u=<?=Yii::app()->request->hostInfo.Yii::app()->request->url;?>" data-bg-color="#3A5795"><i class="fa fa-facebook text-white"></i></a></li>
												<li><a target="_blank" href="https://twitter.com/home?status=<?=$model->name;?>%20<?=Yii::app()->request->hostInfo.Yii::app()->request->url;?>" data-bg-color="#A11312"><i class="fa fa-twitter text-white"></i></a></li>
												<li><a target="_blank" href="https://plus.google.com/share?url=<?=Yii::app()->request->hostInfo.Yii::app()->request->url;?>" data-bg-color="#55ACEE"><i class="fa fa-google-plus text-white"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<p class="img01"><?=CHtml::image($model->large_url);?></p>
								</div>
							</article>
						</div>
					</div>
				</main>
			</div>
		</div>
	</section>