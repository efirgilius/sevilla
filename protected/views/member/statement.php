<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="page-wrap page-about">
    <div class="container">
    <div class="row">
        <main class="main-content">
            <?php $form=$this->beginWidget('CActiveForm', array(
                                                    'id'=>'bonus-form',
                                                    'enableAjaxValidation'=>false,
                                                    'method'=>'POST',
                                                    'action'=>Yii::app()->createUrl('member/statement')
                                            )); ?>
                                            <h4>Period:&nbsp;</h4>
                                            <div class="row">
                                                
                                                
                                                <div class="col-xs-12  col-sm-12">
                                                    <div class="col-xs-12  col-sm-3 monthly">
                                                        <?php echo CHtml::dropDownList('m', '1',Setting::getMonths(),array('tabindex'=>1,'class'=>'form-control  form-control--contact')); ?>
                                                    </div>
                                                    <div class="col-xs-12  col-sm-3 monthly">
                                                        <?php echo CHtml::dropDownList('y', '',Setting::getYears(),array('tabindex'=>2,'class'=>'form-control  form-control--contact')); ?>
                                                    </div>
                                                    
                                                    <div class="col-xs-12  col-sm-3">
                                                        <button type="submit" class="btn  btn-warning">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $this->endWidget(); ?><br>
                                            <hr class="divider">  
            <div class="row">
                <div class="col-xs-12  col-sm-12  push-down-30">
                    <p><strong>Total bonus Anda Rp.<?= number_format($bonus,2,",",".");?></strong> untuk <strong>periode <?= $period;?> </strong></p>
                    <table class="table table-theme table-striped">
                    <thead>
                      <tr>
                          <th rowspan="2" class="center">Date</th>
                          <th colspan="3" class="center">Bonus</th>
                          <th class="center" rowspan="2">Sub Total</th>
                      </tr>
                      <tr>
                        <th><?= CHtml::link('Bonus Sponsor',['sponsorbonus'],['style'=>'color:green;']);?></th>
                        <th><?= CHtml::link('Bonus Pasangan',['pairingbonus'],['style'=>'color:green;']);?></th>
                        <th><?= CHtml::link('Bonus Loyalty',['loyaltybonus'],['style'=>'color:green;']);?></th>
                      </tr>
                     
                    </thead>
                    <tbody>
                        <?php $tot=0;foreach($dates as $date):?>
                            <tr>
                              <td><?= $date;?></td>
                              <td class="right">Rp.<?php $sponsor= BonusSponsor::model()->getTotalPeriod(date('d', strtotime($date)),date('m', strtotime($date)), date('Y', strtotime($date))); echo number_format($sponsor,2,",",".");?></td>
                              <td class="right">Rp.<?php $couple= BonusCouple::model()->getTotalPeriod(date('d', strtotime($date)),date('m', strtotime($date)), date('Y', strtotime($date))); echo number_format($couple,2,",",".");?></td>
                              <td class="right">Rp.<?php $loyalty= BonusLoyalty::model()->getTotalPeriod(date('d', strtotime($date)),date('m', strtotime($date)), date('Y', strtotime($date))); echo number_format($loyalty,2,",",".");?></td>
                              <td class="right">Rp.<?php $tot+=$sponsor + $couple + $loyalty; echo number_format($sponsor + $couple + $loyalty,2,",",".");?></td>
                            </tr>
                        <?php endforeach;?>
                            <tr>
                                <td colspan="4" class="right"><strong>Total</strong></td>
                                <td class="right"><strong>Rp. <?=                 number_format($tot,2,",",".");?></strong></td>
                            </tr>
                    </tbody>
                  </table>
        </div>
            </div>
        </main>
        <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
    </div>
</div>
</section>
<?php Yii::app()->clientScript->registerScript('tab','
    $("input:radio[name=period]").click(function(){
        if ($(this).is(":checked"))
        {
            if($(this).val()=="1")
            {
                $(".monthly").fadeOut("slow").hide();
                $(".daily").fadeIn("slow").show();
                $("#date").attr("required");
            }
            else
            {
                $(".daily").fadeOut("slow").hide();
                $(".monthly").fadeIn("slow").show();
                $("#date").removeAttr("required");
            }
        }
    })
');?>