<h3>STRUK PEMBAYARAN/PEMBELIAN <?= strtoupper($model->name);?></h3>
<table>
    <tr>
        <td>TANGGAL</td>
        <td>:</td>
        <td><?= $model->created_on;?></td>
    </tr>
    <tr>
        <td>No METER/MSISDN</td>
        <td>:</td>
        <td><?= $model->msisdn;?></td>
    </tr>
    <tr>
        <td>NAMA</td>
        <td>:</td>
        <td><?= $model->customer;?></td>
    </tr>
    <tr>
        <td>TOKEN</td>
        <td>:</td>
        <td><?= $model->token;?></td>
    </tr>
    <?php if($model->customer!=''):?>
    <tr>
        <td>RP BIAYA ADMIN</td>
        <td>:</td>
        <td>2500</td>
    </tr>
    <tr>
        <td>RP TOTAL</td>
        <td>:</td>
        <td><?= number_format($model->total + 2500);?></td>
    </tr>
    <?php else:?>
    <tr>
        <td>BIAYA ADMIN</td>
        <td>:</td>
        <td><?= number_format($fee);?></td>
    </tr>
    <tr>
        <td>TOTAL</td>
        <td>:</td>
        <td>Rp. <?= number_format($model->total+$fee);?></td>
    </tr>
    <?php endif;?>
    
</table>
<br><br>
<span>TERIMA KASIH</span>