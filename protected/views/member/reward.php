<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                            <th>#</th>
                            <th>Position</th>
                            <th>Reward</th>
                            <th>Claimed Date</th>
                            <th>Approved Date</th>
                            <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php if($model):?>
                          <?php foreach($model as $row):?>
                          <tr>
                              <td width="2%"><?php echo $i;?></td>
                              <td width="23%"><?php echo $row->debit;?></td>
                              <td><?php echo $row->reward;?></td>
                              <td><?php echo $row->posted;?></td>
                              <td><?php echo $row->approved_on;?></td>
                              <td><?= $row->statusname;?></td>
                          </tr>
                          <?php $i++; endforeach;?>
                        <?php else:?><br>
                          <tr><td colspan="6">Claimed reward is unavailable.</td></tr>                
                        <?php endif;?>
                      </tbody>
                  </table>  
                <div class="pagination right">
                  <?php $this->widget('CLinkPager', array(
                      'pages' => $pages,
                      'header'=>'',
                      'footer'=>'',
                      'nextPageLabel'=>'»',
                      'prevPageLabel'=>'«',
                      'id'=>'link_pager',
                  ))?>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>