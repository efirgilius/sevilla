<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="col-sm-7">
                <?= Notify::renderMflash();?>
                <div class="contact-form">
                    <p>Silahkan isi form konfirmasi pembayaran di bawah ini sesuai dengan pembayaran yang Anda lakukan.</p>
                    <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'loginForm'))); ?>
                    <?= $form->errorSummary($model);?>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank_id'); ?>
                        <select name="EwalletConfirmation[bank_id]" class="form-control" required style="margin-bottom: 20px;">
                              <option value="">- Select Bank -</option>
                              <?php if($banks):?>
                              <?php foreach($banks as $bank):?>
                              <option value="<?php echo $bank->id;?>"><?php echo $bank->name;?> - <?php echo $bank->acc_number;?></option>
                              <?php endforeach;?>
                              <?php endif;?>
                          </select>
                        <?php echo $form->error($model,'bank_id'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('maxlength'=>100,'required'=>'required','class'=>'form-control','placeholder'=>'')); ?>
                        <?php echo $form->error($model,'name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'value'); ?>
                        <?php echo $form->numberField($model,'value',array('maxlength'=>20,'class'=>'form-control','required'=>'required')); ?>
                        <?php echo $form->error($model,'value'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'date'); ?>
                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                          array('attribute'=>'date',
                                'model'=>$model,
                                'options' => array(
                                  'mode'=>'focus',
                                  'dateFormat'=>'yy-mm-dd',
                                  'showAnim' => 'slideDown',
                                ),
                                'htmlOptions'=>array('size'=>30,'class'=>'form-control','required'=>'required'),
                              ));?>
                        <?php echo $form->error($model,'date'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'note'); ?>
                        <?php echo $form->textArea($model,'note',array('class'=>'form-control')); ?>
                        <?php echo $form->error($model,'note'); ?>
                      </div>
                      <hr>
                      <p><input type="submit" value="Submit" class="fixed-button" /></p>
                      <br>
                    <?php $this->endWidget(); ?>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>