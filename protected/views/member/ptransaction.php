                                <!-- Breadcrumb Start -->
	<div class="breadcrumb-standart main-color">
		<div class="container">
			<h2><?=$this->title;?></h2>
		</div>
		<hr>
		<div class="container breadcrumb-link">
			<ul>
				<li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
                                <li><?=CHtml::link('<span><span>Profil</span></span>',['/member']);?></li>
				<li><a class="active" href="#"><?=$this->title;?></a></li>                  
			</ul>
		</div>
	</div>
	<!-- Breadcrumb End -->
                                    <div class="site-content">
					<div class="container">
						<div class="row marginb60 margint60">
							<div class=" col-sm-9 col-md-9 col-lg-9">
			    				<div class="title-line">
                                                                <h4><?=$this->title;?></h4>
                                                                <hr>
                                                        </div>
                                                        <div class="table-responsive">
                                                        <table class="table table-bordered table-striped"">
							<tbody><tr>
								<th class="center-text">#</th>
                                                                <th>Tanggal</th>
								<th>Kode Transaksi</th>
								<th>Nama Transaksi</th>
                                                                <th>Total</th>
                                                                <th>No Pelanggan/MSISDN</th>
                                                                <th>Token</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
							</tr>
                                                        <?php if($model):?>
                                                        <?php foreach($model as $row):?>
							<tr>
								<td class="width-10"><?= $i;?></td>
                                                                <td><?= $row->posted;?></td>
								<td class="center"><?= $row->code;?></td>
                                                                <td><?= $row->name;?></td>
                                                                <td class="right-text"><?= number_format($row->total);?></td>
                                                                <td><?= $row->msisdn;?></td>
                                                                <td><?= $row->token;?></td>
								<td class="center"><?= $row->statusname;?></td>
                                                                <td class="center"><?= CHtml::link('Print',['print','id'=>$row->id],['target'=>'new']);?></td>
							</tr>
                                                        <?php $i++; endforeach;?>
							<?php else:?>
                                                        <tr>
                                                            <td colspan="7">Anda belum melakukan transaksi pulsa &amp; PPOB.</td>
                                                        </tr>
                                                        <?php endif;?>
							</tbody>
						</table>
                                                        </div>
                                                    <?php if($count):?>
                                                    <div class="pagination right">
                                                    <?php 
                                                    $this->widget('CLinkPager', array(
                                                        'pages' => $pages,
                                                        'header'=>'',
                                                        'footer'=>'',
                                                        'nextPageLabel'=>'»',
                                                        'prevPageLabel'=>'«',
                                                        'id'=>'link_pager',
                                                    ))?>
                                                    </div>
                                                    <?php endif;?>
    			    			</div>
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                    
                                <?php $this->widget('MemberPanelWidget');?>
                    </div>
		    			
						</div>
                                                </div>
					</div>
				</div>