<!-- Page Title -->
<section class="breadcrumb-wrap">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?=$this->title;?></h1>
        <ul class="breadcrumb">
          <li><?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Home',Yii::app()->homeurl);?></li>
          <li><?=CHtml::link('Profil',['/member']);?></li>
          <li class="last"><?=$this->title;?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- Contact Page 02 -->
<section class="page-contact02 mgb20">
  <div class="container">
    <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-12">           
              <div class="brochure">   
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                  <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'register-form',
                    'enableClientValidation'=>true,
                    'enableAjaxValidation'=>true,
                    'clientOptions'=>array(
                      'validateOnSubmit'=>true,
                    ),
                  )); ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <h4>Data Pribadi</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('maxlength'=>100,'placeholder'=>'Nama Lengkap','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'identity_number'); ?>
                        <?php echo $form->textField($model,'identity_number',array('maxlength'=>50,'placeholder'=>'Nomor KTP/SIM/Pass','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'identity_number'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'gender'); ?>
                        <?php echo $form->dropDownList($model,'gender',Member::genderList(),['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'gender'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'birth_place'); ?>
                        <?php echo $form->textField($model,'birth_place',array('maxlength'=>50,'placeholder'=>'Tempat Lahir','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'birth_place'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'birth_date'); ?>
                        <?php echo $form->dateField($model,'birth_date',array('placeholder'=>'Tanggal Lahir','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'birth_date'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'address_1'); ?>
                        <?php echo $form->textArea($model,'address_1',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'address_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'address_2'); ?>
                        <?php echo $form->textArea($model,'address_2',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'address_2'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'mobile_phone'); ?>
                        <?php echo $form->textField($model,'mobile_phone',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'mobile_phone'); ?>
                      </div>
                      <div class="input-text">
                          <?php echo $form->labelEx($model,'phone'); ?>
                          <?php echo $form->textField($model,'phone',['maxlength'=>20,'class'=>'form-control']); ?>
                          <?php echo $form->error($model,'phone'); ?>
                      </div>
                      <h4>Data Pekerjaan</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work'); ?>
                        <?php echo $form->textField($model,'work',['maxlength'=>50,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_time'); ?>
                        <?php echo $form->textField($model,'work_time',['maxlength'=>50,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_time'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_address'); ?>
                        <?php echo $form->textField($model,'work_address',['maxlength'=>100,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_address'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_phone'); ?>
                        <?php echo $form->textField($model,'work_phone',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_phone'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'npwp'); ?>
                        <?php echo $form->textField($model,'npwp',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'npwp'); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <h4>Data Keluarga Yang Dapat Dihubungi</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_name'); ?>
                        <?php echo $form->textField($model,'family_name',array('maxlength'=>100,'placeholder'=>'Nama Lengkap','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'family_name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_address'); ?>
                        <?php echo $form->textArea($model,'family_address',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'family_address'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_phone'); ?>
                        <?php echo $form->textField($model,'family_phone',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'family_phone'); ?>
                      </div>
                      <h4>Data Keluarga</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'mother_name'); ?>
                        <?php echo $form->textField($model,'mother_name',array('maxlength'=>100,'class'=>'form-control','placeholder'=>'Nama Lengkap Ibu Kandung')); ?>
                        <?php echo $form->error($model,'mother_name'); ?>
                      </div>
                      <h4>Data Ahli Waris</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_name_1'); ?>
                        <?php echo $form->textField($model,'heir_name_1',array('maxlength'=>100,'placeholder'=>'Nama Ahli Waris 1','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'heir_name_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_relationship_as_1'); ?>
                        <?php echo $form->textField($model,'heir_relationship_as_1',array('maxlength'=>100,'placeholder'=>'Hubungan Ahli Waris 1','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'heir_relationship_as_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_place_1'); ?>
                        <?php echo $form->textField($model,'heir_birth_place_1',array('maxlength'=>50,'class'=>'form-control','placeholder'=>'Tempat Lahir Ahli Waris 1')); ?>
                        <?php echo $form->error($model,'heir_birth_place_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_date_1'); ?>
                        <?php echo $form->dateField($model,'heir_birth_date_1',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'heir_birth_date_1'); ?>
                      </div>
                      
                    </div>
                    <div class="col-sm-12">
                      <div class="input-text">
                        <input class="fixed-send animated" type="submit" value="Simpan">
                      </div>
                    </div>
                  </div>
                  <?php $this->endWidget(); ?> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <aside class="sidebar">
        <?php $this->widget('MemberPanelWidget');?>
      </aside>
    </div>
  </div>
</section>