<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><?=CHtml::link('Ewallet',['/member/ewallet']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="col-sm-6">
                    <div class="contact-form">
                        <?= Notify::renderMflash();?>
                        <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                            <h3>Tarik Deposit Ewallet</h3><hr>
                            <div class="input-text">
                                <label>Biaya Administrasi</label>
                                <?=$this->g('application','biaya_administrasi');?>%
                                <?php echo $form->error($model,'value'); ?>
                            </div>
                            <div class="input-text">
                                <label>Jumlah Deposit<span class="red">*</span></label>
                                <?php echo $form->numberField($model,'value',
                                    array(
                                    'class'=>'form-control',
                                    'required'=>'required',
                                    )
                                ); ?><em>* Minimal deposit Rp 50.000</em>
                                <?php echo $form->error($model,'value'); ?>
                            </div><hr>
                            <div class="input-text">
                                <button class="btn btn-button gray9-bg white">Submit</button>
                            </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>