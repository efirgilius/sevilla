<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <nav>
          <ol class="breadcrumb">
            
            <li><?php echo CHtml::link('Home',Yii::app()->homeUrl);?></li>
            
            <li class="active">Member</li>
            
            <li><?php echo CHtml::link('Profile',array('profile'));?></li>
            
            <li class="active">Bank Account Change Request</li>
            
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-xs-12">
          <h1 class="mo-margin-top"><span class="light">Bank Account</span> Change Request</h1> 
          
      </div>
      
    </div>
</div>

<div class="container  push-down-30">
    
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            
            <?php if($valid==true):?>
                <p>Please enter your new bank account.</p>
                <?php else:?>
                <p>Please enter your password.</p>
                <?php endif;?>
            
            <div class="row">
                <div class="col-xs-12 col-sm-7  push-down-30">
                        
                
            
                    <?php Setting::renderFlash();?>
                    <?php if($valid==true):?>
                    <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_bank">Bank Name <span class="warning">*</span></label>
                            <?php echo $form->textField($model,'bank',array('maxlength'=>30,'class'=>'form-control  form-control--contact','placeholder'=>'Bank name','required'=>'required')); ?>
                            <?php echo $form->error($model,'bank'); ?>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_account_number">Account Number <span class="warning">*</span></label>
                            <?php echo $form->textField($model,'account_number',array('maxlength'=>30,'class'=>'form-control  form-control--contact','placeholder'=>'Account number','required'=>'required')); ?>
                            <?php echo $form->error($model,'account_number'); ?>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_account_name">Account Name <span class="warning">*</span></label>
                            <?php echo $form->textField($model,'account_name',array('maxlength'=>30,'class'=>'form-control  form-control--contact','placeholder'=>'Account name','required'=>'required')); ?>
                            <?php echo $form->error($model,'account_name'); ?>
                        </div>
                        <div class="right">
                            <?php echo CHtml::link('Cancel',array('profile'),array('class'=>'btn btn-danger'));?>&nbsp;<button type="submit" class="btn  btn-primary">Submit</button>
                          </div>
                    <?php $this->endWidget(); ?>
                    <?php else:?>
                        <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                    <?php echo $form->errorSummary($model);?>        
                    <div class="form-group">
                                <label class="text-dark" for="CardForm_password">Password <span class="warning">*</span></label>
                                <?php echo $form->passwordField($card,'password',array('maxlength'=>30,'class'=>'form-control  form-control--contact','placeholder'=>'Password','required'=>'required')); ?>
                                <?php echo $form->error($card,'password'); ?>
                            </div>
                            <?php if(extension_loaded('gd')): ?>
                                <div class="form-group">
                                    <?php $this->widget('CCaptcha'); ?>
                                                                          <?php echo $form->textField($card,'verifyCode',array('maxlength'=>7,'class'=>'form-control  form-control--contact','placeholder'=>'Captcha')); ?>
                                                                              <?php echo $form->error($card,'verifyCode'); ?>
                                </div>
                              <?php endif;?>
                              <div class="right">
                            <?php echo CHtml::link('Cancel',array('profile'),array('class'=>'btn btn-danger'));?>&nbsp;<button type="submit" class="btn  btn-primary">Submit</button>
                          </div>
                        <?php $this->endWidget(); ?>  
                        
                    <?php endif;?>
                </div>
            </div>
            
        </div>
        <?php $this->renderPartial('_partial');?>
    </div>
    
</div>