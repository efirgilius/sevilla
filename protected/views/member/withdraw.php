                                <div class="page-title title-1">
					<div class="container">
						<div class="row">
							<div class="cell-12">
                                                            <h1 class="fx" data-animate="fadeInLeft">Bonus <span>Jaringan</span></h1>
								<div class="breadcrumbs main-bg fx" data-animate="fadeInUp">
									<?= CHtml::link('Home',Yii::app()->homeUrl);?>
                                                                        <span class="line-separate">/</span>
                                                                        <?= CHtml::link('Profil &amp; Jaringan',['profile']);?>
                                                                        <span class="line-separate">/</span>
                                                                        <?= CHtml::link('Sharing Profit',['sharebonus']);?>
                                                                        <span class="line-separate">/</span>
                                                                        Penarikan Bonus
								</div>
							</div>
						</div>
					</div>
				</div>
                                    <div class="sectionWrapper">
					<div class="container">
						<div class="row">
							<div class="cell-9 contact-form fx" data-animate="fadeInLeft">
			    				<h3 class="block-head">Penarikan Bonus</h3>
                                                        <?php Setting::renderFlash();?>
                                                        <div class="row">
                                                    <div class="cell-4">
                                                        <div class="box info-box fx animated fadeInLeft" data-animate="fadeInLeft">
                                                            <h3>Kredit: <?php echo number_format($credit);?></h3>
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="cell-4">
                                                        <div class="box warning-box fx animated fadeInLeft" data-animate="fadeInLeft">
							<h3>Penarikan: <?php echo number_format($debit);?></h3>
						</div>
                                                    </div>
                                                    <div class="cell-4">
                                                        <div class="box success-box fx animated fadeInLeft" data-animate="fadeInLeft">
							<h3>Saldo: <?php echo number_format($balance);?></h3>
						</div>
                                                    </div>
                                                
                                                    
                                                    </div>
                                                        <div class="row">
                                                            <div class="cell-6">
                                                                <?php $form=$this->beginWidget('CActiveForm', array(
                                                            'id'=>'validation',
                                                            'enableAjaxValidation'=>false,
                                                    )); ?>
                                                                <div class="form-input">
				    					<label>Jumlah Penarikan<span class="red">*</span></label>
				    					<?php echo $form->numberField($model,'value',array('min'=>50000,'required'=>'required')); ?>
                                                                        <?php echo $form->error($model,'value'); ?>
			    					</div>   
                                                                <?php if(extension_loaded('gd')): ?>
                                                                <div class="form-input">
                                                                        <label>Kode Verifikasi<span class="red">*</span></label>
                                                                        <?php $this->widget('CCaptcha'); ?><br>

                                                                        <br><label class="normLabel">Silahkan isi huruf atau kata yang muncul pada gambar di atas.</label><br>
                                                                        <?php echo $form->textField($model,'verifyCode',array('class'=>'name','required'=>'required')); ?>
                                                                            <?php echo $form->error($model,'verifyCode'); ?>
                                                                </div>
                                                                <?php endif;?>
			    					<div class="form-input">
			    						<input type="submit" class="btn btn-large main-bg" value="Submit">&nbsp;&nbsp;<input type="reset" class="btn btn-large" value="Reset" id="reset">
			    					</div>
                                                                <p><em>* Biaya administrasi penarikan akan dipotong dari jumlah penarikan.</em></p>
                                                    <?php $this->endWidget(); ?>
                                                            </div>
                                                        </div>
                                                        <table class="table-style2">
							<tbody><tr>
								<th class="center-text">#</th>
								<th>Tanggal</th>
								<th>Jumlah Penarikan</th>
                                                                <th>Status</th>
							</tr>
                                                        <?php if($withdrawals):?>
                                                        <?php $i=1;foreach($withdrawals as $row):?>
							<tr>
								<td class="width-10"><?= $i;?></td>
								<td><?= $row->posted;?></td>
                                                                <td class="right-text"><?= number_format($row->value);?></td>
                                                                <td><?= $row->statusname;?></td>
							</tr>
                                                        <?php $i++; endforeach;?>
							<?php else:?>
                                                        <tr>
                                                            <td colspan="5">Penarikan bonus masih kosong.</td>
                                                        </tr>
                                                        <?php endif;?>
							</tbody>
						</table>
                                                        <?php if($count):?>
                                                    <div class="pagination right">
                                                    <?php 
                                                    $this->widget('CLinkPager', array(
                                                        'pages' => $pages,
                                                        'header'=>'',
                                                        'footer'=>'',
                                                        'nextPageLabel'=>'»',
                                                        'prevPageLabel'=>'«',
                                                        'id'=>'link_pager',
                                                    ))?>
                                                    </div>
                                                    <?php endif;?>
    			    			</div>
                                                <?php $this->widget('RightWidget');?>  
		    			
						</div>
                                                </div>
					</div>
				</div>