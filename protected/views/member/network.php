<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li class="last"><?=$this->title;?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <h4>Genealogy Jaringan Sponsor</h4>
                    <hr>
                    <?php Notify::renderMflash();?>
                    <?php if(!isset($member)):?>
                    <div class="push-down-30">
                        <select id="selectbox" name="" onchange="javascript:location.href = this.value;" class="form-control  form-control--contact">
                            <?php for($x=1;$x<=10;$x++):?>
                                <option value="<?php echo Yii::app()->createUrl('/member/network',array('level'=>$x));?>" <?php echo ($level==$x)?'selected':'';?>>
                                    Level <?php echo $x;?>
                                </option>
                            <?php endfor;?>
                        </select>
                    </div>
                    <?php else:?>
                    <div class="my-img">
                        <div class="my-details">
                            <h4 class="bold main-color my-name fx" data-animate="slideInDown"><?php echo $member->name;?> / <?php echo $member->mid;?></h4>
                            <ul class="list alt list-bookmark cell-4">
                                <li class="fx" data-animate="slideInDown" data-animation-delay="200">Tipe Member <strong><?php echo $member->typename;?></strong></li>
                                <li class="fx" data-animate="slideInDown">Alamat <strong><?php echo $member->address_1;?></strong></li>
                                <li class="fx" data-animate="slideInDown" data-animation-delay="100">No. Telepon <strong><?php echo $member->mobile_phone;?></strong></li>
                            </ul>
                            
                            <ul class="list alt list-bookmark cell-4">
                                <li class="fx" data-animate="slideInDown" data-animation-delay="600">Email <strong><?php echo $member->email;?></strong></li>
                                <li class="fx" data-animate="slideInDown" data-animation-delay="800">Status <strong><?php echo $member->statusname;?></strong></li>
                                <li class="fx" data-animate="slideInDown" data-animation-delay="1000">Direct Sponsor <strong><?php echo strtoupper($member->sponsor->mid);?></strong></li>
                            </ul>
                        </div>
                    </div>
                    <?php echo ($member->id!==Yii::app()->member->id)?CHtml::link('Back to Upline',array('network','id'=>$member->sponsor_id)):'';?><br><br>
                    <?php endif;?>
                    <?php if($model):?>
                    <p>Total anggota di level ini: <strong><?php echo $count;?> anggota</strong></p>
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Member ID</th>
                            <th>Name</th>
                            <th>Jumlah yang Disponsori</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($model as $row):?>
                          <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo CHtml::link($row->member->mid,array('network','id'=>$row->member->id));?></td>
                            <!--<td><?php echo $row->member->mid;?></td>-->
                            <td><?php echo $row->member->name;?></td>
                            <td><?php echo count($row->member->members);?></td>
                            <td><?php echo $row->member->statusname;?></td>
                          </tr>
                          <?php $i++; endforeach;?>
                        </tbody>
                    </table>
                    <?php if($count):?>
                    <div class="pagination right">
                    <?php 
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header'=>'',
                        'footer'=>'',
                        'nextPageLabel'=>'»',
                        'prevPageLabel'=>'«',
                        'id'=>'link_pager',
                    ))?>
                    </div>
                    <?php endif;?>
                    <?php else:?>
                        <p>Genealogy Jaringan Sponsor masih kosong</p>
                    <?php endif;?>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>