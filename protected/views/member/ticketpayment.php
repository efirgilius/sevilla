<div class="breadcrumb-standart main-color">
    <div class="container">
        <h2><?=$this->title;?></h2>
    </div>
    <hr>
    <div class="container breadcrumb-link">
        <ul>
            <li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
            <li><?=CHtml::link('<span><span>Profil Member</span></span>',['/member']);?></li>
            <li><a class="active" href="#">Konfirmasi Pembayaran Tiket</a></li>                  
        </ul>
    </div>
</div>
<div class="site-content">
<section class="container">
    <div class="main-container col2-right-layout">
        <div class="main margint60 marginb60">
            <div class="row">
                <div class=" col-sm-9 col-md-9 col-lg-9">
                    <div class="col-main">
                        <?= Notify::renderMflash();?>
                        <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm','class'=>'account-form-box'))); ?>
                            <div class="title-box">
                                <h2>Konfirmasi Pembayaran</h2>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Biaya:</label>
                                    <?php echo $form->textField($model,'value',array('maxlength'=>100,'class'=>'form-control','readonly'=>'readonly')); ?>
                                    <?php echo $form->error($model,'value'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Ke Bank:</label>
                                        <?php echo $form->dropDownList($model,'bank_receiver',  CHtml::listData(Bank::model()->findAll(),'name','name'),array(
                                            'class'=>'form-control','data-md-selectize'=>1,
                                            'required'=>'required',
                                            )); ?>
                                        <?php echo $form->error($model,'bank_receiver'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Dari Bank:</label>
                                        <?php echo $form->dropDownList($model,'bank_sender', Member::bankList(),array(
                                            'class'=>'form-control','data-md-selectize'=>1,
                                            'required'=>'required',
                                            )); ?>
                                        <?php echo $form->error($model,'bank_sender'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Pemilik Rekening:</label>
                                        <?php echo $form->textField($model,'account_name',array('maxlength'=>100,'class'=>'form-control input','required'=>'required')); ?>
                                        <?php echo $form->error($model,'account_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Tanggal Pembayaran:</label>
                                                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                        'model'=>$model,
                                                        'attribute'=>'date',
                                                        // additional javascript options for the date picker plugin
                                                        'options'=>array(
                                                            'showAnim'=>'fold',
                                                            'dateFormat' => 'yy-mm-dd',
                                                            'yearRange'=>'-1:0',
                                                            'changeYear'=>'true',
                                                            'changeMonth'=>'true',
                                                        ),
                                                        'htmlOptions'=>array(
                                                            'class'=>'form-control input','required'=>'required'
                                                        ),
                                                    ));?>
                                                    <?php echo $form->error($model,'date'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="button-set">
                                        <button class="btn btn-button gray9-bg white">Submit</button>
                                    </div>
                                </div>
                            </div>
                        <?php $this->endWidget(); ?>
                                    </div>
                </div>
                <!--  left side -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    
                                <?php $this->widget('MemberPanelWidget');?>
                    </div>
                </div>
                <!-- / left side-->
            </div>
        </div>
    </div>
</section>
        </div>