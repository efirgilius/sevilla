<!-- Breadcrumb Start -->
    <div class="breadcrumb-standart main-color">
        <div class="container">
            <h2><?=$this->title;?></h2>
        </div>
        <hr>
        <div class="container breadcrumb-link">
            <ul>
                <li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
                <li><?=CHtml::link('<span><span>Profil</span></span>',['/member']);?></li>
                <li><a class="active" href="#">Stock Tiket</a></li>                  
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->
        <div class="site-content">
            <section class="container">
                <div class="main-container col2-right-layout">
                    <div class="main margint60 marginb60">
                        <div class="row">
                            <div class=" col-sm-9 col-md-9 col-lg-9">
                                <?php Notify::renderMFlash();?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= CHtml::link('<button class="fixed-send animated">Pesan Tiket</button>',['requestTicket']);?>
											<?php if($hvUpline):?>
                                            <?= CHtml::link('<button class="fixed-send animated">Pesan Tiket Ke Upline</button>',['requestTicketToUpline']);?>
											<?php endif;?>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="box info-box fx animated fadeInLeft" data-animate="fadeInLeft">
                                                <h3>Kredit: <?php echo number_format($member->ticket_credit);?></h3>
                                            
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="box warning-box fx animated fadeInLeft" data-animate="fadeInLeft">
                                                <h3>Debit: <?php echo number_format($member->ticket_debit);?></h3>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="box success-box fx animated fadeInLeft" data-animate="fadeInLeft">
                                                <h3>Balance: <?php echo number_format(($member->ticket_credit - $member->ticket_debit));?></h3>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <th class="center-text">#</th>
                                            <th>Dari/Ke</th>
                                            <th>Jumlah</th>
                                            <th>Harga (Rp)</th>
                                            <th>Tipe</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php if($model):?>
                                        <?php $i=1;foreach($model as $row):?>
                                        <tr>
                                            <td class="width-10"><?= $i;?></td>
                                            <td><?=($row->type==1)?$row->upline->mid:'Perusahaan';?></td>
                                            <td><?= $row->value;?></td>
                                            <td><?= number_format($row->price);?></td>
                                            <td><?= $row->created_on;?></td>
                                            <td><?= $row->statusname;?></td>
                                            <td>
                                                <?php if ($row->upline_id==null and $row->status==0):?>
                                                    <?=CHtml::link('Cancel',['cancelrequest','id'=>$row->id]);?>
                                                    <?=CHtml::link('<button class="fixed-send animated">Konfirmasi Pembayaran</button>',['ticketpayment','id'=>$row->id]);?>
                                                <?php endif;?>
                                                
                                                <?php if ($row->upline_id==Yii::app()->member->id and $row->status==0):?>
                                                    <?=CHtml::link('<button class="conform fixed-send animated">Konfirmasi</button>',['ticketapprove','id'=>$row->id]);?>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                        <?php $i++; endforeach;?>
                                        <?php else:?>
                                        <tr>
                                            <td colspan="8">Anda belum punya tiket.</td>
                                        </tr>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                    </div>
                                    <?php if($count):?>
                                    <div class="pagination right">
                                    <?php 
                                    $this->widget('CLinkPager', array(
                                        'pages' => $pages,
                                        'header'=>'',
                                        'footer'=>'',
                                        'nextPageLabel'=>'»',
                                        'prevPageLabel'=>'«',
                                        'id'=>'link_pager',
                                    ))?>
                                    </div>
                                    <?php endif;?>
                            </div>
                            <!--  left side -->
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                
                                            <?php $this->widget('MemberPanelWidget');?>
                                </div>
                            </div>
                            <!-- / left side-->
                        </div>
                    </div>
                </div>
            </section>
        </div>

<?php Yii::app()->clientScript->registerScript('delete-d', "
    
$('.conform').on('click',function(){
        if(!confirm('Are you sure want to PROCESS this request? It means this member has transfered to your bank account.')) return false;
    });
	
",CClientScript::POS_READY);?>