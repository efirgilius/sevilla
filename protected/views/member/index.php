<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li class="last">Profil</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <?= Notify::renderMflash();?>
                    <?php if($model->status=='2'):?>
                    <div class="alert alert-danger">
                        <p>Keanggotaan member Anda belum aktif, silahkan melakukan pembayaran sebesar Rp. <?=number_format($model->fee);?> untuk mengaktifkannya ke salah satu No. Rekening di bawah ini.<br>
                        <?php $banks = Bank::model()->findAll();?>
                        <?php if($banks):?>
                        <div class="col-sm-12" style="margin-bottom: 5px;">
                                <div class="row">
                            <?php foreach($banks as $b):?>
                            <div class="col-sm-3">
                                <strong><?= $b->name;?></strong><br>
                                <span><?= $b->acc_number;?></span><br>
                                <span><?= $b->acc_name;?></span><br>
                            </div>
                            <?php endforeach;?>
                            
                                </div>
                            </div>
                        <?php endif;?>
                        <br>
                        Apabila Anda telah melakukan pembayaran silahkan klik <u><?= CHtml::link('link ini',['/member/paymentConfirmation','token'=>$model->tokencode]);?></u> melakukan konfirmasi pembayaran keanggotaan Anda.</p>
                    </div>
                    <?php endif;?>
                    <?php if($model->status=='3'):?>
                    <div class="alert alert-info">
                        <p>Konfirmasi pembayaran Anda sedang diproses.<br>Jika pembayaran Anda terverifikasi, maka secara otomatis akun member Anda akan aktif.</p>
                    </div>
                    <?php endif;?>
                    <?php if($model->status=='4'):?>
                    <div class="alert alert-info">
                        <?php if($model->isupgrade->payment_type=='1'):?>
                        <p>Keanggotaan AGENT Anda belum aktif, silahkan lakukan pembayaran sebesar Rp. <?= number_format($model->isupgrade->value);?>.<br>Metode pembayaran yang Anda pilih dengan pembayaran online, silahkan klik <?= CHtml::link('link',['/payment/pay','token'=>$model->upgradecode]);?> ini untuk melakukan pembayaran.<br>Jika pembayaran Anda terverifikasi, maka secara otomatis keanggotaan AGENT Anda akan aktif.</p>
                        <?php else:?>
                        <p>Keanggotaan AGENT Anda belum aktif, silahkan lakukan pembayaran dan lakukan konfirmasi melalui <?= CHtml::link('link',['/member/upgradeConfirmation']);?> ini.<br>Jika pembayaran Anda terverifikasi, maka secara otomatis keanggotaan AGENT Anda akan aktif.</p>
                        <?php endif;?>
                    </div>
                    <?php endif;?>
                    <div class="table-responsive">
                        <div class="title-line">
                                    <h4>Data Pribadi</h4>
                                    <hr>
                            </div>
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-7">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Sponsor<strong></td><td><?= ($model->sponsor_id!==NULL)?'('.$model->sponsor->mid.') '.$model->sponsor->name:'-';?></td>
                                 </tr>

                                <tr>
                                    <td><strong>ID Member<strong></td><td><?= $model->mid;?></td>
                                </tr>
                                <tr>
                                    <td><strong>No. KTP<strong></td><td><?= $model->identity_number;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nama Lengkap<strong></td><td><?= $model->name;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Jenis Kelamin<strong></td><td><?= $model->gendername;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Tempat dan Tanggal Lahir<strong></td><td><?= $model->birth_place;?>, <?= $model->birth_date;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Alamat Rumah<strong></td><td><?= $model->address_1;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Alamat Surat Menyurat<strong></td><td><?= $model->address_2;?></td>
                                </tr>
                                <tr>
                                    <td><strong>No. Hp<strong></td><td><?= $model->mobile_phone;?></td>
                                </tr>
                                <tr>
                                    <td><strong>No. Telp<strong></td><td><?= $model->phone;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Email<strong></td><td><?= $model->email;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="title-line">
                                    <h4>Data Pekerjaan/Usaha</h4>
                                    <hr>
                            </div>
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-7">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Pekerjaan/Perusahaan<strong></td><td><?= $model->work;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Masa Kerja/Usaha<strong></td><td><?= $model->work_time;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Alamat Kerja<strong></td><td><?= $model->work_address;?></td>
                                </tr>
                                <tr>
                                    <td><strong>No. Telp. Kantor<strong></td><td><?= $model->work_phone;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="title-line">
                                    <h4>Data Keluarga Yang Bisa Dihubungi</h4>
                                    <hr>
                            </div>
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-7">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Nama<strong></td><td><?= $model->family_name;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Alamat Rumah<strong></td><td><?= $model->family_address;?></td>
                                </tr>
                                <tr>
                                    <td><strong>No. Telp/HP<strong></td><td><?= $model->family_phone;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="title-line">
                                    <h4>Data Ahli Waris</h4>
                                    <hr>
                            </div>
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-7">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Ahli Waris 1<strong></td><td><?= $model->heir_name_1;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Hubungan Sebagai<strong></td><td><?= $model->heir_relationship_as_1;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Tempat dan Tanggal Lahir<strong></td><td><?= $model->heir_birth_place_1;?>, <?= $model->heir_birth_date_1;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="title-line">
                                    <h4>Data Bank</h4>
                                    <hr>
                            </div>
                        <?= CHtml::link('Ubah Akun Bank?',['updatebank'],['class'=>'button']);?>
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-7">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Nama Bank<strong></td><td><?= $model->bank_name;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Cabang/Kota<strong></td><td><?= $model->bank_branch;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nomor Rekening<strong></td><td><?= $model->bank_acc_number;?></td>
                                </tr>
                                <tr>
                                    <td><strong>Pemilik Rekening<strong></td><td><?= $model->bank_acc_name;?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>