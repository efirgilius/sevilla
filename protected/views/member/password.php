<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li class="last"><?=CHtml::link('Profil',['/member']);?></li>
                    <li class="last">Ganti Password</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contact-form">
                                <?= Notify::renderMflash();?>
                                <p>Silahkan isi form ubah password di bawah ini dengan benar</p>
                                <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'forgot-form',
                                'enableClientValidation'=>true,
                                'enableAjaxValidation'=>true,
                                'clientOptions'=>array(
                                  'validateOnSubmit'=>true,
                                ))); ?>  
                                <?php Notify::renderMflash();?>
                                <div class="input-text">
                                <?php echo $form->labelEx($model,'oldpassword'); ?>
                                <?php echo $form->passwordField($model,'oldpassword',array('class'=>'form-control','required'=>'required')); ?>
                                <?php echo $form->error($model,'oldpassword'); ?>
                                </div>
                                <div class="input-text">
                                <?php echo $form->labelEx($model,'password'); ?>
                                <?php echo $form->passwordField($model,'password',array('maxlength'=>50,'class'=>'form-control input','required'=>'required')); ?>
                                <?php echo $form->error($model,'password'); ?>
                                </div>
                                <div class="input-text">
                                <?php echo $form->labelEx($model,'verifypassword'); ?>
                                <?php echo $form->passwordField($model,'verifypassword',array('maxlength'=>50,'class'=>'form-control input','required'=>'required')); ?>
                                <?php echo $form->error($model,'verifypassword'); ?>
                                </div>
                                <div class="input-text">
                                <input type="submit" value="Simpan">
                                </div>
                                <?php $this->endWidget(); ?>
                            </div>  
                        </div>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>