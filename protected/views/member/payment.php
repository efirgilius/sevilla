<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="col-sm-6">
                    <div class="contact-form">
                        <?= Notify::renderMflash();?>
                        <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                            <h3>Konfirmasi Pembayaran</h3><hr>
                            <div class="input-text">
                                <label>Biaya:</label>
								<?php echo $form->textField($model,'value',array('maxlength'=>100,'class'=>'form-control','readonly'=>'readonly')); ?>
								<?php echo $form->error($model,'value'); ?>
                            </div>
                            <div class="input-text">
                                <label>Ke Bank:</label>
								<?php echo $form->dropDownList($model,'bank_receiver',  CHtml::listData(Bank::model()->findAll(),'name','name'),array(
									'class'=>'form-control','data-md-selectize'=>1,
									'required'=>'required',
									)); ?>
								<?php echo $form->error($model,'bank_receiver'); ?>
                            </div>
							<div class="input-text">
                                <label>Dari Bank:</label>
								<?php echo $form->dropDownList($model,'bank_sender', Member::bankList(),array(
									'class'=>'form-control','data-md-selectize'=>1,
									'required'=>'required',
									)); ?>
								<?php echo $form->error($model,'bank_sender'); ?>
                            </div>
							<div class="input-text">
								<label>Pemilik Rekening:</label>
								<?php echo $form->textField($model,'account_name',array('maxlength'=>100,'class'=>'form-control input','required'=>'required')); ?>
								<?php echo $form->error($model,'account_name'); ?>
							</div>
							<div class="input-text">
								<label>Tanggal Pembayaran:</label>
								<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
									'model'=>$model,
									'attribute'=>'date',
									// additional javascript options for the date picker plugin
									'options'=>array(
										'showAnim'=>'fold',
										'dateFormat' => 'yy-mm-dd',
										'yearRange'=>'-1:0',
										'changeYear'=>'true',
										'changeMonth'=>'true',
									),
									'htmlOptions'=>array(
										'class'=>'form-control input','required'=>'required'
									),
								));?>
								<?php echo $form->error($model,'date'); ?>
							</div>
							<hr>
                            <div class="input-text">
                                <button class="btn btn-button gray9-bg white">Submit</button>
                            </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>