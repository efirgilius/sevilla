<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li class="last"><?=$this->title;?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <h4>Bonus Generasi Jaringan</h4>
                    <hr>
                    <p>Total bonus generasi jaringan sampai dengan saat ini adalah sebesar <strong>IDR <?= number_format(BonusSponsor::model()->getTotal());?></strong></p>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID Member yang Dsponsori</th>
                                <th>Nama</th>
                                <th>Tanggal</th>
                                <th>Bonus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($model):?>
                                        <?php foreach($model as $row):?>
                                        <tr>
                                            <td width="2%"><?php echo $i;?></td>
                                            <td width="23%"><?php echo $row->from->mid.' ('.$row->from->levelname.')';?></td>
                                            <td><?php echo $row->from->name;?></td>
                                            <td><?php echo $row->date;?></td>
                                            <td><span style="float:right;">IDR <?php echo number_format($row->bonus,2,",",".");?></span></td>
                                        </tr>
                                        <?php $i++; endforeach;?>
                                        <?php else:?>
                                        <tr>
                                            <td colspan="5"><p>Bonus generasi jaringan untuk periode <?php echo Setting::getMonth($m).' '.$y;?> tidak tersedia.</p></td>
                                        </tr>
                                        <?php endif;?>

                        </tbody>
                    </table>
                    <?php if($count):?>
                    <div class="pagination right">
                    <?php 
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header'=>'',
                        'footer'=>'',
                        'nextPageLabel'=>'»',
                        'prevPageLabel'=>'«',
                        'id'=>'link_pager',
                    ))?>
                    </div>
                    <?php endif;?>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>