<div class="breadcrumb-standart main-color">
    <div class="container">
        <h2><?=$this->title;?></h2>
    </div>
    <hr>
    <div class="container breadcrumb-link">
        <ul>
            <li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
            <li><?=CHtml::link('<span><span>Profil Member</span></span>',['/member']);?></li>
            <li><a class="active" href="#">Pesan Tiket</a></li>                  
        </ul>
    </div>
</div>
<div class="site-content">
<section class="container">
    <div class="main-container col2-right-layout">
        <div class="main margint60 marginb60">
            <div class="row">
                <div class=" col-sm-9 col-md-9 col-lg-9">
                    <div class="col-main">
                        <?= Notify::renderMflash();?>
                        <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm','class'=>'account-form-box'))); ?>
                            <div class="title-box">
                                <h2>Pesan Tiket</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Biaya Tiket</label>
                                        <p><?=$this->g('application','biaya_tiket');?></p>
                                    </div>
                                    <?php if (isset($upline)):?>
                                        <div class="form-group">
                                            <label>Upline<span class="red">*</span></label>
                                            <?php echo $form->radioButtonList($model,'upline_id',CHtml::listData($upline,'upline_id','upline.name'),
                                                array('required'=>'required')); ?>
                                            <?php echo $form->error($model,'upline_id'); ?>
                                        </div>
									<?php else:?>
									<p>Anda tidak mempunyai upline.</>
                                    <?php endif;?>
                                    <div class="form-group">
                                        <label>Jumlah Tiket<span class="red">*</span></label>
                                        <?php echo $form->numberField($model,'value',
                                            array(
                                            'class'=>'form-control','data-md-selectize'=>1,
                                            'required'=>'required',
                                            )
                                        ); ?><em>* Minimal pemesanan 1</em>
                                        <?php echo $form->error($model,'value'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="button-set">
                                        <button class="btn btn-button gray9-bg white">Submit</button>
                                    </div>
                                </div>
                            </div>
                        <?php $this->endWidget(); ?>
                                    </div>
                </div>
                <!--  left side -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    
                                <?php $this->widget('MemberPanelWidget');?>
                    </div>
                </div>
                <!-- / left side-->
            </div>
        </div>
    </div>
</section>
        </div>