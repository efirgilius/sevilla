<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <nav>
          <ol class="breadcrumb">
            
            <li><?php echo CHtml::link('Home',Yii::app()->homeUrl);?></li>
            
            <li class="active">Member</li>
            
            <li><?php echo CHtml::link('Profile',array('profile'));?></li>
            
            <li><?php echo CHtml::link('E-voucher Stock & History',array('pin'));?></li>
            
            <li class="active">Buy E-voucher</li>
            
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-xs-12">
          <h1 class="mo-margin-top"><span class="light">Buy</span> E-voucher</h1> 
      </div>
      
    </div>
</div>

<div class="container  push-down-30">
    
    <div class="row">
        
        <div class="col-xs-12 col-sm-9">
            <?php Setting::renderFlash();?>
            <?php if($valid==true):?>
                <div class="alert  alert-info">
                    E-voucher Wallet Balance: $<span class="banners--small--text"><?php echo $total;?></span><br>
                    E-voucher 200 price $200<br>
                    E-voucher 25 price $25
                  
              </div>
                <p>Please fill the form below.</p>
                <?php else:?>
                <p>Please enter your password.</p>
                <?php endif;?>
                
                <div class="row">
                <div class="col-xs-12 col-sm-7  push-down-30">
                     
            <?php if($valid==true):?>
                    <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                    <?php echo $form->errorSummary($model);?>
                        <div class="form-group">
                                <label class="text-dark" for="CardBalance_member_seller_id">Stockist ID <span class="warning">*</span></label>
                                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                        'name'=>'id_member',
                                        'source'=>$this->createUrl('loadmerchant'),
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                            'select'=>"js:function(event, ui) {
                                                                              $(this).val(ui.item.value);
                                                                              $('#CardBalance_member_seller_id').val(ui.item.mid);
                                                                            }"
                                            
                                        ),
                                        'cssFile'=>false,
                                        'htmlOptions'=>array('class'=>'form-control  form-control--contact validate[required]','placeholder'=>'Type Stockist ID','required'=>'required')
                                    ));?>
                                <?php echo $form->hiddenField($model,'member_seller_id',array('maxlength'=>15,'class'=>'form-control  form-control--contact','required'=>'required')); ?>
                                <?php echo $form->error($model,'member_seller_id'); ?>
                        </div>
                        <div class="form-group">
                                <label class="text-dark" for="CardBalance_type">E-voucher Type <span class="warning">*</span></label>
                                <?php echo CHtml::dropDownList('CardBalance[type]',isset($model->type)?$model->type:'', CardBalance::typeList(),array('class'=>'form-control  form-control--contact')); ?>
                                <?php echo $form->error($model,'type'); ?>
                        </div>
                        <div class="form-group">
                                <label class="text-dark" for="CardBalance_credit">Amount <span class="warning">*</span></label>
                                <?php echo $form->numberField($model,'credit',array('maxlength'=>5,'class'=>'form-control  form-control--contact','placeholder'=>'Amount','required'=>'required','min'=>1,'max'=>1000)); ?>
                                <?php echo $form->error($model,'credit'); ?>
                            </div>
                        <div class="right">
                            <?php echo CHtml::link('Cancel',array('pin'),array('class'=>'btn btn-danger'));?>&nbsp;<button type="submit" class="btn  btn-primary">Submit</button>
                        </div>
                    <?php $this->endWidget(); ?>  
            <?php else:?>
            
                <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                            <div class="form-group">
                                <label class="text-dark" for="CardForm_password">Password <span class="warning">*</span></label>
                                <?php echo $form->passwordField($card,'password',array('maxlength'=>30,'class'=>'form-control  form-control--contact','placeholder'=>'Password','required'=>'required')); ?>
                                <?php echo $form->error($card,'password'); ?>
                            </div>
                            <?php if(extension_loaded('gd')): ?>
                                <div class="form-group">
                                    <?php $this->widget('CCaptcha'); ?>
                                                                          <?php echo $form->textField($card,'verifyCode',array('maxlength'=>7,'class'=>'form-control  form-control--contact','placeholder'=>'Captcha')); ?>
                                                                              <?php echo $form->error($card,'verifyCode'); ?>
                                </div>
                              <?php endif;?>
                              <div class="right">
                            <?php echo CHtml::link('Cancel',array('pin'),array('class'=>'btn btn-danger'));?>&nbsp;<button type="submit" class="btn  btn-primary">Submit</button>
                        </div>
                        <?php $this->endWidget(); ?>  
                   
            <?php endif;?>
                     </div>
            </div>
        </div>
        <?php $this->renderPartial('_partial');?>
        
    </div>
</div>