<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <nav>
          <ol class="breadcrumb">
            
            <li><?php echo CHtml::link('Home',Yii::app()->homeUrl);?></li>
            
            <li class="active">Member</li>
            
            <li><?php echo CHtml::link('Profile',array('profile'));?></li>
            
            <li class="active">Bank Account Change Request</li>
            
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-xs-12">
          <h1 class="mo-margin-top"><span class="light">Bank Account</span> Change Request</h1> 
          
      </div>
      
    </div>
</div>

<div class="container  push-down-30">
    
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            
                <p>Your pending request change bank account.</p>
                
            <div class="row">
                <div class="col-xs-12 col-sm-7  push-down-30">
                        
                    <?php Setting::renderFlash();?>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_bank">Bank Name</label>
                            <span><?php echo $model->bank;?></span>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_account_number">Account Number</label>
                            <span><?php echo $model->account_number;?></span>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="BankRequest_account_name">Account Name</label>
                            <span><?php echo $model->account_name;?></span>
                        </div>
                        <div class="right">
                            <?php echo CHtml::link('Cancel Request',array('changecancel'),array('class'=>'btn btn-danger cancel'));?>
                          </div>
                    
                </div>
            </div>
            
        </div>
        <?php $this->renderPartial('_partial');?>
    </div>
    
</div>
<?php Yii::app()->clientScript->registerScript('delete-event', "
    $('.cancel').click(function(){
        if(!confirm('Are you sure want to cancel this request?')) return false;
        $.ajax({
            url:'".$this->createUrl('member/cancelchange')."',
            type: 'GET',
            success:function(){
                window.location.href = '".$this->createUrl('member/profile')."'; 
            }
        });
    return false;
    });
",CClientScript::POS_READY);?>
