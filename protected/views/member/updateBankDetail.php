<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li class="last"><?=$this->title;?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <h4>Bonus Generasi Jaringan</h4>
                    <hr>
                    <div class="table-responsive">
                                    <p>Berikut data permintaan perubahan akun bank Anda:</p>
                                    <table class="table table-bordered table-striped">
                                        <tr>
					                                        <td><strong>Bank<strong></td><td><?= $model->bank;?></td>
					                                    </tr>
					                                    <tr>
					                                        <td><strong>No. Rekening<strong></td><td><?= $model->account_number;?></td>
					                                    </tr>
					                                    <tr>
					                                        <td><strong>Nama Pemilik Rekening<strong></td><td><?= $model->account_name;?></td>
					                                    </tr>
                                    </table>
                                    <?= CHtml::link('Batalkan?',['cancelrequest'],['class'=>'button button-small button-red del']);?>
                                </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>

        <?php Yii::app()->clientScript->registerScript('delete', "
    $('.del').on('click',function(){
        if(!confirm('Anda yakin akan melakukan pembatalan perubahan data bank Anda?')) return false;
    });
",CClientScript::POS_READY);?>