<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li class="last"><?=$this->title;?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                    <?php echo CHtml::beginForm();?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-text">
                                <label class="text-dark" for="member_mid">Search Member <span class="warning">*</span></label>
                                <?php //echo CHtml::textField('member_mid','',array('class'=>'form-control  form-control--contact','placeholder'=>'Member ID or Member Name','maxlength'=>10,'required'=>'required'));?>
                                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                    'name'=>'key',
                                    'source'=>$this->createUrl('loadmydownline'),
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'select'=>"js:function(event, ui) {
                                            $(this).val(ui.item.value);
                                            $('#member_mid').val(ui.item.id);
                                        }"
                                        
                                    ),
                                    'cssFile'=>false,
                                    'htmlOptions'=>array('class'=>'input-name form-control','placeholder'=>'Please type member ID or member\'s name','required'=>'required')
                                ));?>
                                <input type="hidden" value="" name="member_mid" id="member_mid" placeholder="" required="required">
                            </div>
                            <div class="input-text">
                                <button type="submit" class="btn  btn-primary">Search</button>
                            </div>
                        </div>
                    </div>
                    <?php echo CHtml::endForm();?>
                </div>
                <hr>
                <div id="tree-diagram" class="clearfix">
                <?php echo ($upline!==null)?CHtml::link('Upline: '.$upline->mid,array('genealogy','id'=>$upline->id)):'';?>
                    <div class="treewrap clearfix">
                        <div class="lev-1">
                            <div class="box-user">
                                <div class="fig-user">
                                    <?php if ($model->id===Yii::app()->member->id):?>
                                    <?php echo CHtml::image(Yii::app()->baseUrl.'/'.$model->avatar_url, $model->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto'));?>
                                    <?php else:?>
                                        <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model->avatar_url, $model->mid,
                                        array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model->id),array('title'=>'Upline '.$model->name));?>
                                    <?php endif?>
                                    <div class="desc-user">                                              
                                        <span class="name-s">
                                            <?php if ($model->id===Yii::app()->member->id):?>
                                                <?php echo $model->mid;?>
                                            <?php else:?>
                                                <?php echo CHtml::link($model->mid, array('member/genealogy','id'=>$model->id),array('title'=>'Upline '.$model->name));?>
                                            <?php endif;?>
                                        </span>
                                        <span class="desc-s"><?php echo substr($model->name,0,10);?></span>
                                        <span class="desc-s">(<?= $model->typename;?>)</span>
                                                    
                                    </div>
                                </div>
                            </div>
                            <div class="vLine">
                                <div class="box-center">
                                    <div class="pull-left">
                                        <?php echo $model->total_left;?>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $model->total_right;?>
                                    </div>
                                </div>
                            </div>
                            <div class="hLine"></div> <!-- end Level 1 -->
                                        
                            <div class="lev-2 fl">
                                <div class="box-user">
                                    <div class="fig-user">
                                        <?php if ($model->foot_left!==NULL):?>
                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model2_l->avatar_url, $model2_l->mid,
                                                array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), 
                                                array('member/genealogy','id'=>$model2_l->id),array('title'=>'Upline '.$model2_l->name));
                                            ?>
                                        <?php endif;?>
                                        <div class="desc-user">
                                            <?php if ($model->foot_left!==NULL):?>
                                                <span class="name-s">
                                                    <?php echo CHtml::link($model2_l->mid, array('member/genealogy','id'=>$model2_l->id),
                                                        array('title'=>'Upline '.$model2_l->name));?>
                                                </span>
                                                <span class="desc-s"><?php echo substr($model2_l->name,0,10);?></span>
                                                <span class="desc-s">(<?= $model2_l->typename;?>)</span>
                                            <?php else:?>
                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model->id));?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                                <div class="vLine">
                                    <?php if ($model->foot_left!==NULL):?>
                                    <div class="box-center">
                                        <div class="pull-left">
                                            <?php echo $model2_l->total_left;?>
                                        </div>
                                        <div class="pull-right">
                                            <?php echo $model2_l->total_right;?>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="hLine"></div>
                                <div class="treewrap clearfix">
                                    <div class="lev-3 fl">
                                        <div class="box-user">
                                            <div class="fig-user">
                                            <?php if(!empty($model2_l)):?>
                                                <?php if($model3_2l_l):?>
                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2l_l->avatar_url, $model3_2l_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model3_2l_l->id),array('title'=>'Upline '.$model3_2l_l->name));?>
                                                <?php endif;?>
                                            <?php endif;?>
                                            <div class="desc-user">
                                                <?php if(!empty($model2_l)):?>
                                                    <?php if($model3_2l_l):?>
                                                        <span class="name-s"><?php echo CHtml::link($model3_2l_l->mid, array('member/genealogy','id'=>$model3_2l_l->id),array('title'=>'Upline '.$model3_2l_l->name));?></span>
                                                        <span class="desc-s"><?php echo substr($model3_2l_l->name,0,10);?></span>
                                                        <span class="desc-s">(<?= $model3_2l_l->typename;?>)</span>
                                                    <?php else:?>
                                                        <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model2_l->id));?>
                                                    <?php endif;?>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vLine">
                                        <?php if(!empty($model2_l)):?>
                                        <?php if($model3_2l_l):?>
                                        <div class="box-center">
                                            <div class="pull-left">
                                                <?php echo $model3_2l_l->total_left;?>
                                            </div>
                                            <div class="pull-right">
                                                <?php echo $model3_2l_l->total_right;?>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                        <?php endif;?>
                                    </div>
                                    <div class="hLine"></div>
                                    <div class="treewrap clearfix">
                                        <div class="lev-4 fl">
                                            <div class="box-user">
                                                <div class="fig-user">
                                                <?php if(!empty($model3_2l_l)):?>
                                                    <?php if($model4_32ll_l):?>                                                                 
                                                        <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32ll_l->avatar_url, $model4_32ll_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32ll_l->id),array('title'=>'Upline '.$model4_32ll_l->name));?>
                                                    <?php endif;?>
                                                <?php endif;?>
                                                <div class="desc-user">
                                                    <?php if(!empty($model3_2l_l)):?>
                                                        <?php if($model4_32ll_l):?>      
                                                            <span class="name-s"><?php echo CHtml::link($model4_32ll_l->mid, array('member/genealogy','id'=>$model4_32ll_l->id),array('title'=>'Upline '.$model4_32ll_l->name));?></span>
                                                            <span class="desc-s"><?php echo substr($model4_32ll_l->name,0,10);?></span>
                                                            <span class="desc-s">(<?= $model4_32ll_l->typename;?>)</span>
                                                        <?php else:?>
                                                            <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model3_2l_l->id));?>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if(!empty($model3_2l_l)):?>
                                            <?php if($model4_32ll_l):?>  
                                                <div class="box-center">
                                                    <div class="pull-left">
                                                        <?php echo $model4_32ll_l->total_left;?>
                                                    </div>
                                                <div class="pull-right">
                                                    <?php echo $model4_32ll_l->total_right;?>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                        <?php endif;?>
                                    </div><!-- end Level 4 -->
                                        <div class="lev-4 fr">
                                            <div class="box-user">
                                                <div class="fig-user">
                                                    <?php if(!empty($model3_2l_l)):?>
                                                        <?php if($model4_32ll_r):?>                                          
                                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32ll_r->avatar_url, $model4_32ll_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32ll_r->id),array('title'=>'Upline '.$model4_32ll_r->name));?>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                    <div class="desc-user">
                                                        <?php if(!empty($model3_2l_l)):?>
                                                            <?php if($model4_32ll_r):?>         
                                                                <span class="name-s"><?php echo CHtml::link($model4_32ll_r->mid, array('member/genealogy','id'=>$model4_32ll_r->id),array('title'=>'Upline '.$model4_32ll_r->name));?></span>
                                                                <span class="desc-s"><?php echo substr($model4_32ll_r->name,0,10);?></span>
                                                                <span class="desc-s">(<?= $model4_32ll_r->typename;?>)</span>
                                                            <?php else:?>
                                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model3_2l_l->id));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php if(!empty($model3_2l_l)):?>
                                                    <?php if($model4_32ll_r):?>       
                                                        <div class="box-center">
                                                            <div class="pull-left">
                                                                <?php echo $model4_32ll_r->total_left;?>
                                                            </div>
                                                            <div class="pull-right">
                                                                <?php echo $model4_32ll_r->total_right;?>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                </div><!-- end Level 4 -->
                                            </div>
                                        </div><!-- end Level 3 -->
                                                
                                        <div class="lev-3 fr">
                                            <div class="box-user">
                                                <div class="fig-user">
                                                    <?php if(!empty($model2_l)):?>
                                                        <?php if($model3_2l_r):?>
                                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2l_r->avatar_url, $model3_2l_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model3_2l_r->id),array('title'=>'Upline '.$model3_2l_r->name));?>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                    <div class="desc-user">
                                                        <?php if(!empty($model2_l)):?>
                                                            <?php if($model3_2l_r):?>
                                                                <span class="name-s"><?php echo CHtml::link($model3_2l_r->mid, array('member/genealogy','id'=>$model3_2l_r->id),array('title'=>'Upline '.$model3_2l_r->name));?></span>
                                                                <span class="desc-s"><?php echo substr($model3_2l_r->name,0,10);?></span>
                                                                <span class="desc-s">(<?= $model3_2l_r->typename;?>)</span>
                                                            <?php else:?>
                                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model2_l->id));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="vLine">
                                            <?php if(!empty($model2_l)):?>
                                            <?php if($model3_2l_r):?>
                                            <div class="box-center">
                                                <div class="pull-left">
                                                    <?php echo $model3_2l_r->total_left;?>
                                                </div>
                                                <div class="pull-right">
                                                    <?php echo $model3_2l_r->total_right;?>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                        <div class="hLine"></div>
                                        <div class="treewrap clearfix">
                                            <div class="lev-4 fl">
                                                <div class="box-user">
                                                    <div class="fig-user">
                                                        <?php if(!empty($model3_2l_r)):?>
                                                            <?php if($model4_32lr_l):?>                                                                         
                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32lr_l->avatar_url, $model4_32lr_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32lr_l->id),array('title'=>'Upline '.$model4_32lr_l->name));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                        <div class="desc-user">
                                                            <?php if(!empty($model3_2l_r)):?>
                                                            <?php if($model4_32lr_l):?>      
                                                            <span class="name-s"><?php echo CHtml::link($model4_32lr_l->mid, array('member/genealogy','id'=>$model4_32lr_l->id),array('title'=>'Upline '.$model4_32lr_l->name));?></span>
                                                            <span class="desc-s"><?php echo substr($model4_32lr_l->name,0,10);?></span>
                                                            <span class="desc-s">(<?= $model4_32lr_l->typename;?>)</span>
                                                            <?php else:?>
                                                            <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model3_2l_r->id));?>
                                                            <?php endif;?>
                                                            <?php endif;?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if(!empty($model3_2l_r)):?>
                                                <?php if($model4_32lr_l):?>
                                                <div class="box-center">
                                                    <div class="pull-left">
                                                        <?php echo $model4_32lr_l->total_left;?>
                                                    </div>
                                                    <div class="pull-right">
                                                        <?php echo $model4_32lr_l->total_right;?>
                                                    </div>
                                                </div>
                                                <?php endif;?>
                                                <?php endif;?>
                                            </div><!-- end Level 4 -->
                                            <div class="lev-4 fr">
                                                <div class="box-user">
                                                    <div class="fig-user">
                                                        <?php if(!empty($model3_2l_r)):?>
                                                            <?php if($model4_32lr_r):?>                                                                         
                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32lr_r->avatar_url, $model4_32lr_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32lr_r->id),array('title'=>'Upline '.$model4_32lr_r->name));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <div class="desc-user">
                                                        <?php if(!empty($model3_2l_r)):?>
                                                            <?php if($model4_32lr_r):?>     
                                                                <span class="name-s"><?php echo CHtml::link($model4_32lr_r->mid, array('member/genealogy','id'=>$model4_32lr_r->id),array('title'=>'Upline '.$model4_32lr_r->name));?></span>
                                                                <span class="desc-s"><?php echo substr($model4_32lr_r->name,0,10);?></span>
                                                                <span class="desc-s">(<?= $model4_32lr_r->typename;?>)</span>
                                                            <?php else:?>
                                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model3_2l_r->id));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php if(!empty($model3_2l_r)):?>
                                                <?php if($model4_32lr_r):?>  
                                                <div class="box-center">
                                                    <div class="pull-left">
                                                        <?php echo $model4_32lr_r->total_left;?>
                                                    </div>
                                                    <div class="pull-right">
                                                        <?php echo $model4_32lr_r->total_right;?>
                                                    </div>
                                                </div>
                                                <?php endif;?>
                                                <?php endif;?>
                                            </div><!-- end Level 4 -->
                                        </div>
                                    </div><!-- end Level 3 -->
                                </div>
                            </div><!-- end Level 2 -->
                                        
                            <div class="lev-2 fr">
                                <div class="box-user">
                                    <div class="fig-user">
                                        <?php if ($model->foot_right !== NULL):?>
                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model2_r->avatar_url, $model2_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model2_r->id),array('title'=>'Upline '.$model2_r->name));?>
                                        <?php endif;?>
                                    <div class="desc-user">
                                        <?php if ($model->foot_right !== NULL):?>
                                            <span class="name-s"><?php echo CHtml::link($model2_r->mid, array('member/genealogy','id'=>$model2_r->id),array('title'=>'Upline '.$model2_r->name));?></span>
                                            <span class="desc-s"><?php echo substr($model2_r->name,0,10);?></span>
                                            <span class="desc-s">(<?= $model2_r->typename;?>)</span>
                                        <?php else:?>
                                            <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model->id));?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <div class="vLine">
                                <?php if ($model->foot_right !== NULL):?>
                                <div class="box-center">
                                    <div class="pull-left">
                                        <?php echo $model2_r->total_left;?>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $model2_r->total_right;?>
                                    </div>
                                </div>
                                <?php endif;?>
                            </div>
                            <div class="hLine"></div>
                            
                            <div class="treewrap clearfix">
                                <div class="lev-3 fl">
                                    <div class="box-user">
                                        <div class="fig-user">
                                        <?php if(!empty($model2_r)):?>
                                            <?php if($model3_2r_l):?>
                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2r_l->avatar_url, $model3_2r_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model3_2r_l->id),array('title'=>'Upline '.$model3_2r_l->name));?>
                                            <?php endif;?>
                                        <?php endif;?>
                                        <div class="desc-user">
                                            <?php if(!empty($model2_r)):?>
                                                <?php if($model3_2r_l):?>
                                                    <span class="name-s"><?php echo CHtml::link($model3_2r_l->mid, array('member/genealogy','id'=>$model3_2r_l->id),array('title'=>'Upline '.$model3_2r_l->name));?></span>
                                                    <span class="desc-s"><?php echo substr($model3_2r_l->name,0,10);?></span>
                                                    <span class="desc-s">(<?= $model3_2r_l->typename;?>)</span>
                                                <?php else:?>
                                                    <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model2_r->id));?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                                    <div class="vLine">
                                        <?php if(!empty($model2_r)):?>
                                        <?php if($model3_2r_l):?>
                                        <div class="box-center">
                                            <div class="pull-left">
                                                <?php echo $model3_2r_l->total_left;?>
                                            </div>
                                            <div class="pull-right">
                                                <?php echo $model3_2r_l->total_right;?>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                        <?php endif;?>
                                    </div>
                                    <div class="hLine"></div>
                                    <div class="treewrap clearfix">
                                        <div class="lev-4 fl">
                                            <div class="box-user">
                                                <div class="fig-user">
                                                    <?php if(!empty($model3_2r_l)):?>
                                                        <?php if($model4_32rl_l):?>                                                                         
                                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rl_l->avatar_url, $model4_32rl_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32rl_l->id),array('title'=>'Upline '.$model4_32rl_l->name));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <div class="desc-user">
                                                    <?php if(!empty($model3_2r_l)):?>
                                                        <?php if($model4_32rl_l):?>       
                                                            <span class="name-s"><?php echo CHtml::link($model4_32rl_l->mid, array('member/genealogy','id'=>$model4_32rl_l->id),array('title'=>'Upline '.$model4_32rl_l->name));?></span>
                                                            <span class="desc-s"><?php echo substr($model4_32rl_l->name,0,10);?></span>
                                                            <span class="desc-s">(<?= $model4_32rl_l->typename;?>)</span>
                                                        <?php else:?>
                                                    <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model3_2r_l->id));?>
                                                    <?php endif;?>
                                                    <?php endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if(!empty($model3_2r_l)):?>
                                            <?php if($model4_32rl_l):?>   
                                            <div class="box-center">
                                                <div class="pull-left">
                                                    <?php echo $model4_32rl_l->total_left;?>
                                                </div>
                                                <div class="pull-right">
                                                    <?php echo $model4_32rl_l->total_right;?>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div><!-- end Level 4 -->
                                            <div class="lev-4 fr">
                                                <div class="box-user">
                                                    <div class="fig-user">
                                                        <?php if(!empty($model3_2r_l)):?>
                                                            <?php if($model4_32rl_r):?>                                                                         
                                                                <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rl_r->avatar_url, $model4_32rl_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32rl_r->id),array('title'=>'Upline '.$model4_32rl_r->name));?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <div class="desc-user">
                                                    <?php if(!empty($model3_2r_l)):?>
                                                    <?php if($model4_32rl_r):?>    
                                                        <span class="name-s"><?php echo CHtml::link($model4_32rl_r->mid, array('member/genealogy','id'=>$model4_32rl_r->id),array('title'=>'Upline '.$model4_32rl_r->name));?></span>
                                                            <span class="desc-s"><?php echo substr($model4_32rl_r->name,0,10);?></span>
                                                            <span class="desc-s">(<?= $model4_32rl_r->typename;?>)</span>
                                                        <?php else:?>
                                                            <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model3_2r_l->id));?>
                                                        <?php endif;?>
                                                        <?php endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if(!empty($model3_2r_l)):?>
                                        <?php if($model4_32rl_r):?>     
                                        <div class="box-center">
                                            <div class="pull-left">
                                                <?php echo $model4_32rl_r->total_left;?>
                                            </div>
                                            <div class="pull-right">
                                                <?php echo $model4_32rl_r->total_right;?>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                        <?php endif;?>
                            </div><!-- end Level 4 -->
                        </div>
                    </div>
                    
                    <div class="lev-3 fr">
                        <div class="box-user">
                            <div class="fig-user">
                                <?php if(!empty($model2_r)):?>
                                        <?php if($model3_2r_r):?>
                                            <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model3_2r_r->avatar_url, $model3_2r_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model3_2r_r->id),array('title'=>'Upline '.$model3_2r_r->name));?>
                                <?php endif;?>
                                    <?php endif;?>
                                <div class="desc-user">
                                    <?php if(!empty($model2_r)):?>
                                        <?php if($model3_2r_r):?>
                                            <span class="name-s"><?php echo CHtml::link($model3_2r_r->mid, array('member/genealogy','id'=>$model3_2r_r->id),array('title'=>'Upline '.$model3_2r_r->name));?></span>
                                            <span class="desc-s"><?php echo substr($model3_2r_r->name,0,10);?></span>
                                            <span class="desc-s">(<?= $model3_2r_r->typename;?>)</span>
                                        <?php else:?>
                                            <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model2_r->id));?>
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="vLine">
                            <?php if(!empty($model2_r)):?>
                            <?php if($model3_2r_r):?>
                            <div class="box-center">
                                <div class="pull-left">
                                    <?php echo $model3_2r_r->total_left;?>
                                </div>
                                <div class="pull-right">
                                    <?php echo $model3_2r_r->total_right;?>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php endif;?>
                        </div>
                        <div class="hLine"></div>
                        <div class="treewrap clearfix">
                            <div class="lev-4 fl">
                                <div class="box-user">
                                    <div class="fig-user">
                                        <?php if(!empty($model3_2r_r)):?>
                                            <?php if($model4_32rr_l):?>                                                                         
                                                    <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rr_l->avatar_url, $model4_32rr_l->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32rr_l->id),array('title'=>'Upline '.$model4_32rr_l->name));?>
                                        <?php endif;?>
                                            <?php endif;?>
                                        <div class="desc-user">
                                            <?php if(!empty($model3_2r_r)):?>
                                            <?php if($model4_32rr_l):?>        
                                                <span class="name-s"><?php echo CHtml::link($model4_32rr_l->mid, array('member/genealogy','id'=>$model4_32rr_l->id),array('title'=>'Upline '.$model4_32rr_l->name));?></span>
                                                <span class="desc-s"><?php echo substr($model4_32rr_l->name,0,10);?></span>
                                                <span class="desc-s">(<?= $model4_32rr_l->typename;?>)</span>
                                            <?php else:?>
                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'l','upline'=>$model3_2r_r->id));?>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                                    <?php if(!empty($model3_2r_r)):?>
                                    <?php if($model4_32rr_l):?>      
                                    <div class="box-center">
                                        <div class="pull-left">
                                            <?php echo $model4_32rr_l->total_left;?>
                                        </div>
                                        <div class="pull-right">
                                            <?php echo $model4_32rr_l->total_right;?>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                    <?php endif;?>
                            </div><!-- end Level 4 -->
                            <div class="lev-4 fr">
                                <div class="box-user">
                                    <div class="fig-user">
                                        <?php if(!empty($model3_2r_r)):?>
                                            <?php if($model4_32rr_r):?>                                                                         
                                                    <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/'.$model4_32rr_r->avatar_url, $model4_32rr_r->mid,array('style'=>'width:60px;height:60px;display:block;margin: 0 auto')), array('member/genealogy','id'=>$model4_32rr_r->id),array('title'=>'Upline '.$model4_32rr_r->name));?>
                                            <?php endif;?>
                                        <?php endif;?>
                                        <div class="desc-user">
                                            <?php if(!empty($model3_2r_r)):?>
                                            <?php if($model4_32rr_r):?>  
                                                <span class="name-s"><?php echo CHtml::link($model4_32rr_r->mid, array('member/genealogy','id'=>$model4_32rr_r->id),array('title'=>'Upline '.$model4_32rr_r->name));?></span>
                                                <span class="desc-s"><?php echo substr($model4_32rr_r->name,0,10);?></span>
                                                <span class="desc-s">(<?= $model4_32rr_r->typename;?>)</span>
                                            <?php else:?>
                                                <?php echo CHtml::link('Register?', array('member/register','position'=>'r','upline'=>$model3_2r_r->id));?>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                                <?php if(!empty($model3_2r_r)):?>
                                <?php if($model4_32rr_r):?> 
                                <div class="box-center">
                                    <div class="pull-left">
                                        <?php echo $model4_32rr_r->total_left;?>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $model4_32rr_r->total_right;?>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endif;?>
                                </div><!-- end Level 4 -->
                            </div>
                        </div>
                    </div>
                </div><!-- end Level 2 -->
                
            </div>
        </div>
    </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>