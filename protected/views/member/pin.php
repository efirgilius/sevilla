<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <?php Notify::renderMFlash();?>
                <div class="row">
                  <div class="col-xs-12 col-sm-4">
                    <div class="alert  alert-info">
                        <b>Total :</b> <span class="banners--small--text"><?php echo $pin;?></span><br>
                        <b>PIN tipe Standard :</b> <span class="banners--small--text"><?php echo $pinA;?></span><br>
                        <b>PIN tipe Deluxe :</b> <span class="banners--small--text"><?php echo $pinB;?></span><br>
                        <b>PIN tipe Premium :</b> <span class="banners--small--text"><?php echo $pinC;?></span><br>
                        <b>PIN tipe Executive :</b> <span class="banners--small--text"><?php echo $pinD;?></span><br>
                        <b>PIN tipe Executive Diamond :</b> <span class="banners--small--text"><?php echo $pinD;?></span><br>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="alert  alert-success">
                        <b>Active PIN :</b> <span class="banners--small--text"><?php echo $pinActive;?></span><br>
                        <b>PIN tipe Standard :</b> <span class="banners--small--text"><?php echo $pinActiveA;?></span><br>
                        <b>PIN tipe Deluxe :</b> <span class="banners--small--text"><?php echo $pinActiveB;?></span><br>
                        <b>PIN tipe Premium :</b> <span class="banners--small--text"><?php echo $pinActiveC;?></span><br>
                        <b>PIN tipe Executive :</b> <span class="banners--small--text"><?php echo $pinActiveD;?></span><br>
                        <b>PIN tipe Executive Diamond :</b> <span class="banners--small--text"><?php echo $pinActiveE;?></span><br>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="alert  alert-danger">
                        <b>Activated PIN :</b> <span class="banners--small--text"><?php echo $pinInactive;?></span><br>
                        <b>PIN tipe Standard :</b> <span class="banners--small--text"><?php echo $pinInactiveA;?></span><br>
                        <b>PIN tipe Deluxe :</b> <span class="banners--small--text"><?php echo $pinInactiveB;?></span><br>
                        <b>PIN tipe Premium :</b> <span class="banners--small--text"><?php echo $pinInactiveC;?></span><br>
                        <b>PIN tipe Executive :</b> <span class="banners--small--text"><?php echo $pinActiveD;?></span><br>
                        <b>PIN tipe Executive Diamond :</b> <span class="banners--small--text"><?php echo $pinInactiveE;?></span><br>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-4">
                      <?php echo CHtml::link('Buy PIN to Stockist',array('buypin'),array('class'=>'btn btn-success'));?>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <?php echo CHtml::link('Send PIN to Member',array('sendpin'),array('class'=>'btn btn-success'));?>
                  </div>
                </div>

                <hr class="divider">
                <?= CHtml::link('<b>PIN Tipe Standard</b>',['pin','type'=>'standard']);?> | <?= CHtml::link('<b>PIN Tipe Deluxe</b>',['pin','type'=>'deluxe']);?> | <?= CHtml::link('<b>PIN Tipe Premium</b>',['pin','type'=>'premium']);?> | <?= CHtml::link('<b>PIN Tipe Executive</b>',['pin','type'=>'executive']);?> | <?= CHtml::link('<b>PIN Tipe Executive Diamond</b>',['pin','type'=>'diamond']);?>
                <h5><span class="light">PIN <?= $title;?></span> <?php if($m!==null):?>Transaction History Period <?php echo Setting::getMonth($m).' '.$y;?><?php endif;?></h5>
                <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'bonus-form',
                        'enableAjaxValidation'=>false,
                        'method'=>'get',
                        'action'=>(isset($_GET['type']))?Yii::app()->createUrl('member/pin',['type'=>$_GET['type']]):Yii::app()->createUrl('member/pin')
                )); ?>
                <div class="row">
                    <div class="col-xs-12  col-sm-3">
                        <?php echo CHtml::dropDownList('m', '1',Setting::getMonths(),array('tabindex'=>1,'class'=>'form-control  form-control--contact')); ?>
                    </div>
                    <div class="col-xs-12  col-sm-3">
                        <?php echo CHtml::dropDownList('y', '',Setting::getYears(),array('tabindex'=>2,'class'=>'form-control  form-control--contact')); ?>
                    </div>
                    <div class="col-xs-12  col-sm-3">
                        <button type="submit" class="btn btn-warning">Submit</button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <hr class="divider">

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Serial Number</th>
                        <th>PIN</th>
                        <th>Tipe</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if($model):?>
                      <?php foreach($model as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->serial;?></td>
                        <td><?php echo $row->pin;?></td>
                        <td><?php echo $row->typename;?></td>
                        <td><?php echo $row->isactive;?></td>
                      </tr>
                      <?php $i++; endforeach;?>
                    <?php else:?>
                      <tr><td colspan="5">Stok PIN tidak tersedia.</td></tr>
                    <?php endif;?>
                    </tbody>
                </table>
                <?php if($count):?>
                  <div class="pagination right">
                  <?php $this->widget('CLinkPager', array(
                      'pages' => $pages,
                      'header'=>'',
                      'footer'=>'',
                      'nextPageLabel'=>'»',
                      'prevPageLabel'=>'«',
                      'id'=>'link_pager',
                  ))?>
                  </div>
                <?php endif;?>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>