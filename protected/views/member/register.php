<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-12">           
              <div class="brochure">   
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                  <h3>Daftar Member</h3><hr>
                  <p>Silahkan isi form membership di bawah ini dengan lengkap dan benar</p>
                  <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'register-form',
                    'enableClientValidation'=>true,
                    'enableAjaxValidation'=>true,
                    'clientOptions'=>array(
                      'validateOnSubmit'=>true,
                    ),
                  )); ?>
                  <?= $form->errorSummary($model);?>
                  <div class="row">
                    <div class="col-sm-6">
                      <h4>Data Pribadi</h4>
                      <hr>
                      <div class="input-text">
                        <label>Serial Number<span class="red">*</span></label>
                        <?php echo $form->textField($model,'serial',array('maxlength'=>15,'required'=>'required','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'serial'); ?>
                      </div>
                      <div class="input-text">
                        <label>PIN<span class="red">*</span></label>
                        <?php echo $form->passwordField($model,'pin',array('maxlength'=>10,'required'=>'required', 'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'pin'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'email'); ?>
                        <?php echo $form->emailField($model,'email',array('maxlength'=>50,'placeholder'=>'Email','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'email'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password',['maxlength'=>50,'placeholder'=>'Password','class'=>'form-control']); ?>
                        <?php echo $form->error($model,'password'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'repassword'); ?>
                        <?php echo $form->passwordField($model,'repassword',['maxlength'=>50,'placeholder'=>'Password','class'=>'form-control']); ?>
                        <?php echo $form->error($model,'repassword'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('maxlength'=>100,'placeholder'=>'Nama Lengkap','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'identity_number'); ?>
                        <?php echo $form->textField($model,'identity_number',array('maxlength'=>50,'placeholder'=>'Nomor KTP/SIM/Pass','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'identity_number'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'gender'); ?>
                        <?php echo $form->dropDownList($model,'gender',Member::genderList(),['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'gender'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'birth_place'); ?>
                        <?php echo $form->textField($model,'birth_place',array('maxlength'=>50,'placeholder'=>'Tempat Lahir','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'birth_place'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'birth_date'); ?>
                        <?php echo $form->dateField($model,'birth_date',array('placeholder'=>'Tanggal Lahir','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'birth_date'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'address_1'); ?>
                        <?php echo $form->textArea($model,'address_1',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'address_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'address_2'); ?>
                        <?php echo $form->textArea($model,'address_2',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'address_2'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'mobile_phone'); ?>
                        <?php echo $form->textField($model,'mobile_phone',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'mobile_phone'); ?>
                      </div>
                      <div class="input-text">
                          <?php echo $form->labelEx($model,'phone'); ?>
                          <?php echo $form->textField($model,'phone',['maxlength'=>20,'class'=>'form-control']); ?>
                          <?php echo $form->error($model,'phone'); ?>
                      </div>
                      <h4>Data Pekerjaan</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work'); ?>
                        <?php echo $form->textField($model,'work',['maxlength'=>50,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_time'); ?>
                        <?php echo $form->textField($model,'work_time',['maxlength'=>50,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_time'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_address'); ?>
                        <?php echo $form->textField($model,'work_address',['maxlength'=>100,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_address'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'work_phone'); ?>
                        <?php echo $form->textField($model,'work_phone',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'work_phone'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'npwp'); ?>
                        <?php echo $form->textField($model,'npwp',['maxlength'=>20,'class'=>'form-control']); ?>
                        <?php echo $form->error($model,'npwp'); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <h4>Data Keluarga Yang Dapat Dihubungi</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_name'); ?>
                        <?php echo $form->textField($model,'family_name',array('maxlength'=>100,'placeholder'=>'Nama Lengkap','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'family_name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_address'); ?>
                        <?php echo $form->textArea($model,'family_address',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'family_address'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'family_phone'); ?>
                        <?php echo $form->textField($model,'family_phone',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'family_phone'); ?>
                      </div>
                      <h4>Data Keluarga</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'mother_name'); ?>
                        <?php echo $form->textField($model,'mother_name',array('maxlength'=>100,'class'=>'form-control','placeholder'=>'Nama Lengkap Ibu Kandung')); ?>
                        <?php echo $form->error($model,'mother_name'); ?>
                      </div>
                      <h4>Data Ahli Waris</h4>
                      <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_name_1'); ?>
                        <?php echo $form->textField($model,'heir_name_1',array('maxlength'=>100,'placeholder'=>'Nama Ahli Waris 1','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'heir_name_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_relationship_as_1'); ?>
                        <?php echo $form->textField($model,'heir_relationship_as_1',array('maxlength'=>100,'placeholder'=>'Hubungan Ahli Waris 1','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'heir_relationship_as_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_place_1'); ?>
                        <?php echo $form->textField($model,'heir_birth_place_1',array('maxlength'=>50,'class'=>'form-control','placeholder'=>'Tempat Lahir Ahli Waris 1')); ?>
                        <?php echo $form->error($model,'heir_birth_place_1'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_date_1'); ?>
                        <?php echo $form->dateField($model,'heir_birth_date_1',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'heir_birth_date_1'); ?>
                      </div>
                      <div class="input-text">
                        <hr class="divider dev-style1">
                        <?php echo $form->labelEx($model,'heir_name_2'); ?>
                        <?php echo $form->textField($model,'heir_name_2',array('maxlength'=>100,'class'=>'form-control','placeholder'=>'Nama Ahli Waris 2')); ?>
                        <?php echo $form->error($model,'heir_name_2'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_relationship_as_2'); ?>
                        <?php echo $form->textField($model,'heir_relationship_as_2',array('maxlength'=>100,'class'=>'form-control','placeholder'=>'Hubungan Ahli Waris 2')); ?>
                        <?php echo $form->error($model,'heir_relationship_as_2'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_place_2'); ?>
                        <?php echo $form->textField($model,'heir_birth_place_2',array('maxlength'=>50,'class'=>'form-control','placeholder'=>'Tempat Lahir Ahli Waris 2')); ?>
                        <?php echo $form->error($model,'heir_birth_place_2'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'heir_birth_date_2'); ?>
                        <?php echo $form->dateField($model,'heir_birth_date_2',['class'=>'form-control']); ?>
                        <?php echo $form->error($model,'heir_birth_date_2'); ?>
                      </div>
                        <h4>Data Bank</h4>
                        <hr>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank_name'); ?>
                        <?php echo $form->textField($model,'bank_name',array('maxlength'=>20,'placeholder'=>'Nama Bank','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'bank_name'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank_branch'); ?>
                        <?php echo $form->textField($model,'bank_branch',array('maxlength'=>50,'placeholder'=>'Cabang','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'bank_branch'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank_acc_number'); ?>
                        <?php echo $form->textField($model,'bank_acc_number',array('maxlength'=>20,'placeholder'=>'Nomor Rekening','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'bank_acc_number'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank_acc_name'); ?>
                        <?php echo $form->textField($model,'bank_acc_name',array('maxlength'=>50,'placeholder'=>'Nama Pemilik Rekening','class'=>'form-control')); ?>
                        <?php echo $form->error($model,'bank_acc_name'); ?>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="input-text">
                        <?php echo $form->checkbox($model,'term'); ?> Dengan ini saya bersedia mengikuti semua syarat dan ketentuan yang berlaku di www.sevilla.com<br>
                        <input class="fixed-send animated" type="submit" value="Daftar">
                      </div>
                    </div>
                  </div>
                  <?php $this->endWidget(); ?> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
    </div>
    </div>
</section>