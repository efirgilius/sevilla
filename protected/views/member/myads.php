<!-- Breadcrumb Start -->
	<div class="breadcrumb-standart main-color">
		<div class="container">
			<h2><?=$this->title;?></h2>
		</div>
		<hr>
		<div class="container breadcrumb-link">
			<ul>
				<li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
                                <li><?=CHtml::link('<span><span>Profil</span></span>',['/member']);?></li>
				<li><a class="active" href="#"><?=$this->title;?></a></li>                  
			</ul>
		</div>
	</div>
	<!-- Breadcrumb End -->
<section class="main-page container">
    <div class="main-container col2-right-layout">
        <div class="main">
            <div class="row">
                <div class=" col-sm-9 col-md-9 col-lg-9">
                    <div class="col-main">
                        <div class="table-responsive">
                    <div class="heading style3 sm">
                        <h3 class="uppercase"><span class="main-color">Iklan </span> Saya</h3>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Gambar</th>
                                <th>Nama</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $k => $v):?>
                            <tr>
                                <td><?=CHtml::image($v->small_url);?></td>
                                <td><?=$v->name;?></td>
                                <td><?=$v->category->name;?></td>
                                <td>IDR <?=number_format($v->price);?></td>
                                <td><?=$v->statusname;?></td>
                                <td>
                                    <?php if ($v->status==0):?>
                                        <?=CHtml::link('Edit',['editmyads','slug'=>$v->slug]);?> | 
                                    <?php endif;?>
                                    <?=CHtml::link('Hapus',['deleteads','slug'=>$v->slug]);?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>  
                    </div>
                </div>
                <!--  left side -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="account-list">
                        <div class="account-list-inner">
                            <div class="title-box">
                                <span class="sub-title">Member Panel</span>
                            </div>
                            <div class="account-list-box">
                                <?php $this->widget('MemberPanelWidget');?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / left side-->
            </div>
        </div>
    </div>
</section>
<!--  testimonial -->