<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="col-sm-12">
                <?= Notify::renderMflash();?>
                <div class="contact-form">
                    <?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('validate'=>'validate','name'=>'contactForm'))); ?>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="input-text">
                                    <label>Jumlah Deposit<span class="red">*</span></label>
                                    <?php echo $form->numberField($model,'value',
                                        array(
                                        'class'=>'form-control',
                                        'required'=>'required',
                                        )
                                    ); ?><em>* Minimal deposit Rp 100.000</em>
                                    <?php echo $form->error($model,'value'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                              <div class="input-text">
                                <input class="fixed-send animated" type="submit" value="Submit">
                              </div>
                            </div>
                        </div>
                    <?php $this->endWidget(); ?>
                </div>
                </div>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>