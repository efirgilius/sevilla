<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'bonus-form',
                    'enableAjaxValidation'=>false,
                    'method'=>'get',
                    'action'=>Yii::app()->createUrl('member/loyaltybonus')
                )); ?>
                <span>Periode:&nbsp;</span>
                <div class="row">
                    <div class="col-xs-12  col-sm-3">
                        <?php echo CHtml::dropDownList('m', '1',Setting::getMonths(),array('tabindex'=>1,'class'=>'form-control  form-control--contact')); ?>
                    </div>
                    <div class="col-xs-12  col-sm-3">
                        <?php echo CHtml::dropDownList('y', '',Setting::getYears(),array('tabindex'=>2,'class'=>'form-control  form-control--contact')); ?>
                    </div>
                    <div class="col-xs-12  col-sm-3">
                        <button type="submit" class="btn  btn-warning">Submit</button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                                                
                <br>
                  <p>Total bonus loyalty <?php if(isset($_GET['m']) && isset($_GET['y'])):?>periode <?php echo Setting::getMonth($m).' '.$y;?><?php endif;?> sebesar <strong>Rp. <?= (isset($_GET['m']) && isset($_GET['y']))? number_format(BonusCouple::model()->getTotalbonus($m,$y),2,",","."):number_format(BonusCouple::model()->getTotal());?></strong></p>
                    <hr class="divider">        
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                                <th>#</th>
                                                <th>Tanggal</th>
                                                <th>Bonus (Rp)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($model):?>
                            <?php foreach($model as $row):?>
                            <tr>
                                                    <td width="2%"><?php echo $i;?></td>
                                                    <td><?php echo $row->date;?></td>
                                                    <td><span style="float:right;"><?php echo number_format($row->bonus,2,",",".");?></span></td>
                                                </tr>
                            <?php $i++; endforeach;?>
                            <?php else:?>
                                                <tr>
                                                    <td colspan="3"><p>Bonus loyalty <?php if(isset($_GET['m']) && isset($_GET['y'])):?>untuk periode <?php echo Setting::getMonth($m).' '.$y;?><?php endif;?> tidak tersedia.</p></td>
                                                </tr>
                                                <?php endif;?>
                        </tbody>
                    </table>  
                    <div class="pagination right">
                        <?php $this->widget('CLinkPager', array(
                            'pages' => $pages,
                            'header'=>'',
                            'footer'=>'',
                            'nextPageLabel'=>'»',
                            'prevPageLabel'=>'«',
                            'id'=>'link_pager',
                        ))?>
                    </div>
                
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>