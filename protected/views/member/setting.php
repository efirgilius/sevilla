<!-- Breadcrumb Start -->
	<div class="breadcrumb-standart main-color">
		<div class="container">
			<h2><?=$this->title;?></h2>
		</div>
		<hr>
		<div class="container breadcrumb-link">
			<ul>
				<li><?=CHtml::link('<span><span>Beranda</span></span>',Yii::app()->homeurl);?></li>
                                <li><?=CHtml::link('<span><span>Profil</span></span>',['/member']);?></li>
				<li><a class="active" href="#"><?=$this->title;?></a></li>                  
			</ul>
		</div>
	</div>
	<!-- Breadcrumb End -->
        
        <div class="site-content">
            <div class="container">
                        <div class="row marginb60 margint60">
                            <div class="col-sm-9 col-md-9 col-lg-9">
                                <div class="title-line">
                                        <h4>Setting Biaya Transaksi</h4>
                                        <hr>
                                </div>
                                <?= Notify::renderMflash();?>
                                <div class="row">
					<div class="col-lg-6 col-sm-6">
						    <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'validation',
                                'enableAjaxValidation'=>false,
                                'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                            )); ?>
                                <label>Biaya Pulsa</label>
                                <input type="text" name="feePulsa" value="<?=$feePulsa->fee;?>">
                                <label>Biaya PLN</label>
                                <input type="text" name="feePLN" value="<?=$feePLN->fee;?>"><hr>
    							<input type="submit" value="Simpan" name="SaveSetting">
                            <?php $this->endWidget();?>
					</div>
                                    
                                        
                                        </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <?php $this->widget('MemberPanelWidget');?>
                            </div>
                        </div>
                </div>
        </div>