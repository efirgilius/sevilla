<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#"><?=$this->title;?></a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-contact02 mgb20">
  <div class="container">
    <div class="row">
      <main class="main-content">
        <div class="content">
          <div class="contact-info">
            <div class="col-sm-12">           
              <div class="brochure">   
                <?php Notify::renderMflash();?>
                <div class="contact-form">
                  <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'register-form',
                    'enableClientValidation'=>true,
                    'enableAjaxValidation'=>true,
                    'clientOptions'=>array(
                      'validateOnSubmit'=>true,
                    ),
                  )); ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <?php if($valid==true):?>
                      <h4>Data Bank</h4>
                      <hr>
                      <p>Silahkan isi form perubahan akun rekening Anda</p>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'bank'); ?>
                                            <?= $form->dropDownList($model,'bank', Member::bankList(),['class'=>'form-control']);?>
                                            <?php echo $form->error($model,'bank'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'account_number'); ?>
                                            <?php echo $form->numberField($model,'account_number',array('class'=>'form-control')); ?>
                                            <?php echo $form->error($model,'account_number'); ?>
                      </div>
                      <div class="input-text">
                        <?php echo $form->labelEx($model,'account_name'); ?>
                                            <?php echo $form->textField($model,'account_name',array('class'=>'form-control')); ?>
                                            <?php echo $form->error($model,'account_name'); ?>
                      </div>
                      <?php else:?>
                      <h4>Password</h4>
                      <hr>
                      <p>Silahkan masukkan password Anda dan kode verifikasi di bawah ini:</p>
                      <div class="input-text">
                        <?php echo $form->labelEx($passwordForm,'password'); ?>
                                            <?php echo $form->passwordField($passwordForm,'password',array('maxlength'=>50,'class'=>'form-control','required'=>'required')); ?>
                                                    <?php echo $form->error($passwordForm,'password'); ?>
                      </div>
                      <div class="input-text">
                                            <?php echo $form->labelEx($passwordForm,'verifyCode'); ?><br>
                                                    <?php if(extension_loaded('gd')): ?>
                                                    <?php $this->widget('CCaptcha'); ?>
                                                    <?php endif;?>
                                                    <?php echo $form->textField($passwordForm,'verifyCode',array('maxlength'=>10,'class'=>'form-control','required'=>'required')); ?>
                                                    <?php echo $form->error($passwordForm,'verifyCode'); ?>
                      </div>
                      
                      <?php endif;?>
                    </div>
                    
                    <div class="col-sm-12">
                      <div class="input-text">
                        <input class="fixed-send animated" type="submit" value="Simpan">
                      </div>
                    </div>
                  </div>
                  <?php $this->endWidget(); ?> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <aside class="sidebar">
        <?php $this->widget('MemberPanelWidget');?>
      </aside>
    </div>
  </div>
</section>