<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <li><?=CHtml::link('Profil',['/member']);?></li>
                    <li><a class="active" href="#">Ewallet</a></li>    
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap page-about">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <?php Notify::renderMFlash();?>
                <div class="row">
                    <div class="col-md-12">
                        <?= CHtml::link('<button class="fixed-send animated">Tambah Deposit Ewallet</button>',['deposit']);?> atau <?= CHtml::link('<button class="fixed-send animated">Tarik Deposit Ewallet</button>',['withdrawal']);?><hr>
                        <?php if (isset($_GET['status'])=='pending'):?>
                            <?= CHtml::link('<button class="fixed-send animated">Approved Transaction</button>',['ewallet']);?>
                        <?php else:?>
                            <?= CHtml::link('<button class="fixed-send animated">Pending Transaction</button>',['ewallet','status'=>'pending']);?>
                        <?php endif;?>
                    </div>
                    <div class="col-md-4">
                        <div class="box info-box fx animated fadeInLeft" data-animate="fadeInLeft">
                            <h3>Kredit: <?php echo number_format($member->ewallet_credit);?></h3>
                        
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box warning-box fx animated fadeInLeft" data-animate="fadeInLeft">
                            <h3>Penarikan: <?php echo number_format($member->ewallet_debit);?></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box success-box fx animated fadeInLeft" data-animate="fadeInLeft">
                            <h3>Saldo: <?php echo number_format(($member->ewallet_credit - $member->ewallet_debit));?></h3>
                        </div>
                    </div> 
                </div>
                <div class="table-responsive">
                <table class="table table-bordered table-striped"">
                    <tbody>
                    <tr>
                        <th class="center-text">#</th>
                        <th>Tanggal</th>
                        <th>Saldo</th>
                        <th>Tipe</th>
                        <th>Keterangan</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                    <?php if($model):?>
                    <?php $i=1;foreach($model as $row):?>
                    <tr>
                        <td class="width-10"><?= $i;?></td>
                        <td><?= $row->created;?></td>
                        <td class="right-text"><?= number_format($row->value);?></td>
                        <td><?=$row->typename;?></td>
                        <td><?= $row->note;?></td>
                        <td class="right-text"><?= ($row->status==Ewallet::APPROVED)?number_format($row->total):'-';?></td>
                        <td><?= $row->statusname;?></td>
                        <td align="center">
                            <?= ($row->status==0)?CHtml::link('Cancel',['canceldeposit','id'=>$row->id],['class'=>'btn btn-default']):'-';?>
                            <?= ($row->status==0 and $row->type==Ewallet::CREDIT)?CHtml::link('Confirmation',['confirmationdeposit','id'=>$row->id],['class'=>'btn btn-default']):'';?>
                        </td>
                    </tr>
                    <?php $i++; endforeach;?>
                    <?php else:?>
                    <tr>
                        <td colspan="8">Ewallet Anda masih kosong.</td>
                    </tr>
                    <?php endif;?>
                    </tbody>
                </table>
                </div>
                <?php if($count):?>
                <div class="pagination right">
                <?php 
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header'=>'',
                    'footer'=>'',
                    'nextPageLabel'=>'»',
                    'prevPageLabel'=>'«',
                    'id'=>'link_pager',
                ))?>
                </div>
                <?php endif;?>
            </main>
            <aside class="sidebar">
                <?php $this->widget('MemberPanelWidget');?>
            </aside>
        </div>
    </div>
</section>