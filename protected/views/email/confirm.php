<p>Dear <?php echo $model->name;?>,<br/><br/>
    Selamat dan Sukses, terima kasih sudah bergabung bersama Okepay. Akun Anda:<br><br>
    
Sponsor      : <?php echo $sponsor;?><br>
Bank         : <?php echo $model->bank;?><br>
No. Rekening : <?php echo $model->account_number;?><br>
Atas Nama    : <?php echo $model->account_name;?><br>
Jumlah       : Rp. <?php echo number_format($value,2,".",",");?><br>

Setelah melakukan transfer mohon dikonfirmasi melalui website atau SMS. <br>
Untuk login silahkan klik link ini <?php echo $verifyUrl;?>
<br><br>Terima kasih.</p>