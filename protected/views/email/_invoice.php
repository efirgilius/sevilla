<p style="font-size:12px;color:rgb(34,34,34);padding:35px 0px 31px;font-family:Verdana;line-height:20px">Dear <?php echo $name;?>,<br/><br/>
    Produk pembelian Anda sudah diajukan dengan nomor invoice <strong><?php echo $order->code;?></strong>.<br>
    Berikut data pesanan Anda:</p>
<h3 style="font-size:15px;color:rgb(1,1,1);padding:0px 0px 8px;margin:0px;font-weight:bold">Data Pesanan</h3>
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="padding:0px;margin:0px;font-style:normal;font-variant:normal;font-weight:normal;font-size:12px;line-height:normal;font-family:Verdana">
    <thead style="padding:0px;margin:0px">
        <tr>
            <th width="66" bgcolor="#f9f9f9" style="padding:15px 0px 13px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Nomor</th>
            <th width="*" bgcolor="#f9f9f9" style="padding:15px 0px 13px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Nama Produk</th>
            <th width="122" bgcolor="#f9f9f9" style="padding:15px 0px 13px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Harga Satuan</th>
            <th width="122" bgcolor="#f9f9f9" style="padding:15px 0px 13px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Jumlah</th>
            <th width="132" bgcolor="#f9f9f9" style="padding:15px 0px 13px;color:rgb(64,64,64);font-weight:normal;text-align:center">Total Harga</th>
        </tr>
    </thead>
    <tbody style="padding:0px;margin:0px">
        <?php $i=1; foreach($order->orderItems as $item):?>
        <tr>
            <td style="font-size:9pt;color:rgb(102,102,102);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229);text-align:center"><?php echo $i;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:10px 8px;font-weight:normal;text-align:left;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><?php echo $item->name;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:10px 8px;font-weight:normal;text-align:center;font-family:Tahoma;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><?php echo $item->quantity;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:10px 8px;font-weight:normal;text-align:center;font-family:Tahoma;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">IDR <?php echo number_format($item->price);?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:10px 8px;font-weight:normal;text-align:center">IDR <?php echo number_format($item->quantity * $item->price);?></td>
        </tr>
        <?php $i++; endforeach;?>
        <tr>
            <td colspan="4" bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Biaya Pengiriman</td><td bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);">IDR <?php echo number_format($order->shipment_price);?></td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Angka Unik</td><td bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);">IDR <?php echo number_format($order->unique_code);?></td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><strong>Total</strong></td><td bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);"><strong>IDR <?php echo number_format($order->total_price + $order->shipment_price + $order->unique_code);?></strong></td>
        </tr>
    </tbody>
</table><br>
    <p style="font-size:12px;color:rgb(34,34,34);padding:35px 0px 31px;font-family:Verdana;line-height:20px">Silahkan untuk menyelesaikan proses pembayaran Anda sebelum <?php echo $order->expired_on;?> melalui nomor rekening dibawah ini:<br>
    Nama Bank  : <?php echo $order->bank;?><br>
    No. Rekening : <?php echo $order->acc_number;?><br>
    Atas Nama : <?php echo $order->acc_name;?><br></p>
    <h3 style="font-size:15px;color:rgb(1,1,1);padding:0px 0px 8px;margin:0px;font-weight:bold">Alamat Pengiriman</h3>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding:0px;margin:0px;font-style:normal;font-variant:normal;font-weight:normal;font-size:12px;font-family:verdana;line-height:16px">
        <tbody style="padding:0px;margin:0px">
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Penerima</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->first_name.' '.$order->addresss->last_name;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Alamat</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->address.', Kecamatan. '.$order->addresss->district.', '.$order->addresss->city.', '.$order->addresss->province.' - '.$order->addresss->post_code;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Nomor Telepon</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->mobile_phone;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Email</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->email;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            
        </tbody>
    </table><br><br>
    <p style="font-size:12px;color:rgb(34,34,34);padding:35px 0px 31px;font-family:Verdana;line-height:20px">Silahkan klik link di bawah ini untuk melakukan konfirmasi pembayaran.<br>
    <a href="<?php echo $verifyUrl;?>" target="_blank">Konfirmasi pembayaran</a><br>
    <br><br>Terima kasih.<br>RDP Enterprise</p>