<p>
    Dear Admin, <br><br>
    Sebuah pesanan dengan no. invoice <strong><?php echo $order->code;?></strong> telah disimpan, berikut detail pesanannya:<br><br>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="padding:0px;margin:0px;font-style:normal;font-variant:normal;font-weight:normal;font-size:12px;line-height:normal;font-family:Verdana">
    <thead style="padding:0px;margin:0px">
        <tr>
            <th width="66" bgcolor="#f9f9f9" style="padding:5px 0px 3px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Nomor</th>
            <th width="*" bgcolor="#f9f9f9" style="padding:5px 0px 3px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Nama Produk</th>
            <th width="122" bgcolor="#f9f9f9" style="padding:5px 0px 3px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Harga Satuan</th>
            <th width="122" bgcolor="#f9f9f9" style="padding:5px 0px 3px;color:rgb(64,64,64);font-weight:normal;text-align:center;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Jumlah</th>
            <th width="132" bgcolor="#f9f9f9" style="padding:5px 0px 3px;color:rgb(64,64,64);font-weight:normal;text-align:center">Total Harga</th>
        </tr>
    </thead>
    <tbody style="padding:0px;margin:0px">
        <?php $i=1; foreach($order->orderItems as $item):?>
        <tr>
            <td style="font-size:9pt;color:rgb(102,102,102);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229);text-align:center"><?php echo $i;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:5px 5px;font-weight:normal;text-align:left;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><?php echo $item->product->name;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:5px 5px;font-weight:normal;text-align:center;font-family:Tahoma;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><?php echo $item->quantity;?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:5px 5px;font-weight:normal;text-align:center;font-family:Tahoma;border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">IDR <?php echo number_format($item->price);?></td>
            <td style="font-size:9pt;color:rgb(96,96,96);padding:5px 5px;font-weight:normal;text-align:center">IDR <?php echo number_format($item->quantity * $item->price);?></td>
        </tr>
        <?php $i++; endforeach;?>
        <tr>
            <td colspan="4" bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)">Total</td><td bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);">IDR <?php echo number_format($order->shipment->price);?></td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);border-right-width:1px;border-right-style:solid;border-right-color:rgb(229,229,229)"><strong>Total</strong></td><td bgcolor="#f9f9f9" style="text-align: right;padding:15px 0px 13px;color:rgb(64,64,64);"><strong>IDR <?php echo number_format($order->total_price);?></strong></td>
        </tr>
    </tbody>
</table>
<br><br>
<h3 style="font-size:15px;color:rgb(1,1,1);padding:0px 0px 8px;margin:0px;font-weight:bold">Alamat Pengiriman</h3>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding:0px;margin:0px;font-style:normal;font-variant:normal;font-weight:normal;font-size:12px;font-family:verdana;line-height:16px">
        <tbody style="padding:0px;margin:0px">
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Penerima</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->first_name.''.$order->last_name;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Alamat</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->address.', Kecamatan. '.$order->district.', '.$order->city_type.' '.$order->addresss->city->name.', '.$order->addresss->region->name.', '.$order->addresss->country->name.' - '.$order->addresss->post_code;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Nomor Telepon</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->mobile_phone;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            <tr>
                <th width="162" bgcolor="#f9f9f9" style="padding:15px 5px 13px 20px;color:rgb(64,64,64);font-weight:normal;text-align:left">Email</th><td width="*" bgcolor="#ffffff" style="font-size:9pt;color:rgb(96,96,96);padding:15px 10px 13px;font-family:Tahoma;text-align:left"><?php echo $order->addresss->email;?></td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#e5e5e5" height="1" style="font-size:9pt;color:rgb(102,102,102)"></td>
            </tr>
            
        </tbody>
    </table><br><br>
    Thank you,
</p>