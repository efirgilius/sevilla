<!-- Page Title -->
<section class="breadcrumb-wrap">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1><?=$model->title;?></h1>
				<ul class="breadcrumb">
					<li>
					<?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
					</li>
					<li class="last"><?=$model->title;?></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<main class="main-content">
			<div class="content">
				<div class="contact-info">
					<div class="col-sm-12 mgb30">
						<?=$model->content;?>
					</div>
				</div>
			</div>
		</main>
	</div>
</div>