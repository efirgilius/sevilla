<?php if ($banner):?>
<!-- Slider -->
<div class="tp-banner-container">
	<div class="tp-banner" >
		<ul>
			<?php foreach ($banner as $key => $v):?>
			<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
				<img src="<?=$v->large_url;?>"  alt="slider_1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
				<div class="tp-caption 2 sft tp-resizeme" 
					data-x="center" data-hoffset="-240" 
					data-y="center" data-voffset="-99" 
					data-speed="300" 
					data-start="500" 
					data-easing="Power3.easeInOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300">
					<h1><?=$v->title;?></h1>
				</div>
				<!-- LAYER NR. 2 -->
				<div class="tp-caption text1 sft tp-resizeme" 
					data-x="center" data-hoffset="-363" 
					data-y="center" data-voffset="26" 
					data-speed="300" 
					data-start="800" 
					data-easing="Power3.easeInOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300"><?=$v->description;?> 
				</div>
				<?php if ($v->url):?>
				<div class="tp-caption btn01 sft tp-resizeme" 
					data-x="center" data-hoffset="-506" 
					data-y="center" data-voffset="140" 
					data-speed="300" 
					data-start="1000" 
					data-easing="Power3.easeInOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300"><a href="<?=$v->url;?>">Selengkapnya</a> 
				</div>
				<?php endif;?>
			</li>
			<?php endforeach;?>
		</ul>
	</div>
</div>
<?php endif;?>

<!-- Services -->
<section class="news-box">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<?=$this->g('front','text');?>
			</div>
		</div>
	</div>
</section>
<!-- Features post -->
<section class="feature-post popup-gallery">
	<div class="container">
		<div class="row">
			<div class="col-sm-12"><h2 class="title">Produk Unggulan</h2></div>
			<?php if($products):?>
			<?php foreach ($products as $k => $v):?>
			<div class="col-sm-4">
				<article class="feature">
					<p class="image">
						<?=CHtml::link(CHtml::image($v->medium_url),['/product/view','slug'=>$v->slug]);?>
					</p>
					<h3><?=CHtml::link($v->name.' - IDR '.number_format($v->salePrice),['/product/view','slug'=>$v->slug]);?></h3>
				</article>
			</div>
			<?php endforeach;?>
			<?php else:?>
				<p>Belum ada produk.</p>
			<?php endif;?>
		</div>
	</div>
</section>

<!-- Request -->
<section class="news-box">
	<div class="container">
		<div class="row">
			<div class="col-sm-12"><h2 class="title center">Berita Terbaru</h2></div>
			<?php if ($news):?>
			<?php foreach ($news as $key => $v):?>
			<div class="col-sm-6">
				<article class="post clearfix">
					<div class="media">
						<?php if ($v->image):?>
                        <p class="image">
                            <?=CHtml::link(CHtml::image($v->medium_url),['/news/view','slug'=>$v->slug]);?>
                        </p>
                        <?php endif;?>
						<p class="date"><?=date_format(date_create($v->created),'d');?><span><?=date_format(date_create($v->created),'M');?></span></p>
					</div><!-- /.media -->
					<h2 class="title-post">
						<?=CHtml::link($v->title,['/news/view','slug'=>$v->slug]);?>
					</h2><!-- /.title-post -->
					<div class="entry-post">
						<p><?=substr(strip_tags($v->content),0, 150);?></p>
					</div><!-- /.entry-post -->
					<div class="readmore">
						<p><?=CHtml::link('Selengkapnya',['/news/view','slug'=>$v->slug]);?></p>
					</div>
				</article>
			</div>
			<?php endforeach;?>
			<?php else:?>
				<p>Belum ada berita.</p>
			<?php endif;?>
		</div>
	</div>
</section>

<section class="more-info">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<div class="text">
					<h3>Need more information?</h3>
					<p>We can call you and help with your problem, just leave us your phone number.</p>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="subscribe">
					<form method="post" id="subscribeForm" class="subscribe-form" action="http://themesflat.com/html/consultant/contact/subscribe-process.php">
						<label>
							<input type="email" id="emailsubscribe" name="email" placeholder="Enter you email..." value="" required="required" title="Enter your email">
						</label>
						<input type="submit" class="submit" value="Subscribe">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>