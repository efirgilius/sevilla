<!-- Page Title -->
<section class="breadcrumb-wrap">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <ul class="breadcrumb">
                    <li>
                    <?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
                    </li>
                    <?php if ($filter):?>
                        <li><?=CHtml::link('Berita',['/news']);?></li>
                    <?php endif;?>
                    <li class="last"><?=$this->title;?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog post -->
<section class="page-wrap blog-post clearfix">
    <div class="container">
        <div class="row">
            <main class="main-content">
                <div class="content">
                    <?php if ($model):?>
                        <?php foreach ($model as $k => $v):?>
                            <article class="post clearfix">
                                <?php if ($v->image):?>
                                <div class="media">
                                    <p class="image">
                                        <?=CHtml::link(CHtml::image($v->medium_url),['/news/view','slug'=>$v->slug]);?>
                                    </p>
                                </div><!-- /.media -->
                                <?php endif;?>
                                <h2 class="title-post">
                                    <?=CHtml::link($v->title,['/news/view','slug'=>$v->slug]);?></h3>
                                </h2><!-- /.title-post -->
                                <div class="meta-post">
                                    <span class="date"><?=$this->getTime($v->created);?></span>
                                </div><!-- /.meta-post -->
                                <div class="entry-post">
                                    <p><?=substr(strip_tags($v->content),0, 335);?></p>
                                </div><!-- /.entry-post -->
                                <div class="readmore">
                                    <p><?=CHtml::link('Selengkapnya',['/news/view','slug'=>$v->slug],['class'=>'post-readmore']);?></p>
                                </div>
                            </article>
                        <?php endforeach;?>
                    <?php else:?>
                        <p>Belum ada berita.</p>
                    <?php endif;?>

                    <div class="pagination">
                        <?php $this->widget('CLinkPager', array(
                            'pages' => $pages,
                            'header'=>'',
                            'footer'=>'',
                            'nextPageLabel'=>'»',
                            'prevPageLabel'=>'«',
                            'firstPageLabel'=>'<',
                            'lastPageLabel'=>'>',
                            'id'=>'link_pager',
                            'htmlOptions'=>[
                                'class'=>'inline'
                            ]
                        ))?>
                    </div>
                </div>
            </main>
            <?php $this->widget('NewsSidebarWidget');?>
        </div>
    </div>
</section>