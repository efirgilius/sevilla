<!-- Page Title -->
<section class="breadcrumb-wrap">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1><?=$model->title;?></h1>
				<ul class="breadcrumb">
					<li>
					<?=CHtml::link(CHtml::image(Yii::app()->baseurl.'/images/common/icon-home.gif','icon home',['class'=>'img-home']).'Beranda',Yii::app()->homeurl);?>
					</li>
					<li><?=CHtml::link('Berita',['/news']);?></li>
					<li class="last"><?=$model->title;?></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<!-- Blog post -->
<section class="page-wrap blog-post blog-single">
	<div class="container">
		<div class="row">
			<main class="main-content">
				<div class="content">
					<article class="post clearfix">
						<?php if ($model->image):?>
						<div class="feature-post">
							<p class="image">
								<?=CHtml::image($model->large_url);?>
							</p>
						</div><!-- /.feature-post -->
						<?php endif;?>
						<h2 class="title-post">
							<?=$model->title;?>
						</h2><!-- /.title-post -->
						<div class="meta-post">
							<span class="date"><?=$this->getTime($model->created);?></span>
						</div><!-- /.meta-post -->
						<div class="entry-post">
							<?=$model->content;?>
						</div><!-- /.entry-post -->
						<div class="tags-share">
							<div class="tags">
								<span>Label: </span>
								<?php if ($model->tags):?>
								<ul>
									<?php foreach ($model->tagThis as $k => $v):?>
										<li><?=CHtml::link($v,['/news/tag','q'=>$v]);?></li>
									<?php endforeach;?>
								</ul>
								<?php endif;?>
							</div>
							<div class="share">
								<span>Bagikan: </span>
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"  aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</article>
				</div>
			</main>
			<?php $this->widget('NewsSidebarWidget');?>
		</div>
	</div>
</section>