<?php

class PageController extends Controller
{	
	public function actionView($slug)
	{
		$model = Content::model()->publish()->type('page')->findByAttributes(['slug'=>$slug]);

		$this->title = $model->title;
		$this->activeMenu = $model->slug;
		
		$this->render('view',[
			'model'=>$model
			]);
	}
}
