<?php

class SiteController extends Controller
{
	public $title;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->title = 'Home';
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
    {

    	Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?sensor=false',CClientScript::POS_END);
    	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseurl.'/javascript/gmap3.min.js',CClientScript::POS_END);

        $this->activeMenu='contact';
        
        $this->title='Contact Us';
        
        $model=new Contact;   
        $model->scenario='create';
        if(isset($_POST['Contact']))
        {
            $model->attributes=$_POST['Contact'];
            if($model->save())
            {
                $this->sendContact($model->subject, $model->email, $model->name, $model->message);
                Yii::app()->user->setFlash('success','Thank you, your message has been sent.');
                $this->refresh();
            }
        }
        $this->_removeSession();
        $this->render('contact',array('model'=>$model));
    }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
    {
        $this->layout = 'front';

        if(Yii::app()->member->id)
            $this->redirect(Yii::app()->homeUrl);
            
        $this->title='Login';
        $model=new LoginMemberForm;
                
        if (Yii::app()->member->getState('attempts-login') > 3) {
            $model->scenario = 'withCaptcha';                    
        }

        // collect user input data
        if(isset($_POST['LoginMemberForm']))
        {
                    
            $model->attributes=$_POST['LoginMemberForm'];
            if($model->validate() && $model->login())
            {
                if (Yii::app()->member->getState('userStatus') == 3)
                {
                    Yii::app()->member->setFlash('error','Your account has been deactivated.');
                    Yii::app()->member->setState('userStatus',0);
                }
                elseif (Yii::app()->member->getState('userStatus') == 2)
                {
                    Yii::app()->member->setFlash('error','Your account is suspended, please contact administrator to activate it again.');
                    Yii::app()->member->setState('userStatus',0);
                }
                else
                {
                    Yii::app()->member->setState('attempts-login', 0);
                    /*Yii::app()->member->authTimeout=7200;
                    Yii::app()->member->setState(CWebUser::AUTH_TIMEOUT_VAR,time()+Yii::app()->member->authTimeout);*/
                 
                        $this->redirect(array('/member/genealogy'));
                }
            }
            else
            {
                Yii::app()->member->setState('attempts-login', Yii::app()->member->getState('attempts-login', 0) + 1);
                     Yii::app()->member->setFlash('error','Email or password is invalid.');
            }
                        
        }
        $this->_removeSession();

        $this->render('login',array('model'=>$model));
    }
        
    public function actionForgotpassword()
    {
        $this->title='Lupa Password';
        $model=new ForgotForm('forgot');
        
        if(isset($_POST['ForgotForm']))
        {
            $model->attributes=$_POST['ForgotForm'];
            if($model->validate() && $model->forgot())
            {
                Key::model()->deleteAllByAttributes(array('member_id'=>$model->id));
                $key = sha1($model->id.time());
                $token = new Key;
                $token->token=$key;
                $token->member_id = $model->id;
                if($token->save(false))
                {
                    //echo "<a href=".Yii::app()->controller->createAbsoluteUrl('site/resetpassword',array('token'=>$key)).">tes</a>";exit;
                    $this->sendConfirmationPassword($model->email,$model->name,Yii::app()->controller->createAbsoluteUrl('site/resetpassword',array('token'=>$key)));
                    Yii::app()->member->setFlash('success','Password confirmation has been sent to your email.');
                    $this->refresh();
                    Yii::app()->end();
                }
            }
            else
            {
                Yii::app()->member->setFlash('error','We can\'t find your email address, please try again.');
                $this->refresh();
            }
        }
        $this->_removeSession();
        $this->render('forgot',array(
            'model'=>$model
        ));
    }
        
    public function actionResetpassword($token)
    {
        $this->title='Reset Password';
        $token = Key::model()->with('member')->findByAttributes(array('token'=>Yii::app()->request->getQuery('token')));
        if($token)
        {
            if(isset($_POST['ResetForm']))
            {
                $model= new ResetForm;
                $model->attributes=$_POST['ResetForm'];
                
                $modelUser=$token->member;
                if($model->validate())
                {                        
                    $modelUser->password=$modelUser->hashPassword($_POST['ResetForm']['verifypassword'],$modelUser->salt);
                    if($modelUser->save(false))
                    {   
                        $token = Key::model()->findByAttributes(array('token'=>Yii::app()->request->getQuery('token')));
                        $token->delete();
                        $this->render('reset',array('message'=>'Password has been changed. Now you can login with new password.'));
                    }
                }
                else
                {
                    $this->render('reset',array('model'=>$model,'user'=>$modelUser));
                }
            } 
            
                
                
            $date = time();
            if($token->expired_on <= $date) 
            {
                $user=$token->member;
                $model= new ResetForm;
                $this->render('reset',array('model'=>$model,'user'=>$user));
            } else {
                $this->render('reset',array('message'=>'Your token is expired.'));
            }
                
        } else {
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }
        
    public function actionRegister()
    {
        $this->layout = 'front';
        
        $this->title='Daftar Member';
        
        $model= new Member;
        $model->scenario = 'register';
        
        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];
            $model->status=1;
            if($model->save())
            {
                $model->card->saveAttributes(['status'=>1]);
                $model->getStoreMember();
                if($model->type!=='1')
                {
                    for ($i=1;$i<$model->card->type;$i++)
                    {
                        $model->getStoreMulti();
                    }
                }
                $this->saveToken($model);
                Yii::app()->member->setFlash('success','Pendaftaran sudah selesai dilakukan, kami sudah mengirimkan detail informasi akun ke alamat email Anda.');
                $this->redirect(array('login'));
            }
        }
            
            
        $this->render('register',array(
            'model'=>$model,
        ));
    }
    
    public function actionGetupline()
        {
            if(Yii::app()->request->getIsAjaxRequest()){
                   echo json_encode(array('id'=> Member::model()->loadUpline()));
            }
            else{
                   throw new CHttpException(404,'The requested page does not exist.');
            }
        }


	public function actionVerify($token)
    {
        $this->title='Account Verification';
        $token = Key::model()->with('member')->findByAttributes(array('token'=>Yii::app()->request->getQuery('token'),'type'=>1));
        
        if($token)
        {
            $member=Member::model()->findByPk($token->member_id);
            $member->status=1;
            $member->save(false);
            
            $token->delete();
            
            $this->render('verify');
        } else {
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    public function actionSubscribe()
    {
    	$model=new Subscribe;   
        if(Yii::app()->request->getIsAjaxRequest())
        {
            if(isset($_POST['Subscribe']))
            {
                $model->attributes=$_POST['Subscribe'];
                if($model->save()){
					echo '{ "alert": "success", "message": "You have been <strong>successfully</strong> subscribed to our Email List." }';
				} else {
					echo '{ "alert": "error", "message": "' . $model->error . '" }';
				}
            }

            Yii::app()->end();
        }
    }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	protected function sendContact($subject,$senderMail,$senderName,$message)
    {
        $email= $this->g('contact','mailbox_reply');
        if($email)
        {
            Yii::import('application.extensions.YiiMailer.YiiMailer');    
            $mail = new YiiMailer; 
            $body=$this->renderFile(Yii::app()->basePath.'/views/email/contact.php',array('name'=>$senderName,'subject'=>$subject,'message'=>$message),TRUE);

            $mail->SetFrom($email, 'Contact Inbox');
            $mail->AddAddress($email);
            $mail->AddReplyTo($senderMail,$senderName);
            $mail->Subject    = '[Contact Inbox] '.$subject;
            $mail->MsgHTML($body);

            if(!$mail->Send())
            {
                Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.User') ;
            }
        }
    }

    
    protected function sendConfirmationPassword($email,$name,$verifyUrl)
    {
        $setting= $this->g('contact','mailbox_reply');
        Yii::import('application.extensions.YiiMailer.YiiMailer');    
        $mail = new YiiMailer; 
        $body=$this->renderFile(Yii::app()->basePath.'/views/email/confirmPassword.php',array('name'=>$name,'verifyUrl'=>$verifyUrl),TRUE);

        $mail->SetFrom($setting, $this->g('general','site_name'));
        $mail->AddAddress($email, $name);
        $mail->Subject    = "Change Password Confirmation";
        $mail->MsgHTML($body);

        if(!$mail->Send())
        {
            Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.User') ;
        }
    }
	
	protected function saveToken($model)
    {
        $key = sha1($model->id.time());
        $token = new Key;
        $token->member_id = $model->id;
        $token->token=$key;
        $token->type=1;
        $token->save(false);
        {
                $this->sendNewConfirmation($model, $_POST['Member']['password'],Yii::app()->controller->createAbsoluteUrl('site/login'));
        }
                        
    }
    
    protected function sendNewConfirmation($model,$password,$verifyUrl)
    {
        $setting= $this->g('contact','mailbox_reply');
        Yii::import('application.extensions.YiiMailer.YiiMailer');        
        $mail = new YiiMailer; 
        $body=$this->renderFile(Yii::app()->basePath.'/views/email/newconfirm.php',array('model'=>$model,'password'=>$password,'verifyUrl'=>$verifyUrl),TRUE);

        $mail->SetFrom($setting, $this->g('general','site_name'));
        $mail->AddAddress($model->email, $model->name);
        $mail->Subject    = "Selamat Datang Di ".$this->g('general','site_name');
        $mail->MsgHTML($body);

        if(!$mail->Send())
        {
            Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.Member') ;
            return false;
        }
        else
            return true;
    }

    protected function sendConfirmation($sender,$email,$verifyurl)
    {
        $setting= $this->g('contact','mailbox_reply');
        Yii::import('application.extensions.YiiMailer.YiiMailer');        
        $mail = new YiiMailer; 
        $body=Yii::app()->controller->renderFile(Yii::app()->basePath.'/views/email/invitation.php',array('sender'=>$sender,'email'=>$email,'url'=>$verifyurl),TRUE);

        $mail->SetFrom($setting, 'Umroh Network Invitation');
        $mail->AddAddress($email, 'Member Applicant');
        $mail->Subject    = "Member Invitation of Umroh Network";
        $mail->MsgHTML($body);
        if(!$mail->Send())
        {
            Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.Member') ;
            return 'error';
        }
        else
            return 'true';
    }

    protected function _removeSession()
    {
        $session = Yii::app()->session;
        $prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
        foreach($session->keys as $key)
        {
                if(strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
                        $session->remove($key);
        }
    }
}