<?php

class NewsController extends Controller
{	
	public $activeMenu = 'news';
	public $useSidebar = true;

	public function actionIndex()
	{
		$this->title = 'Berita';

		$criteria=new CDbCriteria;
		
        $count = Content::model()->type('news')->order('t.created DESC')->publish()->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        $model = Content::model()->type('news')->order('t.created DESC')->publish()->findAll($criteria);
		
		$this->render('index',array(			
            'pages' => $pages,
            'count'=>$count,
            'model'=>$model,
            'filter'=>false 
		));
	}

	public function actionTag($q)
	{
		$this->title = 'Label "'.ucfirst($q).'"';

		$criteria=new CDbCriteria;
		
        $count = Content::model()->type('news')->tag($q)->order('t.created DESC')->publish()->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        $model = Content::model()->type('news')->tag($q)->order('t.created DESC')->publish()->findAll($criteria);
		
		$this->render('index',array(			
            'pages' => $pages,
            'count'=>$count,
            'model'=>$model, 
            'q'=>ucfirst($q),
            'filter'=>true
		));
	}

	public function actionView($slug)
	{
		$model = Content::model()->publish()->type('news')->publish()->findByAttributes(['slug'=>$slug]);

		$this->title = $model->title;
		
		$this->render('view',[
			'model'=>$model
			]);
	}
}
