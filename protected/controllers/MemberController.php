<?php

class MemberController extends Controller
{
    
    public $_model;
    public $useSidebar = true;

    public function init() {
        if(!Yii::app()->member->id)
                $this->redirect(array('/site/login'));
            $this->_model= Yii::app()->member->model;
       return parent::init();
    }
    
    public function actions()
        {
                return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                'class'=>'CCaptchaAction',
                                'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                'class'=>'CViewAction',
                        ),
                );
        }

    public function actionIndex()
    {
        $this->activeSubMenu='profile';
        $model = $this->_model;
        $this->title='Hai, '.$model->name;
        
        $this->render('index',[
            'model'=>$model,
        ]);
    }
    
    public function actionChangepassword()
    {
        $this->title='Ganti Password';
        $this->activeSubMenu='cpass';
        
        $model= new MemberResetForm('resetMember');
        $user=  $this->_model;
        
        if(isset($_POST['MemberResetForm']))
        {
            $model->attributes=$_POST['MemberResetForm'];
            if($model->validate() && $model->resetMember())
            {
                $user->password=sha1($user->salt.$_POST['MemberResetForm']['verifypassword']);
                if($user->save(false))
                {
                    Yii::app()->member->setFlash('success','Password berhasil diganti.');
                    $this->redirect(array('/member'));
                }
            }
        }
        $this->render('password',array(
            'user'=>$user,
            'model'=>$model,
        )); 
    }

    public function actionUpdateprofile()
    {
        $this->title='Ubah Profil';
        $this->activeSubMenu='uprofile';
        
        $model= $this->_model;
        // $img = $model->avatar;
        
        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];
            if($model->save())
            {
                // if($_FILES['Member']['name']['avatar'] !== '') 
                // {
                //     $uploadedFile=CUploadedFile::getInstance($model, 'avatar');       
                //     $filename=$this->generateName($uploadedFile->name);
                //     $uploadTo=$this->_createDir($model->mid).'/'.$filename;
                //     if($uploadedFile->saveAs($uploadTo))
                //     {
                //         $model->avatar=$filename;
                //         $model->save(false);
                //     }
                //     if(is_file($this->getDirUrl($model->mid).'/'.$img)){
                //         chmod($this->getDirUrl($model->mid).'/'.$img, 0755);
                //         unlink($this->getDirUrl($model->mid).'/'.$img);
                //     } 
                //     $this->removeImg($model->mid,'large',$img);
                //     $this->removeImg($model->mid,'small',$img);
                // }

                Yii::app()->member->setFlash('success','Profil berhasil diubah.');
                $this->redirect(['/member']);
            }
        }
            
        $this->render('update',[
            'model'=>$model
        ]);
    }
    
    public function actionUpdateBank()
    {
        if ($this->_model->is_update_bank_acc==0) {
            $this->title='Permintaan Ubah Akun Rekening Bank';
            $model= new MemberBank;
            $passwordForm=new PasswordForm;
            $valid=false;
            
            if(isset($_POST['PasswordForm']))
            {
                $passwordForm->attributes=$_POST['PasswordForm'];
                if($passwordForm->validate() && $passwordForm->validatePassword())
                    $valid=true;
                else
                    Yii::app()->member->setFlash('error','Password salah.');                        
            }
                
            if(isset($_POST['MemberBank'])){
                $model->attributes= $_POST['MemberBank'];
                if($model->save()){
                    if ($this->_model->saveAttributes(['is_update_bank_acc'=>1])){
                        Yii::app()->member->setFlash('success','Permintaan Anda telah terkirim. Kami akan memverifikasinya terlebih dahulu.');
                        $this->redirect(['/member']);
                    }
                }
            }
            
            $this->_removeSession();
            $this->render('updateBank',[
                'model'=>$model,
                'valid'=>$valid,
                'passwordForm'=>$passwordForm
            ]);
            
        }elseif ($this->_model->is_update_bank_acc=='1') {
            $this->title='Permintaan Ubah Akun Rekening Bank';
            $model= MemberBank::model()->find('member_id=:mid AND status=0',array(':mid'=>(int)Yii::app()->member->id));
            if($model){
                $this->render('updateBankDetail',[
                    'model'=>$model
                ]);
            }
        }elseif ($this->_model->is_update_bank_acc==2) {
            Yii::app()->member->setFlash('success','Permintaan Anda sebelumnya sudah dikonfirmasi. Silahkan melakukan perubahan pada informasi Bank Anda.');
                $this->redirect(['index']);
        }
    }
    
    public function actionCancelrequest()
    {
        $model= MemberBank::model()->find('member_id=:mid AND status=0',array(':mid'=>(int)Yii::app()->member->id));
        if($model){
            if($model->delete()){
                $this->_model->saveAttributes(['is_update_bank_acc'=>0]);
                Yii::app()->member->setFlash('success','Pembatalan perubahan data akun bank sudah berhasil dilakukan.');
                $this->redirect(['index']);
            }
        }
        else{
            Yii::app()->member->setFlash('error','Anda belum melakukan permintaan perubahan akun bank.');
            $this->redirect(['/member']);
        }
    }
    
    public function actionNetwork($id=false,$level=false)
    {
        $this->activeSubMenu='network';
        $this->title='Genealogy Jaringan Sponsor';
        
        if($id)
            {
                if($id!==Yii::app()->member->id)
                {
                    $member= $this->loadModel($id);
                    $val=  Network::model()->findByAttributes(array('member_id'=>$member->id,'upline_id'=>Yii::app()->member->id));
                    if($val===NULL)
                    {
                        Yii::app()->member->setFlash('error','Member ID can not be found in your network.');
                        $this->redirect('network');
                    }
                    $memId=$id;
                    $level=1;
                }
                else
                {
                    $member=Yii::app()->member->model;
                    $memId=Yii::app()->member->id;
                    $level=1;
                }
            }
            elseif($level)
            {
                $memId=Yii::app()->member->id;
                $level=$level;
            }
            else
            {
                $memId=Yii::app()->member->id;
                $level=1;
            }
            
            $criteria=new CDbCriteria;
                $criteria->condition='t.sponsor_id=:id AND t.level=:lvl';
                $criteria->params=array(':id'=>  $memId,':lvl'=>$level);
                $criteria->order='t.id ASC';
                $count = NetworkMatrix::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=NetworkMatrix::model()->findAll($criteria);
                
            if($id && ($id!==Yii::app()->member->id))
            {
                $this->render('network',array(
                        'member'=>$member,
                        'level'=>$level,
            'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
        ));
            }
            else
            {
                $this->render('network',array(
                        'level'=>$level,
            'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
        ));
            }
    }
    
    public function actionGenealogy($id=false)
        {
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile(Yii::app()->request->baseUrl.'/stylesheets/custom.css');
            $this->title='Genealogy Jaringan';
            $this->activeSubMenu='genealogy';
            
            if($id)
            {
                if($id!==Yii::app()->member->id)
                {
                    $member= $this->loadModel($id);
                    $val=  Network::model()->findByAttributes(array('member_id'=>$member->id,'upline_id'=>Yii::app()->member->id));
                    if($val)
                    {
                        $upline=$member->upline;
                        $memId=$member->id;
                    }
                    else
                    {
                        Yii::app()->member->setFlash('error','ID Member tidak ditemukan di jaringan Anda.');
                        $memId=Yii::app()->member->id;
                        $upline=null;
                    }
                }
                else
                {
                    $memId=Yii::app()->member->id;
                    $upline=null;
                }
            }
            else
            {
                $memId=Yii::app()->member->id;
                $upline=null;
            }
            
            if(isset($_POST['member_mid']))
            {
                $mid=$_POST['member_mid'];
                $member= $this->loadModel($mid);
                if($member)
                {
                    $val=  Network::model()->findByAttributes(array('member_id'=>$member->id,'upline_id'=>Yii::app()->member->id));
                    if($val)
                    {
                        $upline=$member->upline;
                        $memId=$member->id;
                    }
                    else
                    {
                        Yii::app()->member->setFlash('error','ID Member tidak ditemukan di jaringan Anda.');
                        $memId=Yii::app()->member->id;
                        $upline=null;
                    }
                }
                else
                    Yii::app()->member->setFlash('error','ID Member tidak ditemukan di jaringan Anda.');
            }
            
            $model2_l=NULL;
            $model2_r=NULL;
            
            $model3_2l_l=NULL;
            $model3_2l_r=NULL;
            $model3_2r_l=NULL;
            $model3_2r_r=NULL;
            
            $model4_32ll_l=NULL;
            $model4_32ll_r=NULL;
            $model4_32lr_l=NULL;
            $model4_32lr_r=NULL;
            $model4_32rl_l=NULL;
            $model4_32rl_r=NULL;
            $model4_32rr_l=NULL;
            $model4_32rr_r=NULL;
            
            $model=  Member::model()->findByPk($memId);
            if($model->foot_left !== NULL)
            {
                $model2_l=  Member::model()->findByPk($model->foot_left);
                if(!empty($model2_l))
                {
                    if($model2_l->foot_left !== NULL)
                    {
                        $model3_2l_l=Member::model()->findByPk($model2_l->foot_left);
                        if(!empty($model3_2l_l))
                        {
                            if($model3_2l_l->foot_left !== NULL)
                            {
                                $model4_32ll_l=Member::model()->findByPk($model3_2l_l->foot_left);
                            }
                            
                            if($model3_2l_l->foot_right !== NULL)
                            {
                                $model4_32ll_r=Member::model()->findByPk($model3_2l_l->foot_right);
                            }
                        }
                    }
                    
                    if($model2_l->foot_right !== NULL)
                    {
                        $model3_2l_r=Member::model()->findByPk($model2_l->foot_right);
                        if(!empty($model3_2l_r))
                        {
                            if($model3_2l_r->foot_left !== NULL)
                            {
                                $model4_32lr_l=Member::model()->findByPk($model3_2l_r->foot_left);
                            }
                            
                            if($model3_2l_r->foot_right !== NULL)
                            {
                                $model4_32lr_r=Member::model()->findByPk($model3_2l_r->foot_right);
                            }
                        }
                    }        
                }
            }
            
            if($model->foot_right !== NULL)
            {
                $model2_r=  Member::model()->findByPk($model->foot_right);
                if(!empty($model2_r))
                {
                    if($model2_r->foot_left !== NULL)
                    {
                        $model3_2r_l=Member::model()->findByPk($model2_r->foot_left);
                        if(!empty($model3_2r_l))
                        {
                            if($model3_2r_l->foot_left !== NULL)
                            {
                                $model4_32rl_l=Member::model()->findByPk($model3_2r_l->foot_left);
                            }
                            
                            if($model3_2r_l->foot_right !== NULL)
                            {
                                $model4_32rl_r=Member::model()->findByPk($model3_2r_l->foot_right);
                            }
                        }
                    }
                    
                    if($model2_r->foot_right !== NULL)
                    {
                        $model3_2r_r=Member::model()->findByPk($model2_r->foot_right);
                        if(!empty($model3_2r_r))
                        {
                            if($model3_2r_r->foot_left !== NULL)
                            {
                                $model4_32rr_l=Member::model()->findByPk($model3_2r_r->foot_left);
                            }
                            
                            if($model3_2r_r->foot_right !== NULL)
                            {
                                $model4_32rr_r=Member::model()->findByPk($model3_2r_r->foot_right);
                            }
                        }
                    }
                }
            }
            
                $this->render('genealogy',array(
                    'upline'=>$upline,
                    'model'=>$model,
                    'model2_l'=>$model2_l,
                    'model2_r'=>$model2_r,
                    'model3_2l_l'=>$model3_2l_l,
                    'model3_2l_r'=>$model3_2l_r,
                    'model3_2r_l'=>$model3_2r_l,
                    'model3_2r_r'=>$model3_2r_r,
                    'model4_32ll_l'=>$model4_32ll_l,
                    'model4_32ll_r'=>$model4_32ll_r,
                    'model4_32lr_l'=>$model4_32lr_l,
                    'model4_32lr_r'=>$model4_32lr_r,
                    'model4_32rl_l'=>$model4_32rl_l,
                    'model4_32rl_r'=>$model4_32rl_r,
                    'model4_32rr_l'=>$model4_32rr_l,
                    'model4_32rr_r'=>$model4_32rr_r,
                ));
        }
        
        public function actionPin($type=false)
        {
            $this->title='PIN Stock';
            $this->activeSubMenu='pin';
            
            if($type){
            if($type=='standard')
                $tp=1;
            elseif($type=='premium')
                $tp=3;
            elseif($type=='deluxe')
                $tp=2;
            elseif($type=='executive')
                $tp=4;
            elseif($type=='diamond')
                $tp=5;
            else
                throw new CHttpException(404,'The drequested page does not exist.');
            }
            else
                $type=null;
           
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $month=$_GET['m'];
                $year=$_GET['y'];
            }
            else
            {
                $month=null;
                $year=null;
            }
            
            $pin=  Card::model()->getTotal();
            $pinA=  Card::model()->getTotalType(1);
            $pinB=  Card::model()->getTotalType(2);
            $pinC=  Card::model()->getTotalType(3);
            $pinD=  Card::model()->getTotalType(4);
            $pinE=  Card::model()->getTotalType(5);
            $pinActive=  Card::model()->countByAttributes(array('member_id'=>Yii::app()->member->id,'status'=>0));
            $pinActiveA=  Card::model()->getTotalType(1,'0');
            $pinActiveB=  Card::model()->getTotalType(2,'0');
            $pinActiveC=  Card::model()->getTotalType(3,'0');
            $pinActiveD=  Card::model()->getTotalType(4,'0');
            $pinActiveE=  Card::model()->getTotalType(5,'0');
            $pinInactive=  Card::model()->countByAttributes(array('member_id'=>Yii::app()->member->id,'status'=>1));
            $pinInactiveA=  Card::model()->getTotalType(1,'1');
            $pinInactiveB=  Card::model()->getTotalType(2,'1');
            $pinInactiveC=  Card::model()->getTotalType(3,'1');
            $pinInactiveD=  Card::model()->getTotalType(4,'1');
            $pinInactiveE=  Card::model()->getTotalType(5,'1');
            
            $criteria=new CDbCriteria;
            if($type)
            {
                $criteria->condition='t.member_id=:id AND t.type=:tp ';
                $criteria->params=array(':id'=>  Yii::app()->member->id,':tp'=>$tp);
            }
            else
            {
                $criteria->condition='t.member_id=:id';
                $criteria->params=array(':id'=>  Yii::app()->member->id);
            } 
                $criteria->order='t.id ASC';
                $count = Card::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=Card::model()->findAll($criteria);
                
            $this->render('pin',array(
                'title'=>$type,
                'm'=>$month,
                        'y'=>$year,
                'pin'=>$pin,
                'pinA'=>$pinA,
                'pinB'=>$pinB,
                'pinC'=>$pinC,
                'pinD'=>$pinD,
                'pinE'=>$pinE,
                'pinActive'=>$pinActive,
                'pinActiveA'=>$pinActiveA,
                'pinActiveB'=>$pinActiveB,
                'pinActiveC'=>$pinActiveC,
                'pinActiveD'=>$pinActiveD,
                'pinActiveE'=>$pinActiveE,
                'pinInactive'=>$pinInactive,
                'pinInactiveA'=>$pinInactiveA,
                'pinInactiveB'=>$pinInactiveB,
                'pinInactiveC'=>$pinInactiveC,
                'pinInactiveD'=>$pinInactiveD,
                'pinInactiveE'=>$pinInactiveE,
                'model'=>$model,
                'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
            ));
        }
        
    public function actionRegister()
    {
        $this->title='Register Anggota Baru';
        $model= new Member;
        $model->scenario = 'register';
        $model->sponsor_mid= $this->_model->mid;
        
        if(isset($_POST['Member']))
        {
            $model->attributes= $_POST['Member'];
            $model->status=2;
            if($model->save())
            {
                $this->saveToken($model);
                Yii::app()->member->setFlash('success','Pendaftaran sudah selesai dilakukan, kami sudah mengirimkan detail informasi akun ke alamat email member.');
                $this->redirect(array('login'));
            }
        }
        
        $this->render('register',[
            'model'=>$model
        ]);
    }
    
    public function actionSponsorbonus()
        {
            $this->title='Bonus Sponsor';
            $this->activeSubMenu='sbonus';
            
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $month=$_GET['m'];
                $year=$_GET['y'];
            }
            else
            {
                $month=date('m');
                $year=date('Y');
            }
            
            $criteria=new CDbCriteria;
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $criteria->condition='t.member_id=:id AND MONTH(t.date)=:mt AND YEAR(t.date)=:yr';
                $criteria->params=array(':id'=>  Yii::app()->member->id,':mt'=>$month,':yr'=>$year);
            }
            else
            {
                $criteria->condition='t.member_id=:id';
                $criteria->params=array(':id'=>  Yii::app()->member->id);
            }
                $criteria->order='t.id DESC';
                $count = BonusSponsor::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=BonusSponsor::model()->findAll($criteria);
                
		$this->render('sponsor',array(
			'model'=>$model,
                        'm'=>$month,
                        'y'=>$year,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
        }
        
        public function actionPairingbonus()
        {
            $this->title='Bonus Pasangan';
            $this->activeSubMenu='pbonus';
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $month=$_GET['m'];
                $year=$_GET['y'];
            }
            else
            {
                $month=null;
                $year=null;
            }
            
            $criteria=new CDbCriteria;
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $criteria->condition='t.member_id=:id AND MONTH(t.date)=:mt AND YEAR(t.date)=:yr';
                $criteria->params=array(':id'=>  Yii::app()->member->id,':mt'=>$month,':yr'=>$year);
            }
            else
            {
                $criteria->condition='t.member_id=:id';
                $criteria->params=array(':id'=>  Yii::app()->member->id);
            }
                $criteria->order='t.id DESC';
                $count = BonusCouple::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=BonusCouple::model()->findAll($criteria);
                
        $this->render('couple',array(
            'model'=>$model,
                        'm'=>$month,
                        'y'=>$year,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
        ));
            
        }
        
        public function actionLoyaltybonus()
        {
            $this->title='Bonus Loyalty';
            $this->activeSubMenu='lbonus';
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $month=$_GET['m'];
                $year=$_GET['y'];
            }
            else
            {
                $month=null;
                $year=null;
            }
            
            $criteria=new CDbCriteria;
            if(isset($_GET['m']) && isset($_GET['y']))
            {
                $criteria->condition='t.member_id=:id AND MONTH(t.date)=:mt AND YEAR(t.date)=:yr';
                $criteria->params=array(':id'=>  Yii::app()->member->id,':mt'=>$month,':yr'=>$year);
            }
            else
            {
                $criteria->condition='t.member_id=:id';
                $criteria->params=array(':id'=>  Yii::app()->member->id);
            }
                $criteria->order='t.id DESC';
                $count = BonusLoyalty::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=BonusLoyalty::model()->findAll($criteria);
                
        $this->render('loyalty',array(
            'model'=>$model,
                        'm'=>$month,
                        'y'=>$year,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
        ));
            
        }
        
        public function actionReward()
        {
            $this->title='Reward';
            $this->activeSubMenu='reward';
            
            
            $criteria=new CDbCriteria;
           
                $criteria->condition='t.member_id=:id';
                $criteria->params=array(':id'=>  Yii::app()->member->id);
                $criteria->order='t.id DESC';
                $count = Reward::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=  Reward::model()->findAll($criteria);
                
		$this->render('reward',array(
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
        }
        
        public function actionStatement()
        {
            $this->title='Statement Bonus';
            $this->activeSubMenu='statement';
            
            if(isset($_POST['m']))
            {
                    $month=$_POST['m'];
                    $year=$_POST['y'];
                    $day=null;
                    $query_date=$year.'-'.$month.'-01';
                    $period=Setting::getMonth($month).' '.$year;
                
            }
            else
            {
                $day=date('j');
                $month=date('m');
                $year=date('Y');
                $query_date=date('Y-m-d');
                $period=date("F Y");
            }
            
            $dates=  $this->dateRange(date('Y-m-01', strtotime($query_date)),date('Y-m-t', strtotime($query_date)));
            $sp= BonusSponsor::model()->getTotal();
            $bc= BonusCouple::model()->getTotal();
            $bl= BonusLoyalty::model()->getTotal();
            
            $this->render('statement',array(
                'period'=>$period,
                'bonus'=>$sp + $bc + $bl,
                'dates'=>$dates,
            ));
        }
    
    public function actionEwallet($status='approved')
    {
        $this->title='Ewallet';        

        $member= Yii::app()->member->model;

        $criteria=new CDbCriteria;

        switch ($status) {
            case 'approved':
                $criteria->condition='t.member_id=:mid and status=:st';
                break;
            case 'pending':
                $this->title = 'Pending Transaction';
                $criteria->condition='t.member_id=:mid and status!=:st';
                break;
        }

        $criteria->params=array(':mid'=> Yii::app()->member->id,':st'=>1);
        $criteria->order='t.id DESC';

        $count = Ewallet::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $model=  Ewallet::model()->findAll($criteria);
        $this->render('ewallet',array(
                'member'=>$member,
                'model'=>$model,
                'pages' => $pages,
                'count'=>$count,
                'i'=>$pages->offset+1
        ));
    }

    public function actionConfirmationDeposit($id)
    {
        $this->title='Konfirmasi Deposit';
        if($id)
        {
            $ewallet = $this->loadModelEWallet($id);
            if($ewallet)
            {
                $model=new EwalletConfirmation;
                $model->ewallet_id=$ewallet->id;
                $model->name=$ewallet->member->name;
                $model->value= $ewallet->value;
            }
            else {
                $model=new EwalletConfirmation;
            }
        }
        else
            $model=new EwalletConfirmation;
        
        if(isset($_POST['EwalletConfirmation']))
        {
            //print_r($_POST);exit;
            $model->attributes=$_POST['EwalletConfirmation'];
            if($ewallet)
            {
                $model->ewallet_id=$ewallet->id;
                $model->name=$ewallet->member->name;
            }
            $model->bank_id= $_POST['EwalletConfirmation']['bank_id'];
            if($model->save())
            {
                $ewallet=$model->ewallet;
                $ewallet->status=Ewallet::CONFIRM;
                if($ewallet->save(false))
                
                Yii::app()->member->setFlash('success','Konfirmasi deposit sudah disimpan, kami akan memverifikasinya sesegera mungkin, terim kasih.');
                $this->redirect(['ewallet','status'=>'pending']);
            }
        }
        $banks=  Bank::model()->findAll();
        $this->render('confirmation',array(
            'model'=>$model,
            'banks'=>$banks
        ));
    }
    
    public function actionWithdrawal()
    {
        
        $find= Ewallet::model()->find('member_id=:mid AND status=:confirm or status=:pending',[':mid'=>$this->_model->id,':pending'=>0,':confirm'=>2]);
        if($find)
        {
            Yii::app()->member->setFlash('error','Anda masih mempunyai transaksi yang belum diverifikasi.');
            $this->redirect(['ewallet']); 
        }
        elseif($this->_model->balance<50000)
        {
            Yii::app()->member->setFlash('error','Saldo ewallet Anda tidak mencukupi untuk melakukan penarikan.');
            $this->redirect(['ewallet']); 
        }
        else
        {
        
            $this->title='Tarik Deposit Ewallet';

            $model = new Ewallet;
            if(isset($_POST['Ewallet']))
            {
                $total_penarikan = $_POST['Ewallet']['value'];

                $model->value=$total_penarikan;

                $model->type=Ewallet::DEBIT;
                $model->member_id= Yii::app()->member->id;
                $model->status=0;
                $model->note='Tarik Ewallet';

                if($model->save())
                {
                    Yii::app()->member->setFlash('success','Penarikan Anda sudah disimpan, kami akan melakukan pembayaran setelah diverifikasi terlebih dahulu.');
                    $this->redirect(['ewallet','status'=>'pending']);
                }
            }

            $this->render('withdrawal',[
                'model'=>$model
            ]);
        }
    }
        
    public function actionDeposit()
    {
        $find= Ewallet::model()->find('member_id=:mid AND status=:confirm or status=:pending',[':mid'=>$this->_model->id,':pending'=>0,':confirm'=>2]);
        if($find)
        {
            Yii::app()->member->setFlash('error','Anda masih mempunyai transaksi ewallet yang belum diverifikasi.');
            $this->redirect(['ewallet']); 
        }
        
        $this->title='Tambah Deposit Ewallet';
        $model= new Ewallet;
        $model->scenario='transfer';
        
        if(isset($_POST['Ewallet']))
        {
            $model->member_id= Yii::app()->member->id;
            $model->value= $_POST['Ewallet']['value'];
            $model->note='Deposit Transfer';
            $model->type=Ewallet::CREDIT;
            $model->status=0;
            if ($model->value >= 100000) {
                if($model->save())
                {
                    Yii::app()->member->setFlash('success','Deposit Anda sudah disimpan, silahkan melakukan transfer sebesar Rp '.  number_format($model->value).' dan segera konfirmasi pembayaran anda.');
                    $this->redirect(['ewallet','status'=>'pending']);
                }
            }else{
                Yii::app()->member->setFlash('error','Minimal deposit Rp.100.000.');
                $this->refresh(); 
            }
        }
        
        $this->render('deposit',[
            'model'=>$model
        ]);
    }

    public function actionCanceldeposit($id)
    {
        $model= $this->loadModelEWallet($id);
        if($model->delete())
        {
            Yii::app()->member->setFlash('success','Deposit ewallet sudah berhasil dibatalkan');
            
        }
        else
            Yii::app()->member->setFlash('error','Deposit Anda tidak bisa diproses');
        
        $this->redirect(['ewallet']);
    }
        
        public function actionClaimreward()
        {
            $reward= Reward::model()->getReward();
            if($reward)
            {
                $model= new Reward;
                $model->member_id=Yii::app()->member->id;
                $model->debit=$reward->pv;
                $model->reward=$reward->reward;
                if($model->save())
                {
                    Yii::app()->member->setFlash('success','Your reward has been claimed');
                    $this->redirect(['reward']);
                }
            }
            else
                throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }


        public function actionRequestTicket()
        {
            $find= StockTicket::model()->find('member_id=:mid AND status=:st',[':mid'=>  $this->_model->id,':st'=>0]);
            if($find)
            {
                Yii::app()->member->setFlash('error','Anda masih mempunyai pemesanan yang belum diverifikasi.');
                $this->redirect(['ticket']); 
            }
            
            $this->title='Pesan Ticket';
            $model= new StockTicket;
            
            if(isset($_POST['StockTicket']))
            {
                $model->member_id= Yii::app()->member->id;
                $model->value= $_POST['StockTicket']['value'];
                $model->type=0;
                $model->status=0;
                if($model->save())
                {
                    Yii::app()->member->setFlash('success','Pemesanan Anda sudah disimpan, silahkan melakukan transfer sebesar Rp '.  number_format($model->price).' ke bank dan nomor rekening yang Anda pilih.');
                    $this->redirect(['ticket']);
                }
            }
            
            $this->render('requestTicket',[
                'model'=>$model
            ]);
        }

        public function actionRequestTicketToUpline()
        {
            $find= StockTicket::model()->find('member_id=:mid AND status=:st',[':mid'=>  $this->_model->id,':st'=>0]);
            if($find)
            {
                Yii::app()->member->setFlash('error','Anda masih mempunyai pemesanan yang belum diverifikasi.');
                $this->redirect(['ticket']); 
            }

            $findUl= Network::model()->find('member_id=:mid',[':mid'=>$this->_model->id]);
            if(!$findUl)
            {
                Yii::app()->member->setFlash('error','Anda tidak mempunyai upline.');
                $this->redirect(['ticket']); 
            }
            
            $this->title='Pesan Ticket';
            $model= new StockTicket;
            
            if(isset($_POST['StockTicket']))
            {
                $model->member_id= Yii::app()->member->id;
                $model->value= $_POST['StockTicket']['value'];
                $model->upline_id= $_POST['StockTicket']['upline_id'];
                $model->status=0;
                $cxUl = Network::model()->findByAttributes(['member_id'=>Yii::app()->member->id,'upline_id'=>$model->upline_id]);
                if ($cxUl) {
                    if ($cxUl->upline->ticketBalance>0) {
                        if ($cxUl->upline->ticketBalance >= $_POST['StockTicket']['value']) {
                            $model->type=StockTicket::UPLINE;
                            $model->upline_id = $model->upline_id;
                            if($model->save())
                            {
                                Yii::app()->member->setFlash('success','Pemesanan Anda sudah dikirim ke Upline dan menunggu konfirmasi dari Upline.');
                                $this->redirect(['ticket']);
                            }
                        }else{
                            Yii::app()->member->setFlash('error','Saat ini stock ticket upline kurang dari jumlah pemesanan Anda.');
                        }
                    }else{
                        Yii::app()->member->setFlash('error','Saat ini Upline belum mempunyai stock ticket.');
                    }
                }else{
                    Yii::app()->member->setFlash('error','Upline tidak ditemukan atau bukan termasuk jaringan Anda.');
                }
                // if($model->save())
                // {
                //     Yii::app()->member->setFlash('success','Pemesanan Anda sudah disimpan, silahkan melakukan transfer sebesar Rp '.  number_format($model->price).' ke bank dan nomor rekening yang Anda pilih.');
                //     $this->redirect(['ticket']);
                // }
            }
            
            $upline = Network::model()->findAllByAttributes(['member_id'=>Yii::app()->member->id]);

            $this->render('requestTicket',[
                'model'=>$model,
                'upline'=>$upline
            ]);
        }
        
        public function actionLoadmydownline()
        {
            $data=[];
            $request=trim($_GET['term']);
            $id=Yii::app()->member->id;
            $models= Network::model()->with('member')->findAll('t.upline_id=:id AND (member.mid LIKE :key OR member.name LIKE :key) AND member.status!=0',array(':id'=>$id,':key'=>'%'.$request.'%'));
            if($models)
            {
                foreach($models as $model) {
                    $data[] = array(
                        'label'=>$model->member->mid.' - '.$model->member->name,  // label for dropdown list          
                        'value'=>$model->member->mid.' - '.$model->member->name,  // value for input field   
                        'id'=>$model->member->id,   
                        ); 
                }
                echo json_encode($data);
            }
        }
        
        public function actionLoadallmember(){
            $request=trim($_GET['term']);
            if($request!=''){
                $data=array();
                
                $models= Member::model()->findAll("t.id!=:mid AND t.mid LIKE :key AND t.status!=0",array(':key'=>$request.'%',':mid'=>Yii::app()->member->id));
                
                foreach($models as $model) {
                    $data[] = array(
                        'label'=>$model->mid.' - '.$model->name,  // label for dropdown list          
                        'value'=>$model->mid.' - '.$model->name,  // value for input field   
                        'name'=>$model->name,
                        'mid'=>$model->mid,
                        'id'=>$model->id,   
                        );      
                }
                
                echo json_encode($data);
            }
        }
        
    
    protected function loadModelEWallet($id)
        {
            $model= Ewallet::model()->find('id=:id AND member_id=:mid AND status=:st',[':id'=>$id,':mid'=>Yii::app()->member->id,':st'=>0]);
            if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
        }

        protected function loadModelTicket($id)
        {
            $model= StockTicket::model()->find('id=:id AND member_id=:mid AND status=:st',[':id'=>$id,':mid'=>Yii::app()->member->id,':st'=>0]);
            if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
        }
        
        public function actionDeleteimage()
    {
        if(Yii::app()->request->getIsAjaxRequest())
        {
            if(Yii::app()->request->getQuery('productid') && Yii::app()->request->getQuery('imgid'))
            {
                echo json_encode($this->getResponse());                
            }
            else
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        else
                throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
    }
        
    protected function getResponse()
    {
        $image=Yii::app()->request->getQuery('imgid');
        $model = ProductImage::model()->findByAttributes(array('id'=>$image,'product_id'=>Yii::app()->request->getQuery('productid')));   
        
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'));

        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'small');
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'medium');
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'large');
        
        $response = array();    
        if($model->delete())
        {
            $response = array(
              'ok' => true,
              'msg' => "The Image has been deleted!");
        }
        else
        {
            $response = array(
                  'ok' => false,
                  'msg' => "We can't find the image!");
        }
        return $response;
    }
        
        public function actionTestimoni()
        {
            if(Yii::app()->request->getIsAjaxRequest())
            {
                $model=  Testimonial::model()->findByAttributes(array('member_id'=>Yii::app()->member->model->id));
                if($model==NULL)
                {
                    $model=new Testimonial;
                    $model->status=0;
                }
                    
                if(isset($_POST['Testimonial']))
                {
                    $model->title=$_POST['Testimonial']['title'];
                    $model->content=$_POST['Testimonial']['content'];
                    
                    $model->save(false);
                    
                    echo CJSON::encode( array ('status'=>'success','title'=>$model->title,'content'=>$model->content));
                }
            }
            else
                 throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }

    public function actionPaymentConfirmation($token)
    {
		$this->activeSubMenu='profile';
        if($this->_model->status=='2')
        {
            $this->title='Payment Confirmation';
            $key= Key::model()->find('token=:cd AND is_used=:st AND type=:tp',[':cd'=>$token,':st'=>0,':tp'=>1]);
            if($key)
            {
                $model= new PremiumPayment;
                $model->value=$this->g('application','biaya_pendaftaran');
                if(isset($_POST['PremiumPayment']))
                {
                    $model->type=0;
                    $model->value= $this->g('application','biaya_pendaftaran');
                    $model->key_id=$key->id;
                    $model->bank_receiver=$_POST['PremiumPayment']['bank_receiver'];
                    $model->bank_sender=$_POST['PremiumPayment']['bank_sender'];
                    $model->account_name=$_POST['PremiumPayment']['account_name'];
                    $model->date=$_POST['PremiumPayment']['date'];
                    $model->status=0;
                    if($model->save())
                    {
                        $this->_model->saveAttributes(['status'=>3]);
                        Yii::app()->member->setFlash('success','Thank you, your payment confirmation has been saved successfully, we will verify it first.');
                        $this->redirect(['/member']);
                    }
                }
                $this->render('payment',[
                    'model'=>$model
                ]);
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
        }
        else
            $this->redirect(['/member']);
    }

    public function actionTicketPayment($id)
    {
            
            $this->title='Ticket Payment Confirmation';
            $ticket= $this->loadModelTicket($id);
            
            $model= new TicketPayment;
            $model->value=$ticket->price;
            if(isset($_POST['TicketPayment']))
            {
                $model->type=0;
                $model->value= $ticket->price;
                $model->request_id=$ticket->id;
                $model->bank_receiver=$_POST['TicketPayment']['bank_receiver'];
                $model->bank_sender=$_POST['TicketPayment']['bank_sender'];
                $model->account_name=$_POST['TicketPayment']['account_name'];
                $model->date=$_POST['TicketPayment']['date'];
                $model->status=0;
                if($model->save())
                {
                    $ticket->saveAttributes(['status'=>2]);
                    Yii::app()->member->setFlash('success','Thank you, your payment confirmation has been saved successfully, we will verify it first.');
                    $this->redirect(['ticket']);
                }
            }
            $this->render('ticketpayment',[
                'model'=>$model
            ]);
    }
        
        // public function actionWithdrawal()
        // {
        //     $this->title='Penarikan Dana';
        //     $member=Yii::app()->member->model;
        //     $credit=$member->profit_credit;
        //     $debit=$member->profit_debit;
        //     $balance=$credit - $debit;
            
        //    if($balance < 50000)
        //    {
        //        Yii::app()->member->setFlash('error','Saldo Anda tidak mencukupi untuk penarikan');
        //        $this->redirect(['sharebonus']);
        //        exit;
        //    }
            
        //     $model= new Withdrawal;
        //     $model->scenario='insert';
            
        //     if(isset($_POST['Withdrawal']))
        //     {
        //         $cek= Withdrawal::model()->find('member_id=:mid AND status=:st',[':mid'=>$member->id,':st'=>0]);
        //         if(!$cek)
        //         {
        //             if($_POST['Withdrawal']['value']>$balance)
        //             {
        //                 Yii::app()->member->setFlash('error','Saldo Anda tidak mencukupi.');
        //             }
        //             else if($_POST['Withdrawal']['value']<50000)
        //             {
        //                 Yii::app()->member->setFlash('error','Jumlah penarikan minimal IDR 50,000.');
        //             }
        //             else
        //             {
        //                 $model->member_id=$member->id;
        //                 $model->value=$_POST['Withdrawal']['value'];
        //                 $model->verifyCode=$_POST['Withdrawal']['verifyCode'];
        //                 if($model->save())
        //                 {
        //                     Yii::app()->member->setFlash('success','Penarikan sudah disimpan, kami akan melakukan pembayaran setelah diverifikasi terlebih dahulu.');
        //                     $this->refresh();
        //                 }
        //             }
        //         }
        //         else
        //         {
        //             Yii::app()->member->setFlash('error','Anda masih mempunya permintaan penarikan yang pending');
        //         }
        //     }
            
        //     $criteria=new CDbCriteria;
        //     $criteria->condition='t.member_id=:id';
        //     $criteria->params=array(':id'=>Yii::app()->member->id);
        //     $criteria->order='t.id DESC';
        //     $withdrawals= Withdrawal::model()->findAll($criteria);
        //     $this->_removeSession();
        //     $this->render('withdraw',[
        //         'credit'=>$credit,
        //         'debit'=>$debit,
        //         'balance'=>$balance,
        //         'model'=>$model,
        //         'withdrawals'=>$withdrawals
        //     ]);
        // }
    
    protected function sendNewConfirmation($model)
        {
            Yii::import('application.extensions.YiiMailer.YiiMailer');        
            $mail = new YiiMailer; 
            $body=$this->renderFile(Yii::app()->basePath.'/views/email/confirmTicket.php',array('model'=>$model),TRUE);

            $mail->SetFrom('noreply@sevilla100.com', 'Info');
            $mail->AddAddress($model->member->email, $model->member->name);
            $mail->Subject    = "Info Ticket";
            $mail->MsgHTML($body);

            if(!$mail->Send())
            {
                Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.Member') ;
                return false;
            }
            else
                return true;
        }
        
        public function loadModel($id)
    {
        $model=Member::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The drequested page does not exist.');
        return $model;
    }
    
    protected function dateRange( $first, $last, $step = '+1 day', $format = 'j F Y' ) {

                $dates = array();
                $current = strtotime( $first );
                $last = strtotime( $last );

                while( $current <= $last ) {

                        $dates[] = date( $format, $current );
                        $current = strtotime( $step, $current );
                }

                return $dates;
        }
        
        protected function saveToken($model)
    {
        $key = sha1($model->id.time());
        $token = new Key;
        $token->member_id = $model->id;
        $token->token=$key;
        $token->type=1;
        $token->save(false);
        {
                $this->sendNewConfirmation($model, $_POST['Member']['password'],Yii::app()->controller->createAbsoluteUrl('site/login'));
        }
                        
    }
    
    protected function _removeSession()
        {
            $session = Yii::app()->session;
                $prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
                foreach($session->keys as $key)
                {
                        if(strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
                                $session->remove($key);
                }
        }
}