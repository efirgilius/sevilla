<?php

class HomeController extends Controller
{	
	public $activeMenu = 'home';
	
	public function actionIndex()
	{	

		$this->metaTag($this->g('meta','meta_description'),'description');
		$this->metaTag($this->g('meta','meta_keywords'),'keywords');
		$this->metaTag($this->g('meta','meta_author'),'author');

		$banner = Banner::model()->active()->order('t.order DESC')->findAll();
		$news = Content::model()->publish()->order('t.created DESC')->type('news')->limit(2)->findAll();
		
		$products = Product::model()->publish()->limit(3)->order('rand()')->findAll();

		$this->render('index',[
			'banner'=>$banner,
			'news'=>$news,
			'products'=>$products
			]);
	}

}