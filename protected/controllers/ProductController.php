<?php

class ProductController extends Controller
{	
	public $activeMenu = 'product';
	public function actionView($slug)
	{
		$model = Product::model()->publish()->findByAttributes(['slug'=>$slug]);
		if ($model) {
			$model->saveAttributes(['viewed'=>$model->viewed+1]);
		}

		$this->title = ($model->meta_title)?$model->meta_title:$model->name;
		
		$this->render('view',[
			'model'=>$model,
			]);
	}

	public function actionIndex($category=false, $brand=false, $group=false)
	{

		$this->title = "Produk";

		$banner = null;
		
		$criteria=new CDbCriteria;

		if ($category) {
			$cat = Category::model()->active()->type('product')->findByAttributes(['slug'=>$category]);
			$this->title = ($cat->meta_title)?$cat->meta_title:$cat->name;
			if ($cat->categories) {

				$criteria->with = [
					'category'=>[
						'select'=>false,
						'joinType'=>'INNER JOIN',
						'condition'=>'category.parent_id =:pid',
						'params'=>[':pid'=>$cat->id]
					]
				];

			}else{
				$criteria->condition = 'category_id='.$cat->id;
			}
			if ($cat->image) {
				$banner = $cat->large_url;
			}
		}

		if ($brand) {
			$b = Brand::model()->findByAttributes(['slug'=>$brand]);
			$criteria->AddCondition('brand_id=:bi');
			$criteria->params=[':bi'=>$b->id];
			$this->title = 'Semua Produk '.$b->name;
		}

		if ($brand and $category) {
			$this->title = 'Jual '.$cat->name.' '.$b->name;
		}

		if ($group) {
			$cat = Category::model()->active()->type('group')->findByAttributes(['slug'=>$group]);
			$this->title = ($cat->meta_title)?$cat->meta_title:$cat->name;
			
			$criteria->join = 'inner join tbl_product_group	on t.id = tbl_product_group.product_id';
			$criteria->condition = 'tbl_product_group.category_id = '.$cat->id;
			

			if ($cat->image) {
				$banner = $cat->large_url;
			}
		}

        $count = Product::model()->order('t.created DESC')->publish()->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 9;
        $pages->applyLimit($criteria);
        
        $model= Product::model()->order('t.created DESC')->publish()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
            'pages' => $pages,
            'count'=>$count,
            'banner'=>$banner
		));
	}

	public function actionSearch($q)
	{

		$this->title = "Jual ".ucfirst($q);

		$banner = null;
		
		$criteria=new CDbCriteria;

		$criteria->AddCondition('t.name like :q or t.overview like :q or t.description like :q or t.specification like :q or t.price like :q or category.name like :q or brand.name like :q');
		$criteria->params=[':q'=>'%'.$q.'%'];
		$criteria->with=['category','brand'];

        $count = Product::model()->order('t.created DESC')->publish()->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 9;
        $pages->applyLimit($criteria);
        
        $model= Product::model()->order('t.created DESC')->publish()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
            'pages' => $pages,
            'count'=>$count,
            'banner'=>$banner
		));
	}

	protected function loadRelated($id,$tag)
    {
        $tags=  str_replace(",", "|", $tag);
        $criteria=new CDbCriteria;
        $criteria->condition="t.id!=:id AND t.tags REGEXP '$tags'";
        $criteria->params=array('id'=>$id);
        $criteria->limit=4;
        $data=Product::model()->publish()->findAll($criteria);
        return $data;
    }

}
