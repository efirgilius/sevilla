<?php

class MemberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='member';
        
        private $_identity;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','search','package','loadupmember'),
//				'expression'=>'Yii::app()->user->isManager',
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('setting','index','view','search','package','loadupmember','create','update','move','genealogy','delete','activate','loadmember','merchants','setmerchant','unmerchant','updatepassword','bankaccount','apprequest','rejectrequest','login','loadallmember'),
				'expression'=>'Yii::app()->user->isManager',
			),
                        
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $this->title='Member Detail';
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile(Yii::app()->request->baseUrl.'/stylesheets/custom.css');
            
            $model=  $this->loadModel($id);
            
            if($model->status=='0')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='inactive';
            }
            elseif($model->status=='1')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='active';
            }
            elseif($model->status=='2')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='blocked';
            }

            $model2_l=NULL;
            $model2_r=NULL;
            
            $model3_2l_l=NULL;
            $model3_2l_r=NULL;
            $model3_2r_l=NULL;
            $model3_2r_r=NULL;
            
            $model4_32ll_l=NULL;
            $model4_32ll_r=NULL;
            $model4_32lr_l=NULL;
            $model4_32lr_r=NULL;
            $model4_32rl_l=NULL;
            $model4_32rl_r=NULL;
            $model4_32rr_l=NULL;
            $model4_32rr_r=NULL;
            
            if($model->foot_left !== NULL)
            {
                $model2_l=  Member::model()->findByPk($model->foot_left);
                if(!empty($model2_l))
                {
                    if($model2_l->foot_left !== NULL)
                    {
                        $model3_2l_l=Member::model()->findByPk($model2_l->foot_left);
                        if(!empty($model3_2l_l))
                        {
                            if($model3_2l_l->foot_left !== NULL)
                            {
                                $model4_32ll_l=Member::model()->findByPk($model3_2l_l->foot_left);
                            }
                            
                            if($model3_2l_l->foot_right !== NULL)
                            {
                                $model4_32ll_r=Member::model()->findByPk($model3_2l_l->foot_right);
                            }
                        }
                    }
                    
                    if($model2_l->foot_right !== NULL)
                    {
                        $model3_2l_r=Member::model()->findByPk($model2_l->foot_right);
                        if(!empty($model3_2l_r))
                        {
                            if($model3_2l_r->foot_left !== NULL)
                            {
                                $model4_32lr_l=Member::model()->findByPk($model3_2l_r->foot_left);
                            }
                            
                            if($model3_2l_r->foot_right !== NULL)
                            {
                                $model4_32lr_r=Member::model()->findByPk($model3_2l_r->foot_right);
                            }
                        }
                    }        
                }
            }
            
            if($model->foot_right !== NULL)
            {
                $model2_r=  Member::model()->findByPk($model->foot_right);
                if(!empty($model2_r))
                {
                    if($model2_r->foot_left !== NULL)
                    {
                        $model3_2r_l=Member::model()->findByPk($model2_r->foot_left);
                        if(!empty($model3_2r_l))
                        {
                            if($model3_2r_l->foot_left !== NULL)
                            {
                                $model4_32rl_l=Member::model()->findByPk($model3_2r_l->foot_left);
                            }
                            
                            if($model3_2r_l->foot_right !== NULL)
                            {
                                $model4_32rl_r=Member::model()->findByPk($model3_2r_l->foot_right);
                            }
                        }
                    }
                    
                    if($model2_r->foot_right !== NULL)
                    {
                        $model3_2r_r=Member::model()->findByPk($model2_r->foot_right);
                        if(!empty($model3_2r_r))
                        {
                            if($model3_2r_r->foot_left !== NULL)
                            {
                                $model4_32rr_l=Member::model()->findByPk($model3_2r_r->foot_left);
                            }
                            
                            if($model3_2r_r->foot_right !== NULL)
                            {
                                $model4_32rr_r=Member::model()->findByPk($model3_2r_r->foot_right);
                            }
                        }
                    }
                }
            }
            
		$this->render('view',array(
			'model'=>$model,
                        'model2_l'=>$model2_l,
                    'model2_r'=>$model2_r,
                    'model3_2l_l'=>$model3_2l_l,
                    'model3_2l_r'=>$model3_2l_r,
                    'model3_2r_l'=>$model3_2r_l,
                    'model3_2r_r'=>$model3_2r_r,
                    'model4_32ll_l'=>$model4_32ll_l,
                    'model4_32ll_r'=>$model4_32ll_r,
                    'model4_32lr_l'=>$model4_32lr_l,
                    'model4_32lr_r'=>$model4_32lr_r,
                    'model4_32rl_l'=>$model4_32rl_l,
                    'model4_32rl_r'=>$model4_32rl_r,
                    'model4_32rr_l'=>$model4_32rr_l,
                    'model4_32rr_r'=>$model4_32rr_r,
                       
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            $this->title='Update Member';
            $model=$this->loadModel($id);

            if($model->status=='1')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='pactive';
            }
            elseif($model->status=='0')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='pinactive';
            }
            elseif($model->status=='2')
            {
                $this->activeMenu='member';
                $this->activeSubMenu='ppending';
            }
            
                $sponsor=$model->sponsor_id;
                $model->scenario='updateByAdmin';
                $model->sponsor_id=($model->sponsor_id!==null)?$model->sponsor->mid:'';
                $model->sponsor_mid=($model->sponsor_id!==null)?$model->sponsor_id:'';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
                        $model->sponsor_id=$sponsor;
			if($model->save())
                        {
                            Yii::app()->user->setFlash('success','Member info has been updated.');
                                    $this->redirect(array('view','id'=>$id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function actionUpdatepassword($id)
        {
                $this->title='Edit Password';
		$model=$this->loadModel($id);
                
                if(isset($_POST['Member']))
		{
                    if($_POST['Member']['newPassword']!=='')
                    {
                        $model->password=$model->hashPassword($_POST['Member']['newPassword'], $model->salt);
                        if($model->save(false))
                        {
                            Yii::app()->user->setFlash('success','Member password "'.$model->name.'" has been changed.');
                                    $this->redirect(array('view','id'=>$id));
                        }
                    }
                    else
                    {
                        Yii::app()->user->setFlash('error','New Password can not be blank.');
                                    $this->redirect(array('updatepassword','id'=>$model->id));
                    }
                }
                
                $this->render('updatePassword',array(
			'model'=>$model,
		));
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionActivate($id,$type)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    if($type=='activate')
                    {
                        $tp=1;
                        $title='activated';
                    }
                    elseif($type=='deactivate')
                    {
                        $tp=0;
                        $title='Deactivated';
                    }
                    else
                        Yii::app()->user->setFlash('error','Type can not be found.');
                    
                    $model=$this->loadModel($id);
                    if(($model->status=='0' || $model->status=='2' || $model->status=='3') && $type=='activate')
                    {
                        $model->saveAttributes(array('status'=>1));
                        $model->getStoreMember($model->id, $model->sponsor_id);
                        $this->sendSms($model->mobile_phone, 'Terima kasih, pembayaran untuk pendaftaran keanggotaan Anda telah berhasil dilakukan dan akun Anda telah aktif');
                        Yii::app()->user->setFlash('success','The membership payment has been confirmed, and this member has been activated as member.');
                    }
                    else
                    {
                        $model->saveAttributes(array('status'=>1));
                        Yii::app()->user->setFlash('success','Member has been '.$title);
                    }
                     Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($state,$type=false)
	{
        if($type)
        {
            if($type=='member')
            {
                $tTitle='Member Type';
                $tp=1;
                $this->activeMenu='fmember';
            }
            elseif($type=='agent')
            {
                $tTitle='Agent Type';
                $tp=2;
                $this->activeMenu='member';
            }
            elseif($type=='agent')
            {
                $tTitle='Master Agent Type';
                $tp=2;
                $this->activeMenu='member';
            }
        }
        
        if($state)
        {
            if($state=='active')
            {
                $sTitle='Active';
                $st=1;
                    $this->activeSubMenu='active';
            }
            elseif($state=='inactive')
            {
                $sTitle='Inactive';
                $st=0;
                $this->activeSubMenu='inactive';
            }
            elseif($state=='blocked')
            {
                $sTitle='Blocked';
                $st=2;
                $this->activeSubMenu='blocked';
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
        }
		$this->title=$sTitle.' Member List';
		$criteria=new CDbCriteria;
        if($type)
        {
            if($state=='active')
            {
                $criteria->condition='(t.status!=:st1 AND t.status!=:st2) AND t.type=:tp';
                $criteria->params=array(':st1'=>0,':st2'=>2,':tp'=>$tp);
            }
            else
            {
                $criteria->condition='t.status=:st AND t.type=:tp';
                $criteria->params=array(':st'=>$st,':tp'=>$tp);
            }
        }
        else
        {
            if($state=='active')
            {
                $criteria->condition='t.status!=:st1 AND t.status!=:st2';
                $criteria->params=array(':st1'=>0,':st2'=>2);
            }
            else
            {
                $criteria->condition='t.status=:st';
                $criteria->params=array(':st'=>$st);
            }
        }
        $criteria->order='t.id DESC';
        $count = Member::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model=Member::model()->findAll($criteria);
                
		$this->render('index',array(
                        'title'=>$sTitle,
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
	}
        
        public function actionSearch($q)
	{
		$this->title='Find Member';
		$criteria=new CDbCriteria;
                $criteria->condition='t.mid LIKE :key OR t.first_name LIKE :key OR t.last_name LIKE :key OR t.email LIKE :key OR t.mobile_phone LIKE :key';
                $criteria->params=array(':key'=>'%'.$q.'%');
                $criteria->order='t.first_name ASC';
                $count = Member::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 10;
                $pages->applyLimit($criteria);
                $model=Member::model()->findAll($criteria);
                
                if($count>0)
                    Yii::app()->user->setFlash('success','Search result for key search "'.$q.'"');
                else
                    Yii::app()->user->setFlash('error','Search result for key "'.$q.'" is resturn zero result');
                
		$this->render('search',array(
                        'title'=>'Find',
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
	}
        
        public function actionMerchants()
        {
            $this->title='Merchants';
            
            $criteria=new CDbCriteria;
                $criteria->condition='t.is_merchant!=:st';
                $criteria->params=array(':st'=>0);
                $criteria->order='t.id DESC';
                $count = Member::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 10;
                $pages->applyLimit($criteria);
                $model=Member::model()->findAll($criteria);
                
		$this->render('merchant',array(
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
        }
        
        public function actionSetmerchant($id,$type)
        {
                    if($type=='country')
                    {
                        $tp=1;
                        $title='Country';
                    }
                    elseif($type=='regional')
                    {
                        $tp=2;
                        $title='Regional';
                    }
                    else
                        Yii::app()->user->setFlash('error','Type can not be found.');
                    
                    $model=$this->loadModel($id);
                    $model->saveAttributes(array('is_merchant'=>$tp));
                    Yii::app()->user->setFlash('success','Member has been set as a '.$title.' Merchant');
                    $this->redirect(array('view','id'=>$model->id));
               
        }
        
        public function actionUnmerchant($id)
        {
            if(Yii::app()->request->getIsAjaxRequest())
                {
                    
                    $model=$this->loadModel($id);
                    $model->saveAttributes(array('is_merchant'=>0));
                    Yii::app()->user->setFlash('success','Member has been deactivated as Merchant');
                     Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }
        
        public function actionBankaccount()
        {
            $this->title='Change Bank Account Request';
            
            $criteria=new CDbCriteria;
                $criteria->order='t.date DESC';
                $count = BankRequest::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=BankRequest::model()->findAll($criteria);
                
		$this->render('bank',array(
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
        }
        
        public function actionApprequest($id)
        {
            if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model=$this->loadModelRequest($id);
                    $model->member->bank=$model->bank;
                    $model->member->account_number= $model->account_number;
                    $model->member->account_name= $model->account_name;
                    $model->member->save(false);
                    $model->delete();
                    Yii::app()->user->setFlash('success','Change request has been approved, and member bank account has been changed successfully');
                     Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }
        
        public function actionRejectrequest($id)
        {
            if(Yii::app()->request->getIsAjaxRequest())
                {
                    
                    $model=$this->loadModelRequest($id);
                    $model->delete();
                    Yii::app()->user->setFlash('success','Change bank account request has been rejected.');
                     Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }
        
        public function actionLogin($id)
        {
            $model=  $this->loadModel($id);
//            Yii::app()->member->logout();
            if($this->_identity===null)
            {
                    $this->_identity=new HybridIdentity($model->mid,$model->password);
                    $this->_identity->authenticate();
            }
            if($this->_identity->errorCode===HybridIdentity::ERROR_NONE)
            {
                    Yii::app()->member->login($this->_identity,0);
                    $this->redirect(['/member']);
            }
            else
            {
                Yii::app()->user->setFlash('error','Cannot login to this member');
                $this->redirect(['view','id'=>$id]);
            }
        }
        
        public function actionSetting()
        {
            if(Yii::app()->request->getIsAjaxRequest())
            {
                $setting=  Tsetting::model()->findByAttributes(array('type'=>  Tsetting::UPGRADE));
                
                if(isset($_POST['value']))
                {
                    $setting->value=$_POST['value'];
                    $setting->save(false);
                    
                    echo CJSON::encode( array ('status'=>'success','value'=>$setting->value));
                }
            }
            else
                 throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
        }
        
        public function actionLoadmember(){
            $up=(int)$_GET['up'];
            $request=trim($_GET['term']);
            if($request!=''){
                $data=array();
                
                $model= Network::model()->with('upline')->findAll('t.member_id=:id AND upline.mid LIKE :key AND upline.status!=0',array(':id'=>$up,':key'=>'%'.$request.'%'));
                
                foreach($model as $model) {
                    $data[] = array(
                        'label'=>$model->upline->mid.' - '.$model->upline->name,  // label for dropdown list          
                        'value'=>$model->upline->mid.' - '.$model->upline->name,  // value for input field   
                        'name'=>$model->upline->name,
                        'mid'=>$model->upline->mid,
                        'id'=>$model->upline->id,   
                        );      
                }
                
                $self=  Member::model()->find('id=:id AND mid LIKE :key AND status!=0',[':id'=>$up,':key'=>'%'.$request.'%']);
                if($self)
                {
                    $data1[0]=[
                        'label'=>$self->mid.' - '.$self->name,
                        'value'=>$self->mid.' - '.$self->name,
                        'name'=>$self->name,
                        'mid'=>$self->mid,
                        'id'=>$self->id
                    ];
                    
                    $data=array_merge($data,$data1);
                }

                echo json_encode($data);
            }
        }
        
        public function actionLoadupmember(){
            $up=(int)$_GET['up'];
            $request=trim($_GET['term']);
            if($request!=''){
                $data=array();
                
                $model= Network::model()->with('upline')->findAll('t.member_id=:id AND upline.mid LIKE :key AND upline.status!=0',array(':id'=>$up,':key'=>'%'.$request.'%'));
                
                foreach($model as $model) {
                    $data[] = array(
                        'label'=>$model->upline->mid.' - '.$model->upline->name,  // label for dropdown list          
                        'value'=>$model->upline->mid.' - '.$model->upline->name,  // value for input field   
                        'name'=>$model->upline->name,
                        'mid'=>$model->upline->mid,
                        'id'=>$model->upline->id,   
                        );      
                }
                
                $self=  Member::model()->find('id=:id',[':id'=>$up]);
                if($self)
                {
                    $data1[0]=[
                        'label'=>$self->mid.' - '.$self->name,
                        'value'=>$self->mid.' - '.$self->name,
                        'name'=>$self->name,
                        'mid'=>$self->mid,
                        'id'=>$self->id
                    ];
                    
                    $data=array_merge($data,$data1);
                }

                echo json_encode($data);
            }
        }
        
        public function actionLoadallmember(){
            $request=trim($_GET['term']);
            if($request!=''){
                $data=array();
                
                $models= Member::model()->findAll("t.name LIKE :key OR t.mid LIKE :key",array(':key'=>$request.'%'));
                
                foreach($models as $self) {
                    $data[] = array(
                       'label'=>$self->mid.' - '.$self->name,
                        'value'=>$self->mid.' - '.$self->name,
                        'name'=>$self->name,
                        'mid'=>$self->mid,
                        'id'=>$self->id 
                        );      
                }
                
                echo json_encode($data);
            }
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Member the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Member::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadModelRequest($id)
	{
		$model=  BankRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Member $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function saveSponsor($mid,$sid,$lvl)
        {
            $model=new NetworkMatrix;
            $model->member_id=$mid;
            $model->sponsor_id=$sid;
            $model->level=$lvl;
            $model->save();
        }
}
