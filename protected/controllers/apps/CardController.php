<?php

class CardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='card';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create','update','delete','companytransaction','send','loadmember','captcha','activate'),
				'expression'=>'Yii::app()->user->isManager',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xF9F9F9,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->title='Generate PIN';
                $card=new CardForm;
                
		$model=new Card;
                
                $valid=false;
                if(isset($_POST['CardForm']))
                {
                    $card->attributes=$_POST['CardForm'];
                    if($card->validate() && $card->validatePasswordAdmin())
                        $valid=true;
                    else
                        Yii::app()->member->setFlash('error','Password is invalid.');                        
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Card']))
		{
                    $valid=true;
                    $val=$_POST['Card']['value'];
                    if($val>0 && $val<=300)
                    {
                        for($i=1;$i<=$val;$i++)
                        {
                            $new= new Card;
                            $new->is_active=0;
                            $new->type= $_POST['Card']['type'];
                            $new->save(false);                       
                        }
                        Yii::app()->user->setFlash('success',$val.' PIN has been generated successfully.');
                        if($new->type=='1')
                            $rd='standard';
                        elseif($new->type=='2')
                            $rd='premium';
                        elseif($new->type=='3')
                            $rd='deluxe';
                        elseif($new->type=='4')
                            $rd='executive';
                        else
                            $rd='diamond';
                        $this->redirect(array('/apps/card','type'=>$rd));
                    }
                    else
                    {
                        Yii::app()->user->setFlash('error','Please input 1 to 300 value.');
                        $this->redirect(array('create'));
                    }
		}

                $this->_removeSession();
		$this->render('create',array(
                        'valid'=>$valid,'card'=>$card,
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->title='Edit PIN';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Card']))
		{
			$model->attributes=$_POST['Card'];
			if($model->save())
                        {
                            Yii::app()->user->setFlash('success','PIN '.$model->serial.' has been updated successfully.');
				if($new->type=='1')
                            $rd='standard';
                        elseif($new->type=='2')
                            $rd='premium';
                        elseif($new->type=='3')
                            $rd='deluxe';
                        elseif($new->type=='4')
                            $rd='executive';
                        else
                            $rd='diamond';
                        $this->redirect(array('/apps/card','type'=>$rd));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function actionSend()
        {
            $this->title='Send E-voucher';
            
            $card=new CardForm;
            $model= new CardBalance;
            $model->scenario='send';
            
            $valid=false;
            if(isset($_POST['CardForm']))
            {
                $card->attributes=$_POST['CardForm'];
                if($card->validate() && $card->validatePasswordAdmin())
                    $valid=true;
                else
                    Yii::app()->member->setFlash('error','Password is invalid.');                        
            }
            
            if(isset($_POST['CardBalance']))
            {
                $valid=true;
                $last= CardBalance::model()->find('t.member_id=:id AND t.type=:tp AND t.status=:st ORDER BY t.id DESC',[':id'=>  $_POST['CardBalance']['member_id'],':tp'=>$_POST['CardBalance']['type'],':st'=>1]);
                $model->attributes=$_POST['CardBalance'];
                if($model->save())
                {
                    if($_POST['CardBalance']['credit']>=10)
                    {
                        $val=0;
                        if($model->member->is_merchant=='1')
                        {
                            $val= floor(($model->credit * 15) / 100);
                        }
                        elseif($model->member->is_merchant=='2')
                        {
                            $val= floor(($model->credit * 10) / 100);
                        }
                        $tot = $model->credit + $val;
                        $model->credit= $tot;
                        
                        if($last)
                            $model->total= $last->total + $tot;
                        else
                            $model->total= $tot;
                        
                        $model->save(false);
                    }
                    else
                    {
                        if($last)
                            $model->total= $last->total + $model->credit;
                        else
                            $model->total= $model->credit;
                        
                        $model->save(false);
                    }
                    
                    if($_POST['CardBalance']['type']=='1')
                        $in= $_POST['CardBalance']['credit'] * 200;
                    else
                        $in= $_POST['CardBalance']['credit'] * 25;
                    
                    $margin=  Margin::model()->findByattributes(array('name'=>'cashin'));
                    $margin->value=$margin->value + $in;
                    $margin->save(false);
                    
                    $cr=$model->credit;
                    $tp=$model->type;
                    //masalah
                    $cards= Card::model()->findAll('member_id IS NULL AND type=:tp AND status=:st LIMIT '.$cr,array(':tp'=>  $model->type,':st'=>0));
                    $tcard=count($cards);
                    if($tcard>=$cr)
                    {
                        foreach ($cards as $cd)
                        {
                            $card=  $this->loadModel($cd->id);
                            $card->member_id=$model->member_id;
                            $card->save(false);
                        }
                    }
                    else
                    {
                        if($tcard<1)
                        {
                            for($i=1;$i<=$cr;$i++)
                            {
                                $new= new Card;
                                $new->type=$tp;
                                $new->member_id=$model->member_id;
                                $new->is_active=1;
                                $new->save(false);                       
                            }
                        }
                        else
                        {
                            foreach ($cards as $cd)
                            {
                                $card=  $this->loadModel($cd->id);
                                $card->member_id=$model->member_id;
                                $card->save(false);
                            }
                            
                            for($i=1;$i<=$cr - $tcard;$i++)
                            {
                                $new= new Card;
                                $new->type=$tp;
                                $new->member_id=$model->member_id;
                                $new->is_active=1;
                                $new->save(false);                       
                            }
                        }
                    }
                    
                    Yii::app()->user->setFlash('success',$model->credit.' e-voucher(s) has been sent successfully to '.$model->member->name.'.');
                    $this->refresh();
                }
            }
            
            $this->_removeSession();
            $this->render('send',array('valid'=>$valid,'card'=>$card,'model'=>$model));
        }
        
        public function actionSendw()
        {
            $this->title='Send PIN';
            
            $card=new CardForm;
            $model= new CardbalanceForm;
            
            $valid=false;
            if(isset($_POST['CardForm']))
            {
                $card->attributes=$_POST['CardForm'];
                if($card->validate() && $card->validatePasswordAdmin())
                    $valid=true;
                else
                    Yii::app()->member->setFlash('error','Password is invalid.');                        
            }
            
            if(isset($_POST['CardbalanceForm']))
            {
                $valid=true;
                $model->attributes=$_POST['CardbalanceForm'];
                if($model->validate())
                {
                    $value=$_POST['CardbalanceForm']['value'];
                    $type= $_POST['CardbalanceForm']['type'];
                    $cards= Card::model()->findAll('member_id IS NULL AND type=:tp AND status=:st AND is_active=:act',array(':tp'=>$type,':st'=>0,':act'=>0));
                    $tcard=count($cards);
                    if($tcard>=$value)
                    {
                        foreach ($cards as $cd)
                        {
                            $card=  $this->loadModel($cd->id);
                            $card->saveAttributes(['member_id'=>$model->member_id,'is_active'=>1]);
                        }
                    }
                    else
                    {
                        for($i=1;$i<=$value;$i++)
                        {
                            $new= new Card;
                            $new->member_id=$model->member_id;
                            $new->type= $type;
                            $new->is_active=1;
                            $new->save(false);                       
                        }
                    }
                    
                    Yii::app()->user->setFlash('success',$model->value.' Serial Number sudah dikirim ke member.');
                    $this->refresh();
                }
            }
            
            $this->_removeSession();
            $this->render('send',array('valid'=>$valid,'card'=>$card,'model'=>$model));
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            $model=$this->loadModel($id);
            if(!$model->members)
            {
                $name=$model->serial;
                    $model->delete();
                    Yii::app()->user->setFlash('success','PIN '.$name.' has been deleted.');
                    Yii::app()->end();
            }
            else
            {
                Yii::app()->user->setFlash('error','PIN '.$model->serial.' is not expired.');
                    $this->redirect(array('index'));
            } 
	}
        
        public function actionActivate($id)
	{
            $model=$this->loadModel($id);
            if(!$model->members)
            {
                $name=$model->serial;
                    $model->saveAttributes(['is_active'=>1]);
                    Yii::app()->user->setFlash('success','PIN '.$name.' has been deleted.');
                    Yii::app()->end();
            }
            else
            {
                Yii::app()->user->setFlash('error','PIN '.$model->serial.' is not expired.');
                    $this->redirect(array('index'));
            } 
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($type=false,$q=false)
	{
            $this->title='PIN';
            $title=null;
            if($q)
            {
		$criteria=new CDbCriteria;
                $criteria->with=array('member');
                $criteria->condition='t.pin LIKE :key OR t.serial LIKE :key OR member.mid LIKE :key OR member.name LIKE :key';
                $criteria->params=array(':key'=>'%'.$q.'%');
                $criteria->order='t.serial DESC';
                $count = Card::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=Card::model()->findAll($criteria);
                
                if($count>0)
                    Yii::app()->user->setFlash('success','Search result for "'.$q.'".');
                else
                    Yii::app()->user->setFlash('error','Search result for "'.$q.'" is not found.');
            }
            else
            {
		if($type)
                {
                    if($type=='standard')
                    {
                        $tp=1;
                        $this->activeSubMenu='p1';
                        $this->title='PIN Standard';
                    }
                    elseif($type=='premium')
                    {
                        $tp=2;
                        $this->activeSubMenu='p2';
                        $this->title='PIN Premium';
                    }
                    elseif($type=='deluxe')
                    {
                        $tp=3;
                        $this->activeSubMenu='p3';
                        $this->title='PIN Deluxe';
                    }
                    elseif($type=='executive')
                    {
                        $tp=4;
                        $this->activeSubMenu='p4';
                        $this->title='PIN Executive';
                    }
                    elseif($type=='diamond')
                    {
                        $tp=5;
                        $this->activeSubMenu='p5';
                        $this->title='PIN Executive Diamond';
                    }
                    else
                        throw new CHttpException(404,'The requested page does not exist.');
                    
                    $criteria=new CDbCriteria;
                    $criteria->condition='type=:tp';
                    $criteria->params=[':tp'=>$tp];
                    $criteria->order='t.status ASC, t.id DESC';
                    $count = Card::model()->count($criteria);
                    $pages = new CPagination($count);
                    $pages->pageSize = 20;
                    $pages->applyLimit($criteria);
                    $model=Card::model()->findAll($criteria);
                }
                else
                {
                    $criteria=new CDbCriteria;
                    $criteria->order='t.status ASC, t.id DESC';
                    $count = Card::model()->count($criteria);
                    $pages = new CPagination($count);
                    $pages->pageSize = 20;
                    $pages->applyLimit($criteria);
                    $model=Card::model()->findAll($criteria);
                }
            }
            
            $pin=  Card::model()->getTotalPin();
            $pinActive=  Card::model()->getTotalPin(0);
            $pinInactive=  Card::model()->getTotalPin(1);
            $pinStock= Card::model()->getTotalStock();
            
                
		$this->render('index',array(
                        'pin'=>$pin,
                        'pinActive'=>$pinActive,
                        'pinInactive'=>$pinInactive,
                        'pinStock'=>$pinStock,
                       
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1,
                        'title'=>$title
		));
	}
        
        public function actionCompanytransaction()
        {
            $this->title='Company PIN Transaction History';
            $criteria=new CDbCriteria;
            $criteria->condition='t.partner_id IS NULL AND t.is_transaction=1';
                $criteria->order='t.date DESC';
                $count = CardBalance::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=CardBalance::model()->findAll($criteria);
                
                $this->render('history',array(
                        'title'=>'Company',
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1,
		));
        }
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Card the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Card::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Card $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='card-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionLoadmember(){
            $request=trim($_GET['term']);
            if($request!=''){
                $model=  Member::model()->findAll(array("condition"=>"(mid LIKE :key OR name LIKE :key) AND status=1",'params'=>array(':key'=>'%'.$request.'%')));
                $data=array();
                foreach($model as $model) {
                    $data[] = array(
                        'label'=>$model->mid.' - '.$model->name,  // label for dropdown list          
                        'value'=>$model->mid.' - '.$model->name,  // value for input field   
                        'name'=>$model->name,
                        'mid'=>$model->mid,
                        'id'=>$model->id,   
                        );      
                }

                //$this->layout='empty';
                echo json_encode($data);
            }
        }
        
        protected function _removeSession()
        {
            $session = Yii::app()->session;
                $prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
                foreach($session->keys as $key)
                {
                        if(strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
                                $session->remove($key);
                }
        }
}
