<?php

class BlogController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='cms';
    public $activeSubMenu="blogpost";

    public $title='Blog';


	public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
		
		return parent::init();
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model=$this->loadModel($id);
        $this->title=$model->title;
		$this->render('view',array(
			'model'=>$model,
			'tags'=>$model->getTagThis()
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
        
        $this->title='Add Post';

		$model=new Content;
		$model->type='blog';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			$model->user_id = Yii::app()->user->id;
			if($model->save())
            {
                if($_FILES['Content']['name']['image'] !== '') 
                {
                    $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('contents',$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->image=$filename;
                        $model->save(false);
                    }
                }

				Yii::app()->user->setFlash('success','Post has been saved succesfully.');
				$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
        
		$model=$this->loadModel($id);
		$img=$model->image;

		$this->title = 'Update Post';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			if($model->save())
            {
                if($_FILES['Content']['name']['image'] !== '') 
                {
                    $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('contents',$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->image=$filename;
                        $model->save(false);
                    }
                    Yii::app()->hsnImage->removeImg($img,'contents',$type.$model->id);

                    Yii::app()->hsnImage->removeImg($img,'contents',$type.$model->id,'small');
                    Yii::app()->hsnImage->removeImg($img,'contents',$type.$model->id,'medium');
                    Yii::app()->hsnImage->removeImg($img,'contents',$type.$model->id,'large');
                }
				Yii::app()->user->setFlash('success','Post has been updated succesfully.');
				$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','Post has been deleted.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

        // $cs=Yii::app()->clientScript;
        // $cs->registerScriptFile(Yii::app()->request->baseUrl.'/assets/js/pages/ecommerce_product_edit.min.js', CClientScript::POS_END);

        $criteria=new CDbCriteria;

        $count = Content::model()->type('blog')->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        
		$model = Content::model()->type('blog')->order('created DESC')->findAll($criteria);

		$this->render('index',array(
            'model'=>$model,
            'count'=>$count,
            'pages'=>$pages
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Content the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Content::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Content $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='content-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
