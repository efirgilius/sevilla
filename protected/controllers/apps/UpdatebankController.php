<?php

class UpdatebankController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='updatebank';
    
    private $_identity;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','search','package','loadupmember'),
//				'expression'=>'Yii::app()->user->isManager',
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','approve'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isAdmin',
			),
                        
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionApprove($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $model=$this->loadModel($id);
                if($model->saveAttributes(['status'=>1]))
                {
                    $model->member->saveAttributes(['is_update_bank_acc'=>0,'bank_name'=>$model->bank,'bank_acc_name'=>$model->account_name,'bank_acc_number'=>$model->account_number]);
                    $model->member->sendSms('Permintaan perubahan informasi Bank Anda telah dikonfirmasi. Cozmeed Network');
                    Yii::app()->user->setFlash('success','Request has been approved');
                }
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($state)
	{
        
        if($state)
        {
            if($state=='processed')
            {
                $sTitle='Processed';
                $st=1;
                $this->activeSubMenu='rprocessed';
            }
            elseif($state=='pending')
            {
                $sTitle='Pending';
                $st=0;
                $this->activeSubMenu='rpending';
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
        }
		$this->title=$sTitle.' Reques Update Bank Member List';
		$criteria=new CDbCriteria;
        $criteria->condition='t.status=:st';
        $criteria->params=array(':st'=>$st);
        $criteria->order='t.id DESC';
        $count = MemberBank::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model=MemberBank::model()->findAll($criteria);
                
		$this->render('index',array(
            'title'=>$sTitle,
			'model'=>$model,
	        'pages' => $pages,
	        'count'=>$count,
	        'i'=>$pages->offset+1,
	        'state'=>$state
		));
	}
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Member the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=  MemberBank::model()->find('id=:rid AND status=0',array(':rid'=>(int)$id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadModelRequest($id)
	{
		$model=  BankRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Member $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function sendSms($no,$message)
        {
            $url = "https://reguler.zenziva.net/apps/smsapi.php?";
            $curlHandle = curl_init();

            curl_setopt($curlHandle, CURLOPT_URL, $url);

            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$this->g('system','sms_gateway_user_key').'&passkey='.$this->g('system','sms_gateway_pass_key').'&nohp='.$no.'&tipe=reguler&pesan='.urlencode($message));

            curl_setopt($curlHandle, CURLOPT_HEADER, 0);

            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);

            curl_setopt($curlHandle, CURLOPT_POST, 1);

            $results = curl_exec($curlHandle);

            curl_close($curlHandle);

            return $results;

        }

}
