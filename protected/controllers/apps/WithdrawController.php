<?php

class WithdrawController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='wdewallet';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete','approve','index'),
				'expression'=>'Yii::app()->user->isManager',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreates()
	{
            $this->title='Add Ewallet Transaction';
		$model=new Ewallet;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ewallet']))
		{
			$model->attributes=$_POST['Ewallet'];
                        $model->type=1;
                        $model->note='Deposit Transfer';
			if($model->save())
                        {
                            $credit= $model->member->ewallet_credit;
                            $model->member->saveAttributes(['ewallet_credit'=>$credit + $model->value]);
                            Yii::app()->user->setFlash('success','Ewallet transaction has been saved successfully.');
                            $this->redirect(array('/apps/ewallet'));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdatess($id)
	{
            $this->title='Edit Ewallet Transaction';
		$model=$this->loadModel($id);
                $value= $model->value;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ewallet']))
		{
			$model->attributes=$_POST['Ewallet'];
			if($model->save())
                        {
                            $credit= $model->member->ewallet_credit;
                            $model->member->saveAttributes(['ewallet_credit'=>($credit - $value) + $model->value]);
                            Yii::app()->user->setFlash('success','Ewallet transaction has been updated successfully.');
                            $this->redirect(array('/apps/ewallet'));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model=$this->loadModel($id);
                   // $credit= $model->member->ewallet_credit;
                   // $model->member->saveAttributes(['ewallet_credit'=>$credit - $model->value]);
                    if($model->status==0)
                    {
                        $model->delete();
                        Yii::app()->user->setFlash('success','Withdraw Ewallet has been deleted successfully.');
                    }
                    Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}
        
    public function actionApprove($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $model=$this->loadModel($id);
           	$fee = ($model->value*$this->g('application','biaya_administrasi')) / 100;
            $debit=$model->member->ewallet_debit;
            
            if($model->status==0)
            {
            	if($model->member->saveAttributes(['ewallet_debit'=>$debit + $model->value])){
	                $model->saveAttributes(['status'=>1]);
	                $model->status = 1;
		            if($model->save()){
                		$model->member->saveAttributes(['ewallet_debit'=>$model->member->ewallet_debit + $fee]);
						$ewallet = new Ewallet;
		                $ewallet->member_id = $model->member->id;
		                $ewallet->note = 'Biaya Admin';
		                $ewallet->value = $fee;
		                $ewallet->status = 1;
		                $ewallet->type=Ewallet::DEBIT;
		                $ewallet->save();
	                }
            	}

                Yii::app()->user->setFlash('success','Ewallet transaction has been approved successfully.');
            }
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($type)
	{
        if($type=='new')
        {
            $tp=0;
            $title='Pending';
        }elseif($type=='approved')
        {
            $tp=1;
            $title='Approved';
        }

		$this->title=$title.' Withdraw Ewallet';
		$criteria=new CDbCriteria;
        $criteria->condition='t.status=:st and t.type=:tp';
        $criteria->params=[':st'=>$tp,':tp'=>Ewallet::DEBIT];
        $criteria->order='t.id DESC';
        $count = Ewallet::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model=  Ewallet::model()->findAll($criteria);
                
		$this->render('index',array(
                        'title'=>$title,
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Ewallet the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Ewallet::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Ewallet $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ewallet-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
