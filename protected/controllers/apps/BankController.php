<?php

class BankController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='setting';
        public $activeSubMenu='bank';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','index','set'),
				'users'=>array('@'),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bank;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bank']))
		{
			$model->attributes=$_POST['Bank'];
			if($model->save())
                        {
                            if($_FILES['Bank']['name']['image'] !== '') 
                             {
                                 $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                                 $filename=$this->generateName($uploadedFile->name);
                                 $uploadTo=$this->_createDir('b'.$model->id).'/'.$filename;
                                 if($uploadedFile->saveAs($uploadTo))
                                 {
                                     $model->image=$filename;
                                     $model->save(false);
                                 }
                             }
                            Yii::app()->user->setFlash('success','Bank has been saved.');
                             $this->redirect(array('index'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $img=$model->image;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bank']))
		{
			$model->attributes=$_POST['Bank'];
			if($model->save())
                        {
                            if($_FILES['Bank']['name']['image'] !== '') 
                             {
                                 $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                                 $filename=$this->generateName($uploadedFile->name);
                                 $uploadTo=$this->_createDir('b'.$model->id).'/'.$filename;
                                 if($uploadedFile->saveAs($uploadTo))
                                 {
                                     $model->image=$filename;
                                     $model->save(false);
                                 }
                                 if(is_file($this->getDirUrl($model->id).'/'.$img)){
                                    chmod($this->getDirUrl($model->id).'/'.$img, 0755);
                                    unlink($this->getDirUrl($model->id).'/'.$img);
                                 } 
                                 $this->removeImg($model->id,'large',$img);
                                 $this->removeImg($model->id,'small',$img);
                             }
                            Yii::app()->user->setFlash('success','Bank has been updated.');
                             $this->redirect(array('index'));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $this->loadModel($id)->delete();
                    Yii::app()->user->setFlash('success','Bank has been deleted.');
                    Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}
        
        public function actionSet($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model=$this->loadModel($id);
                    if($model->status=='0')
                    {
                        $status='online';
                        $st=1;
                    }
                    else
                    {
                        $status='offline';
                        $st=0;
                    }
//                    $model->status=$st;
//                    $model->save(false);
                    $model->saveAttributes(['status'=>$st]);
                    Yii::app()->user->setFlash('success','Bank has set '.$status);
                    if($model->status=='0')
                        echo json_encode (['status'=>'ok','msg'=>'<img src="'.Yii::app()->baseUrl.'/assets/images/offline.png" alt="off" title="Offline">','link'=>'<a href="javascript:void(0);" title="Set Online" class="icon-ok on-market" data-id="'.$model->id.'"></a>']);
                    else
                        echo json_encode (['status'=>'ok','msg'=>'<img src="'.Yii::app()->baseUrl.'/assets/images/online.png" alt="on" title="Online">','link'=>'<a href="javascript:void(0);" title="Set Offline" class="icon-remove off-market" data-id="'.$model->id.'"></a>']);
                    Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->title='Data Bank';
		$criteria=new CDbCriteria;
                $criteria->order='t.name ASC';
                $count = Bank::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 10;
                $pages->applyLimit($criteria);
                $model=Bank::model()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
                        'newModel'=>new Bank,
                        'pages' => $pages,
                        'count'=>$count,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bank the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bank::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bank $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bank-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function generateName($filename)
	{
		$ts=time();
		return $ts.'_'.preg_replace("/[^a-zA-Z0-9\.]/", "_", strtolower($filename));
	}
        
       protected function _createDir($dir)
        {
            $baseDir=Yii::app()->basePath.'/../archives/tmp';
            $path_upload = $baseDir.'/'.$dir;

            if(!is_dir($path_upload))
            {
                    mkdir($path_upload, 0777,true);
            }
            
            $path_upload = str_replace(Yii::app()->basePath.'/../', "", $path_upload);
            return $path_upload;
        }
        
        public function getDirUrl($id)
        {
            $link_dir = str_replace('protected','',Yii::app()->basePath);
              return $link_dir.'archives/tmp/b'.$id;
        }
        
        protected function removeImg($id,$type,$img)
        {
            if(is_file($this->getDirUrl($id).'/'.$type.'_'.$img)){
                chmod($this->getDirUrl($id).'/'.$type.'_'.$img, 0755);
                unlink($this->getDirUrl($id).'/'.$type.'_'.$img);
            } 
            return true;
        }
}
