<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='user';
    public $activeSubMenu='user';
        
    public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	public function actionProfile()
    {

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseurl.'/assets/js/custom/uikit_fileinput.min.js',CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseurl.'/assets/js/pages/page_user_edit.min.js',CClientScript::POS_END);
        
        $this->title='Update Profile';
        $model=User::model()->findByPk(Yii::app()->user->getState('adminId'));
        $img=$model->avatar;

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            if($model->save()) 
            {
                if($_FILES['User']['name']['avatar'] !== '') 
                 {
                    $uploadedFile=CUploadedFile::getInstance($model, 'avatar');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('users',$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->avatar=$filename;
                        $model->save(false);
                    }
                    Yii::app()->hsnImage->removeImg($img,'users',$model->id);
                    Yii::app()->hsnImage->removeImg($img,'users',$model->id,'small');
                    Yii::app()->hsnImage->removeImg($img,'users',$model->id,'large');
                 }
                if($_POST['User']['newPassword']!=='')
                {
                    $model->saveAttributes(['password'=>$model->hashPassword($_POST['User']['newPassword'], $model->salt)]);
                    Yii::app()->user->setFlash('success','User detail and password has been updated successfully.');
                }
                else
                Yii::app()->user->setFlash('success','User has been updated successfully.');
                $this->redirect(array('profile'));
            }
        }
        $this->render('profile',array('model'=>$model));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $this->title='User Detail';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->title='Add User';
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{                    
			$model->attributes=$_POST['User'];
			if($model->save())
            {
                Yii::app()->user->setFlash('success','User has been saved successfully.');
                $this->redirect(array('/apps/user'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->title='Edit User';
		$model=$this->loadModel($id);
        $img=$model->avatar;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
            {
                if($_POST['User']['newPassword']!=='')
                {
                    $model->saveAttributes(['password'=>$model->hashPassword($_POST['User']['newPassword'], $model->salt)]);
                    Yii::app()->user->setFlash('success','User detail and password has been updated successfully.');
                }
                else
                  Yii::app()->user->setFlash('success','User has been updated successfully.');
                  $this->redirect(array('/apps/user'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

        /**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','User has been removed successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->title='User';

		$criteria=new CDbCriteria;
        $criteria->condition='t.id!=:ids';
        $criteria->params=array(':ids'=>Yii::app()->user->id);
        $criteria->order='t.id DESC';

        $count = User::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        
        $model=User::model()->findAll($criteria);

        $newModel= new User;
		
        $this->render('index',array(			
			'model'=>$model,
            'newModel'=>$newModel,
            'pages' => $pages,
            'count'=>$count,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->find('id=:id AND level!=:lvl',[':id'=>$id,':lvl'=>1]);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
