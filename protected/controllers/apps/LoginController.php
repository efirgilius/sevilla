<?php

class LoginController extends Controller
{
    public $layout='login';
        
        public function	init() {
            if(Yii::app()->user->id)
                    $this->redirect(array('/apps/dashboard'));
            
		return parent::init();
	}
        
	public function actionIndex()
	{
        $this->title='Admin Login';
		$model=new LoginForm;

		if(isset($_POST['LoginForm']))
		{
//                    if(Yii::app()->request->getIsAjaxRequest())
//                    {
//                        if($_POST['LoginForm']['username']=='')
//                        {
//                            echo CJSON::encode(array('status'=>'error','errorReason'=>'username can not be blank'));
//                            Yii::app()->end();
//                        }
//                        else
//                        {
//                            if($_POST['LoginForm']['password']=='')
//                            {
//                                echo CJSON::encode(array('status'=>'error','errorReason'=>'Password can not be blank'));
//                                Yii::app()->end();
//                            }
//                            else
//                            {
//                                $model->attributes=$_POST['LoginForm'];
//                                // validate user input and redirect to the previous page if valid
//                                if($model->validate() && $model->login())
//                                {                  
//                                    echo CJSON::encode(array('status'=>'success'));
//                                    Yii::app()->end();
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
            $model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
            {
				$this->redirect(array('/apps/dashboard'));
            }
                    //}
		}
		// display the login form
		$this->render('index',array('model'=>$model));
	}
}