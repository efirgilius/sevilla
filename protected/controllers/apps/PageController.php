<?php

class PageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='cms';
    public $activeSubMenu="page";
    public $title='Page';

	public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model=$this->loadModel($id);
        $this->title=$model->title;
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
        
        $this->title='Add Page';

		$model=new Content;
		$model->user_id = Yii::app()->user->id;
		$model->type = 'page';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			if($model->save())
				Yii::app()->user->setFlash('success','Page has been saved succesfully.');
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->title = 'Update Page';
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
        
		$model=$this->loadModel($id);
		$model->scenario='category';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			if($model->save())
				Yii::app()->user->setFlash('success','Page has been updated succesfully.');
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','Page has been deleted.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	public function actionDeleteGroup($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
        	$group = Category::model()->findByPk($id);
            $group->delete();
            Yii::app()->user->setFlash('success','Page Group has been deleted.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	public function actionDeleteFromGroup($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
        	$page = Content::model()->findByPk($id);
            $page->saveAttributes(['category_id'=>null]);
            Yii::app()->user->setFlash('success','Page has been deleted from group.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria;

        $count = Content::model()->type('page')->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        
		$model = Content::model()->type('page')->order('created DESC')->findAll($criteria);

		$this->render('index',array(
            'model'=>$model,
            'count'=>$count,
            'pages'=>$pages
		));
	}

	public function actionGroup()
	{
		$this->title = 'Page Group';
		$this->activeSubMenu = 'pagegroup';
		
		$category = Category::model()->type('pagegroup')->active()->order('name ASC')->findAll();
		
		$group = new Category;

		if(isset($_POST['Category']))
		{
			$group->attributes=$_POST['Category'];
			$group->status = 1;
			$group->type = 'pagegroup';
			if($group->save()){
				Yii::app()->user->setFlash('success','Page Group has been created succesfully.');
			}else{
				Yii::app()->user->setFlash('error','Page Group failed to save.');
			}
			$this->refresh();
		}

		$pages = Content::model()->type('page')->findAll('category_id is null');
		$groups = Category::model()->type('pagegroup')->findAll();

		$this->render('group',array(
            'category'=>$category,
            'group'=>$group,
            'pages'=>$pages,
            'groups'=>$groups
		));
	}

	public function actionUpdateGroup()
	{
		if (isset($_POST['group'])) {
			$model = Category::model()->findByPk($_POST['group']['id']);
			$model->name = $_POST['group']['name'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success','Page Group has been updated succesfully.');
				$this->redirect(array('group'));
			}
		}else{
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}

	public function actionAddPageGroup()
	{
		if (isset($_POST['GroupPage'])) {
			$model = Content::model()->findByPk($_POST['GroupPage']['content_id']);
			$model->category_id = $_POST['GroupPage']['category_id'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success','Page has been added to Group Page.');
				$this->redirect(array('group'));
			}
		}else{
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}

	public function actionUpdatePageGroup()
	{
		if (isset($_POST['GroupPage'])) {
			$model = Content::model()->findByPk($_POST['GroupPage']['page_id']);
			$model->category_id = $_POST['GroupPage']['group_id'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success','Group Page has been updated.');
				$this->redirect(array('group'));
			}
		}else{
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Content the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Content::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Content $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='content-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
