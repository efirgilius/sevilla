<?php

class ProductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='product';
	public $activeSubMenu='list-product';

	public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$cs=Yii::app()->clientScript;
        $cs->registerScriptFile(Yii::app()->request->baseUrl.'/assets/js/jquery.multifile.js', CClientScript::POS_END);
		$cs->registerScriptFile(Yii::app()->baseurl.'/assets/js/pages/ecommerce_product_edit.min.js',CClientScript::POS_END);
		
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
		
		$this->title = 'Create Product';

		$model=new Product;

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			//print_r($_POST);exit;
			
			$model->attributes=$_POST['Product'];
			if($model->save())
            {
                $mImg=CUploadedFile::getInstanceByName('mainimage');
                if(isset($mImg) && !empty($mImg))
                {
                    $mainImg=ProductImage::model()->findByAttributes(array('product_id'=>$model->id,'is_primary'=>1));
                    if($mainImg)
                        $dataImg=$mainImg;
                    else
                        $dataImg=new ProductImage;
                     
                    $nameImg=Yii::app()->hsnImage->generateName($mImg->name);
                    $uplTo=Yii::app()->hsnImage->_createDir('products',$model->id).'/'.$nameImg;
                    if($mImg->saveAs($uplTo))
                    {
                        $dataImg->product_id=$model->id;
                        $dataImg->is_primary=1;
                        $dataImg->image=$nameImg;
                        $dataImg->save(false);
                    }
                }

                $images = CUploadedFile::getInstances($model,'images');
                if (isset($images) && count($images) > 0) 
                {
                    $i=1;
                    foreach ($images as $key => $pic)
                    {                                    
                        $filename=Yii::app()->hsnImage->generateName($pic->name);
                        $uploadTo=Yii::app()->hsnImage->_createDir('products',$model->id).'/'.$filename;
                        
                        if($pic->saveAs($uploadTo))
                        {
                            $data=new ProductImage;
                            $data->product_id=$model->id;
                            $data->image=$filename;
                            $data->save();
                        }
                        $i++;
                    }
                }
                
                Yii::app()->user->setFlash('success','Product has been saved successfully.');
                $this->redirect(array('view','id'=>$model->id));
            }    
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$cs=Yii::app()->clientScript;
        $cs->registerScriptFile(Yii::app()->baseUrl.'/assets/js/jquery.multifile.js', CClientScript::POS_END);
		
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";

		
		$model=$this->loadModel($id);
				
		$this->title = 'Update Product';
		
		//$img=$model->image;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{

			$model->attributes=$_POST['Product'];
			if($model->save())
            {
            	$mImg=CUploadedFile::getInstanceByName('mainimage');
                if(isset($mImg) && !empty($mImg))
                {
                    $mainImg=ProductImage::model()->findByAttributes(array('product_id'=>$model->id,'is_primary'=>1));
                    if($mainImg)
                        $dataImg=$mainImg;
                    else
                        $dataImg=new ProductImage;
                     
                    $nameImg=Yii::app()->hsnImage->generateName($mImg->name);
                    $uplTo=Yii::app()->hsnImage->_createDir('products',$model->id).'/'.$nameImg;
                    if($mImg->saveAs($uplTo))
                    {
                        $dataImg->product_id=$model->id;
                        $dataImg->is_primary=1;
                        $dataImg->image=$nameImg;
                        $dataImg->save(false);
                    }
                }

            	$images = CUploadedFile::getInstances($model,'images');
                if (isset($images) && count($images) > 0) 
                {
                    $i=1;
                    foreach ($images as $key => $pic)
                    {                                    
                        $filename=Yii::app()->hsnImage->generateName($pic->name);
                        $uploadTo=Yii::app()->hsnImage->_createDir('products',$model->id).'/'.$filename;
                        
                        if($pic->saveAs($uploadTo))
                        {
                            $data=new ProductImage;
                            $data->product_id=$model->id;
                            $data->image=$filename;
                            $data->save();
                        }
                        $i++;
                    }
                }

                Yii::app()->user->setFlash('success','Product has been updated successfully.');
                $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','Product has been removed successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->activeSubMenu='list-product';
		$this->title='Product List';

		$criteria=new CDbCriteria;
		
        $count = Product::model()->order('created DESC')->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $model = Product::model()->order('created DESC')->findAll($criteria);
		
		$this->render('index',array(			
            'pages' => $pages,
            'count'=>$count,
            'model'=>$model 
		));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->title = $model->name;

		$this->render('view',[
			'model'=>$model
			]);
	}

    public function actionGroup()
    {
        $this->title = 'Product Group';
        $this->activeSubMenu = 'group';
        
        $category = Category::model()->type('group')->active()->order('name ASC')->findAll();
        
        $group = new Category;

        if(isset($_POST['Category']))
        {
            $group->attributes=$_POST['Category'];
            $group->status = 1;
            $group->type = 'group';
            if($group->save()){
                Yii::app()->user->setFlash('success','Product Group has been created succesfully.');
            }else{
                Yii::app()->user->setFlash('error','Product Group failed to save.');
            }
            $this->refresh();
        }

        $criteria = new CDbCriteria;

        $product = Product::model()->order('created DESC')->findAll();

        $groups = Category::model()->type('group')->findAll();

        $this->render('group',array(
            'category'=>$category,
            'group'=>$group,
            'product'=>$product,
            'groups'=>$groups
        ));
    }

    public function actionAddProductGroup()
    {
        if (isset($_POST['ProductGroup'])) {

            $model = new ProductGroup;
            $model->product_id = $_POST['ProductGroup']['product_id'];
            $model->category_id = $_POST['ProductGroup']['category_id'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success','Product has been added to Group.');
                $this->redirect(array('group'));
            }
        }else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    public function actionUpdateProductGroup()
    {
        if (isset($_POST['ProductGroup'])) {
            //print_r($_POST['ProductGroup']);exit;
            $model = ProductGroup::model()->findByAttributes(['product_id'=>$_POST['ProductGroup']['productid']]);
            $model->category_id = $_POST['ProductGroup']['groupid'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success','Product Group has been updated.');
                $this->redirect(array('group'));
            }
        }else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

	public function actionDeleteimage()
    {
        if(Yii::app()->request->getIsAjaxRequest())
        {
            if(Yii::app()->request->getQuery('productid') && Yii::app()->request->getQuery('imgid'))
            {
                echo json_encode($this->getResponse());                
            }
            else
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        else
                throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
    }
        
    protected function getResponse()
    {
        $image=Yii::app()->request->getQuery('imgid');
        $model = ProductImage::model()->findByAttributes(array('id'=>$image,'product_id'=>Yii::app()->request->getQuery('productid')));   
        
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'));

        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'small');
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'medium');
        Yii::app()->hsnImage->removeImg($model->image,'products',Yii::app()->request->getQuery('productid'),'large');
        
        $response = array();    
        if($model->delete())
        {
            $response = array(
              'ok' => true,
              'msg' => "The Image has been deleted!");
        }
        else
        {
            $response = array(
                  'ok' => false,
                  'msg' => "We can't find the image!");
        }
        return $response;
    }

    public function actionDeleteFromGroup($id)
    {
        if(Yii::app()->request->getIsAjaxRequest())
        {
            $page = ProductGroup::model()->findByPk($id);
            $page->delete();
            Yii::app()->user->setFlash('success','Product has been deleted from group.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Product the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Product $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
