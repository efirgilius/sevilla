<?php 

/**
* 
*/
class MenuController extends Controller
{
	
	public $layout='admin';
    public $activeMenu='cms';
    public $activeSubMenu="menu";
    public $title='Menu';

    public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	public function actionIndex()
	{
		$this->title = 'Menu';
		
		
		$menu = new Menu;

		if(isset($_POST['Menu']))
		{
			$menu->attributes=$_POST['Menu'];
			if($menu->save()){
				Yii::app()->user->setFlash('success','Menu has been created succesfully.');
			}else{
				Yii::app()->user->setFlash('error','Menu failed to save.');
			}
			$this->refresh();
		}

		$menus = Menu::model()->findAll();

		$page = Content::model()->type('page')->findAll();
		$pagegroup = Category::model()->type('pagegroup')->findAll();
		$blogcat = Category::model()->type('blog')->findAll();

		$this->render('index',array(
            'menus'=>$menus,
            'menu'=>$menu,
            'pagegroup'=>$pagegroup,
            'page'=>$page,
            'blogcat'=>$blogcat
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
        	$menu = Menu::model()->findByPk($id);
            $menu->delete();
            Yii::app()->user->setFlash('success','Menu has been deleted.');
             Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

}
