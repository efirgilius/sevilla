<?php

class MembershipConfirmationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='mconfirm';
        public $userKey='vdobfj';
        public $passKey='C1bulan!';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete','approve'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isOperator',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
//	public function actionView($id)
//	{
//		$this->title='Membership Payment Detail';
//                $model=$this->loadModel($id);
//                if($model->status==0)
//                {
//                    $this->activeSubMenu='new-order';
//                    $models= Order::model()->findAll('status=:st LIMIT 10',[':st'=>$model->status]);
//                }
//                elseif($model->status==1)
//                {
//                    $this->activeSubMenu='con-order';
//                    $models= Order::model()->findAll('status=:st LIMIT 10',[':st'=>$model->status]);
//                }
//                elseif($model->status==2)
//                {
//                    $this->activeSubMenu='pro-order';
//                    $models= Order::model()->findAll('status=:st LIMIT 10',[':st'=>$model->status]);
//                }
//                
//		$this->render('view',array(
//			'model'=>$model,
//                        'models'=>$models
//		));
//	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionApprove($id)
	{
            if(Yii::app()->request->getIsAjaxRequest())
            {
//            if(isset($_POST['pid']))
//            {
				$model=$this->loadModel($id);
                if($model->saveAttributes(['status'=>1]))
                {
                    $model->member->getStoreMember($model->member_id, $model->member->sponsor_id);
                    $this->sendSms($model->member->mobile_phone, 'Terima kasih, pembayaran untuk pendaftaran keanggotaan Anda telah berhasil dilakukan dan akun Anda telah aktif');
                    Yii::app()->user->setFlash('success','The membership payment has been confirmed, and this member has been activated as member.');
                    $this->redirect(['/apps/member/view','id'=>$model->member_id]);
                }
            }
            else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
//                else
//                {
//                    Yii::app()->user->setFlash('error','Please try again.');
//                    $this->redirect(['view','id'=>$id]);
//                }
//            }
//            else
//                throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model= $this->loadModel($id);
                    $model->member->saveAttributes(['status'=>2]);
                    $model->key->saveAttributes(['is_used'=>0]);
                    $model->delete();
                    Yii::app()->user->setFlash('success','Membership confirmation has been deleted successfully.');
                    Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($type)
	{
		$this->title='Membership Payment Confirmation';
		if($type=='new')
            {
                $tp=0;
                $title='New';
                $this->activeSubMenu='new-mconfirm';
            }
            elseif($type=='proceed')
            {
                $tp=1;
                $title='Processed';
                $this->activeSubMenu='pro-mconfirm';
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
                    
		$criteria=new CDbCriteria;
                    $criteria->condition='t.status=:st and t.type=0';
                    $criteria->params=array(':st'=>$tp);
                $criteria->order='t.id DESC';
                $count = PremiumPayment::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=PremiumPayment::model()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
                        'title'=>$title,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PackageOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=  PremiumPayment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PackageOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='package-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function sendSms($no,$message)
        {
            $url = "https://reguler.zenziva.com/apps/smsapi.php?";
            $curlHandle = curl_init();

            curl_setopt($curlHandle, CURLOPT_URL, $url);

            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$this->userKey.'&passkey='.$this->passKey.'&nohp='.$no.'&tipe=reguler&pesan='.urlencode($message));

            curl_setopt($curlHandle, CURLOPT_HEADER, 0);

            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);

            curl_setopt($curlHandle, CURLOPT_POST, 1);

            $results = curl_exec($curlHandle);

            curl_close($curlHandle);

            return $results;

        }
}
