<?php

class DashboardController extends Controller
{

	public $title='Dashboard';
	public $layout='admin';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
        /**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isAdmin || Yii::app()->user->isOperator',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{

		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseurl.'/assets/js/pages/dashboard.min.js',CClientScript::POS_END);

		$this->render('index');
	}
}