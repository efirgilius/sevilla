<?php

class ContactController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='mailbox';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete','email','setting','read','reply'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isAdmin || Yii::app()->user->isOperator',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','Mailbox has been deleted successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	public function actionRead($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->saveAttributes(['status'=>Contact::READ]);
            Yii::app()->user->setFlash('success','Mark as read successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		$this->title='Mailbox';
		$cs=Yii::app()->clientScript;
        $cs->registerScriptFile(Yii::app()->request->baseUrl.'/assets/js/pages/page_mailbox.min.js', CClientScript::POS_END);
    
        $criteria=new CDbCriteria;
        $criteria->condition='reply_id IS NULL';
        
        $count = Contact::model()->latest()->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $model=Contact::model()->latest()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
            'pages' => $pages,
            'count'=>$count,
            'i'=>$pages->offset+1,
		));
	}

	public function actionReply()
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
			$model=new Contact;   
	        $model->scenario='reply';
	        
			$model->name=$this->g('general','site_name');
			$model->email=$this->g('contact','mailbox_reply');
			$model->subject='Reply';
			$model->message=$_POST['msg'];
			$model->reply_id=$_POST['id'];


			if($model->save())
			{	

				$to = Contact::model()->findByPk($model->reply_id);
                
                //$this->sendContact($model->subject, $to->email, $to->name, $model->message);

				$to      = $to->email;
				$subject = 'Reply [Contact] on hasanprayoga.com';
				$message = $model->message;
				$headers = 'From: '.$this->g('contact','mailbox_reply')."\r\n".'Reply-To: '.$to."\r\n".'X-Mailer: PHP/'.phpversion();

				mail($to, $subject, $message, $headers);

                $replied = $this->loadModel($model->reply_id);
                $replied->saveAttributes(['status'=>Contact::REPLIED]);

				echo CJSON::encode( array ('status'=>'success','message'=>$model->message,'name'=>$replied->name,'email'=>$replied->email,'time'=>$replied->created_on));
			}

	        $this->_removeSession();
	    }
        else
             throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Contact the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Contact::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function sendContact($subject,$senderMail,$senderName,$message)
    {
        $email= $this->g('contact','mailbox_reply');
        if($email)
        {
            Yii::import('application.extensions.YiiMailer.YiiMailer');    
            $mail = new YiiMailer; 
            $body=$this->renderFile(Yii::app()->basePath.'/views/email/contact.php',array('name'=>$senderName,'subject'=>$subject,'message'=>$message),TRUE);

            $mail->SetFrom($email, 'Contact Inbox');
            $mail->AddReplyTo($senderMail,$senderName);
            $mail->Subject    = '[Contact Inbox] '.$subject;
            $mail->MsgHTML($body);

            if(!$mail->Send())
            {
                Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.User') ;
            }
        }
    }

    protected function _removeSession()
    {
        $session = Yii::app()->session;
            $prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
            foreach($session->keys as $key)
            {
                    if(strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
                            $session->remove($key);
            }
    }

	/**
	 * Performs the AJAX validation.
	 * @param Contact $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contact-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
