<?php

class UpgradeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='upgrade';
        
        private $_identity;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','search','package','loadupmember'),
//				'expression'=>'Yii::app()->user->isManager',
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','approve'),
				'expression'=>'Yii::app()->user->isManager',
			),
                        
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionApprove($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model=$this->loadModel($id);
                    if($model->status=='0')
                    {
                        $payment= PremiumPayment::model()->findByAttributes(['member_id'=>$model->member_id,'type'=>1]);
                        if($payment)
                        {
                            $model->saveAttributes(array('status'=>1));
                            $payment->saveAttributes(array('status'=>1));
                            $model->member->getStoreUpgrade($model->member_id,$payment->value);
                            $this->sendSms($model->member->mobile_phone, 'Terima kasih, pembayaran untuk upgrade keanggotaan Anda telah berhasil dilakukan dan akun Anda telah aktif');
                            Yii::app()->user->setFlash('success','Member has been approved');
                        }
                    }
                     Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($state)
	{
        
        if($state)
        {
            if($state=='proceed')
            {
                $sTitle='Processed';
                $st=1;
                $this->activeSubMenu='uactive';
            }
            elseif($state=='new')
            {
                $sTitle='Pending';
                $st=0;
                $this->activeSubMenu='upending';
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
        }
		$this->title=$sTitle.' Upgrade Member List';
		$criteria=new CDbCriteria;
        $criteria->condition='t.status=:st';
        $criteria->params=array(':st'=>$st);
        $criteria->order='t.id DESC';
        $count = UpgradeMember::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model=UpgradeMember::model()->findAll($criteria);
                
		$this->render('index',array(
                        'title'=>$sTitle,
			'model'=>$model,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
	}
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Member the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Member::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadModelRequest($id)
	{
		$model=  BankRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Member $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function sendNewConfirmation($model,$password,$verifyUrl)
        {
            Yii::import('application.extensions.YiiMailer.YiiMailer');        
            $mail = new YiiMailer; 
            $body=$this->renderFile(Yii::app()->basePath.'/views/email/newconfirm.php',array('model'=>$model,'password'=>$password,'verifyUrl'=>$verifyUrl),TRUE);

            $mail->SetFrom('noreply@secretofwealth.co.id', 'Info');
            $mail->AddAddress($model->email, $model->name);
            $mail->Subject    = "Welcome to Secret of Wealth Indonesia";
            $mail->MsgHTML($body);
            if(!$mail->Send())
            {
                Yii::log("Mailer Error: " . $mail->ErrorInfo, 'warning', 'application.models.Member') ;
                return false;
            }
            else
                return true;
        }
        
        protected function saveSponsor($mid,$sid,$lvl)
        {
            $model=new NetworkMatrix;
            $model->member_id=$mid;
            $model->sponsor_id=$sid;
            $model->level=$lvl;
            $model->save();
        }
}
