<?php

class EwalletController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='ewallet';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create','update'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isAdmin',
			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array(),
//				'expression'=>'Yii::app()->user->isManager',
//			),
                        array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('transaction','delete','approve'),
				'expression'=>'Yii::app()->user->isManager',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $model=$this->loadModel($id);
           // $credit= $model->member->ewallet_credit;
           // $model->member->saveAttributes(['ewallet_credit'=>$credit - $model->value]);
            if($model->status==0)
            {
                $model->delete();
                Yii::app()->user->setFlash('success','Ewallet Confirmation has been deleted successfully.');
            }
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}
        
    public function actionApprove($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
	    {
	        $model=$this->loadModel($id);
	        if($model->status==0)
	        {
	            if($model->saveAttributes(['status'=>1])){
		            $ewallet = $model->ewallet;
		            if ($ewallet) {
			            $credit= $ewallet->member->ewallet_credit;
			            if($ewallet->member->saveAttributes(['ewallet_credit'=>$credit + $model->value])){
				            if($ewallet->saveAttributes(['status'=>1])){
				            	Yii::app()->user->setFlash('success','Ewallet confirmation has been approved successfully.');
	        					Yii::app()->end();	
				            }
				        }
		            }
	        	}
	        }
	    }
	    else
	        throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($status)
	{
        if($status=='pending')
        {
            $st=EwalletConfirmation::PENDING;
            $title='Pending';
            $this->activeSubMenu = 'pen-ewallet';
        }
        elseif($status=='approved')
        {
            $st=EwalletConfirmation::APPROVED;;
            $title='Approved';
            $this->activeSubMenu = 'pro-ewallet';
        }
		$this->title=$title.' Ewallet Confirmation';
		$criteria=new CDbCriteria;
        $criteria->condition='t.status=:st';
        $criteria->params=[':st'=>$st];
        $criteria->order='t.id DESC';

        $count = EwalletConfirmation::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model=  EwalletConfirmation::model()->findAll($criteria);
                
		$this->render('index',array(
            'title'=>$title,
			'model'=>$model,
            'pages' => $pages,
            'count'=>$count,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Ewallet the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EwalletConfirmation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Ewallet $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ewallet-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
