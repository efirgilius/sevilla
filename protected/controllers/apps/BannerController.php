<?php

class BannerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='cms';
    public $activeSubMenu='banner';

	public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{                
		$this->title='Detail Banner';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            
        $this->title='Add Banner';
		$model=new Banner;
		$model->order = $model->count() + 1;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Banner']))
		{
            if($_FILES['Banner']['name']['image'] !== '') 
            {
				$model->attributes=$_POST['Banner'];
				$model->type = 'slide';
				if($model->save())
                {
                    if($_FILES['Banner']['name']['image'] !== '') 
                    {
                        $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                        $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                        $uploadTo=Yii::app()->hsnImage->_createDir('banners','b'.$model->id).'/'.$filename;
                        if($uploadedFile->saveAs($uploadTo))
                        {
                            $model->image=$filename;
                            $model->save();
                        }
                    }
                    Yii::app()->user->setFlash('success','Banner has been saved successfully.');
                    $this->redirect(array('/apps/banner'));
                }    
            }
            else
            {
                Yii::app()->user->setFlash('error','Image file is required.');
				$this->redirect(array('create'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->title='Edit Banner';
		$model=$this->loadModel($id);
        $img=$model->image;
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Banner']))
		{
			$model->attributes=$_POST['Banner'];
			if($model->save())
            {
                if($_FILES['Banner']['name']['image'] !== '') 
                {
                    $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('banners','b'.$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->image=$filename;
                        $model->save(false);
                    }
                    Yii::app()->hsnImage->removeImg($img,'banners','b'.$model->id);
                    Yii::app()->hsnImage->removeImg($img,'banners','b'.$model->id,'small');
                    Yii::app()->hsnImage->removeImg($img,'banners','b'.$model->id,'medium');
                    Yii::app()->hsnImage->removeImg($img,'banners','b'.$model->id,'large');
                }
                Yii::app()->user->setFlash('success','Banner has been updated successfully.');
                $this->redirect(array('/apps/banner'));
            }                        
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success','Banner has been deleted successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{   
        $this->title='Banner';

		$criteria=new CDbCriteria;

        $count = Banner::model()->order('created DESC')->type('slide')->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        
        $model=Banner::model()->order('created DESC')->type('slide')->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
            'pages' => $pages,
            'count'=>$count,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Banner the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Banner::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Banner $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='banner-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}
