<?php

class SettingController extends Controller
{
    public $layout='admin';
    public $activeMenu='setting';

	/**
	 * @return array action filters
	 */
	public function	init() {
        if(!Yii::app()->user->isManager)
        $this->redirect(array('/apps/login'));
            
		return parent::init();	
	}

	public function actionGeneral()
	{
        $this->title='General Settings';
        $this->activeMenu='setting';
        $this->activeSubMenu='general';

        if (isset($_POST['saveSetting'])) 
        {
            $img = $this->g('general','logo');
            if ($_POST['general']) {
                foreach ($_POST['general'] as $key => $value) {
                    if($_FILES['general']['name']['logo'] !== '') 
                    {
                        $uploadedFile=CUploadedFile::getInstanceByName('general[logo]');       
                        $filename=Yii::app()->hsnImage->generateName($uploadedFile);
                        $uploadTo=Yii::app()->hsnImage->_createDir('settings','logo').'/'.$filename;
                        if($uploadedFile->saveAs($uploadTo))
                        {
                            $this->s('general','logo',$filename);
                        }
                        Yii::app()->hsnImage->removeImg($img,'settings','logo');

                        //lamun logona bade diresize : Yii::app()->hsnImage->generateThumb(20,20,'settings','logo',$this->g('general','logo')); 
                    }
                    $this->s('general',$key,$value);
                }
            }

            if ($_POST['meta']) {
                foreach ($_POST['meta'] as $key => $value) {
                    $this->s('meta',$key,$value);
                }
            }

            if ($_POST['contact']) {
                foreach ($_POST['contact'] as $key => $value) {
                    $this->s('contact',$key,$value);
                }
            }

            if ($_POST['socmed']) {
                foreach ($_POST['socmed'] as $key => $value) {
                    $this->s('socmed',$key,$value);
                }
            }
			
			if ($_POST['front']) {
                foreach ($_POST['front'] as $key => $value) {
                    $this->s('front',$key,$value);
                }
            }

            Yii::app()->user->setFlash('success','Settings has been changed successfully.');
            $this->refresh();
        }

        $this->render('general');
    }

    public function actionApplication()
    {
        $this->title='Application Settings';
        $this->activeMenu='setting';
        $this->activeSubMenu='application';

        if (isset($_POST['saveSetting'])) 
        {

            if ($_POST['application']) {
                foreach ($_POST['application'] as $key => $value) {
                    $this->s('application',$key,$value);
                }
            }

            Yii::app()->user->setFlash('success','Settings has been changed successfully.');
            $this->refresh();
        }

        $this->render('application');
    }

    public function actionBonus()
    {
        $this->title='Bonus Settings';
        $this->activeMenu='setting';
        $this->activeSubMenu='bonus';

        if (isset($_POST['saveSetting'])) 
        {

            if ($_POST['bonus']) {
                foreach ($_POST['bonus'] as $key => $value) {
                    $this->s('bonus',$key,$value);
                }
            }

            Yii::app()->user->setFlash('success','Settings has been changed successfully.');
            $this->refresh();
        }

        $this->render('bonus');
    }
}