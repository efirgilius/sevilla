<?php

class UpgradeMemberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
        public $activeMenu='uconfirm';
        public $userKey='vdobfj';
        public $passKey='C1bulan!';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete','setting','approve'),
				'expression'=>'Yii::app()->user->isManager || Yii::app()->user->isOperator',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionApprove($id)
	{
                $model=$this->loadModel($id);
                if($model->status=='0')
                {
                    if($model->saveAttributes(['status'=>1]))
                    {
                        $model->member->getStoreUpgrade($model->member_id,$model->value);
                        $upgradeModel= $model->member->isupgradeconfirm;
                        $upgradeModel->saveAttributes(['status'=>1]);
                        $this->sendSms($model->member->mobile_phone, 'Terima kasih, pembayaran untuk upgrade keanggotaan Anda telah berhasil dilakukan dan akun Anda telah aktif');
                        Yii::app()->user->setFlash('success','The membership payment has been confirmed, and this member has been activated as AGENT.');
                        $this->redirect(['/apps/member/view','id'=>$model->member_id]);
                    }
                }
                else
                    $this->redirect(['index']);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
                {
                    $model= $this->loadModel($id);
                    $model->member->saveAttributes(['status'=>2]);
                    $model->key->saveAttributes(['is_used'=>0]);
                    $model->delete();
                    Yii::app()->user->setFlash('success','Membership confirmation has been deleted successfully.');
                    Yii::app()->end();
                }
                else
                    throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($type)
	{
           // $this->sendSms('085220322999', '');exit;
		$this->title='Upgrade Payment Confirmation';
		if($type=='new')
            {
                $tp=0;
                $title='New';
                $this->activeSubMenu='new-uconfirm';
            }
            elseif($type=='proceed')
            {
                $tp=1;
                $title='Processed';
                $this->activeSubMenu='pro-uconfirm';
            }
            else
                throw new CHttpException(404,'The requested page does not exist.');
                    
		$criteria=new CDbCriteria;
                    $criteria->condition='t.status=:st and t.type=1';
                    $criteria->params=array(':st'=>$tp);
                $criteria->order='t.id DESC';
                $count = PremiumPayment::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 20;
                $pages->applyLimit($criteria);
                $model=PremiumPayment::model()->findAll($criteria);
                
		$this->render('index',array(
			'model'=>$model,
                        'title'=>$title,
                        'pages' => $pages,
                        'count'=>$count,
                        'i'=>$pages->offset+1
		));
	}

	public function actionSetting()
	{
		$this->activeSubMenu = 'settingupgrade';
		$this->title = 'Information Setting';
		
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/";
        
		$page=Content::model()->findByAttributes(['type'=>Content::UPGRADE]);
		if ($page==null) {
			$page = new Content;
		}

		if(isset($_POST['Content']))
		{                    

			$page->attributes=$_POST['Content'];
			$page->type=Content::UPGRADE;
			if($page->save())
            {
                                 
                Yii::app()->user->setFlash('success','Upgrade Member information has been updated succesfully.');
				$this->refresh();
            }
		}

		$this->render('setting',array(			
			'page'=>$page,
		));

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PackageOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=  PremiumPayment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PackageOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='package-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function sendSms($no,$message)
        {
            $url = "https://reguler.zenziva.com/apps/smsapi.php?";
            $curlHandle = curl_init();

            curl_setopt($curlHandle, CURLOPT_URL, $url);

            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$this->userKey.'&passkey='.$this->passKey.'&nohp='.$no.'&tipe=reguler&pesan='.urlencode($message));

            curl_setopt($curlHandle, CURLOPT_HEADER, 0);

            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);

            curl_setopt($curlHandle, CURLOPT_POST, 1);

            $results = curl_exec($curlHandle);

            curl_close($curlHandle);

            return $results;

        }
}
