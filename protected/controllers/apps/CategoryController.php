<?php

class CategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
    public $activeMenu='cms';

	public function	init() {
        if(!Yii::app()->user->isManager)
            $this->redirect(array('/apps/login'));
        
	return parent::init();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($type)
	{
		$this->activeSubMenu=$type.'cat';

		$this->title = 'Create '.ucfirst($type).' Category';

		$model=new Category;
		$model->type = $type;

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Category']))
		{
			//print_r($_POST);exit;
			
			$model->attributes=$_POST['Category'];
			if($model->save())
            {
                if($_FILES['Category']['name']['image'] !== '') 
                {
                    $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('categories',$type.$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->image=$filename;
                        $model->save();
                    }
                }
                Yii::app()->user->setFlash('success',ucfirst($type).' Category has been saved successfully.');
                $this->redirect(array('index','type'=>$type));
            }    
		}
		$parents = Category::model()->type($type)->null()->findAll();
		$this->render('create',array(
			'model'=>$model,
			'type'=>$type,
			'parents'=>$parents
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{


		$model=$this->loadModel($id);
		
		$type = $model->type;
		
		$this->activeSubMenu=$type.'cat';
		$this->title = 'Update '.ucfirst($type).' Category';
		
		$img=$model->image;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Category']))
		{
			$model->attributes=$_POST['Category'];
			if($model->save())
            {
                if($_FILES['Category']['name']['image'] !== '') 
                {
                    $uploadedFile=CUploadedFile::getInstance($model, 'image');       
                    $filename=Yii::app()->hsnImage->generateName($uploadedFile->name);
                    $uploadTo=Yii::app()->hsnImage->_createDir('categories',$type.$model->id).'/'.$filename;
                    if($uploadedFile->saveAs($uploadTo))
                    {
                        $model->image=$filename;
                        $model->save(false);
                    }
                    Yii::app()->hsnImage->removeImg($img,'categories',$type.$model->id);

                    Yii::app()->hsnImage->removeImg($img,'categories',$type.$model->id,'small');
                    Yii::app()->hsnImage->removeImg($img,'categories',$type.$model->id,'medium');
                    Yii::app()->hsnImage->removeImg($img,'categories',$type.$model->id,'large');
                }
                Yii::app()->user->setFlash('success',ucfirst($type).' Category has been updated successfully.');
                $this->redirect(array('index','type'=>$model->type));
            }
		}

		$parents = Category::model()->type($type)->null()->findAll('id!='.$model->id);
		$this->render('update',array(
			'model'=>$model,
			'type'=>$type,
			'parents'=>$parents
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->getIsAjaxRequest())
        {
            $this->loadModel($id)->delete();
            Yii::app()->user->setFlash('success',ucfirst($type).' Category has been removed successfully.');
            Yii::app()->end();
        }
        else
            throw new CHttpException(403, "You open a wrong page. Please don't call this page directly");
	}	

	/**
	 * Lists all models.
	 */
	public function actionIndex($type)
	{
		$this->activeSubMenu=$type.'cat';
		$this->title=ucfirst($type).' Categories';

		$criteria=new CDbCriteria;
		
        $count = Category::model()->null()->type($type)->count($criteria);
        
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $parents = Category::model()->null()->type($type)->findAll();
		
		$this->render('index',array(			
            'pages' => $pages,
            'count'=>$count,
            'type'=>$type,
            'parents'=>$parents 
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Category the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Category::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Category $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
