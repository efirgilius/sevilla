<?php

class ErrorController extends Controller
{
    public $layout='theme_error';
	public function actionIndex()
	{
		if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
            else
            {
                $this->title='Error '.$error['code'];
                    $this->render('index', $error);
            }
        }
	}
}