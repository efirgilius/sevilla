<?php

/**
 * This is the model class for table "tbl_card_balance".
 *
 * The followings are the available columns in table 'tbl_card_balance':
 * @property string $member_id
 * @property integer $value
 */
class CardbalanceForm extends CFormModel
{
        public $id_member;
        public $member_id;
        public $type;
        public $value;
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('id_member, member_id, type, value', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('member_id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'member_id' => 'Member',
			'value' => 'Jumlah SN',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CardBalance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
}
