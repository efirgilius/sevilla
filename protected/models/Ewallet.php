<?php

/**
 * This is the model class for table "tbl_ewallet".
 *
 * The followings are the available columns in table 'tbl_ewallet':
 * @property string $id
 * @property string $member_id
 * @property string $value
 * @property integer $type
 * @property string $note
 * @property integer $status
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Member $member
 * @property EwalletConfirmation[] $ewalletConfirmations
 */
class Ewallet extends CActiveRecord
{
	const CREDIT=1; //MASUK
    const DEBIT=0; //KELUAR

    const PENDING=0; //MASUK
    const APPROVED=1; //KELUAR
    const CONFIRM=2; //MASUK
    
    public $coin;
    
    public function getTypename()
    {
        switch($this->type)
        {
            case self::CREDIT:
                return 'Credit';
                break;
            case self::DEBIT:
                return 'Debit';
                break;
        }
    }
    
    public function getStatusname()
    {
        switch($this->status)
        {
        	case 2:
        		return 'Confirmed';
        		break;
            case 1:
                return 'Approved';
                break;
            case 0:
                return 'Pending';
                break;
        }
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_ewallet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id, value', 'required'),
			array('type, status', 'numerical', 'integerOnly'=>true),
			array('member_id, value, total', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, value, total, type, note, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
			'ewalletConfirmations' => array(self::HAS_MANY, 'EwalletConfirmation', 'ewallet_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'value' => 'Value',
			'type' => 'Type',
			'note' => 'Note',
			'total' => 'Total',
			'status' => 'Status',
			'created' => 'Created',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ewallet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave() {
        parent::beforeSave();
        if($this->scenario=='transfer')
            $this->value= $this->value + $this->generateRandomString();
        return TRUE;
    }
    
    protected function generateRandomString($length = 3) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
