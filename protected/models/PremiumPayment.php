<?php

/**
 * This is the model class for table "tbl_premium_payment".
 *
 * The followings are the available columns in table 'tbl_premium_payment':
 * @property string $id
 * @property string $member_id
 * @property string $key_id
 * @property string $value
 * @property string $bank_receiver
 * @property string $bank_sender
 * @property string $account_name
 * @property integer $status
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property Key $key
 * @property Member $member
 */
class PremiumPayment extends CActiveRecord
{
        const PENDING=0;
        const CONFIRMED=2;
        const PROCESSED=1;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case '1':
                    return 'Processed';
                    break;
                case '0':
                    return 'Pending';
                    break;
            }
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_premium_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bank_receiver, bank_sender, account_name, date', 'required'),
			array('status, type', 'numerical', 'integerOnly'=>true),
			array('member_id, key_id, value', 'length', 'max'=>10),
			array('bank_receiver, bank_sender', 'length', 'max'=>30),
			array('account_name', 'length', 'max'=>100),
                        array('date','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, key_id, value, type, bank_receiver, bank_sender, account_name, date, status, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'key' => array(self::BELONGS_TO, 'Key', 'key_id'),
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'key_id' => 'Key',
			'bank_receiver' => 'Bank Receiver',
			'bank_sender' => 'Bank Sender',
			'account_name' => 'Account Name',
			'status' => 'Status',
			'created_on' => 'Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('key_id',$this->key_id,true);
		$criteria->compare('bank_receiver',$this->bank_receiver,true);
		$criteria->compare('bank_sender',$this->bank_sender,true);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PremiumPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function beforeSave()
        {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                $this->member_id= Yii::app()->member->id;
                
            }
            return TRUE;
        }
}
