<?php

/**
 * This is the model class for table "tbl_network".
 *
 * The followings are the available columns in table 'tbl_network':
 * @property integer $id
 * @property string $member_id
 * @property string $upline_id
 * @property integer $level
 *
 * The followings are the available model relations:
 * @property Member $upline
 * @property Member $member
 */
class Network extends CActiveRecord
{
        public $val;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_network';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('member_id', 'required'),
			array('level, status', 'numerical', 'integerOnly'=>true),
			array('member_id, upline_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, upline_id, level, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'upline' => array(self::BELONGS_TO, 'Member', 'upline_id'),
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'upline_id' => 'Upline',
			'level' => 'Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('upline_id',$this->upline_id,true);
		$criteria->compare('level',$this->level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Network the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
//        public function getTotal()
//        {
//            $var_sum = self::model()->findBySql("select c(`couple`) as `val` from `tbl_bonus_couple` where `member_id`=:id", array(':id'=>Yii::app()->member->id));
//            return $var_sum->val;
//        }
}
