<?php

/**
 * This is the model class for table "tbl_reward".
 *
 * The followings are the available columns in table 'tbl_reward':
 * @property string $id
 * @property string $member_id
 * @property integer $debit
 * @property string $reward
 * @property integer $status
 * @property string $approved_on
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property Member $member
 */
class Reward extends CActiveRecord
{
        public $val;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case '1':
                    return 'Processed';
                    break;
                case '0':
                    return 'Pending';
                    break;
            }
        }
        
        public function getPosted()
        {
            $formatter = Yii::app()->getDateFormatter();
            $format = Yii::app()->getLocale()->getDateFormat('medium',true,true);
                return $formatter->format($format, $this->created_on);
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_reward';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id, debit, reward', 'required'),
			array('debit, status', 'numerical', 'integerOnly'=>true),
			array('member_id', 'length', 'max'=>10),
			array('reward', 'length', 'max'=>100),
			array('approved_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, debit, reward, status, approved_on, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'debit' => 'Debit',
			'reward' => 'Reward',
			'status' => 'Status',
			'approved_on' => 'Approved On',
			'created_on' => 'Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('debit',$this->debit);
		$criteria->compare('reward',$this->reward,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('approved_on',$this->approved_on,true);
		$criteria->compare('created_on',$this->created_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reward the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getTotal()
        {
            $var_sum = self::model()->findBySql("select sum(`debit`) as `val` from `tbl_reward` where `member_id`=:id", array(':id'=>Yii::app()->member->id));
            return $var_sum->val;
        }
        
        public function getReward()
        {
            $var_sum = self::model()->findBySql("select MAX(`debit`) as `val` from `tbl_reward` where `member_id`=:id", array(':id'=>Yii::app()->member->id));
            if($var_sum->val)
                $sum=$var_sum->val;
            else
                $sum=0;
            
            $reward=RewardSetting::model()->find('pv>:pv',[':pv'=>$sum]);
            $member= Yii::app()->member->model;
            if($member->total_left>=$reward->pv&& $member->total_right>=$reward->pv)
                return $reward;
            else
                return false;
           
        }
}
