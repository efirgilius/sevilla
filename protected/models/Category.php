<?php

/**
 * This is the model class for table "tbl_category".
 *
 * The followings are the available columns in table 'tbl_category':
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $type
 * @property integer $status
 * @property string $parent_id
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Category $parent
 * @property Category[] $categories
 * @property Content[] $contents
 * @property PageGroup[] $pageGroups
 */
class Category extends CActiveRecord
{
	const SMALL_THUMB_WIDTH=48;
	const SMALL_THUMB_HEIGHT=48;

	const MEDIUM_THUMB_WIDTH=300;
	const MEDIUM_THUMB_HEIGHT=225;

	const LARGE_THUMB_WIDTH=860;
	const LARGE_THUMB_HEIGHT=570;

	private $_small_url;
	private $_medium_url;
	private $_large_url;

	public function behaviors(){
	    return array(
		    'sluggable' => array(
			    'class'=>'ext.slug.SluggableBehavior',
			    'columns' => array('name'),
			    'unique' => true,
			    'update' => true,
		    ),
	  	);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('slug', 'length', 'max'=>100),
			array('image, meta_keywords', 'length', 'max'=>255),
			array('meta_title', 'length', 'max'=>70),
			array('meta_description', 'length', 'max'=>160),
			array('type', 'length', 'max'=>20),
			array('parent_id', 'length', 'max'=>10),
			array('description', 'safe'),
			['parent_id','default', 'value'=>null],
			['modified','setModified'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, slug, image, description, meta_title, meta_keywords, meta_description, type, status, parent_id, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
			'categories' => array(self::HAS_MANY, 'Category', 'parent_id'),
			'contents' => array(self::HAS_MANY, 'Content', 'category_id'),
			'pageGroups' => array(self::HAS_MANY, 'PageGroup', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'slug' => 'Slug',
			'image' => 'Image',
			'description' => 'Description',
			'meta_title' => 'Meta Title',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
			'type' => 'Type',
			'status' => 'Status',
			'parent_id' => 'Parent',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/

	public function setModified()
    {
        if (!$this->isNewRecord) {
	        $this->modified = date('Y-m-d H:m:s');
        }

    }

	public function scopes()
	{
        return [
            'active'=>[
                'condition'=>'status=1',
            ],
            'null'=>[
                'condition'=>'parent_id is null',
            ],
            'notnull'=>[
                'condition'=>'parent_id is not null',
            ],
        ];
    }

    public function order($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'order'=>$data,
    		]);
    	return $this;
    }

    public function limit($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'limit'=>$data,
    		]);
    	return $this;
    }

    public function type($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'type=:tp',
    		'params'=>[':tp'=>$data]
    		]);
    	return $this;
    }

    public function slug($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'slug=:tp',
    		'params'=>[':slug'=>$data]
    		]);
    	return $this;
    }

    public function getStatusname()
    {
        switch($this->status)
        {
            case '1':
                return 'Active';
                break;
            case '0':
                return 'Not Active';
                break;
        }
    }
    
    public static function statusList()
    {
        return array(
            1=>'Active',
            0=>'Not Active'
        );
    }

    public function afterSave()
	{
        if(!empty($this->image))
        {
        	Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'categories',$this->type.$this->id,$this->image,'small');
        	Yii::app()->hsnImage->generateThumb(self::MEDIUM_THUMB_WIDTH,self::MEDIUM_THUMB_HEIGHT,'categories',$this->type.$this->id,$this->image,'medium');
        	Yii::app()->hsnImage->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,'categories',$this->type.$this->id,$this->image,'large'); 
        }	           

	} 

	protected function beforeDelete() {
        parent::beforeDelete();
        Yii::app()->hsnImage->SureRemoveDir('categories',$this->type.$this->id);
        return TRUE;
    }

    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('categories',$this->type.$this->id).$this->image;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'categories',$this->type.$this->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getMedium_url()
	{
		if($this->_medium_url===NULL)
		{
			$this->_medium_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'categories',$this->type.$this->id,'medium');
		}
		return Yii::app()->baseUrl.'/'.$this->_medium_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'categories',$this->type.$this->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}
}
