<?php

class CardForm extends CFormModel
{
	public $password;
        public $verifyCode;
        
        private $_identity;
        
	public function rules()
	{
		return array(
			// email are required
			array('password, verifyCode', 'required'),
                        array('password', 'length', 'max'=>30),
		);
	}
        
        public function attributeLabels()
	{
		return array(
			'password' => 'Password',
                        'verifyCode' => 'Verification Code'
		);
	}      
        
        public function validatePassword()
        {
            $this->_identity= Yii::app()->member->model;
            $checkPwd=$this->_identity->hashPassword($this->password, $this->_identity->salt);
            if($this->_identity->password!==$checkPwd) {
                    $this->addError('password','Password is invalid.');
            }
            else
            {
                //$this->id=$this->_identity->id;
                return true;
            }
        }
        
        public function validatePasswordAdmin()
        {
            $this->_identity= Yii::app()->user->model;
            $checkPwd=$this->_identity->hashPassword($this->password, $this->_identity->salt);
            if($this->_identity->password!==$checkPwd) {
                    $this->addError('password','Password is invalid.');
            }
            else
            {
                //$this->id=$this->_identity->id;
                return true;
            }
        }
}
