<?php

/**
 * This is the model class for table "tbl_setting".
 *
 * The followings are the available columns in table 'tbl_setting':
 * @property string $id
 * @property integer $name
 * @property string $value
 */
class Setting extends CActiveRecord
{

    public static function getMonths()
    {
        $months = array(
            1=>'January',
            2=>'February',
            3=>'March',
            4=>'April',
            5=>'May',
            6=>'June',
            7=>'July',
            8=>'August',
            9=>'September',
            10=>'October',
            11=>'November',
            12=>'December');
         return $months;
    }
    public static function getYears()
    {
        $year=array();
        $now=date('Y');
        $min=2016;
        $max=$now;
        for($i=$min; $i<=$max;$i++)
        {
            $year[$i]=$i;
        }
         return $year;
    }
    
    public static function getMonth($month)
    {
        switch($month)
        {
            case 1:
                return 'January';
                break;
            case 2:
                return 'February';
                break;
            case 3:
                return 'March';
                break;
            case 4:
                return 'April';
                break;
            case 5:
                return 'May';
                break;
            case 6:
                return 'June';
                break;
            case 7:
                return 'July';
                break;
            case 8:
                return 'August';
                break;
            case 9:
                return 'September';
                break;
            case 10:
                return 'October';
                break;
            case 11:
                return 'November';
                break;
            case 12:
                return 'December';
                break;
        }
    }
}
