<?php

/**
 * This is the model class for table "tbl_menu_item".
 *
 * The followings are the available columns in table 'tbl_menu_item':
 * @property string $id
 * @property string $menu_id
 * @property string $value
 * @property string $type
 * @property string $label
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property Menu $menu
 */
class MenuItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_menu_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_id, value, type, label, order', 'required'),
			array('order', 'numerical', 'integerOnly'=>true),
			array('menu_id, label', 'length', 'max'=>10),
			array('value', 'length', 'max'=>50),
			array('type', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, menu_id, value, type, label, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'value'),
			'content' => array(self::BELONGS_TO, 'Content', 'value'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'menu_id' => 'Menu',
			'value' => 'Value',
			'type' => 'Type',
			'label' => 'Label',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('menu_id',$this->menu_id,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('order',$this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MenuItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/
	
	public function type($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'type=:tp',
    		'params'=>[':tp'=>$data]
    		]);
    	return $this;
    }

    public function typeName()
    {
    	$type = explode('_', $this->type);
    	return $typename = ucfirst(implode(' ', $type));
    }

    public function valueName()
    {
    	switch ($this->type) {
    		case 'page':
    			return Content::model()->findByPk($this->value)->title;
    			break;
    		case 'page_group':
    			return Category::model()->findByPk($this->value)->name;
    			break;
    		case 'blog_category':
    			return Category::model()->findByPk($this->value)->name;
    			break;
    		case 'url':
    			return $this->value;
    	}
    }

    public function typeList()
    {
    	return [
    		'url'=>'Url',
    		'page'=>'Page',
    		'page_group'=>'Page Group',
    		'blog_category'=>'Blog Category',
    	];
    }
}
