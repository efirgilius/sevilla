<?php

/**
 * This is the model class for table "tbl_member".
 *
 * The followings are the available columns in table 'tbl_member':
 * @property string $id
 * @property string $mid
 * @property string $sponsor_id
 * @property string $upline_id
 * @property integer $position
 * @property string $email
 * @property string $password
 * @property integer $salt
 * @property string $name
 * @property string $birth_place
 * @property string $birth_date
 * @property integer $gender
 * @property string $identity_number
 * @property string $address_1
 * @property string $address_2
 * @property string $mobile_phone
 * @property string $phone
 * @property string $work
 * @property string $work_time
 * @property string $work_address
 * @property string $work_phone
 * @property string $npwp
 * @property string $family_name
 * @property string $family_address
 * @property string $family_phone
 * @property string $mother_name
 * @property string $heir_name_1
 * @property string $heir_relationship_as_1
 * @property string $heir_birth_place_1
 * @property string $heir_birth_date_1
 * @property string $bank_name
 * @property string $bank_branch
 * @property string $bank_acc_number
 * @property string $bank_acc_name
 * @property integer $level
 * @property string $foot_left
 * @property string $foot_right
 * @property string $total_left
  * @property string $fee
 * @property string $total_right
 * @property string $omzet_credit
 * @property string $omzet_debit
 * @property string $ewallet_credit
 * @property string $ewallet_debit
 * @property integer $type
 * @property string $card_id
 * @property integer $status
 * @property string $created
 * @property string $modified
 * @property string $last_login
 *
 * The followings are the available model relations:
 * @property Key[] $keys
 * @property Member $sponsor
 * @property Member[] $members
 */
class Member extends CActiveRecord
{
	const INACTIVE=0;
    const ACTIVE=1;
    const PENDING=2;
    const CONFORM=3;
    const UPGRADE=4;
    const BLOCK=5;
    
    const SMALL_THUMB_WIDTH=70;
	const SMALL_THUMB_HEIGHT=70;

	const LARGE_THUMB_WIDTH=136;
	const LARGE_THUMB_HEIGHT=136;
       public $newPassword;
        private $_small_url;
	private $_large_url;
        
    public $randomId;
    public $serial;
    public $pin;
    public $card_id_temp;
    public $upline_temp;
    public $oldpassword;        
    public $newpassword;
    public $repeatpassword;  
    public $verifyCode;
    public $id_sponsor;
	public $term, $repassword, $sponsor_mid;
    
    public static function bankList()
    {
        return array(
            'BCA'=>'BCA',
            'BNI'=>'BNI',
            'BII'=>'BII',
            'BRI'=>'BRI',
            'Bank Mandiri'=>'Bank Mandiri',
            'CIMB Niaga'=>'CIMB Niaga',
            'Lainnya'=>'Lainnya',
        );
    }
    
    public function getBalance()
    {
        return $this->ewallet_credit - $this->ewallet_debit;
    }

    public function getTicketBalance()
    {
        return $this->ticket_credit - $this->ticket_debit;
    }
    

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, birth_place, birth_date, identity_number, address_1, address_2, mobile_phone, work, work_time, work_address, family_name, family_address, family_phone, mother_name, heir_name_1, heir_relationship_as_1, heir_birth_place_1, heir_birth_date_1, bank_name, bank_branch, bank_acc_number, bank_acc_name', 'required','on'=>'update'),
			array('sponsor_mid, email, password, repassword, name, birth_place, birth_date, identity_number, address_1, address_2, mobile_phone, work, work_time, work_address, family_name, family_address, family_phone, mother_name, heir_name_1, heir_relationship_as_1, heir_birth_place_1, heir_birth_date_1, bank_name, bank_branch, bank_acc_number, serial, pin, bank_acc_name', 'required','on'=>'register'),
			array('position, gender, level, type, is_update_bank_acc, status', 'numerical', 'integerOnly'=>true),
			array('mid, sponsor_id, upline_id, foot_left, foot_right, card_id, omzet_credit, omzet_debit, fee, ewallet_credit, ewallet_debit, ticket_credit, ticket_debit', 'length', 'max'=>10),
                        array('sponsor_mid', 'length', 'max'=>12),
                        ['serial, pin', 'validateCard','on'=>'register'],
                        array('upline_id, position', 'validateUpline','on'=>'registerMember'),
			array('email, name, identity_number, work', 'length', 'max'=>50),
			array('password', 'length', 'max'=>255),
			array('birth_place, npwp, heir_relationship_as_1, heir_birth_place_1, bank_name, bank_branch, bank_acc_number, bank_acc_name', 'length', 'max'=>30),
			array('mobile_phone, phone, work_phone, family_phone', 'length', 'max'=>20),
			array('work_time', 'length', 'max'=>4),
			array('family_name, mother_name, heir_name_1', 'length', 'max'=>40),
                        array('avatar', 'length', 'max'=>100),
			array('password', 'checkPasswordStrength'),
            array('email, mobile_phone, bank_acc_number, identity_number','unique','on'=>'insert'),
                        array('email','email'),
            array('repassword','compare','compareAttribute'=>'password','on'=>'register','message'=>'Reepassword tidak sama dengan password!'),
            array('sponsor_mid', 'validateSponsor', 'on'=>'register'),
                        array('term', 'boolean'),
                        array('term','compare','compareValue'=>TRUE, 'message'=>'{attribute} harus dicentang.','on'=>'register'),
                        array('avatar', 'file', 'allowEmpty'=>true, 'types'=>'jpg,gif,png', 'maxSize'=>1024*600,'tooLarge'=>'File is too big, please choose a different one.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mid, sponsor_mid, sponsor_id, email, password, salt, name, birth_place, birth_date, gender, identity_number, address_1, address_2, mobile_phone, phone, work, work_time, work_address, work_phone, npwp, family_name, family_address, family_phone, mother_name, heir_name_1, heir_relationship_as_1, fee, heir_birth_place_1, heir_birth_date_1, bank_name, bank_branch, bank_acc_number, bank_acc_name, level, type, is_update_bank_acc, card_id, status, created, modified, last_login', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'keys' => array(self::HAS_MANY, 'Key', 'member_id'),
			'sponsor' => array(self::BELONGS_TO, 'Member', 'sponsor_id'),
                        'upline' => array(self::BELONGS_TO, 'Member', 'upline_id'),
			'members' => array(self::HAS_MANY, 'Member', 'sponsor_id'),
                        'sponsors' => array(self::HAS_MANY, 'Member', 'sponsor_id'),
                        'bonusSponsors' => array(self::HAS_MANY, 'BonusSponsor', 'member_id'),
                        'bonusPairings' => array(self::HAS_MANY, 'BonusCouple', 'member_id'),
            'card' => array(self::BELONGS_TO, 'Card', 'card_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sponsor_id' => 'Sponsor',
                        'sponsor_mid' => 'ID Sponsor',
			'email' => 'Email',
			'password' => 'Password',
			'salt' => 'Salt',
			'name' => 'Nama Lengkap',
			'birth_place' => 'Tempat Lahir',
			'birth_date' => 'Tanggal Lahir',
			'gender' => 'Jenis Kelamin',
			'identity_number' => 'Nomor KTP/SIM/Pass ',
			'address_1' => 'Alamat Rumah',
			'address_2' => 'Alamat Surat Menyurat',
			'mobile_phone' => 'No. HP',
			'phone' => 'No. Telp',
			'work' => 'Pekerjaan / Perusahaan',
			'work_time' => 'Masa Kerja/Usaha',
			'work_address' => 'Alamat Kerja / Usaha',
			'work_phone' => 'No. Telp. Kantor',
			'npwp' => 'Nomor NPWP',
			'family_name' => 'Nama',
			'family_address' => 'Alamat',
			'family_phone' => 'No. Telp / HP',
			'mother_name' => 'Nama Ibu Kandung',
			'heir_name_1' => 'Ahli waris 1',
			'heir_relationship_as_1' => 'Hubungan Sebagai',
			'heir_birth_place_1' => 'Tempat Lahir',
			'heir_birth_date_1' => 'Tanggal Lahir',
			'heir_name_2' => 'Ahli Waris 2',
			'heir_relationship_as_2' => 'Hubungan Sebagai 2',
			'heir_birth_place_2' => 'Tempat Lahir',
			'heir_birth_date_2' => 'Tanggal Lahir',
			'bank_name' => 'Nama Bank',
			'bank_branch' => 'Cabang/Kota',
			'bank_acc_number' => 'Nomor Rekening',
			'bank_acc_name' => 'Pemilik Rekening',
			'sponsor_mid'=>'ID Sponsor',
			'level' => 'Level',
			'status' => 'Status',
			'created' => 'Created',
			'modified' => 'Modified',
			'last_login' => 'Last Login',
			'repassword'=>'Ulangi Password',
            'fee'=>'Jumlah Pembayaran',
            'serial' => 'Nomor Seri Kartu',
                        'pin' => 'PIN'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sponsor_id',$this->sponsor_id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('birth_place',$this->birth_place,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('identity_number',$this->identity_number,true);
		$criteria->compare('address_1',$this->address_1,true);
		$criteria->compare('address_2',$this->address_2,true);
		$criteria->compare('mobile_phone',$this->mobile_phone,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('work',$this->work,true);
		$criteria->compare('work_time',$this->work_time,true);
		$criteria->compare('work_address',$this->work_address,true);
		$criteria->compare('work_phone',$this->work_phone,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('family_name',$this->family_name,true);
		$criteria->compare('family_address',$this->family_address,true);
		$criteria->compare('family_phone',$this->family_phone,true);
		$criteria->compare('mother_name',$this->mother_name,true);
		$criteria->compare('heir_name_1',$this->heir_name_1,true);
		$criteria->compare('heir_relationship_as_1',$this->heir_relationship_as_1,true);
		$criteria->compare('heir_birth_place_1',$this->heir_birth_place_1,true);
		$criteria->compare('heir_birth_date_1',$this->heir_birth_date_1,true);
		$criteria->compare('heir_name_2',$this->heir_name_2,true);
		$criteria->compare('heir_relationship_as_2',$this->heir_relationship_as_2,true);
		$criteria->compare('heir_birth_place_2',$this->heir_birth_place_2,true);
		$criteria->compare('heir_birth_date_2',$this->heir_birth_date_2,true);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('bank_branch',$this->bank_branch,true);
		$criteria->compare('bank_acc_number',$this->bank_acc_number,true);
		$criteria->compare('bank_acc_name',$this->bank_acc_name,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('last_login',$this->last_login,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/
	
	public static function genderList()
    {
        return array(
            1=>'Laki-laki',
            0=>'Perempuan',
        );
    }

    public function getGendername()
    {
        switch($this->gender)
        {
            case 0:
                return 'Perempuan';
                break;
            case 1:
                return 'Laki-laki';
                break;
        }
    }

    public function getTime($time)
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
            return $formatter->format($format, $time);
    }

    public function getStatusname()
    {
        switch($this->status)
        {
            case '0':
                return 'Inactive';
                break;
            case '1':
                return 'Active';
                break;
            case '2':
                return 'Pending';
                break;
            case '3':
                return 'Confirmed';
                break;
            case '4':
                return 'Upgrade';
                break;
        }
    }

    public function getLevelname()
    {
        switch($this->level)
        {
            case '0':
                return '-';
                break;
            case '1':
                return 'Silver';
                break;
            case '2':
                return 'Gold';
                break;
            case '3':
                return 'Diamond';
                break;
            case '4':
                return 'Crown';
                break;
            case '5':
            	return 'Executif Direktur';
            	break;
        }
    }

    public function getTypename()
        {
            switch($this->type)
            {
                case '1':
                    return 'standard';
                    break;
                case '2':
                    return 'premium';
                    break;
                case '3':
                    return 'deluxe';
                    break;
                case '4':
                    return 'executive';
                    break;
                case '5':
                    return 'diamond';
                    break;
            }
        }

    public function validateSponsor($attribute)
    {
        if($this->sponsor_mid!=='')
        {
            $model=self::model()->findByAttributes(array('mid'=>$this->sponsor_mid,'status'=>1));
            if(empty($model))
            {
                $this->addError ($attribute, 'Sponsor ID is invalid.');
                return false;
            }
            else
            {
                    $this->sponsor_id=$model->id;
                    return true;
            }
        }
        else
            return true;
    }

    public function getTokencode()
    {
        $model= Key::model()->find('member_id=:mid AND is_used=:st AND type=:tp',array(':mid'=>$this->id,':st'=>0,':tp'=>1));
        if($model)
            return $model->token;
        else
            return false;
    }

    protected function beforeSave() 
    {
        parent::beforeSave();
        if($this->isNewRecord)
        {
            //$upline= $this->getUpline();
            $this->mid = $this->getMemberId();
            $this->salt = $this->generateSalt();
            $this->password = $this->hashPassword($this->password, $this->salt);
            //$this->upline_id= $upline['result'];
            //$this->position= $upline['position'];
            $this->card_id= $this->card_id_temp;
            //$this->fee=Yii::app()->controller->g('application','biaya_pendaftaran') + 1000 + $this->generateRandomString(3);
        }
        
        return TRUE;
    }

    protected function afterSave() {
            parent::afterSave();
            if(!empty($this->avatar))
            {
                    //generate small
                    $this->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,Yii::app()->basePath.'/../archives/members/'.$this->mid.'/small_'.$this->avatar);
                    //generate large thumbnail
                    $this->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,Yii::app()->basePath.'/../archives/members/'.$this->mid.'/large_'.$this->avatar);
            }
            
        }
        
     
    public function getMemberId()
    {
        $mid_=  $this->generateRandomString(8);
        $mid = 'SV'.$mid_;
        return $mid;
    }
    
    public function validateUpline($attribute)
        {
            $model=self::model()->findByAttributes(array('id'=>  $this->upline_id));
            
            if(!empty($model))
            {
                if($this->position == '0')
                {
                    if($model->foot_left === NULL)
                    {
                        return true;
                    }
                    else
                    {
                        $this->addError($attribute, 'Posisi kaki kiri tidak tersedia.');
                        return false;
                    }
                }
                elseif($this->position == '1')
                {
                    if($model->foot_right === NULL)
                    {
                        return true;
                    }
                    else
                    {
                        $this->addError($attribute, 'Posisi kaki kanan tidak tersedia.');
                        return false;
                    }
                }
                else
                {
                    $this->addError($attribute, 'Posisi tidak valid.');
                    return false;
                }
            }
            else
            {
                $this->addError ($attribute, 'ID Upline tidak valid.');
                return false;
            }
        }

    public function checkPasswordStrength($attribute, $params)
    {
        $password = $this->$attribute;
        $valid = true;
       // $valid = $valid && preg_match('/[0-9]/', $password); 
        //$valid = $valid && preg_match('#[a-z]+#', $password); 
        $valid = $valid && (strlen($password) > 5); 
        if ($valid) {
            return true;
        } else {
            $this->addError($attribute, "Your password is not secure enough. Your password must have a minimum of 6 characters and include number and character.");
            return false;
        }
    }

    public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->salt)===$this->password;
	}

	public function hashPassword($password,$salt)
	{
		return sha1($salt.$password);
	}

	protected function generateSalt()
	{
		return uniqid('',true);
	}

	protected function generateRandomString($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getStoreMember()
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_register_tes($this->id,$this->sponsor_id)");
            $cmd->execute();
            
//            $cmd2 = $conn->createCommand("call sp_matrix($member,$sponsor)");
//            $cmd2->execute();
        }
        
        public function getStoreNetwork($member,$sponsor)
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_network($member,$sponsor)");
            $cmd->execute();
        }
        
        public function getStoreUpgrade($member,$fee)
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_upgrade($member,$fee)");
            $cmd->execute();
        }
        
        public function getStoreUpgradepoint($member,$point)
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_upgrade_point($member,$point)");
            $cmd->execute();
        }
        
        public function getStoreMulti()
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_clone_complete	($this->id,'$this->memberId')");
            $cmd->execute();
        }
        
        public function getUpline()
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_upline($this->sponsor_id,@mid,@pos)");
            $cmd->execute();
            $valueOut = $conn->createCommand("select @mid as result, @pos as position;")->queryRow();
            return $valueOut;
        }
        
        public function loadUpline()
        {
            $conn = Yii::app()->db;
            $cmd = $conn->createCommand("call sp_sponsor(@mmid)");
            $cmd->execute();
            $valueOut = $conn->createCommand("select @mmid as mid;")->queryRow();
            return $valueOut['mid'];
        }
        
        public function getImgurl()
        {
            return Yii::app()->baseUrl.'/archives/members/'.$this->mid.'/';
        }
        
        public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=self::generateThumbUrl($this->avatar,'archives/members/'.$this->mid.'/'.$this->avatar,'small');
		}
		return $this->_small_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=self::generateThumbUrl($this->avatar,'archives/members/'.$this->mid.'/'.$this->avatar,'large');
		}
		return $this->_large_url;
	}
        
        public function getMain_url()
        {
            if($this->avatar!==NULL)
                return $this->small_url;
            else
                return 'archives/members/avatar.png';
        }
        
        public function getAvatar_url()
        {
            if($this->gender=='0')
                return 'archives/members/female_avatar.png';
            else
                return 'archives/members/avatar.png';
        }
        
        protected function generateThumb($width,$height,$path)
	{
		$thumb=Yii::app()->iwi->load(Yii::app()->basePath.'/../archives/members/'.$this->mid.'/'.$this->avatar)
                        ->adaptive($width,$height)
                        ->save($path);
	}
        
        public static function generateThumbUrl($filename,$url,$sizeType)
	{
		return str_replace($filename, '', $url).$sizeType.'_'.$filename;
	}

    public function validateCard($attribute)
    {
        $model=Card::model()->findByAttributes(array('serial'=>$this->serial,'status'=>0,'is_active'=>1));
        if(!empty($model))
        {
            $model=Card::model()->findByAttributes(array('serial'=>  $this->serial,'pin'=>  $this->pin,'status'=>0,'is_active'=>1));
            if(!empty($model))
            {
                $this->card_id_temp=$model->id;
                $this->type= $model->type;
                return true;
            }
            else
            {
                $this->addError($attribute, "Serial nomor kartu dan PIN Anda salah.");
                return false;
            }
        }
        else
        {
            $this->addError($attribute, "Serial nomor kartu Anda salah.");
            return false;
        }
    }
    
    public function sendSms($message)
        {
            $url = "https://reguler.zenziva.net/apps/smsapi.php?";
            $curlHandle = curl_init();

            curl_setopt($curlHandle, CURLOPT_URL, $url);

            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=ajywpm&passkey=%m00rp&nohp='.$this->mobile_phone.'&tipe=reguler&pesan='.urlencode($message));

            curl_setopt($curlHandle, CURLOPT_HEADER, 0);

            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);

            curl_setopt($curlHandle, CURLOPT_POST, 1);

            $results = curl_exec($curlHandle);

            curl_close($curlHandle);

            return $results;

        }
    
}
