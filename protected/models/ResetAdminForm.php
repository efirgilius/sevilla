<?php

class ResetAdminForm extends CFormModel
{
        public $id;
        public $oldpassword;
        public $newpassword;
	public $verifypassword;
        
	public function rules()
	{
		return array(
			array('oldpassword, newpassword, verifypassword', 'required'),
                        array('verifypassword','compare','compareAttribute'=>'newpassword','message'=>'Verify Password is not match with New Password.'),
		);
	}
        
        public function attributeLabels()
	{
		return array(
			'oldpassword' => 'Old Password',
                        'newpassword' => 'New Password',
                        'verifypassword' => 'Verify Password'
		);
	}        
}
