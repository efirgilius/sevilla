<?php

/**
 * This is the model class for table "tbl_withdrawal".
 *
 * The followings are the available columns in table 'tbl_withdrawal':
 * @property string $id
 * @property string $member_id
 * @property string $value
 * @property string $fee
 * @property string $date
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Member $member
 */
class Withdrawal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_withdrawal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value', 'required'),
                        array('value','validateValue','on'=>'insert'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('member_id, fee', 'length', 'max'=>10),
			array('value', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, value, fee, date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'value' => 'Value',
			'fee' => 'Fee',
			'date' => 'Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('fee',$this->fee,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Withdrawal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function validateValue($attribute, $params)
        {
            if($this->value<50000)
            {
                $this->addError($attribute, "Jumlah penarikan minimal Rp.50.000");
                return false;
            }
            elseif($this->value>Yii::app()->member->model->balance)
            {
                $this->addError($attribute, "Jumlah saldo ewallet Anda tidak mencukupi untuk melakukan penarikan");
                return false;
            }
            else
                return true;
        }
}