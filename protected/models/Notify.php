<?php

	class Notify {

		public static function t($string, $params = array())
		{
			//Yii::import('application.modules.shop.SettingModule');

			return Yii::t('SettingModule.shop', $string, $params);
		}
		/* set a flash message to display after the request is done */
		public static function setFlash($message) 
		{
			Yii::app()->member->setFlash('yiishop',  Notify::t($message));
		}

		public static function hasFlash() 
		{
			return Yii::app()->member->hasFlash($type);
		}

		/* retrieve the flash message again */
		public static function getFlash($type) {
			if(Yii::app()->user->hasFlash($type)) {
				return Yii::app()->user->getFlash($type);
			}
		}
                
        public static function getMflash($type) {
			if(Yii::app()->member->hasFlash($type)) {
				return Yii::app()->member->getFlash($type);
			}
		}

		public static function renderFlash()
		{
			if(Yii::app()->user->hasFlash('success')) {
				echo '<div class="uk-notify uk-notify-top-center" style="display: block;"><div class="uk-notify-message uk-notify-message-success loop" style="opacity: 1; margin-top: 0px; margin-bottom: 10px;"><a class="uk-close"></a>';
                                echo '<div>';
				echo Notify::getFlash('success');
				echo '</div></div></div>';
				Yii::app()->clientScript->registerScript('fade',"
						setTimeout(function() { $('.loop').fadeOut('slow'); }, 8000);	
						"); 
			}
                        
                        if(Yii::app()->user->hasFlash('error')) {
				echo '<div class="uk-notify uk-notify-top-center" style="display: block;"><div class="uk-notify-message uk-notify-message-danger loop" style="opacity: 1; margin-top: 0px; margin-bottom: 10px;"><a class="uk-close"></a>';
                                echo '<div>';
				echo Notify::getFlash('error');
				echo '</div></div></div>';
				Yii::app()->clientScript->registerScript('fade',"
						setTimeout(function() { $('.loop').fadeOut('slow'); }, 8000);	
						"); 
			}
		}
                
        public static function renderMflash()
		{
            // if(Yii::app()->member->hasFlash('success')) {
            //     Yii::app()->clientScript->registerScript('success', 
            //         "$.notify('".Notify::getMflash('success')."','success');",
            //     CClientScript::POS_READY);
            // }

            // if(Yii::app()->member->hasFlash('error')) {
            //     Yii::app()->clientScript->registerScript('error', 
            //         "$.notify('".Notify::getMflash('error')."','error');",
            //     CClientScript::POS_READY);
            // }
            
			if(Yii::app()->member->hasFlash('success')) {
				echo '<div class="success-box"><div class="alert alert-success loop">';
				echo Notify::getMflash('success');
				echo '</div></div>';
				Yii::app()->clientScript->registerScript('fade',"
						setTimeout(function() { $('.loop').fadeOut('slow'); }, 8000);	
						"); 
			}
                        
            if(Yii::app()->member->hasFlash('error')) {
				echo '<div class="error-box"><div class="alert alert-warning loop">';
				echo Notify::getMflash('error');
				echo '</div></div>';
				Yii::app()->clientScript->registerScript('fade',"
						setTimeout(function() { $('.loop').fadeOut('slow'); }, 8000);	
						"); 
			}
		}
                
                public static function getMonths()
                {
                    $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December');
                     return $months;
                }
                public static function getYears()
                {
                    $year=array();
                    $now=date('Y');
                    $min=2015;
                    $max=$now;
                    for($i=$min; $i<=$max;$i++)
                    {
                        $year[$i]=$i;
                    }
                     return $year;
                }
                
                public static function getMonth($month)
                {
                    switch($month)
                    {
                        case 1:
                            return 'January';
                            break;
                        case 2:
                            return 'February';
                            break;
                        case 3:
                            return 'March';
                            break;
                        case 4:
                            return 'April';
                            break;
                        case 5:
                            return 'May';
                            break;
                        case 6:
                            return 'June';
                            break;
                        case 7:
                            return 'July';
                            break;
                        case 8:
                            return 'August';
                            break;
                        case 9:
                            return 'September';
                            break;
                        case 10:
                            return 'October';
                            break;
                        case 11:
                            return 'November';
                            break;
                        case 12:
                            return 'December';
                            break;
                    }
                }
	}
