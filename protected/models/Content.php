<?php

/**
 * This is the model class for table "tbl_content".
 *
 * The followings are the available columns in table 'tbl_content':
 * @property string $id
 * @property string $category_id
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $content
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $tags
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $type
 * @property integer $comment_status
 * @property integer $viewed
 * @property integer $status
 * @property string $user_id
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property User $user
 * @property Gallery[] $galleries
 * @property PageGroup[] $pageGroups
 */
class Content extends CActiveRecord
{	
	private $_oldTags;

	const SMALL_THUMB_WIDTH=48;
	const SMALL_THUMB_HEIGHT=48;

	const MEDIUM_THUMB_WIDTH=300;
	const MEDIUM_THUMB_HEIGHT=225;

	const LARGE_THUMB_WIDTH=860;
	const LARGE_THUMB_HEIGHT=570;

	const NEWS_SMALL_THUMB_WIDTH=100;
	const NEWS_SMALL_THUMB_HEIGHT=80;

	const NEWS_MEDIUM_THUMB_WIDTH=370;
	const NEWS_MEDIUM_THUMB_HEIGHT=300;

	const NEWS_LARGE_THUMB_WIDTH=799;
	const NEWS_LARGE_THUMB_HEIGHT=400;

	private $_small_url;
	private $_medium_url;
	private $_large_url;

	public function behaviors(){
	    return array(
		    'sluggable' => array(
			    'class'=>'ext.slug.SluggableBehavior',
			    'columns' => array('title'),
			    'unique' => true,
			    'update' => true,
		    ),
	  	);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content', 'required'),
			array('comment_status, viewed, status, is_menu', 'numerical', 'integerOnly'=>true),
			array('category_id, user_id', 'length', 'max'=>10),
			array('title', 'length', 'max'=>100),
			array('slug, image', 'length', 'max'=>150),
			array('meta_title', 'length', 'max'=>70),
			array('meta_keywords', 'length', 'max'=>255),
			array('meta_description', 'length', 'max'=>160),
			array('type', 'length', 'max'=>20),
			array('start_date, end_date, start_time, end_time, tags, modified', 'safe'),
			array('tags', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Tags can only contain word characters.'),
			array('tags', 'normalizeTags'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, title, slug, image, content, start_date, end_date, start_time, end_time, tags, meta_title, meta_keywords, meta_description, type, comment_status, viewed, is_menu, status, user_id, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'galleries' => array(self::HAS_MANY, 'Gallery', 'content_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'title' => 'Title',
			'slug' => 'Slug',
			'image' => 'Image',
			'content' => 'Content',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'tags' => 'Tags',
			'meta_title' => 'Meta Title',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
			'type' => 'Type',
			'comment_status' => 'Comment Status',
			'viewed' => 'Viewed',
			'status' => 'Status',
			'is_menu'=>'Show on menu',
			'user_id' => 'User',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('comment_status',$this->comment_status);
		$criteria->compare('viewed',$this->viewed);
		$criteria->compare('status',$this->status);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/

	public function searchThis($q){
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'title LIKE :q or content LIKE :q or tags LIKE :q',
    		'params'=>array(
				':q'=>'%'.strtr($q,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
			),
    		]);
    	return $this;
    }

    public function scopes()
	{
        return [
            'publish'=>[
                'condition'=>'t.status=1',
            ],
            'isMenu'=>[
                'condition'=>'t.is_menu=1',
            ],
        ];
    }

    public function order($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'order'=>$data,
    		]);
    	return $this;
    }

    public function limit($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'limit'=>$data,
    		]);
    	return $this;
    }

    public function tag($q){
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'tags LIKE :q',
    		'params'=>array(
				':q'=>'%'.strtr($q,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
			),
    		]);
    	return $this;
    }

    public function type($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'type=:tp',
    		'params'=>[':tp'=>$data]
    		]);
    	return $this;
    }

    public function slug($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'slug=:tp',
    		'params'=>[':slug'=>$data]
    		]);
    	return $this;
    }

    public function getStatusname()
    {
        switch($this->status)
        {
            case '1':
                return 'Publish';
                break;
            case '0':
                return 'Draft';
                break;
        }
    }
    
    public static function statusList()
    {
        return array(
            1=>'Publish',
            0=>'Draft'
        );
    }

    public function getTime($date)
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
            return $formatter->format($format, $date);
    }

    public function afterSave()
	{
		parent::afterSave();
		Tag::model()->updateFrequency($this->_oldTags, $this->tags, $this->type);
        
        if(!empty($this->image))
        {
        	switch ($this->type) {
        		case 'news':
		        	Yii::app()->hsnImage->generateThumb(self::NEWS_SMALL_THUMB_WIDTH,self::NEWS_SMALL_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'small');
		        	Yii::app()->hsnImage->generateThumb(self::NEWS_MEDIUM_THUMB_WIDTH,self::NEWS_MEDIUM_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'medium');
		        	Yii::app()->hsnImage->generateThumb(self::NEWS_LARGE_THUMB_WIDTH,self::NEWS_LARGE_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'large'); 
        			break;
        		
        		default:
        			Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'small');
		        	Yii::app()->hsnImage->generateThumb(self::MEDIUM_THUMB_WIDTH,self::MEDIUM_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'medium');
		        	Yii::app()->hsnImage->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,'contents/'.$this->type,$this->id,$this->image,'large'); 
        			break;
        	}
        }	           

	} 

	protected function beforeDelete() {
        parent::beforeDelete();
        Yii::app()->hsnImage->SureRemoveDir('contents/'.$this->type,$this->id);
        return TRUE;
    }

    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('contents/'.$this->type,$this->id).$this->image;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'contents/'.$this->type,$this->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getMedium_url()
	{
		if($this->_medium_url===NULL)
		{
			$this->_medium_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'contents/'.$this->type,$this->id,'medium');
		}
		return Yii::app()->baseUrl.'/'.$this->_medium_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'contents/'.$this->type,$this->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}
        
        public function getCreateTime()
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
            return $formatter->format($format, $this->created);
    }

    protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=date('Y-m-d H:m:s');
				$this->user_id=Yii::app()->user->id;
			}
			else
				$this->modified=date('Y-m-d H:m:s');
			return true;
		}
		else
			return false;
	}

    public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}

	protected function afterDelete()
	{
		parent::afterDelete();
		
		Tag::model()->updateFrequency($this->tags, '',$this->type);
	}

	public function getTagThis()
	{
		$tags = Tag::string2array($this->tags);
		return $tags;
	}
}
