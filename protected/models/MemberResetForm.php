<?php

class MemberResetForm extends CFormModel
{
        public $id;
        public $oldpassword;
        public $password;
	public $verifypassword;
        
        private $_identity;
        
	public function rules()
	{
		return array(
			array('oldpassword, password, verifypassword', 'required'),
                        array('password', 'checkPasswordStrength'),
                        array('verifypassword','compare','compareAttribute'=>'password','message'=>'The confirmation password does not match the password.'),
		);
	}
        
        public function attributeLabels()
	{
		return array(
                        'oldpassword' => 'Old Password',
			'password' => 'New Password',
                        'verifypassword' => 'Confirmation Password'
		);
	} 
        
        public function resetMember()
        {
            $this->_identity= Yii::app()->member->model;
            $checkPwd=$this->_identity->hashPassword($this->oldpassword, $this->_identity->salt);
            if($this->_identity->password!==$checkPwd) {
                    $this->addError('oldpassword','Your old password is invalid.');
            }
            else
            {
                $this->id=$this->_identity->id;
                return true;
            }
        }
        
        public function checkPasswordStrength($attribute, $params)
        {
            $password = $this->$attribute;
            $valid = true;
            $valid = $valid && preg_match('/[0-9]/', $password); 
            $valid = $valid && preg_match('#[a-z]+#', $password); 
            $valid = $valid && (strlen($password) > 5); 
            if ($valid) {
                return true;
            } else {
                $this->addError($attribute, "Your password is not secure enough. Your password must have a minimum of 6 characters and include number and character.");
                return false;
            }
        }
}
