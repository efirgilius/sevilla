<?php

/**
 * This is the model class for table "h_user".
 *
 * The followings are the available columns in table 'h_user':
 * @property string $id
 * @property string $email
 * @property string $name
 * @property string $password
 * @property string $salt
 * @property string $avatar
 * @property integer $level
 * @property integer $status
 * @property string $last_login
 */
class User extends CActiveRecord
{
	const MANAGER=1;
    const ADMINISTRATOR=2;
    const OPERATOR=3;
    
    const SMALL_THUMB_WIDTH=34;
	const SMALL_THUMB_HEIGHT=33;

	const LARGE_THUMB_WIDTH=82;
	const LARGE_THUMB_HEIGHT=82;
    
    private $_small_url;
    private $_large_url;
    
    public $newPassword;
    /*
     * Return the level of user
     */
    public function getLevelName()
    {
        switch($this->level)
        {
            case self::MANAGER:
                return 'Manager';
                break;
            case self::ADMINISTRATOR:
                return 'Administrator';
                break;
            case self::OPERATOR:
                return 'Operator';
                break;
        }
    }
        
    public function getStatusname()
    {
        switch($this->status)
        {
            case '1':
                return 'Active';
                break;
            case '0':
                return 'InActive';
                break;
        }
    }
    
    public static function statusList()
    {
        return array(
            1=>'Active',
            0=>'InActive'
        );
    }
    
    public static function levelList()
    {
        return array(
            2=>'Administrator',
            3=>'Operator'
        );
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, name, password, level, status', 'required'),
			array('level, status', 'numerical', 'integerOnly'=>true),
			array('email, name', 'length', 'max'=>50),
			['email','email'],
			['email','unique'],
			array('password, avatar', 'length', 'max'=>225),
			array('salt', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, name, password, salt, avatar, level, status, last_login', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contents' => array(self::HAS_MANY, 'Content', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'name' => 'Name',
			'password' => 'Password',
			'salt' => 'Salt',
			'avatar' => 'Avatar',
			'level' => 'Level',
			'status' => 'Status',
			'last_login' => 'Last Login',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('status',$this->status);
		$criteria->compare('last_login',$this->last_login,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave() {
        parent::beforeSave();
        if($this->isNewRecord)
        {
            $this->salt = $this->generateSalt();
            $this->password = $this->hashPassword($this->password, $this->salt);
        }
        return TRUE;
    }

    public function afterSave()
	{
        if(!empty($this->avatar))
        {
        	Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'users',$this->id,$this->avatar,'small'); 
        	Yii::app()->hsnImage->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,'users',$this->id,$this->avatar,'large'); 
        }	            
	}
    
    protected function beforeDelete() {
        parent::beforeDelete();
        Yii::app()->hsnImage->SureRemoveDir('users',$this->id);
        return TRUE;
    }

        /**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->salt)===$this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password,$salt)
	{
		return sha1($salt.$password);
	}
        
        /**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
	protected function generateSalt()
	{
		return uniqid('',true);
	}
        
    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('users',$this->id).$this->avatar;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->avatar,'users',$this->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->avatar,'users',$this->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}
}
