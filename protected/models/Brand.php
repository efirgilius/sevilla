<?php

/**
 * This is the model class for table "tbl_brand".
 *
 * The followings are the available columns in table 'tbl_brand':
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property integer $is_recommend
 * @property integer $is_active
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Brand extends CActiveRecord
{
	const SMALL_THUMB_WIDTH=166;
	const SMALL_THUMB_HEIGHT=80;

	private $_small_url;

	public function behaviors(){
	    return array(
		    'sluggable' => array(
			    'class'=>'ext.slug.SluggableBehavior',
			    'columns' => array('name'),
			    'unique' => true,
			    'update' => true,
		    ),
	  	);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_brand';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('is_recommend, is_active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('slug, image', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, slug, image, is_recommend, is_active, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'brand_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'slug' => 'Slug',
			'image' => 'Image',
			'is_recommend' => 'Is Recommend',
			'is_active' => 'Is Active',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('is_recommend',$this->is_recommend);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Brand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function scopes(){
        return [
            'active'=>[
                'condition'=>'is_active=1',
            ],
            'recommend'=>[
                'condition'=>'is_recommend=1',
            ],
        ];
    }

    public function order($data){
    	$this->getDbCriteria()->mergeWith([
    		'order'=>$data,
    		]);
    	return $this;
    }

    public function limit($data){
    	$this->getDbCriteria()->mergeWith([
    		'limit'=>$data,
    		]);
    	return $this;
    }

    public function type($data){
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'type=:tp',
    		'params'=>[':tp'=>$data]
    		]);
    	return $this;
    }

    public function getStatusname()
    {
        switch($this->is_active)
        {
            case '1':
                return 'Active';
                break;
            case '0':
                return 'Not Active';
                break;
        }
    }
    
    public static function statusList()
    {
        return array(
            1=>'Active',
            0=>'Not Active'
        );
    }

    public function afterSave()
	{
        if(!empty($this->image))
        {
        	Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'brands',$this->id,$this->image,'small'); 
        }	           
	} 

	protected function beforeDelete() {
        parent::beforeDelete();
        Yii::app()->hsnImage->SureRemoveDir('brands',$this->id);
        return TRUE;
    }

    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('brands',$this->id).$this->image;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'brands',$this->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'brands',$this->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}
}
