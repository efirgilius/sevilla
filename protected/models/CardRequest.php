<?php

/**
 * This is the model class for table "tbl_card_request".
 *
 * The followings are the available columns in table 'tbl_card_request':
 * @property string $id
 * @property string $seller_id
 * @property string $buyer_id
 * @property integer $type
 * @property integer $value
 * @property integer $total_seller
 * @property integer $total_buyer
 * @property string $date
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Member $to
 * @property Member $member
 */
class CardRequest extends CActiveRecord
{
        const PENDING=0;
        const VERIFIED=1;
        const REJECTED=2;
        
        const A=1;
        const B=2;
        
        public $member_seller_id;
        public $member_buyer_id;
        public $seller_id_temp;
        public $buyer_id_temp;
        public $last_total;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case 1:
                    return 'Success';
                    break;
                case 0:
                    return 'Pending';
                    break;
                case 2:
                    return 'Rejected';
                    break;
            }
        }
        
        public function getTypename()
        {
            switch($this->type)
            {
                case self::A:
                    return 'E-voucher 200';
                    break;
                case self::B:
                    return 'E-voucher 25';
                    break;
            }
        }
        
        public static function typeList()
        {
            return array(
                1=>'E-voucher 200',
                2=>'E-voucher 25'
            );
        }
        
        public function getPosted()
        {
            $formatter = Yii::app()->getDateFormatter();
            $format = Yii::app()->getLocale()->getDateFormat('medium',true,true);
                return $formatter->format($format, $this->date);
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_card_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buyer_id, type, value', 'required','on'=>'send'),
                        array('type, value','required','on'=>'buy'),
                        array('member_seller_id, type, value', 'required','on'=>'request'),
                        array('member_buyer_id, type, value', 'required','on'=>'sendMember'),
                        array('value','validateCardNumber','on'=>'send, buy'),
                        array('member_buyer_id','validateMemberId','on'=>'sendMember'),
                        array('member_seller_id','validateMember','on'=>'request'),
                        array('member_seller_id','validateCard','on'=>'request'),
			array('type, value, total_seller, total_buyer, status', 'numerical', 'integerOnly'=>true),
			array('seller_id, buyer_id', 'length', 'max'=>10),
                        array('seller_id', 'default', 'setOnEmpty' => true, 'value' => null),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, seller_id, buyer_id, type, value, total_seller, total_buyer, date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyer' => array(self::BELONGS_TO, 'Member', 'buyer_id'),
			'seller' => array(self::BELONGS_TO, 'Member', 'seller_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'seller_id' => 'Member',
			'buyer_id' => 'Buyer',
			'type' => 'Type',
			'value' => 'Value',
			'date' => 'Date',
			'status' => 'Status',
                        'member_buyer_id' => 'Merchant ID',
                        'member_seller_id' => 'Seller ID'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('seller_id',$this->seller_id,true);
		$criteria->compare('buyer_id',$this->buyer_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('value',$this->value);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CardRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function validateMember($attribute)
        {
            $member = Member::model()->find('mid=:m AND id!=:id',array(':m'=>  $this->member_seller_id,':id'=>Yii::app()->member->id));
            if($member===null)
                {
                    $this->addError($attribute, "Member/Merchant ID can not be found.");
                    return false;
                }
                else
                {
                    $this->seller_id_temp=$member->id;
                    return true;
                }
        }
        
        public function validateMemberId($attribute)
        {
            $member = Member::model()->find('mid=:m',array(':m'=>  $this->member_buyer_id));
            if($member===null)
                {
                    $this->addError($attribute, "Member/Merchant ID can not be found.");
                    return false;
                }
                else
                {
                    $this->buyer_id_temp=$member->id;
                    return true;
                }
        }
        
        protected function beforeSave() {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                if(Yii::app()->member->id && $this->scenario=='request')
                    $this->buyer_id = Yii::app()->member->id;  
                elseif(Yii::app()->member->id && $this->scenario=='sendMember')
                {
                    $this->buyer_id = $this->buyer_id_temp;
                    $this->seller_id = Yii::app()->member->id;  
                }
                
                if($this->seller_id_temp)
                    $this->seller_id = $this->seller_id_temp;
                
                if($this->scenario=='send')
                {
                    $last=  self::model()->find('t.buyer_id=:id AND t.status=:st',[':id'=>$this->buyer_id,':st'=>1]);
                    if($last)
                    {
                        $this->total_buyer=$last->total_buyer + $this->value;
                        //$this->total_seller=$last->total_seller - $this->value;
                    }
                }
                else
                {
                    $last=  self::model()->find('(t.seller_id=:id OR t.buyer_id=:id) AND t.status=:st',[':id'=>Yii::app()->member->id,':st'=>1]);
                    if($last)
                    {
                        if($this->buyer_id == Yii::app()->member->id)
                            $this->total=$last->total + $this->value;
                        elseif($this->seller_id == Yii::app()->member->id)
                            $this->total=$last->total - $this->value;
                    }
                }
            }
            
            return TRUE;
        }
        
        public function validateCard($attribute)
        {
            $count = Card::model()->count('member_id=:m AND type=:tp AND status=:st',array(':m'=>  $this->seller_id_temp,':tp'=>  $this->type,':st'=>0));
            if($count<=0)
                {
                    $this->addError($attribute, "PIN is unavailable in this Member/Merchant.");
                    return false;
                }
                else
                {
                    if($count>=$this->value)
                    {
                        return true;
                    }
                    else
                    {
                        $this->addError($attribute, "PIN is unavailable in this Member/Merchant.");
                        return false;
                    }
                }
        }
        
        public function validateCardNumber($attribute)
        {
            $count = Card::model()->count('member_id IS NULL AND type=:tp AND status=:st',array(':tp'=>  $this->type,':st'=>0));
            if($count===null)
                {
                    $this->addError($attribute, "PIN is unavailable.");
                    return false;
                }
                else
                {
                    if($count>=$this->value)
                        return true;
                    else
                        $this->addError($attribute, "PIN stock is less than your request.");
                }
        }
        
        
}
