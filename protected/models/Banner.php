<?php

/**
 * This is the model class for table "tbl_banner".
 *
 * The followings are the available columns in table 'tbl_banner':
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $url
 * @property string $type
 * @property integer $order
 * @property integer $is_active
 * @property string $created
 */
class Banner extends CActiveRecord
{
	const SMALL_THUMB_WIDTH=48;
	const SMALL_THUMB_HEIGHT=48;

	const MEDIUM_THUMB_WIDTH=300;
	const MEDIUM_THUMB_HEIGHT=225;

	const LARGE_THUMB_WIDTH=1920;
	const LARGE_THUMB_HEIGHT=700;

	private $_small_url;
	private $_medium_url;
	private $_large_url;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order, is_active', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>50),
			array('image, url', 'length', 'max'=>100),
			array('type', 'length', 'max'=>20),
			array('description', 'safe'),
		
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, image, url, type, order, is_active, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'image' => 'Image',
			'url' => 'Url',
			'type' => 'Type',
			'order' => 'Order',
			'is_active' => 'Is Active',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/

	public function scopes(){
        return [
            'active'=>[
                'condition'=>'is_active=1',
            ],
        ];
    }

    public function order($data){
    	$this->getDbCriteria()->mergeWith([
    		'order'=>$data,
    		]);
    	return $this;
    }

    public function limit($data){
    	$this->getDbCriteria()->mergeWith([
    		'limit'=>$data,
    		]);
    	return $this;
    }

    public function type($data){
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'type=:tp',
    		'params'=>[':tp'=>$data]
    		]);
    	return $this;
    }

    public function getStatusname()
    {
        switch($this->is_active)
        {
            case '1':
                return 'Active';
                break;
            case '0':
                return 'Not Active';
                break;
        }
    }
    
    public static function statusList()
    {
        return array(
            1=>'Active',
            0=>'Not Active'
        );
    }

    public function afterSave()
	{
        if(!empty($this->image))
        {
        	Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'banners','b'.$this->id,$this->image,'small');
        	Yii::app()->hsnImage->generateThumb(self::MEDIUM_THUMB_WIDTH,self::MEDIUM_THUMB_HEIGHT,'banners','b'.$this->id,$this->image,'medium');
        	Yii::app()->hsnImage->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,'banners','b'.$this->id,$this->image,'large'); 
        }	           
	} 

	protected function beforeDelete() {
        parent::beforeDelete();
        Yii::app()->hsnImage->SureRemoveDir('banners','b'.$this->id);
        return TRUE;
    }

    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('banners','b'.$this->id).$this->image;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'banners','b'.$this->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getMedium_url()
	{
		if($this->_medium_url===NULL)
		{
			$this->_medium_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'banners','b'.$this->id,'medium');
		}
		return Yii::app()->baseUrl.'/'.$this->_medium_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'banners','b'.$this->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}
}
