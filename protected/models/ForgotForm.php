<?php

class ForgotForm extends CFormModel
{
    public $id;
    public $name;
    public $member_mid;
    public $email;
    private $_mid;
	private $_identity;
        
	public function rules()
	{
		return array(
			// email are required
			array('member_mid, email', 'required'),
            ['email','email'],
            array('member_mid', 'length', 'max'=>10),
                        array('email', 'length', 'max'=>50),
		);
	}
        
        public function attributeLabels()
	{
		return array(
			'member_mid' => 'ID Member',
                        'email' => 'Email',
		);
	}

	public function forgot()
	{
        $this->_identity= Member::model()->findByAttributes(array('mid'=>  $this->member_mid,'email'=>$this->email,'status'=>1));
        if(!$this->_identity) {
            $this->addError('mid','ID Member atau Email Anda salah.');
        }
        else
        {
            $this->id=$this->_identity->id;
            $this->email=  $this->_identity->email;
            $this->name=$this->_identity->name;
            return true;
        }
	}
        
}
