<?php

/**
 * This is the model class for table "tbl_bank".
 *
 * The followings are the available columns in table 'tbl_bank':
 * @property string $id
 * @property string $name
 * @property string $acc_number
 * @property string $acc_name
 * @property string $image
 * @property integer $status
 */
class Bank extends CActiveRecord
{
        const SMALL_THUMB_WIDTH=120;
	const SMALL_THUMB_HEIGHT=36;

	const LARGE_THUMB_WIDTH=280;
	const LARGE_THUMB_HEIGHT=120;
        
        private $_small_url;
	private $_large_url;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case '1':
                    return 'Online';
                    break;
                case '0':
                    return 'Offline';
                    break;
            }
        }
        
        public function getFullname()
        {
           return $this->name.' '.$this->acc_number; 
        }
        
        public static function statusList()
        {
            return array(
                1=>'Online',
                0=>'Offline'
            );
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, acc_number, acc_name', 'required'),
			array('name, acc_number', 'length', 'max'=>20),
			array('acc_name', 'length', 'max'=>30),
                        array('image', 'length', 'max'=>100),
                        array('status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, acc_number, acc_name, image, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'acc_number' => 'Acc Number',
			'acc_name' => 'Acc Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('acc_number',$this->acc_number,true);
		$criteria->compare('acc_name',$this->acc_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function afterSave()
	{
            if(!empty($this->image))
            {
                
                    //generate small
                    $this->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,Yii::app()->basePath.'/../archives/tmp/b'.$this->id.'/small_'.$this->image);
                    //generate large thumbnail
                    $this->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,Yii::app()->basePath.'/../archives/tmp/b'.$this->id.'/large_'.$this->image);
                
            }	            
	}
        
        protected function beforeDelete() {
            parent::beforeDelete();
            $this->SureRemoveDir($this->getDirUrl());
            return TRUE;
        }
        
        public function getImgurl()
        {
            return Yii::app()->baseUrl.'/archives/tmp/b'.$this->id.'/';
        }
        
        public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=self::generateThumbUrl($this->image,'archives/tmp/b'.$this->id.'/'.$this->image,'small');
		}
		return $this->_small_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=self::generateThumbUrl($this->image,'archives/tmp/b'.$this->id.'/'.$this->image,'large');
		}
		return $this->_large_url;
	}
        
        protected function generateThumb($width,$height,$path)
	{
		$thumb=Yii::app()->iwi->load(Yii::app()->basePath.'/../archives/tmp/b'.$this->id.'/'.$this->image)
                        ->adaptive($width,$height)
                        ->save($path);
	}
        
        public static function generateThumbUrl($filename,$url,$sizeType)
	{
		return str_replace($filename, '', $url).$sizeType.'_'.$filename;
	}
        
        public function onBeforeDelete($event)
	{
		$filepath=Yii::app()->basePath.'/../archives/tmp/b'.$this->id.'/'.$this->image;
		if(is_file($filepath))
			unlink($filepath);
		parent::onBeforeDelete($event);
	}
        
        protected function SureRemoveDir($dir) {
            if(!$dh = @opendir($dir)) return;
            while (false !== ($obj = readdir($dh))) {
                if($obj=='.' || $obj=='..') continue;
                if (!@unlink($dir.'/'.$obj)) SureRemoveDir($dir.'/'.$obj, true);
            }
            //closedir($dh);            
            @rmdir($dir);           
        }
        
        public function getDirUrl()
        {
            $link_dir = str_replace('protected','',Yii::app()->basePath);
              return $link_dir.'archives/tmp/b'.$this->id;
        }
}
