<?php

/**
 * This is the model class for table "tbl_card".
 *
 * The followings are the available columns in table 'tbl_card':
 * @property string $id
 * @property string $serial
 * @property string $pin
 * @property integer $type
 * @property integer $status
 * @property string $member_id
 * @property integer $is_active
 * @property string $expired_on
 * @property string $created_by
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property Member[] $members
 */
class Card extends CActiveRecord
{
        public $value;
        public $val;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case '1':
                    return 'Yes';
                    break;
                case '0':
                    return 'No';
                    break;
            }
        }
       
        public function getIsActive()
        {
            switch($this->is_active)
            {
                case '1':
                    return '<span style="display:inherit;padding: 5px; background-color:green;color:#fff;">Active</span>';
                    break;
                case '0':
                    return '<span style="display:block;padding: 5px; background-color:red;">Activated</span>';
                    break;
            }
        }
        
        public static function statusList()
        {
            return array(
                1=>'Active',
                0=>'Inactive'
            );
        }
        
        public static function typeList()
        {
            return array(
                1=>'Standard',
                2=>'Premium',
                3=>'Deluxe',
                4=>'Executive',
                5=>'Executive Diamond'
            );
        }

        public function getTypename()
        {
            switch($this->type)
            {
                case '1':
                    return 'Standard';
                    break;
                case '2':
                    return 'Deluxe';
                    break;
                case '3':
                    return 'Premium';
                    break;
                case '4':
                    return 'Executive';
                    break;
                case '5':
                    return 'Executive Diamond';
                    break;
            }
        }
        
        public function getOwner()
        {
            $data=Member::model()->findByAttributes(array('card_id'=>$this->id));
            if($data)
            {
                    return CHtml::link($data->mid,array('apps/member/view','id'=>$data->id));
            }
            else
                return '-';
        }
        
        public function getOwnermember()
        {
            $data=Member::model()->findByAttributes(array('card_id'=>$this->id));
            if($data)
            {
                    return array(CHtml::link($data->mid,array('/member/genealogy','id'=>$data->id)),$data->name,$data->country->name,$data->created_on);
            }
            else
                return '-';
        }
        
        public function getMemberPin($id)
        {
            return self::model()->countByAttributes(array('member_id'=>$id));
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_card';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, type, is_active', 'numerical', 'integerOnly'=>true),
			array('serial', 'length', 'max'=>50),
			array('pin', 'length', 'max'=>15),
			array('member_id, created_by', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, serial, pin, status, member_id, is_active, expired_on, type, created_by, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
			'members' => array(self::HAS_MANY, 'Member', 'card_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serial' => 'Serial Number',
			'pin' => 'Pin',
			'status' => 'Status',
			'is_active' => 'Is Active',
			'expired_on' => 'Expired On',
			'created_by' => 'Created By',
			'created_on' => 'Created On',
            'type'=>'Type',
                        'value' => 'Jumlah PIN'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('expired_on',$this->expired_on,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_on',$this->created_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Card the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function  beforeSave() 
        {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                $this->serial = $this->generateRandomString();
                $this->pin = mt_rand(100000,999999);
                $this->expired_on=date('Y-m-d', strtotime("+2 year"));
                $this->created_by=Yii::app()->user->id;
            }
            return TRUE;
        }
        
        protected function generateRandomString($length = 8) {
            $characters = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
        
        public function getTotal()
        {
            $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card` where `member_id`=:id", array(':id'=>Yii::app()->member->id));
            return $var_sum->val;
        }

        public function getTotalType($data)
        {
            $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card` where `member_id`=:id and `type`=:data", array(':id'=>Yii::app()->member->id,':data'=>$data));
            return $var_sum->val;
        }
        
        public function getTotalPin($st=null)
        {
            if($st===null)
                $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card`");
            else
                $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card` where status=:st",array(':st'=>$st));
            return $var_sum->val;
        }
        
        public function getTotalStock($tp=null)
        {
            if($tp===null)
                $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card` WHERE member_id IS NULL AND status=0");
            else
                $var_sum = self::model()->findBySql("select count(`id`) as `val` from `tbl_card` WHERE type=:tp AND member_id IS NULL AND status=0",array(':tp'=>$tp));
            return $var_sum->val;
        }
        
}
