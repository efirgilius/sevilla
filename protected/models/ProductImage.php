<?php

/**
 * This is the model class for table "tbl_product_image".
 *
 * The followings are the available columns in table 'tbl_product_image':
 * @property string $id
 * @property string $product_id
 * @property string $title
 * @property string $image
 * @property integer $is_primary
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class ProductImage extends CActiveRecord
{

	const SMALL_THUMB_WIDTH=68;
	const SMALL_THUMB_HEIGHT=82;

	const MEDIUM_THUMB_WIDTH=370;
	const MEDIUM_THUMB_HEIGHT=270;

	const LARGE_THUMB_WIDTH=570;
	const LARGE_THUMB_HEIGHT=520;

	public $_small_url;
	public $_medium_url;
	public $_large_url;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_product_image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, image', 'required'),
			array('is_primary, order', 'numerical', 'integerOnly'=>true),
			array('product_id', 'length', 'max'=>10),
			array('title', 'length', 'max'=>30),
			array('image', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, title, image, is_primary, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'title' => 'Title',
			'image' => 'Image',
			'is_primary' => 'Is Primary',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('is_primary',$this->is_primary);
		$criteria->compare('order',$this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductImage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/

	public function scopes()
	{
        return [
            'main'=>[
                'condition'=>'is_primary=1',
            ],
        ];
    }

	public function afterSave()
	{
		parent::afterSave();
        
        if(!empty($this->image))
        {
        	Yii::app()->hsnImage->generateThumb(self::SMALL_THUMB_WIDTH,self::SMALL_THUMB_HEIGHT,'products',$this->product->id,$this->image,'small');
        	Yii::app()->hsnImage->generateThumb(self::MEDIUM_THUMB_WIDTH,self::MEDIUM_THUMB_HEIGHT,'products',$this->product->id,$this->image,'medium');
        	Yii::app()->hsnImage->generateThumb(self::LARGE_THUMB_WIDTH,self::LARGE_THUMB_HEIGHT,'products',$this->product->id,$this->image,'large'); 
        }	           
	}

	public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('products',$this->product->id).$this->image;
    }
        
    public function getSmall_url()
	{
		if($this->_small_url===NULL)
		{
			$this->_small_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'products',$this->product->id,'small');
		}
		return Yii::app()->baseUrl.'/'.$this->_small_url;
	}

	public function getMedium_url()
	{
		if($this->_medium_url===NULL)
		{
			$this->_medium_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'products',$this->product->id,'medium');
		}
		return Yii::app()->baseUrl.'/'.$this->_medium_url;
	}

	public function getLarge_url()
	{
		if($this->_large_url===NULL)
		{
			$this->_large_url=Yii::app()->hsnImage->generateThumbUrl($this->image,'products',$this->product->id,'large');
		}
		return Yii::app()->baseUrl.'/'.$this->_large_url;
	}

    public function onBeforeDelete($event)
	{
		$filepath=Yii::app()->hsnImage->getPathUrl('products',$this->product->id,$this->image);
		if(is_file($filepath))
			unlink($filepath);
		parent::onBeforeDelete($event);
	}


}
