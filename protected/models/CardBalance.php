<?php

/**
 * This is the model class for table "tbl_card_balance".
 *
 * The followings are the available columns in table 'tbl_card_balance':
 * @property string $id
 * @property string $member_id
 * @property string $partner_id
 * @property integer $type
 * @property integer $debit
 * @property integer $credit
 * @property integer $total
 * @property integer $is_transaction
 * @property integer $status
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Member $partner
 * @property Member $member
 */
class CardBalance extends CActiveRecord
{
        const PENDING=0;
        const VERIFIED=1;
        const REJECTED=2;
        
        const A=1;
        const B=2;
        
        public $member_seller_id;
        public $member_buyer_id;
        public $seller_id_temp;
        public $buyer_id_temp;
        public $value;
        
        public function getStatusname()
        {
            switch($this->status)
            {
                case 1:
                    return 'Success';
                    break;
                case 0:
                    return 'Pending';
                    break;
                case 2:
                    return 'Rejected';
                    break;
            }
        }
        
        public function getTypename()
        {
            switch($this->type)
            {
                case self::A:
                    return 'E-voucher 200';
                    break;
                case self::B:
                    return 'E-voucher 25';
                    break;
            }
        }
        
        public function getTrans()
        {
            switch($this->is_transaction)
            {
                case 1:
                    return 'Card Transaction';
                    break;
                case 0:
                    return 'Member Activation';
                    break;
                case 3:
                    return 'Upgrade Member Type';
                    break;
                case 4:
                    return 'ID Reward';
                    break;
            }
        }
        
        public static function typeList()
        {
            return array(
                1=>'E-voucher 200',
                2=>'E-voucher 25'
            );
        }
        
        public function getPosted()
        {
            $formatter = Yii::app()->getDateFormatter();
            $format = Yii::app()->getLocale()->getDateFormat('medium',true,true);
                return $formatter->format($format, $this->date);
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_card_balance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('member_id, type, credit', 'required','on'=>'send'),
                        array('type, credit','required','on'=>'buy'),
                        array('member_seller_id, type, credit', 'required','on'=>'request'),
			array('member_buyer_id, type, debit', 'required','on'=>'sendMember'),
                        array('debit','validateCardNumber','on'=>'send, buy'),
                        array('debit','validateSendCard','on'=>'sendMember'),
                        array('member_buyer_id','validateMemberId','on'=>'sendMember'),
                        array('member_seller_id','validateMember','on'=>'request'),
                        array('member_seller_id','validateCard','on'=>'request'),
			array('type, debit, credit, total, is_transaction, status, value', 'numerical', 'integerOnly'=>true),
			array('member_id, partner_id', 'length', 'max'=>10),
                        array('member_id, partner_id, debit, credit', 'default', 'setOnEmpty' => true, 'value' => null),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, partner_id, type, debit, credit, total, is_transaction, status, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partner' => array(self::BELONGS_TO, 'Member', 'partner_id'),
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'partner_id' => 'Partner',
			'type' => 'Type',
			'debit' => 'Debit',
			'credit' => 'Credit',
			'total' => 'Total',
			'status' => 'Status',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('partner_id',$this->partner_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('debit',$this->debit);
		$criteria->compare('credit',$this->credit);
		$criteria->compare('total',$this->total);
		$criteria->compare('status',$this->status);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CardBalance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function beforeSave() {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                if($this->scenario=='sendMember')
                {
                    $this->partner_id = $this->buyer_id_temp;
                }
                
                if($this->seller_id_temp)
                    $this->partner_id = $this->seller_id_temp;
            }
            return TRUE;
        }
        
        public function validateMember($attribute)
        {
            $member = Member::model()->find('mid=:m AND id!=:id',array(':m'=>  $this->member_seller_id,':id'=>Yii::app()->member->id));
            if($member===null)
                {
                    $this->addError($attribute, "Member/Merchant ID can not be found.");
                    return false;
                }
                else
                {
                    $this->seller_id_temp=$member->id;
                    return true;
                }
        }
        
        public function validateMemberId($attribute)
        {
            $member = Member::model()->find('mid=:m',array(':m'=>  $this->member_buyer_id));
            if($member===null)
                {
                    $this->addError($attribute, "Member/Merchant ID can not be found.");
                    return false;
                }
                else
                {
                    $this->buyer_id_temp=$member->id;
                    return true;
                }
        }
        
        public function validateCard($attribute)
        {
            $count = Card::model()->count('member_id=:m AND type=:tp AND status=:st',array(':m'=>  $this->seller_id_temp,':tp'=>  $this->type,':st'=>0));
            if($count<=0)
                {
                    $this->addError($attribute, "Voucher is unavailable in this Member/Merchant.");
                    return false;
                }
                else
                {
                    if($count>=$this->credit)
                    {
                        return true;
                    }
                    else
                    {
                        $this->addError($attribute, "Voucher is less than your request in this Member/Merchant.");
                        return false;
                    }
                }
        }
        
        public function validateCardNumber($attribute)
        {
            $count = Card::model()->count('member_id IS NULL AND type=:tp AND status=:st',array(':tp'=>  $this->type,':st'=>0));
            if($count===null)
                {
                    $this->addError($attribute, "Voucher is unavailable.");
                    return false;
                }
                else
                {
                    if($count>=$this->debit)
                        return true;
                    else
                        $this->addError($attribute, "Voucher stock is less than your request.");
                }
        }
        
        public function validateSendCard($attribute)
        {
            $count = Card::model()->count('member_id=:m AND type=:tp AND status=:st',array(':m'=>Yii::app()->member->id,':tp'=>  $this->type,':st'=>0));
            if($count===null)
                {
                    $this->addError($attribute, "Your voucher is unavailable.");
                    return false;
                }
                else
                {
                    if($count>=$this->debit)
                        return true;
                    else
                        $this->addError($attribute, "Voucher stock is less than your request.");
                }
        }
}
