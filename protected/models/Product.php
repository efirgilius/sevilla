<?php

/**
 * This is the model class for table "tbl_product".
 *
 * The followings are the available columns in table 'tbl_product':
 * @property string $id
 * @property string $category_id
 * @property string $brand_id
 * @property string $name
 * @property string $slug
 * @property string $code
 * @property string $price
 * @property integer $discount
 * @property string $overview
 * @property string $description
 * @property string $specification
 * @property integer $is_recommend
 * @property integer $is_new
 * @property integer $weight
 * @property integer $is_stock
 * @property string $tags
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $status
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Brand $brand
 * @property ProductImage[] $productImages
 * @property ProductSpecification[] $productSpecifications
 */
class Product extends CActiveRecord
{

	private $_oldTags;
	public $images;

	public function behaviors(){
	    return array(
		    'sluggable' => array(
			    'class'=>'ext.slug.SluggableBehavior',
			    'columns' => array('name'),
			    'unique' => true,
			    'update' => true,
		    ),
	  	);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, code, price', 'required'),
			array('discount, is_recommend, is_new, weight, is_stock, status', 'numerical', 'integerOnly'=>true),
			array('category_id, brand_id, code, price', 'length', 'max'=>10),
			array('name', 'length', 'max'=>50),
			array('slug', 'length', 'max'=>100),
			array('overview, tags, meta_keywords', 'length', 'max'=>255),
			array('meta_title', 'length', 'max'=>70),
			array('meta_description', 'length', 'max'=>160),
			array('modified', 'safe'),
			['modified','setModified'],
			array('tags', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Tags can only contain word characters.'),
			array('tags', 'normalizeTags'),
			['discount','default','value'=>0],
			['brand_id','default','value'=>null],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, brand_id, name, slug, code, price, discount, overview, description, specification,is_recommend, is_new, weight, is_stock, tags, meta_title, meta_description, meta_keywords, status, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'productGroups' => array(self::HAS_MANY, 'ProductGroup', 'product_id'),
			'brand' => array(self::BELONGS_TO, 'Brand', 'brand_id'),
			'productImages' => array(self::HAS_MANY, 'ProductImage', 'product_id'),
            'productImagesNotMain'=>array(self::HAS_MANY, 'ProductImage', 'product_id','condition'=>'productImagesnotmain.is_primary=0'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'images'=>'Gambar Lainnya',
			'category_id' => 'Category',
			'brand_id' => 'Brand',
			'name' => 'Name',
			'slug' => 'Slug',
			'code' => 'Code',
			'price' => 'Price',
			'discount' => 'Discount (%)',
			'overview' => 'Overview',
			'description' => 'Description',
			'specification' => 'Specification',
			'is_recommend' => 'Is Recommend',
			'is_new' => 'Is New',
			'weight' => 'Weight (gram)',
			'is_stock' => 'Is Stock',
			'tags' => 'Tags',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'status' => 'Status',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('brand_id',$this->brand_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('overview',$this->overview,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('specification',$this->specification,true);
		$criteria->compare('is_recommend',$this->is_recommend);
		$criteria->compare('is_new',$this->is_new);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('is_stock',$this->is_stock);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**/

	public function setModified()
    {
        if (!$this->isNewRecord) {
	        $this->modified = date('Y-m-d H:m:s');
        }
    }

    public function scopes()
	{
        return [
            'publish'=>[
                'condition'=>'t.status=1',
            ],
            'stock'=>[
            	'condition'=>'t.is_stock=1',
            ],
            'new'=>[
            	'condition'=>'t.is_new=1',
            ],
            'discount'=>[
            	'condition'=>'t.discount>0',
            ],
            'recommend'=>[
            	'condition'=>'t.is_recommend=1',
            ],
        ];
    }

    public function order($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'order'=>$data,
    		]);
    	return $this;
    }

    public function limit($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'limit'=>$data,
    		]);
    	return $this;
    }

    public function brand($data)
    {
    	$this->getDbCriteria()->mergeWith([
    		'condition'=>'brand_id=:bi',
    		'params'=>[':bi'=>$data]
    		]);
    	return $this;
    }

    protected function beforeDelete() {
        parent::beforeDelete();
        
        ProductImage::model()->deleteAllByAttributes(['product_id'=>$this->id]);

        return TRUE;
    }

    public function getTime($date)
    {
        $formatter = Yii::app()->getDateFormatter();
        $format = Yii::app()->getLocale()->getDateFormat('long',true,true);
            return $formatter->format($format, $date);
    }

    public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}

	protected function afterDelete()
	{
		parent::afterDelete();
		
		Tag::model()->updateFrequency($this->tags, '','product');
	}

	public function getTagThis()
	{
		$tags = Tag::string2array($this->tags);
		return $tags;
	}

	protected function afterSave()
	{
		parent::afterSave();
		Tag::model()->updateFrequency($this->_oldTags, $this->tags, 'product');
	}

	public function getSalePrice()
	{	
		if ($this->discount>0) {
			$discount = $this->price * $this->discount / 100;
			$salePrice = $this->price - $discount;
			return $salePrice;
		}else{
			return $this->price;
		}
	}

	public function getOldPrice()
	{
		if ($this->discount>0) {
			return $this->price;
		}else{
			return false;
		}
	}

	public function getId()
	{
		return $this->id;
	}

	public function getStock()
    {
        switch($this->is_stock)
        {
            case '1':
                return 'In Stock';
                break;
            case '0':
                return 'Out Stock';
                break;
        }
    }

    public function getImgurl(){
        return Yii::app()->hsnImage->getUrl('products',$this->id).$this->mainImage->image;
    }
    
    public function getSmall_url()
    {
        $img=ProductImage::model()->main()->findByAttributes(array('product_id'=>$this->id));
        if($img!==NULL)
        {
            return Yii::app()->baseUrl.'/archives/products/'.$this->id.'/small_'.$img->image;               
        }
        else
            return Yii::app()->baseUrl.'/archives/products/noImageAvailable.jpg';
    }

	public function getMedium_url()
	{
		$img=ProductImage::model()->main()->findByAttributes(array('product_id'=>$this->id));
        if($img!==NULL)
        {
            return Yii::app()->baseUrl.'/archives/products/'.$this->id.'/medium_'.$img->image;               
        }
        else
            return Yii::app()->baseUrl.'/archives/products/noImageAvailable.jpg';
	}

	public function getLarge_url()
	{
		$img=ProductImage::model()->main()->findByAttributes(array('product_id'=>$this->id));
        if($img!==NULL)
        {
            return Yii::app()->baseUrl.'/archives/products/'.$this->id.'/large_'.$img->image;               
        }
        else
            return Yii::app()->baseUrl.'/archives/products/noImageAvailable.jpg';
	}

}
