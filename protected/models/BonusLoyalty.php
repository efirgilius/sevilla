<?php

/**
 * This is the model class for table "tbl_bonus_loyalty".
 *
 * The followings are the available columns in table 'tbl_bonus_loyalty':
 * @property string $id
 * @property string $member_id
 * @property string $date
 * @property string $bonus
 *
 * The followings are the available model relations:
 * @property Member $member
 */
class BonusLoyalty extends CActiveRecord
{
        public $val;
        public $valtotal;
        public $totals;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bonus_loyalty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id', 'required'),
			array('member_id', 'length', 'max'=>10),
			array('bonus', 'length', 'max'=>15),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, date, bonus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'date' => 'Date',
			'bonus' => 'Bonus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('bonus',$this->bonus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BonusLoyalty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getTotalbonus($month,$year)
        {
            $var_sum = self::model()->findBySql("select sum(`bonus`) as `val` from `tbl_bonus_loyalty` where `member_id`=:id AND MONTH(`date`)=:mt AND YEAR(`date`)=:yr", array(':id'=>Yii::app()->member->id,':mt'=>$month,':yr'=>$year));
            return $var_sum->val;
        }
        
        public function getTotalPeriod($day,$month,$year)
        {
            if($day!==null)
                $var_sum = self::model()->findBySql("select sum(`bonus`) as `val` from `tbl_bonus_loyalty` where `member_id`=:id AND DAY(`date`)=:day AND MONTH(`date`)=:mt AND YEAR(`date`)=:yr", array(':id'=>Yii::app()->member->id,':day'=>$day,':mt'=>$month,':yr'=>$year));
            else
                $var_sum = self::model()->findBySql("select sum(`bonus`) as `val` from `tbl_bonus_loyalty` where `member_id`=:id AND MONTH(`date`)=:mt AND YEAR(`date`)=:yr", array(':id'=>Yii::app()->member->id,':mt'=>$month,':yr'=>$year));
            return $var_sum->val;
        }
        
        public function getTotal()
        {
            $var_sum = self::model()->findBySql("select sum(`bonus`) as `valtotal` from `tbl_bonus_loyalty` where `member_id`=:id", array(':id'=>Yii::app()->member->id));
            return $var_sum->valtotal;
        }
        
        public function getPerdateId($id,$month,$year)
        {
            $var_sum = self::model()->findBySql("select sum(`bonus`) as `val` from `tbl_bonus_loyalty` where `member_id`=:id AND MONTH(`date`)=:mt AND YEAR(`date`)=:yr", array(':id'=>$id,':mt'=>$month,':yr'=>$year));
            return $var_sum->val;
        }
        
        public function getTotalperiodall($day,$month,$year)
        {
            $var_sum = self::model()->findBySql("select sum(`bonus`) as `val` from `tbl_bonus_loyalty` where DAY(`date`)=:day AND MONTH(`date`)=:mt AND YEAR(`date`)=:yr", array(':day'=>$day,':mt'=>$month,':yr'=>$year));
            return $var_sum->val;
        }
}
