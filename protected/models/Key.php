<?php

/**
 * This is the model class for table "tbl_key".
 *
 * The followings are the available columns in table 'tbl_key':
 * @property string $id
 * @property string $token
 * @property string $expired_on
 * @property string $member_id
 * @property integer $is_used
 *
 * The followings are the available model relations:
 * @property Member $member
 */
class Key extends CActiveRecord
{
        const VERIFY=1;
        const FORGOT=2;
        const UPGRADE=3;
        
        const PENDING=0;
        const VERIFIED=1;
        const CONFIRMED=2;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_key';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('token, expired_on, member_id', 'required'),
			array('is_used, type', 'numerical', 'integerOnly'=>true),
			array('token', 'length', 'max'=>100),
			array('member_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, token, expired_on, member_id, is_used, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'token' => 'Token',
			'expired_on' => 'Expired On',
			'member_id' => 'Member',
			'is_used' => 'Is Used',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('expired_on',$this->expired_on,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('is_used',$this->is_used);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Key the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function  beforeSave() 
        {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                $this->expired_on=date("Y-m-d H:i:s", strtotime("+2 day"));
            }
            return TRUE;
        }
}
