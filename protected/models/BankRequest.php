<?php

/**
 * This is the model class for table "tbl_bank_request".
 *
 * The followings are the available columns in table 'tbl_bank_request':
 * @property string $id
 * @property string $member_id
 * @property string $bank
 * @property string $account_number
 * @property string $account_name
 * @property integer $status
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Member $member
 */
class BankRequest extends CActiveRecord
{
        public function getPosted()
        {
            $formatter = Yii::app()->getDateFormatter();
            $format = Yii::app()->getLocale()->getDateFormat('medium',true,true);
                return $formatter->format($format, $this->date);
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bank_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bank, account_number, account_name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('member_id', 'length', 'max'=>10),
			array('bank, account_number, account_name', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, bank, account_number, account_name, status, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'bank' => 'Bank',
			'account_number' => 'Account Number',
			'account_name' => 'Account Name',
			'status' => 'Status',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('account_number',$this->account_number,true);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BankRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function  beforeSave() 
        {
            parent::beforeSave();
            if($this->isNewRecord)
            {
                $this->member_id= Yii::app()->member->id;
            }
            return TRUE;
        }
}
