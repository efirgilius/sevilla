<?php

/**
 * This is the model class for table "tbl_ewallet_confirmation".
 *
 * The followings are the available columns in table 'tbl_ewallet_confirmation':
 * @property string $id
 * @property string $ewallet_id
 * @property string $date
 * @property string $bank_id
 * @property string $value
 * @property string $note
 * @property string $image
 * @property integer $status
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Bank $bank
 * @property Ewallet $ewallet
 */
class EwalletConfirmation extends CActiveRecord
{

	const PENDING=0; //MASUK
    const APPROVED=1; //KELUAR
     
    public function getStatusname()
    {
        switch($this->status)
        {
            case 1:
                return 'Approved';
                break;
            case 0:
                return 'Pending';
                break;
        }
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_ewallet_confirmation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ewallet_id, date, bank_id, name, value', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('ewallet_id, bank_id, value', 'length', 'max'=>10),
			array('note, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ewallet_id, date, bank_id, value, note, image, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
			'ewallet' => array(self::BELONGS_TO, 'Ewallet', 'ewallet_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ewallet_id' => 'Ewallet',
			'date' => 'Tanggal',
			'bank_id' => 'Ke Bank',
			'value' => 'Jumlah',
			'note' => 'Note',
			'image' => 'Image',
			'status' => 'Status',
			'name'=>'Nama Pengirim',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ewallet_id',$this->ewallet_id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EwalletConfirmation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
